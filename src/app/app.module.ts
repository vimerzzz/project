import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './material.module';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { HeaderComponent } from './pages/shared/header/header.component';
import { FooterComponent } from './pages/shared/footer/footer.component';
import { ProgressBarComponent } from './shared/components/progress-bar/progress-bar.component';
import { SharedPagesModule } from './pages/shared/shared-pages.module';
import { SharedComponentsModule } from './shared/components/shared-components.module';
import { MAT_DATE_LOCALE } from '@angular/material/core';
import { AuthInterceptor } from './shared/interceptors/auth.interceptor';
import { ChatComponent } from './pages/shared/chat/chat.component';
import { DirectivesModule } from './shared/directives/directives.module';

@NgModule({
    declarations: [
        AppComponent,
        HeaderComponent,
        FooterComponent,
        ProgressBarComponent,
        ChatComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        MaterialModule,
        HttpClientModule,

        SharedPagesModule,
        SharedComponentsModule,
        DirectivesModule
    ],
    providers: [
        {
            provide: MAT_DATE_LOCALE,
            useValue: "en-GB"
        },
        {
            provide: HTTP_INTERCEPTORS,
            useClass: AuthInterceptor,
            multi: true
        }
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
