import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { MaterialModule } from 'src/app/material.module';
import { LoginComponent } from './login/login.component';
import { RecoverPasswordComponent } from './recover-password/recover-password.component';
import { SignupComponent } from './signup/signup.component';

@NgModule({
    declarations: [
        LoginComponent,
        SignupComponent,
        RecoverPasswordComponent
    ],
    imports: [
        MaterialModule
    ],
    schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class SharedPagesModule { }
