import { Component, Inject, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MessageConstants } from 'src/app/shared/constants/message.constants';
import { mType, PopUpService } from 'src/app/shared/services/base/pop-up.service';
import { LogService } from 'src/app/shared/services/main/log.service';

@Component({
    selector: 'app-signup',
    templateUrl: './signup.component.html',
    styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {
    formControl: FormGroup;
    error: any;
    spinner: any = false;
    passwordType: "password" | "text";
    rePasswordType: "password" | "text";

    constructor(private formBuilder: FormBuilder, private dialogRef: MatDialogRef<SignupComponent>, private logService: LogService, private popUpService: PopUpService,
        @Inject(MAT_DIALOG_DATA) public data: any) {}

    ngOnInit(): void {
        this.formControl = this.formBuilder.group({
            userName: ["", [Validators.required, Validators.pattern("^[a-zA-Z0-9-_]+$"), Validators.minLength(5)]],
            email: ["", [Validators.required, Validators.pattern("^[a-zA-Z]([.]?[a-zA-Z0-9])*@[a-zA-Z0-9]{2,}(.[a-zA-Z0-9]{2,})+$")]],
            password: ["", [Validators.required, Validators.pattern("^[a-zA-Z0-9-_]+$"), Validators.minLength(5)]],
            rePassword: ["", [Validators.required, Validators.pattern("^[a-zA-Z0-9-_]+$"), Validators.minLength(5)]]
        });
        this.passwordType = "password";
        this.rePasswordType = "password";
    }

    openDialogLogin() {
        this.dialogRef.close("login");
    }

    unMatchedPassword(control: AbstractControl): {[key: string]: any} {
        return {"notMatched": true};
    }

    checkPassword() {
        if(this.data.item.rePassword == "") {
            this.f.rePassword.setValidators([Validators.required, Validators.pattern("^[a-zA-Z0-9-_]+$"), Validators.minLength(5)]);
            if(this.f.rePassword.dirty) {
                this.f.rePassword.reset();
            }
        }
        else {
            if(this.data.item.password == this.data.item.rePassword) {
                this.f.rePassword.clearValidators();
                let tmp = this.data.item.rePassword;
                this.f.rePassword.reset();
                this.f.rePassword.setValidators([Validators.pattern("^[a-zA-Z0-9-_]+$"), Validators.minLength(5)]);
                this.f.rePassword.setValue(tmp);
            }
            else {
                this.f.rePassword.setValidators([this.unMatchedPassword, Validators.pattern("^[a-zA-Z0-9-_]+$"), Validators.minLength(5)]);
                let tmp = this.data.item.rePassword;
                this.f.rePassword.reset();
                this.f.rePassword.setValue(tmp);
            }
        }
    }

    checkUserName() {
        this.formControl.markAllAsTouched();
        if(this.formControl.invalid) {
            return;
        }
        else {
            this.spinner = true;
            this.logService.checkExistedUserName(this.data.item).subscribe({
                next: (res: any) => {
                    if(res.statusCode == 404) {
                        this.checkEmail();
                    }
                    else if(res.statusCode == 200 || res.statusCode == 406) {
                        this.error = MessageConstants.EXISTED_USERNAME;
                        this.spinner = false;
                        this.popUpService.open(MessageConstants.SIGNUP_FAILED, mType.error);
                    }
                    else {
                        this.error = MessageConstants.SYSTEM_ERROR;
                        this.spinner = false;
                        this.popUpService.open(MessageConstants.SIGNUP_FAILED, mType.error);
                    }
                },
                error: (res: any) => {
                    this.error = MessageConstants.SYSTEM_ERROR;
                    this.spinner = false;
                    this.popUpService.open(MessageConstants.SIGNUP_FAILED, mType.error);
                }
            });
        }
    }

    checkEmail() {
        this.logService.checkExistedEmail(this.data.item).subscribe({
            next: (res: any) => {
                if(res.statusCode == 404) {
                    this.close();
                }
                else if(res.statusCode == 200) {
                    this.error = MessageConstants.EXISTED_EMAIL;
                    this.spinner = false;
                    this.popUpService.open(MessageConstants.SIGNUP_FAILED, mType.error);
                }
                else {
                    this.error = MessageConstants.SYSTEM_ERROR;
                    this.spinner = false;
                    this.popUpService.open(MessageConstants.SIGNUP_FAILED, mType.error);
                }
            },
            error: (res: any) => {
                this.error = MessageConstants.SYSTEM_ERROR;
                this.spinner = false;
                this.popUpService.open(MessageConstants.SIGNUP_FAILED, mType.error);
            }
        });
    }

    close() {
        this.error = null;
        this.spinner = false;
        this.dialogRef.close(true);
    }

    passwordToggle() {
        if(this.passwordType == "password") {
            this.passwordType = "text";
        }
        else if(this.passwordType == "text") {
            this.passwordType = "password";
        }
    }

    rePasswordToggle() {
        if(this.rePasswordType == "password") {
            this.rePasswordType = "text";
        }
        else if(this.rePasswordType == "text") {
            this.rePasswordType = "password";
        }
    }

    get f() {
        return this.formControl.controls;
    }

    keyCapture(event: any) {
        let e = <KeyboardEvent>event;
        if(e.keyCode === 13) {
            e.preventDefault();
            this.checkUserName();
        }
    }
}
