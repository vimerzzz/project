import { AfterViewInit, Component, OnDestroy, OnInit } from '@angular/core';
import { MatButton } from '@angular/material/button';
import { MatDialog } from '@angular/material/dialog';
import { MatMenuTrigger } from '@angular/material/menu';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { HubConnection, HubConnectionBuilder } from '@microsoft/signalr';
import { fromEvent, Observable, Subscription } from 'rxjs';
import { filter } from 'rxjs/operators';
import { BaseAuth } from 'src/app/shared/auth/base.auth';
import { GuidConstants } from 'src/app/shared/constants/guid.constants';
import { MessageConstants } from 'src/app/shared/constants/message.constants';
import { SocketConstants } from 'src/app/shared/constants/socket.constants';
import { NavigationService } from 'src/app/shared/services/base/navigation.service';
import { mType, PopUpService } from 'src/app/shared/services/base/pop-up.service';
import { HeaderService } from 'src/app/shared/services/main/header.service';
import { ResetAvatar, ResetGioHang, ResetThongBao } from 'src/app/shared/utilities/reset-status.utility';
import { environment } from 'src/environments/environment';
import { LoginComponent } from '../login/login.component';
import { RecoverPasswordComponent } from '../recover-password/recover-password.component';
import { SignupComponent } from '../signup/signup.component';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit, AfterViewInit, OnDestroy {
    loggedUser: any;
    userName: any;
    smallWidth: any = false;
    widthBreakpoint: any = 500;
    resizeSub: Subscription;
    avatarImage: any;
    hasAvatar: any;
    isMenuOpen: any = false;
    enteredButton: any = false;
    prevButtonTrigger: any;
    soThongBao: any = 0;
    soBaoCao: any = 0;
    soGioHang: any = 0;
    hub: HubConnection;
    rHub: HubConnection;
    roleId: any;
    ADMIN_ROLE_ID: any = GuidConstants.ONE;
    DELIVER_ROLE_ID: any = GuidConstants.THREE;
    EXPERT_ROLE_ID: any = GuidConstants.FOUR;
    constructor(private base: BaseAuth, private dialog: MatDialog, private router: Router, private navigationService: NavigationService, private headerService: HeaderService, private popUpService: PopUpService, private resetAvatar: ResetAvatar, private resetThongBao: ResetThongBao, private resetGioHang: ResetGioHang) { }

    ngOnInit(): void {
        this.checkLoggedUser();
        this.checkAvatar();
        this.checkThongBao();
        this.checkGioHang();
    }

    ngAfterViewInit() {
        this.resizeSub = fromEvent(window, "resize").subscribe(() => {
            this.buttonCollapse(window.innerWidth);
        });
    }

    checkLoggedUser() {
        this.base.loggedUser.subscribe((res: any) => {
            if (res) {
                let object = localStorage.getItem("loggedUser");
                if (object) {
                    this.loggedUser = JSON.parse(object);
                    this.roleId = this.loggedUser.roleId;
                    this.getUserById(this.loggedUser.userId);
                    this.getUserInfoByUserId(this.loggedUser.userId);
                    this.getAllThongBao();
                    this.getAllGioHang();
                    if(this.roleId == this.ADMIN_ROLE_ID) {
                        this.getAllBaoCao();
                    }
                    this.startSocket();
                    this.addThongBao();
                    this.addBaoCaoListener();
                }
                else {
                    this.loggedUser = null;
                }
            }
            else {
                this.loggedUser = null;
            }
        });
    }

    getUserById(id: any) {
        this.headerService.getUserById(id).subscribe({
            next: (res: any) => {
                this.userName = res.userName;
            }
        });
    }

    getUserInfoByUserId(id: any) {
        this.headerService.getUserInfoByUserId(id).subscribe({
            next: (res: any) => {
                if (res) {
                    this.avatarImage = res.anhDaiDien;
                    this.hasAvatar = res.coAnhDaiDien;
                    if (this.avatarImage != null) {
                        this.avatarImage = environment.apiUrl + "/api/" + this.avatarImage.replaceAll("\\", "/");
                    }
                }
            }
        });
    }

    openDialogLogin() {
        let item = {
            userName: null,
            password: null
        }
        let dialogRef = this.dialog.open(LoginComponent, {
            width: "32rem",
            data: {
                item
            }
        });
        dialogRef.afterClosed().subscribe((resp: any) => {
            if (resp == "signup") {
                this.openDialogSignup();
            }
            else if (resp == "recoverPassword") {
                this.openDialogRecoverPassword();
            }
            else if (resp == true) {
                this.headerService.login(item).subscribe({
                    next: (res: any) => {
                        if (res.statusCode == 200) {
                            this.popUpService.open(MessageConstants.LOGIN_SUCCESS, mType.success);
                            this.saveUser(res);
                        }
                        else {
                            this.popUpService.open(MessageConstants.LOGIN_FAILED, mType.error);
                        }
                    },
                    error: (res: any) => {
                        this.popUpService.open(MessageConstants.LOGIN_FAILED, mType.error);
                    }
                });
            }
        });
    }

    openDialogSignup() {
        let item = {
            userName: null,
            email: null,
            password: null,
            rePassword: null
        }
        let dialogRef = this.dialog.open(SignupComponent, {
            width: "32rem",
            data: {
                item
            }
        });
        dialogRef.afterClosed().subscribe((resp: any) => {
            if (resp == "login") {
                this.openDialogLogin();
            }
            else if (resp == true) {
                this.headerService.createUser(item).subscribe({
                    next: (res: any) => {
                        if (res.statusCode == 200) {
                            this.headerService.login(item).subscribe({
                                next: (ress: any) => {
                                    if (ress.statusCode == 200) {
                                        this.popUpService.open(MessageConstants.SIGNUP_SUCCESS, mType.success);
                                        this.saveUser(ress);
                                    }
                                    else {
                                        this.popUpService.open(MessageConstants.SIGNUP_FAILED, mType.error);
                                    }
                                },
                                error: (ress: any) => {
                                    this.popUpService.open(MessageConstants.SIGNUP_FAILED, mType.error);
                                }
                            });
                        }
                        else {
                            this.popUpService.open(MessageConstants.SIGNUP_FAILED, mType.error);
                        }
                    },
                    error: (res: any) => {
                        this.popUpService.open(MessageConstants.SIGNUP_FAILED, mType.error);
                    }
                });
            }
        });
    }

    openDialogRecoverPassword() {
        let item = {
            email: null
        }
        let dialogRef = this.dialog.open(RecoverPasswordComponent, {
            width: "32rem",
            data: {
                item
            }
        });
        dialogRef.afterClosed().subscribe((resp: any) => {
            if (resp == "login") {
                this.openDialogLogin();
            }
        });
    }

    logout() {
        let item: any = {
            id: this.loggedUser.userId
        }
        localStorage.removeItem("loggedUser");
        this.base.checkExpire();
        this.popUpService.open(MessageConstants.LOGOUT_SUCCESS, mType.success);
    }

    saveUser(res: any) {
        let user: any = {
            userId: res.userId,
            roleId: res.roleId,
            token: res.token,
            timeExpire: new Date().getTime() + 15 * 24 * 60 * 60 * 1000
        };
        localStorage.setItem("loggedUser", JSON.stringify(user));
        this.base.setLoggedUser(true);
        if(window.location.pathname.startsWith("/error")) {
            this.navigationService.back();
        }
    }

    buttonCollapse(width: number) {
        if (width < this.widthBreakpoint) {
            this.smallWidth = true;
        }
        else {
            this.smallWidth = false;
        }
    }

    checkAvatar() {
        this.resetAvatar.avatarUrl.subscribe((res: any) => {
            if (res != "") {
                if(res != "Statics\\Images\\Users\\blank_avatar.png"){
                    this.hasAvatar = true;
                }
                this.avatarImage = res;
                this.avatarImage = environment.apiUrl + "/api/" + this.avatarImage.replaceAll("\\", "/");
            }
        });
    }

    getAllThongBao() {
        let params: any = {
            pageSize: 0,
            pageIndex: 0,
            daXem: false,
            userId: this.loggedUser.userId
        }
        this.headerService.getAllThongBao(params).subscribe({
            next: (res: any) => {
                this.soThongBao = res.totalRecord;
                this.resetThongBao.setThongBaoCount(this.soThongBao);
            }
        });
    }

    getAllBaoCao() {
        let params: any = {
            pageSize: 0,
            pageIndex: 0,
            daXem: false,
        }
        this.headerService.getAllBaoCao(params).subscribe({
            next: (res: any) => {
                this.soBaoCao = res.totalRecord;
            }
        });
    }

    checkThongBao() {
        this.resetThongBao.thongBaoCount.subscribe((res: any) => {
            this.soThongBao = res;
        });
    }

    getAllGioHang() {
        this.headerService.getAllGioHang(this.loggedUser.userId).subscribe({
            next: (res: any) => {
                if(res) {
                    for(let item of res) {
                        this.soGioHang += item.listSanPhamInGioHang && item.listSanPhamInGioHang.length ? item.listSanPhamInGioHang.length : 0;
                    }
                }
                this.headerService.getAllGioHangDauGia(this.loggedUser.userId).subscribe({
                    next: (res: any) => {
                        if(res) {
                            for(let item of res) {
                                this.soGioHang += item.listSanPhamInGioHang && item.listSanPhamInGioHang.length ? item.listSanPhamInGioHang.length : 0;
                            }
                        }
                        this.resetGioHang.setGioHangCount(this.soGioHang);
                    }
                });
            }
        });
    }

    checkGioHang() {
        this.resetGioHang.gioHangCount.subscribe((res: any) => {
            this.soGioHang = res;
        });
    }

    openMenu(trigger: MatMenuTrigger, button: MatButton) {
        button._elementRef.nativeElement.style.zIndex = "1001";
        setTimeout(() => {
            if (this.prevButtonTrigger && this.prevButtonTrigger != trigger) {
                this.prevButtonTrigger.closeMenu();
                this.prevButtonTrigger = trigger;
                this.isMenuOpen = false;
                trigger.openMenu();
            }
            else if (!this.isMenuOpen) {
                this.enteredButton = true;
                this.prevButtonTrigger = trigger;
                trigger.openMenu();
            }
            else {
                this.enteredButton = true;
                this.prevButtonTrigger = trigger;
            }
        }, 100);
    }

    closeMenu(trigger: MatMenuTrigger, button: MatButton) {
        button._elementRef.nativeElement.style.zIndex = "";
        setTimeout(() => {
            if (this.enteredButton && !this.isMenuOpen) {
                trigger.closeMenu();
                button._elementRef.nativeElement.classList.remove("cdk-focused");
                button._elementRef.nativeElement.classList.remove("cdk-program-focused");
            } if (!this.isMenuOpen) {
                trigger.closeMenu();
                button._elementRef.nativeElement.classList.remove("cdk-focused");
                button._elementRef.nativeElement.classList.remove("cdk-program-focused");
            } else {
                this.enteredButton = false;
            }
        }, 100);
    }

    holdMenu() {
        this.isMenuOpen = true;
    }

    throwMenu(trigger: MatMenuTrigger, button: MatButton) {
        setTimeout(() => {
            if (!this.enteredButton) {
                this.isMenuOpen = false;
                trigger.closeMenu();
                button._elementRef.nativeElement.classList.remove("cdk-focused");
                button._elementRef.nativeElement.classList.remove("cdk-program-focused");
            } else {
                this.isMenuOpen = false;
            }
        }, 80)
    }

    ngOnDestroy() {
        this.resizeSub.unsubscribe();
    }

    startSocket() {
        this.hub = new HubConnectionBuilder().withUrl(SocketConstants.thongBaoHub).build();
        this.hub.start();
        if(this.roleId == this.ADMIN_ROLE_ID) {
            this.rHub = new HubConnectionBuilder().withUrl(SocketConstants.baoCaoHub).build();
            this.rHub.start();
        }
    }

    addThongBao() {
        this.hub.on("thongBao", (res: any) => {
            if(res && res.length) {
                for(let item of res) {
                    if(item.userId == this.loggedUser.userId) {
                        let count = this.resetThongBao.getThongBaoCount();
                        this.resetThongBao.setThongBaoCount(count + 1);
                    }
                }
            }
        });
    }

    addBaoCaoListener() {
        if(this.roleId == this.ADMIN_ROLE_ID) {
            this.rHub.on("baoCaoChuaXem", (res: any) => {
                if(res) {
                    this.soBaoCao = res.totalRecord;
                }
            });
        }
    }
}
