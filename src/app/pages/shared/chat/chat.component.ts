import { HttpEventType } from '@angular/common/http';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { DomSanitizer } from '@angular/platform-browser';
import { HubConnection, HubConnectionBuilder } from '@microsoft/signalr';
import { Rotate180Degree } from 'src/app/shared/animations/rotate.animations';
import { HeightSlide, WidthSlide } from 'src/app/shared/animations/size.animations';
import { NewMessageButton } from 'src/app/shared/animations/specific.animations';
import { SuccessColor } from 'src/app/shared/animations/text-color.animations';
import { BaseAuth } from 'src/app/shared/auth/base.auth';
import { ReportComponent } from 'src/app/shared/components/report/report.component';
import { MessageConstants } from 'src/app/shared/constants/message.constants';
import { SocketConstants } from 'src/app/shared/constants/socket.constants';
import { mType, PopUpService } from 'src/app/shared/services/base/pop-up.service';
import { ViewImageService } from 'src/app/shared/services/base/view-image.service';
import { ChatShopService } from 'src/app/shared/services/main/chat/chat-shop.service';
import { ChatUserService } from 'src/app/shared/services/main/chat/chat-user.service';
import { ResetChat } from 'src/app/shared/utilities/reset-status.utility';
import { environment } from 'src/environments/environment';

@Component({
    selector: 'app-chat',
    templateUrl: './chat.component.html',
    styleUrls: ['./chat.component.scss'],
    animations: [
        Rotate180Degree,
        SuccessColor,
        NewMessageButton,
        HeightSlide,
        WidthSlide
    ]
})
export class ChatComponent implements OnInit {
    animation: any = false;
    logged: any = false;
    loggedUser: any;
    listShopUser: any;
    listUserUser: any;
    listUser: any;
    activeShopIdUser: any;
    activeUserIdUser: any;
    activeUserId: any;
    userShopLoaded: Promise<boolean>;
    userUserLoaded: Promise<boolean>;
    shopLoaded: Promise<boolean>;
    userShopChatField: any;
    userUserChatField: any;
    shopChatField: any;
    userShopListNhanTin: any;
    userUserListNhanTin: any;
    shopListNhanTin: any;
    linkAnimation: any = false;
    hub: HubConnection;
    userShopSending: any = false;
    userUserSending: any = false;
    userShopSendError: any = false;
    userUserSendError: any = false;
    moreUserShopPartner: any = true;
    moreUserUserPartner: any = true;
    shopSending: any = false;
    shopSendError: any = false;
    moreShopPartner: any = true;
    blockGetMore: any = false;
    userInfo: any;
    tabIndex: any = 0;
    collapseTabIndex: any = 0;
    shop: any;
    userShopMessageCount: any = 0;
    userUserMessageCount: any = 0;
    shopMessageCount: any = 0;
    userShopNewMessageAnimation: any = false;
    userUserNewMessageAnimation: any = false;
    shopNewMessageAnimation: any = false;
    heightPopup: any = "320px";
    widthPopup: any = "480px";
    heightDownPopup: any = "0px";
    widthDownPopup: any = "0px";
    userChuaXemCount: any = 0;
    userShopChuaXemCount: any = 0;
    userUserChuaXemCount: any = 0;
    shopChuaXemCount: any = 0;
    totalChuaXemCount: any = 0;
    userShopViewed: any = false;
    userUserViewed: any = false;
    shopViewed: any = false;
    contentHeight: any;
    mouse: any = false;
    paramsShopId: any = {
        userId: null,
        pageIndex: 1,
        pageSize: 10
    }
    paramsUserId: any = {
        shopId: null,
        pageIndex: 1,
        pageSize: 10
    }
    paramsUserNhanId: any = {
        userGuiId: null,
        pageIndex: 1,
        pageSize: 10
    }
    paramsNhanTinShop: any = {
        userId: null,
        shopId: null,
        pageIndex: 1,
        pageSize: 20
    }
    paramsNhanTinUser: any = {
        userGuiId: null,
        userNhanId: null,
        pageIndex: 1,
        pageSize: 20
    }
    @ViewChild("userShopPartnerHeader") userShopPartnerHeader: ElementRef;
    @ViewChild("userShopMessage") userShopMessage: ElementRef;
    @ViewChild("userShopMessageField") userShopMessageField: ElementRef;
    @ViewChild("userShopPartnerField") userShopPartnerField: ElementRef;
    @ViewChild("userUserPartnerHeader") userUserPartnerHeader: ElementRef;
    @ViewChild("userUserMessage") userUserMessage: ElementRef;
    @ViewChild("userUserMessageField") userUserMessageField: ElementRef;
    @ViewChild("userUserPartnerField") userUserPartnerField: ElementRef;
    @ViewChild("shopMessage") shopMessage: ElementRef;
    @ViewChild("shopMessageField") shopMessageField: ElementRef;
    @ViewChild("shopPartnerField") shopPartnerField: ElementRef;
    @ViewChild("userShopImageField") userShopImageField: ElementRef;
    @ViewChild("userUserImageField") userUserImageField: ElementRef;
    @ViewChild("shopImageField") shopImageField: ElementRef;

    constructor(private dialog: MatDialog, private resetChat: ResetChat, private base: BaseAuth, private sanitizer: DomSanitizer, private chatShopService: ChatShopService, private chatUserService: ChatUserService, private popUpService: PopUpService, private viewImageService: ViewImageService) { }

    ngOnInit(): void {
        this.checkLoggedUser();
        this.checkChatStatus();
        this.checkChatShopId();
        this.checkChatUserId();
        this.checkChatUserIdForShop();
        this.startSocket();
        this.addChatShopListener();
        this.addChatUserListener();
    }

    checkLoggedUser() {
        this.base.loggedUser.subscribe((res: any) => {
            if(res) {
                let object = localStorage.getItem("loggedUser");
                if(object) {
                    this.loggedUser = JSON.parse(object);
                    this.logged = true;
                    this.paramsShopId.userId = this.loggedUser.userId;
                    this.paramsUserNhanId.userGuiId = this.loggedUser.userId;
                    this.getUserInfo();
                    this.getAllShopId();
                    this.getAllUserNhanId();
                    this.initialSetting();
                }
            }
        });
    }

    initialSetting() {
        setTimeout(() => {
            let chatPopup = <HTMLElement>document.getElementById("chatPopup");
            let matTabHeader = <HTMLElement>chatPopup.getElementsByClassName("mat-tab-header")[0];
            this.contentHeight = parseInt(this.heightPopup) - chatPopup.offsetHeight - matTabHeader.offsetHeight -
                                this.userShopPartnerHeader.nativeElement.offsetHeight - this.userUserPartnerHeader.nativeElement.offsetHeight;
        }, 10);
    }

    checkChatStatus() {
        this.resetChat.chatStatus.subscribe((res: any) => {
            this.animation = res;
            if(this.animation) {
                this.userShopPartnerField.nativeElement.style.transition = null;
                this.userUserPartnerField.nativeElement.style.transition = null;
                if(this.collapseTabIndex == 0) {
                    this.userShopPartnerField.nativeElement.style.height = this.contentHeight + "px";
                }
                else {
                    this.userUserPartnerField.nativeElement.style.height = this.contentHeight + "px";
                }
            }
        });
        this.resetChat.userShopChatStatus.subscribe((res: any) => {
            if(res) {
                this.tabIndex = 0;
                if(!this.animation) {
                    this.collapseTabIndex = 0;
                }
            }
        });
        this.resetChat.userUserChatStatus.subscribe((res: any) => {
            if(res) {
                this.tabIndex = 0;
                if(!this.animation) {
                    this.collapseTabIndex = 1;
                }
            }
        });
        this.resetChat.shopChatStatus.subscribe((res: any) => {
            if(res) {
                this.tabIndex = 1;
            }
        });
    }

    checkChatShopId() {
        this.resetChat.chatShopId.subscribe((res: any) => {
            if(res) {
                this.userShopLoaded = Promise.resolve(false);
                if(this.listShopUser && this.listShopUser.length) {
                    for(let index in this.listShopUser) {
                        if(this.listShopUser[index].shopId == res) {
                            this.userShopLoaded = Promise.resolve(true);
                            if(this.animation) {
                                setTimeout(() => {
                                    if(this.collapseTabIndex != 0) {
                                        this.userUserPartnerField.nativeElement.style.transition = "height 0.5s ease-in-out";
                                        this.userShopPartnerField.nativeElement.style.transition = "height 0.5s ease-in-out";
                                    }
                                    this.collapseTabIndex = 0;
                                    this.userShopPartnerField.nativeElement.style.height = this.contentHeight + "px";
                                    this.userUserPartnerField.nativeElement.style.height = "0px";
                                }, 1);
                            }
                            this.userShopOpenChatField(this.listShopUser[index]);
                            this.activeShopIdUser = res;
                            break;
                        }
                        if(parseInt(index) == this.listShopUser.length - 1) {
                            let item = {
                                userId: this.loggedUser.userId,
                                shopId: res
                            }
                            this.getShopByShopId(item, 1, 1);
                            this.listShopUser.unshift(item);
                            this.userShopOpenChatField(item);
                            this.activeShopIdUser = res;
                        }
                    }
                }
                else {
                    let item = {
                        userId: this.loggedUser.userId,
                        shopId: res
                    }
                    this.getShopByShopId(item, 1, 1);
                    this.listShopUser.unshift(item);
                    this.userShopOpenChatField(item);
                    this.activeShopIdUser = res;
                }
            }
        });
    }

    checkChatUserId() {
        this.resetChat.chatUserId.subscribe((res: any) => {
            if(res) {
                this.userUserLoaded = Promise.resolve(false);
                if(this.listUserUser && this.listUserUser.length) {
                    for(let index in this.listUserUser) {
                        if(this.listUserUser[index].userNhanId == res) {
                            this.userUserLoaded = Promise.resolve(true);
                            if(this.animation) {
                                setTimeout(() => {
                                    if(this.collapseTabIndex != 1) {
                                        this.userUserPartnerField.nativeElement.style.transition = "height 0.5s ease-in-out";
                                        this.userShopPartnerField.nativeElement.style.transition = "height 0.5s ease-in-out";
                                    }
                                    this.collapseTabIndex = 1;
                                    this.userUserPartnerField.nativeElement.style.height = this.contentHeight + "px";
                                    this.userShopPartnerField.nativeElement.style.height = "0px";
                                }, 1);
                            }
                            this.userUserOpenChatField(this.listUserUser[index]);
                            this.activeUserIdUser = res;
                            break;
                        }
                        if(parseInt(index) == this.listUserUser.length - 1) {
                            let item = {
                                userGuiId: this.loggedUser.userId,
                                userNhanId: res
                            }
                            this.getUserNhanByUserId(item, 1, 1);
                            this.listUserUser.unshift(item);
                            this.userUserOpenChatField(item);
                            this.activeUserIdUser = res;
                        }
                    }
                }
                else {
                    let item = {
                        userGuiId: this.loggedUser.userId,
                        userNhanId: res
                    }
                    this.getUserNhanByUserId(item, 1, 1);
                    this.listUserUser.unshift(item);
                    this.userUserOpenChatField(item);
                    this.activeUserIdUser = res;
                }
            }
        });
    }

    checkChatUserIdForShop() {
        this.resetChat.chatUserIdForShop.subscribe((res: any) => {
            if(res) {
                this.shopLoaded = Promise.resolve(false);
                if(this.listUser && this.listUser.length) {
                    for(let index in this.listUser) {
                        if(this.listUser[index].userId == res) {
                            this.shopLoaded = Promise.resolve(true);
                            this.shopOpenChatField(this.listUser[index]);
                            this.activeUserId = res;
                            break;
                        }
                        if(parseInt(index) == this.listUser.length - 1) {
                            let item = {
                                shopId: this.shop.id,
                                userId: res
                            }
                            this.getUserInfoByUserId(item, 1, 1);
                            this.listUser.unshift(item);
                            this.shopOpenChatField(item);
                            this.activeUserId = res;
                        }
                    }
                }
                else {
                    let item = {
                        shopId: this.shop.id,
                        userId: res
                    }
                    this.getUserInfoByUserId(item, 1, 1);
                    this.listUser.unshift(item);
                    this.shopOpenChatField(item);
                    this.activeUserId = res;
                }
            }
        });
    }

    openChat() {
        this.resetChat.setChatStatus(!this.animation);
    }

    getUserInfo() {
        this.chatShopService.getUserInfoByUserId(this.loggedUser.userId).subscribe({
            next: (res: any) => {
                this.userInfo = res ? res : null;
                if(this.userInfo.coShop) {
                    this.getShopByUserId();
                }
            }
        });
    }

    getShopByUserId() {
        this.chatShopService.getShopByUserId(this.loggedUser.userId).subscribe({
            next: (res: any) => {
                this.shop = res;
                this.paramsUserId.shopId = this.shop.id;
                this.getAllUserId();
            }
        });
    }

    getAllShopId() {
        this.paramsShopId.pageIndex = 1;
        this.userShopLoaded = Promise.resolve(false);
        this.chatShopService.getAllShopId(this.paramsShopId).subscribe({
            next: async (res: any) => {
                this.listShopUser = res.data ? res.data : [];
                if(this.listShopUser.length <= 0) {
                    this.userShopLoaded = Promise.resolve(true);
                }
                else {
                    for (let obj of this.listShopUser) {
                        await this.getShopByShopId(obj, this.listShopUser.indexOf(obj) + 1, this.listShopUser.length);
                    }
                    this.activeShopIdUser = this.listShopUser[0].shopId;
                    this.userShopOpenChatField(this.listShopUser[0]);
                }
            }
        });
    }

    getAllUserNhanId() {
        this.paramsUserNhanId.pageIndex = 1;
        this.userUserLoaded = Promise.resolve(false);
        this.chatUserService.getAllUserNhanId(this.paramsUserNhanId).subscribe({
            next: async (res: any) => {
                this.listUserUser = res.data ? res.data : [];
                if(this.listUserUser.length <= 0) {
                    this.userUserLoaded = Promise.resolve(true);
                }
                else {
                    for(let obj of this.listUserUser) {
                        await this.getUserNhanByUserId(obj, this.listUserUser.indexOf(obj) + 1, this.listUserUser.length);
                    }
                    this.activeUserIdUser = this.listUserUser[0].userNhanId;
                    this.userUserOpenChatField(this.listUserUser[0]);
                }
            }
        });
    }

    getAllUserId() {
        this.paramsUserId.pageIndex = 1;
        this.shopLoaded = Promise.resolve(false);
        this.chatShopService.getAllUserId(this.paramsUserId).subscribe({
            next: async (res: any) => {
                this.listUser = res.data ? res.data : [];
                if(this.listUser.length <= 0) {
                    this.shopLoaded = Promise.resolve(true);
                }
                else {
                    for(let obj of this.listUser) {
                        await this.getUserInfoByUserId(obj, this.listUser.indexOf(obj) + 1, this.listUser.length);
                    }
                    this.activeUserId = this.listUser[0].userId;
                    this.shopOpenChatField(this.listUser[0]);
                }
            }
        });
    }

    getMoreShopId() {
        if(this.moreUserShopPartner && this.animation && !this.blockGetMore && this.tabIndex == 0 && this.collapseTabIndex == 0) {
            let partnerElement = document.getElementsByName("userShopPartnerElement")[document.getElementsByName("userShopPartnerElement").length - 1];
            let elementTop = partnerElement.offsetTop;
            let elementBottom = elementTop + partnerElement.clientHeight;
            let fieldTop = this.userShopPartnerField.nativeElement.scrollTop;
            let fieldBottom = fieldTop + this.userShopPartnerField.nativeElement.clientHeight;
            if(elementBottom <= fieldBottom) {
                this.paramsShopId.pageIndex++;
                this.blockGetMore = true;
                this.chatShopService.getAllShopId(this.paramsShopId).subscribe({
                    next: (res: any) => {
                        if(res.data && res.data.length) {
                            for(let index in res.data) {
                                for(let index2 in this.listShopUser) {
                                    if(this.listShopUser[index2].shopId == res.data[index].shopId) {
                                        break;
                                    }
                                    if(parseInt(index2) == this.listShopUser.length - 1) {
                                        this.getShopByShopId(res.data[index], -1, res.data.length);
                                        this.listShopUser.push(res.data[index]);
                                    }
                                }
                                if(parseInt(index) == res.data.length - 1) {
                                    this.blockGetMore = false;
                                }
                            }
                        }
                        else {
                            this.blockGetMore = false;
                            this.moreUserShopPartner = false;
                        }
                    },
                    error: (res: any) => {
                        this.blockGetMore = false;
                    }
                });
            }
        }
    }

    getMoreUserNhanId() {
        if(this.moreUserUserPartner && this.animation && !this.blockGetMore && this.tabIndex == 0 && this.collapseTabIndex == 1) {
            let partnerElement = document.getElementsByName("userUserPartnerElement")[document.getElementsByName("userUserPartnerElement").length - 1];
            let elementTop = partnerElement.offsetTop;
            let elementBottom = elementTop + partnerElement.clientHeight;
            let fieldTop = this.userUserPartnerField.nativeElement.scrollTop;
            let fieldBottom = fieldTop + this.userUserPartnerField.nativeElement.clientHeight;
            if(elementBottom <= fieldBottom) {
                this.paramsUserNhanId.pageIndex++;
                this.blockGetMore = true;
                this.chatUserService.getAllUserNhanId(this.paramsUserNhanId).subscribe({
                    next: (res: any) => {
                        if(res.data && res.data.length) {
                            for(let index in res.data) {
                                for(let index2 in this.listUserUser) {
                                    if(this.listUserUser[index2].userNhanId == res.data[index].userNhanId) {
                                        break;
                                    }
                                    if(parseInt(index2) == this.listShopUser.length - 1) {
                                        this.getUserNhanByUserId(res.data[index], -1, res.data.length);
                                        this.listUserUser.push(res.data[index]);
                                    }
                                }
                                if(parseInt(index) == res.data.length - 1) {
                                    this.blockGetMore = false;
                                }
                            }
                        }
                        else {
                            this.blockGetMore = false;
                            this.moreUserUserPartner = false;
                        }
                    },
                    error: (res: any) => {
                        this.blockGetMore = false;
                    }
                });
            }
        }
    }

    getMoreUserId() {
        if(this.moreShopPartner && this.animation && !this.blockGetMore && this.tabIndex == 1) {
            let partnerElement = document.getElementsByName("shopPartnerElement")[document.getElementsByName("shopPartnerElement").length - 1];
            let elementTop = partnerElement.offsetTop;
            let elementBottom = elementTop + partnerElement.clientHeight;
            let fieldTop = this.shopPartnerField.nativeElement.scrollTop;
            let fieldBottom = fieldTop + this.shopPartnerField.nativeElement.clientHeight;
            if(elementBottom <= fieldBottom) {
                this.paramsUserId.pageIndex++;
                this.blockGetMore = true;
                this.chatShopService.getAllUserId(this.paramsUserId).subscribe({
                    next: (res: any) => {
                        if(res.data && res.data.length) {
                            for(let index in res.data) {
                                for(let index2 in this.listUser) {
                                    if(this.listUser[index2].userId == res.data[index].userId) {
                                        break;
                                    }
                                    if(parseInt(index2) == this.listUser.length - 1) {
                                        this.getUserInfoByUserId(res.data[index], -1, res.data.length);
                                        this.listUser.push(res.data[index]);
                                    }
                                }
                                if(parseInt(index) == res.data.length - 1) {
                                    this.blockGetMore = false;
                                }
                            }
                        }
                        else {
                            this.blockGetMore = false;
                            this.moreShopPartner = false;
                        }
                    },
                    error: (res: any) => {
                        this.blockGetMore = false;
                    }
                });
            }
        }
    }

    async getShopByShopId(item: any, currentItem: any, itemLength: any) {
        item.chuaXemCount = 0;
        item.listChuaXem = [];
        this.getAllTinNhanShopChuaXem(item, 1);
        item.moreNhanTinShop = true;
        item.imagePreUpload = {
            files: [],
            closed: false,
            scrollableRight: false,
            scrollableLeft: false,
            scrollLeft: 0
        }
        await this.chatShopService.getShopByShopId(item.shopId).toPromise().then((res: any) => {
            if(res) {
                item.info = res;
                if(item.info.anhDaiDien) {
                    item.info.anhDaiDien = environment.apiUrl + "/api/" + item.info.anhDaiDien.replaceAll("\\", "/");
                }
            }
            if(currentItem == itemLength) {
                this.userShopLoaded = Promise.resolve(true);
                if(this.animation) {
                    setTimeout(() => {
                        if(this.collapseTabIndex != 0) {
                            this.userUserPartnerField.nativeElement.style.transition = "height 0.5s ease-in-out";
                            this.userShopPartnerField.nativeElement.style.transition = "height 0.5s ease-in-out";
                        }
                        this.collapseTabIndex = 0;
                        this.userShopPartnerField.nativeElement.style.height = this.contentHeight + "px";
                        this.userUserPartnerField.nativeElement.style.height = "0px";
                    }, 1);
                }
            }
        });
    }

    async getUserNhanByUserId(item: any, currentItem: any, itemLength: any) {
        item.chuaXemCount = 0;
        item.listChuaXem = [];
        this.getAllTinNhanUserChuaXem(item);
        item.moreNhanTinUser = true;
        item.imagePreUpload = {
            files: [],
            closed: false,
            scrollableRight: false,
            scrollableLeft: false,
            scrollLeft: 0
        }
        await this.chatUserService.getUserInfoByUserId(item.userNhanId).toPromise().then((res: any) => {
            if(res) {
                item.info = res;
                if(item.info.anhDaiDien) {
                    item.info.anhDaiDien = environment.apiUrl + "/api/" + item.info.anhDaiDien.replaceAll("\\", "/");
                }
                this.chatUserService.getUserById(item.userNhanId).subscribe({
                    next: (ress: any) => {
                        if(ress) {
                            if((item.info.ho && item.info.ho.trim()) &&
                                ((item.info.tenDem && item.info.tenDem.trim()) ||
                                (item.info.ten && item.info.ten.trim()))) {
                                    item.info.name = "";
                                    if(item.info.ho && item.info.ho.trim()) {
                                        item.info.name += item.info.ho + " ";
                                    }
                                    if(item.info.tenDem && item.info.tenDem.trim()) {
                                        item.info.name += item.info.tenDem + " ";
                                    }
                                    if(item.info.ten && item.info.ten.trim()) {
                                        item.info.name += item.info.ten;
                                    }
                            }
                            else {
                                item.info.name = ress.userName;
                            }
                        }
                        if(currentItem == itemLength) {
                            this.userUserLoaded = Promise.resolve(true);
                            if(this.animation) {
                                setTimeout(() => {
                                    if(this.collapseTabIndex != 1) {
                                        this.userUserPartnerField.nativeElement.style.transition = "height 0.5s ease-in-out";
                                        this.userShopPartnerField.nativeElement.style.transition = "height 0.5s ease-in-out";
                                    }
                                    this.collapseTabIndex = 1;
                                    this.userUserPartnerField.nativeElement.style.height = this.contentHeight + "px";
                                    this.userShopPartnerField.nativeElement.style.height = "0px";
                                }, 1);
                            }
                        }
                    }
                });
            }
        });
    }

    async getUserInfoByUserId(item: any, currentItem: any, itemLength: any) {
        item.chuaXemCount = 0;
        item.listChuaXem = [];
        this.getAllTinNhanShopChuaXem(item, 2);
        item.moreNhanTinShop = true;
        item.imagePreUpload = {
            files: [],
            closed: false,
            scrollableRight: false,
            scrollableLeft: false,
            scrollLeft: 0
        }
        await this.chatShopService.getUserInfoByUserId(item.userId).toPromise().then((res: any) => {
            if(res) {
                item.info = res;
                if(item.info.anhDaiDien) {
                    item.info.anhDaiDien = environment.apiUrl + "/api/" + item.info.anhDaiDien.replaceAll("\\", "/");
                }
                this.chatShopService.getUserById(item.userId).subscribe({
                    next: (ress: any) => {
                        if(ress) {
                            if((item.info.ho && item.info.ho.trim()) &&
                                ((item.info.tenDem && item.info.tenDem.trim()) ||
                                (item.info.ten && item.info.ten.trim()))) {
                                    item.info.name = "";
                                    if(item.info.ho && item.info.ho.trim()) {
                                        item.info.name += item.info.ho + " ";
                                    }
                                    if(item.info.tenDem && item.info.tenDem.trim()) {
                                        item.info.name += item.info.tenDem + " ";
                                    }
                                    if(item.info.ten && item.info.ten.trim()) {
                                        item.info.name += item.info.ten;
                                    }
                            }
                            else {
                                item.info.name = ress.userName;
                            }
                        }
                        if(currentItem == itemLength) {
                            this.shopLoaded = Promise.resolve(true);
                        }
                    }
                });
            }
        });
    }

    async userShopOpenChatField(item: any) {
        item.moreNhanTinShop = true;
        this.blockGetMore = true;
        this.userShopChatField = item;
        this.activeShopIdUser = this.userShopChatField.shopId;
        this.userShopListNhanTin = [];
        this.paramsNhanTinShop.userId = this.userShopChatField.userId;
        this.paramsNhanTinShop.shopId = this.userShopChatField.shopId;
        this.paramsNhanTinShop.pageIndex = 1;
        await this.chatShopService.getAllNhanTinShop(this.paramsNhanTinShop).toPromise().then((res: any) => {
            if(res.data && res.data.length) {
                for(let index in res.data) {
                    if(parseInt(index) == 0) {
                        this.userShopViewed = res.data[index].daXem;
                    }
                    res.data[index].sendError = false;
                    if (res.data[index].isImage) {
                        res.data[index].noiDungHienThi = environment.apiUrl + "/api/" + res.data[index].noiDung.replaceAll("\\", "/");
                    }
                    if(parseInt(index) == res.data.length - 1) {
                        this.userShopListNhanTin = res.data.reverse();
                    }
                }
            }
            else {
                this.blockGetMore = false;
            }
        }).catch((res: any) => {
            this.blockGetMore = false;
        });
        setTimeout(() => {
            if(this.tabIndex == 0 && this.collapseTabIndex == 0) {
                document.getElementById("userShopCheck")!.scrollIntoView();
            }
            this.blockGetMore = false;
            this.setScrollLeft(this.userShopChatField.imagePreUpload.scrollLeft);
        }, 1);
        if(this.animation) {
            this.updateNhanTinShop(this.userShopChatField, 1);
        }
    }

    async userUserOpenChatField(item: any) {
        item.moreNhanTinUser = true;
        this.blockGetMore = true;
        this.userUserChatField = item;
        this.activeUserIdUser = this.userUserChatField.userNhanId;
        this.userUserListNhanTin = [];
        this.paramsNhanTinUser.userGuiId = this.userUserChatField.userGuiId;
        this.paramsNhanTinUser.userNhanId = this.userUserChatField.userNhanId;
        this.paramsNhanTinUser.pageIndex = 1;
        await this.chatUserService.getAllNhanTinUser(this.paramsNhanTinUser).toPromise().then((res: any) => {
            if(res.data && res.data.length) {
                for(let index in res.data) {
                    if(parseInt(index) == 0) {
                        this.userUserViewed = res.data[index].daXem;
                    }
                    res.data[index].sendError = false;
                    if (res.data[index].isImage) {
                        res.data[index].noiDungHienThi = environment.apiUrl + "/api/" + res.data[index].noiDung.replaceAll("\\", "/");
                    }
                    if(parseInt(index) == res.data.length - 1) {
                        this.userUserListNhanTin = res.data.reverse();
                    }
                }
            }
            else {
                this.blockGetMore = false;
            }
        }).catch((res: any) => {
            this.blockGetMore = false;
        });
        setTimeout(() => {
            if(this.tabIndex == 0 && this.collapseTabIndex == 1) {
                document.getElementById("userUserCheck")!.scrollIntoView();
            }
            this.blockGetMore = false;
            this.setScrollLeft(this.userUserChatField.imagePreUpload.scrollLeft);
        }, 1);
        if(this.animation) {
            this.updateNhanTinUser(this.userUserChatField);
        }
    }

    async shopOpenChatField(item: any) {
        item.moreNhanTinShop = true;
        this.blockGetMore = true;
        this.shopChatField = item;
        this.activeUserId = this.shopChatField.userId;
        this.shopListNhanTin = [];
        this.paramsNhanTinShop.userId = this.shopChatField.userId;
        this.paramsNhanTinShop.shopId = this.shopChatField.shopId;
        this.paramsNhanTinShop.pageIndex = 1;
        await this.chatShopService.getAllNhanTinShop(this.paramsNhanTinShop).toPromise().then((res: any) => {
            if(res.data && res.data.length) {
                for(let index in res.data) {
                    if(parseInt(index) == 0) {
                        this.shopViewed = res.data[index].daXem;
                    }
                    res.data[index].sendError = false;
                    if (res.data[index].isImage) {
                        res.data[index].noiDungHienThi = environment.apiUrl + "/api/" + res.data[index].noiDung.replaceAll("\\", "/");
                    }
                    if(parseInt(index) == res.data.length - 1) {
                        this.shopListNhanTin = res.data.reverse();
                    }
                }
            }
            else {
                this.blockGetMore = false;
            }
        }).catch((res: any) => {
            this.blockGetMore = false;
        });
        setTimeout(() => {
            if(this.tabIndex == 1) {
                document.getElementById("shopCheck")!.scrollIntoView();
            }
            this.blockGetMore = false;
            this.setScrollLeft(this.shopChatField.imagePreUpload.scrollLeft);
        }, 1);
        if(this.animation) {
            this.updateNhanTinShop(this.shopChatField, 2);
        }
    }

    userGetMoreNhanTinShop(item: any) {
        if(item.moreNhanTinShop && this.animation && !this.blockGetMore) {
            let messageElement = document.getElementsByName("userShopMessageElement")[1];
            let elementTop = messageElement.offsetTop;
            let fieldTop = this.userShopMessageField.nativeElement.scrollTop;
            if(elementTop >= fieldTop) {
                this.blockGetMore = true;
                this.paramsNhanTinShop.pageIndex += 1 + (this.userShopMessageCount - this.userShopMessageCount % this.paramsNhanTinShop.pageSize) / this.paramsNhanTinShop.pageSize;
                this.paramsNhanTinShop.userId = this.userShopChatField.userId;
                this.paramsNhanTinShop.shopId = this.userShopChatField.shopId;
                this.chatShopService.getAllNhanTinShop(this.paramsNhanTinShop).subscribe({
                    next: (res: any) => {
                        if(res.data && res.data.length) {
                            for(let index in res.data) {
                                if (res.data[index].isImage) {
                                    res.data[index].noiDungHienThi = environment.apiUrl + "/api/" + res.data[index].noiDung.replaceAll("\\", "/");
                                }
                                for(let index2 in this.userShopListNhanTin) {
                                    if(this.userShopListNhanTin[index2].id == res.data[index].id) {
                                        break;
                                    }
                                    if(parseInt(index2) == this.userShopListNhanTin.length - 1) {
                                        res.data[index].sendError = false;
                                        this.userShopListNhanTin.unshift(res.data[index]);
                                    }
                                }
                                if(parseInt(index) == res.data.length - 1) {
                                    this.userShopMessageCount = 0;
                                    this.blockGetMore = false;
                                }
                            }
                        }
                        else {
                            item.moreNhanTinShop = false;
                            this.blockGetMore = false;
                        }
                    },
                    error: (res: any) => {
                        this.blockGetMore = false;
                    }
                });
            }
        }
        setTimeout(() => {
            let messageElements = document.getElementsByName("userShopMessageElement");
            let length = messageElements.length;
            let messageElement = messageElements[length - 2];
            if(length == 1) {
                messageElement = messageElements[length - 1];
            }
            let elementTop = messageElement.offsetTop;
            let elementBottom = elementTop + messageElement.clientHeight;
            let fieldTop = this.userShopMessageField.nativeElement.scrollTop;
            let fieldBottom = fieldTop + this.userShopMessageField.nativeElement.clientHeight;
            if(elementBottom <= fieldBottom) {
                this.userShopNewMessageAnimation = false;
                if(this.animation) {
                    this.updateNhanTinShop(this.userShopChatField, 1);
                }
            }
        }, 10);
    }

    userGetMoreNhanTinUser(item: any) {
        if(item.moreNhanTinUser && this.animation && !this.blockGetMore) {
            let messageElement = document.getElementsByName("userUserMessageElement")[1];
            let elementTop = messageElement.offsetTop;
            let fieldTop = this.userUserMessageField.nativeElement.scrollTop;
            if(elementTop >= fieldTop) {
                this.blockGetMore = true;
                this.paramsNhanTinUser.pageIndex += 1 + (this.userUserMessageCount - this.userUserMessageCount % this.paramsNhanTinUser.pageSize) / this.paramsNhanTinUser.pageSize;
                this.paramsNhanTinUser.userGuiId = this.userUserChatField.userGuiId;
                this.paramsNhanTinUser.userNhanId = this.userUserChatField.userNhanId;
                this.chatUserService.getAllNhanTinUser(this.paramsNhanTinUser).subscribe({
                    next: (res: any) => {
                        if(res.data && res.data.length) {
                            for(let index in res.data) {
                                if (res.data[index].isImage) {
                                    res.data[index].noiDungHienThi = environment.apiUrl + "/api/" + res.data[index].noiDung.replaceAll("\\", "/");
                                }
                                for(let index2 in this.userUserListNhanTin) {
                                    if(this.userUserListNhanTin[index2].id == res.data[index].id) {
                                        break;
                                    }
                                    if(parseInt(index2) == this.userUserListNhanTin.length - 1) {
                                        res.data[index].sendError = false;
                                        this.userUserListNhanTin.unshift(res.data[index]);
                                    }
                                }
                                if(parseInt(index) == res.data.length - 1) {
                                    this.userUserMessageCount = 0;
                                    this.blockGetMore = false;
                                }
                            }
                        }
                        else {
                            item.moreNhanTinUser = false;
                            this.blockGetMore = false;
                        }
                    },
                    error: (res: any) => {
                        this.blockGetMore = false;
                    }
                });
            }
        }
        setTimeout(() => {
            let messageElements = document.getElementsByName("userUserMessageElement");
            let length = messageElements.length;
            let messageElement = messageElements[length - 2];
            if(length == 1) {
                messageElement = messageElements[length - 1];
            }
            let elementTop = messageElement.offsetTop;
            let elementBottom = elementTop + messageElement.clientHeight;
            let fieldTop = this.userUserMessageField.nativeElement.scrollTop;
            let fieldBottom = fieldTop + this.userUserMessageField.nativeElement.clientHeight;
            if(elementBottom <= fieldBottom) {
                this.userUserNewMessageAnimation = false;
                if(this.animation) {
                    this.updateNhanTinUser(this.userUserChatField);
                }
            }
        }, 10);
    }

    shopGetMoreNhanTinShop(item: any) {
        if(item.moreNhanTinShop && this.animation && !this.blockGetMore) {
            let messageElement = document.getElementsByName("shopMessageElement")[1];
            let elementTop = messageElement.offsetTop;
            let fieldTop = this.shopMessageField.nativeElement.scrollTop;
            if(elementTop >= fieldTop) {
                this.blockGetMore = true;
                this.paramsNhanTinShop.pageIndex += 1 + (this.shopMessageCount - this.shopMessageCount % this.paramsNhanTinShop.pageSize) / this.paramsNhanTinShop.pageSize;
                this.paramsNhanTinShop.userId = this.shopChatField.userId;
                this.paramsNhanTinShop.shopId = this.shopChatField.shopId;
                this.chatShopService.getAllNhanTinShop(this.paramsNhanTinShop).subscribe({
                    next: (res: any) => {
                        if(res.data && res.data.length) {
                            for(let index in res.data) {
                                if (res.data[index].isImage) {
                                    res.data[index].noiDungHienThi = environment.apiUrl + "/api/" + res.data[index].noiDung.replaceAll("\\", "/");
                                }
                                for(let index2 in this.shopListNhanTin) {
                                    if(this.shopListNhanTin[index2].id == res.data[index].id) {
                                        break;
                                    }
                                    if(parseInt(index2) == this.shopListNhanTin.length - 1) {
                                        res.data[index].sendError = false;
                                        this.shopListNhanTin.unshift(res.data[index]);
                                    }
                                }
                                if(parseInt(index) == res.data.length - 1) {
                                    this.shopMessageCount = 0;
                                    this.blockGetMore = false;
                                }
                            }
                        }
                        else {
                            item.moreNhanTinShop = false;
                            this.blockGetMore = false;
                        }
                    },
                    error: (res: any) => {
                        this.blockGetMore = false;
                    }
                });
            }
        }
        setTimeout(() => {
            let messageElements = document.getElementsByName("shopMessageElement");
            let length = messageElements.length;
            let messageElement = messageElements[length - 2];
            if(length == 1) {
                messageElement = messageElements[length - 1];
            }
            let elementTop = messageElement.offsetTop;
            let elementBottom = elementTop + messageElement.clientHeight;
            let fieldTop = this.shopMessageField.nativeElement.scrollTop;
            let fieldBottom = fieldTop + this.shopMessageField.nativeElement.clientHeight;
            if(elementBottom <= fieldBottom) {
                this.shopNewMessageAnimation = false;
                if(this.animation) {
                    this.updateNhanTinShop(this.shopChatField, 2);
                }
            }
        }, 10);
    }

    keyCapture(event: any, nguoiGuiId: any) {
        let e = <KeyboardEvent>event;
        if(e.keyCode === 13) {
            e.preventDefault();
            if(this.tabIndex == 0 && this.collapseTabIndex == 0) {
                this.userCreateNhanTinShop(nguoiGuiId);
            }
            else if(this.tabIndex == 0 && this.collapseTabIndex == 1) {
                this.userCreateNhanTinUser(nguoiGuiId);
            }
            else if(this.tabIndex == 1) {
                this.shopCreateNhanTinShop(nguoiGuiId);
            }
        }
    }

    async userCreateNhanTinShop(nguoiGuiId: any) {
        if(this.userShopMessage.nativeElement.value && this.userShopMessage.nativeElement.value.trim()) {
            this.userShopViewed = false;
            this.updateNhanTinShop(this.userShopChatField, 1);
            this.userShopSending = true;
            this.userShopSendError = false;
            let item: any = {
                userId: this.userShopChatField.userId,
                shopId: this.userShopChatField.shopId,
                nguoiGuiId: nguoiGuiId,
                noiDung: this.userShopMessage.nativeElement.value.trim(),
                createdAt: this.getTimeNow(),
                sendError: false,
                isFile: false,
                isImage: false
            }
            let index = this.listShopUser.indexOf(this.userShopChatField);
            if(index != -1) {
                this.listShopUser.unshift(this.listShopUser.splice(index, 1)[0]);
            }
            this.userShopListNhanTin.push(item);
            this.userShopMessage.nativeElement.value = "";
            setTimeout(() => {
                document.getElementById("userShopCheck")!.scrollIntoView();
            }, 1);
            await this.chatShopService.createNhanTinShop(this.loggedUser.userId, item).toPromise().then((res: any) => {
                if(res.statusCode == 200) {
                    this.userShopMessageCount++;
                    this.userShopSending = false;
                    this.userShopSendError = false;
                }
                else {
                    this.userShopSending = false;
                    this.userShopSendError = true;
                    item.sendError = true;
                }
            }).catch((res: any) => {
                this.userShopSending = false;
                this.userShopSendError = true;
                item.sendError = true;
            });
        }
        if(this.userShopChatField.imagePreUpload.files.length) {
            this.uploadFile(this.userShopChatField.imagePreUpload);
        }
    }

    async userCreateNhanTinUser(nguoiGuiId: any) {
        if(this.userUserMessage.nativeElement.value && this.userUserMessage.nativeElement.value.trim()) {
            this.userUserViewed = false;
            this.updateNhanTinUser(this.userUserChatField);
            this.userUserSending = true;
            this.userUserSendError = false;
            let item: any = {
                userGuiId: nguoiGuiId,
                userNhanId: this.userUserChatField.userNhanId,
                noiDung: this.userUserMessage.nativeElement.value.trim(),
                createdAt: this.getTimeNow(),
                sendError: false
            }
            let index = this.listUserUser.indexOf(this.userUserChatField);
            if(index != -1) {
                this.listUserUser.unshift(this.listUserUser.splice(index, 1)[0]);
            }
            this.userUserListNhanTin.push(item);
            this.userUserMessage.nativeElement.value = "";
            setTimeout(() => {
                document.getElementById("userUserCheck")!.scrollIntoView();
            }, 1);
            await this.chatUserService.createNhanTinUser(this.loggedUser.userId, item).toPromise().then((res: any) => {
                if(res.statusCode == 200) {
                    this.userUserMessageCount++;
                    this.userUserSending = false;
                    this.userUserSendError = false;
                }
                else {
                    this.userUserSending = false;
                    this.userUserSendError = true;
                    item.sendError = true;
                }
            }).catch((res: any) => {
                this.userUserSending = false;
                this.userUserSendError = true;
                item.sendError = true;
            });
        }
        if(this.userUserChatField.imagePreUpload.files.length) {
            this.uploadFile(this.userUserChatField.imagePreUpload);
        }
    }

    async shopCreateNhanTinShop(nguoiGuiId: any) {
        if(this.shopMessage.nativeElement.value && this.shopMessage.nativeElement.value.trim()) {
            this.shopViewed = false;
            this.updateNhanTinShop(this.shopChatField, 2);
            this.shopSending = true;
            this.shopSendError = false;
            let item: any = {
                userId: this.shopChatField.userId,
                shopId: this.shopChatField.shopId,
                nguoiGuiId: nguoiGuiId,
                noiDung: this.shopMessage.nativeElement.value.trim(),
                createdAt: this.getTimeNow(),
                sendError: false
            }
            let index = this.listUser.indexOf(this.shopChatField);
            if(index != -1) {
                this.listUser.unshift(this.listUser.splice(index, 1)[0]);
            }
            this.shopListNhanTin.push(item);
            this.shopMessage.nativeElement.value = "";
            setTimeout(() => {
                document.getElementById("shopCheck")!.scrollIntoView();
            }, 1);
            await this.chatShopService.createNhanTinShop(this.loggedUser.userId, item).toPromise().then((res: any) => {
                if(res.statusCode == 200) {
                    this.shopMessageCount++;
                    this.shopSending = false;
                    this.shopSendError = false;
                }
                else {
                    this.shopSending = false;
                    this.shopSendError = true;
                    item.sendError = true;
                }
            }).catch((res: any) => {
                this.shopSending = false;
                this.shopSendError = true;
                item.sendError = true;
            });
        }
        if(this.shopChatField.imagePreUpload.files.length) {
            this.uploadFile(this.shopChatField.imagePreUpload);
        }
    }

    getTimeNow() {
        let now = new Date();
        let dayNow = now.getDate();
        let monthNow = now.getMonth() + 1;
        let yearNow = now.getFullYear();
        let hourNow = now.getHours();
        let minuteNow = now.getMinutes();
        let secondNow = now.getSeconds();
        let maxDate = "";
        if (dayNow < 10) {
            if (monthNow < 10) {
                maxDate = yearNow + "-" + "0" + monthNow + "-" + "0" + dayNow;
            }
            else {
                maxDate = yearNow + "-" + monthNow + "-" + "0" + dayNow;
            }
        }
        else {
            if (monthNow < 10) {
                maxDate = yearNow + "-" + "0" + monthNow + "-" + dayNow;
            }
            else {
                maxDate = yearNow + "-" + monthNow + "-" + dayNow;
            }
        }
        if(hourNow < 10) {
            if(minuteNow < 10) {
                if(secondNow < 10) {
                    maxDate += "T" + "0" + hourNow + ":0" + minuteNow + ":0" + secondNow;
                }
                else {
                    maxDate += "T" + "0" + hourNow + ":0" + minuteNow + ":" + secondNow;
                }
            }
            else {
                if(secondNow < 10) {
                    maxDate += "T" + "0" + hourNow + ":" + minuteNow + ":0" + secondNow;
                }
                else {
                    maxDate += "T" + "0" + hourNow + ":" + minuteNow + ":" + secondNow;
                }
            }
        }
        else {
            if(minuteNow < 10) {
                if(secondNow < 10) {
                    maxDate += "T" + hourNow + ":0" + minuteNow + ":0" + secondNow;
                }
                else {
                    maxDate += "T" + hourNow + ":0" + minuteNow + ":" + secondNow;
                }
            }
            else {
                if(secondNow < 10) {
                    maxDate += "T" + hourNow + ":" + minuteNow + ":0" + secondNow;
                }
                else {
                    maxDate += "T" + hourNow + ":" + minuteNow + ":" + secondNow;
                }
            }
        }
        return maxDate;
    }

    changeTab(eventType: any) {
        if(this.tabIndex == 0 && this.collapseTabIndex == 0) {
            if(this.userShopChatField) {
                setTimeout(() => {
                    if(eventType == 1) {
                        setTimeout(() => {
                            document.getElementById("userShopCheck")!.scrollIntoView();
                        }, 1);
                    }
                    let messageElements = document.getElementsByName("userShopMessageElement");
                    let length = messageElements.length;
                    let messageElement = messageElements[length - 3];
                    let elementTop = messageElement.offsetTop;
                    let elementBottom = elementTop + messageElement.clientHeight;
                    let fieldTop = this.userShopMessageField.nativeElement.scrollTop;
                    let fieldBottom = fieldTop + this.userShopMessageField.nativeElement.clientHeight;
                    if(elementBottom <= fieldBottom) {
                        this.userShopNewMessageAnimation = false;
                        if(this.animation && eventType == 1) {
                            this.updateNhanTinShop(this.userShopChatField, 1);
                        }
                        if(eventType == 2) {
                            setTimeout(() => {
                                document.getElementById("userShopCheck")!.scrollIntoView();
                            }, 1);
                        }
                    }
                    this.setScrollLeft(this.userShopChatField.imagePreUpload.scrollLeft);
                }, 10);
            }
        }
        else if(this.tabIndex == 0 && this.collapseTabIndex == 1) {
            if(this.userUserChatField) {
                setTimeout(() => {
                    if(eventType == 1) {
                        setTimeout(() => {
                            document.getElementById("userUserCheck")!.scrollIntoView();
                        }, 1);
                    }
                    let messageElements = document.getElementsByName("userUserMessageElement");
                    let length = messageElements.length;
                    let messageElement = messageElements[length - 3];
                    let elementTop = messageElement.offsetTop;
                    let elementBottom = elementTop + messageElement.clientHeight;
                    let fieldTop = this.userUserMessageField.nativeElement.scrollTop;
                    let fieldBottom = fieldTop + this.userUserMessageField.nativeElement.clientHeight;
                    if(elementBottom <= fieldBottom) {
                        this.userUserNewMessageAnimation = false;
                        if(this.animation && eventType == 1) {
                            this.updateNhanTinUser(this.userUserChatField);
                        }
                        if(eventType == 2) {
                            setTimeout(() => {
                                document.getElementById("userUserCheck")!.scrollIntoView();
                            }, 1);
                        }
                    }
                    this.setScrollLeft(this.userUserChatField.imagePreUpload.scrollLeft);
                }, 10);
            }
        }
        else if(this.tabIndex == 1) {
            if(this.shopChatField) {
                setTimeout(() => {
                    if(eventType == 1) {
                        setTimeout(() => {
                            document.getElementById("shopCheck")!.scrollIntoView();
                        }, 1);
                    }
                    let messageElements = document.getElementsByName("shopMessageElement");
                    let length = messageElements.length;
                    let messageElement = messageElements[length - 3];
                    let elementTop = messageElement.offsetTop;
                    let elementBottom = elementTop + messageElement.clientHeight;
                    let fieldTop = this.shopMessageField.nativeElement.scrollTop;
                    let fieldBottom = fieldTop + this.shopMessageField.nativeElement.clientHeight;
                    if(elementBottom <= fieldBottom) {
                        this.shopNewMessageAnimation = false;
                        if(this.animation && eventType == 1) {
                            this.updateNhanTinShop(this.shopChatField, 2);
                        }
                        if(eventType == 2) {
                            setTimeout(() => {
                                document.getElementById("shopCheck")!.scrollIntoView();
                            }, 1);
                        }
                    }
                    this.setScrollLeft(this.shopChatField.imagePreUpload.scrollLeft);
                }, eventType == 1 ? 400 : 10);
            }
        }
    }

    changeCollapseTab() {
        this.collapseTabIndex = this.collapseTabIndex == 0 ? 1 : 0;
        this.userUserPartnerField.nativeElement.style.transition = "height 0.5s ease-in-out";
        this.userShopPartnerField.nativeElement.style.transition = "height 0.5s ease-in-out";
        if(this.collapseTabIndex == 0) {
            this.userShopPartnerField.nativeElement.style.height = this.contentHeight + "px";
            this.userUserPartnerField.nativeElement.style.height = "0px";
            if(this.userShopChatField) {
                setTimeout(() => {
                    document.getElementById("userShopCheck")!.scrollIntoView();
                    let messageElements = document.getElementsByName("userShopMessageElement");
                    let length = messageElements.length;
                    let messageElement = messageElements[length - 3];
                    let elementTop = messageElement.offsetTop;
                    let elementBottom = elementTop + messageElement.clientHeight;
                    let fieldTop = this.userShopMessageField.nativeElement.scrollTop;
                    let fieldBottom = fieldTop + this.userShopMessageField.nativeElement.clientHeight;
                    if(elementBottom <= fieldBottom) {
                        this.userShopNewMessageAnimation = false;
                        setTimeout(() => {
                            document.getElementById("userShopCheck")!.scrollIntoView();
                        }, 1);
                    }
                    this.setScrollLeft(this.userShopChatField.imagePreUpload.scrollLeft);
                }, 10);
            }
        }
        else {
            this.userUserPartnerField.nativeElement.style.height = this.contentHeight + "px";
            this.userShopPartnerField.nativeElement.style.height = "0px";
            if(this.userUserChatField) {
                setTimeout(() => {
                    document.getElementById("userUserCheck")!.scrollIntoView();
                    let messageElements = document.getElementsByName("userUserMessageElement");
                    let length = messageElements.length;
                    let messageElement = messageElements[length - 3];
                    let elementTop = messageElement.offsetTop;
                    let elementBottom = elementTop + messageElement.clientHeight;
                    let fieldTop = this.userUserMessageField.nativeElement.scrollTop;
                    let fieldBottom = fieldTop + this.userUserMessageField.nativeElement.clientHeight;
                    if(elementBottom <= fieldBottom) {
                        this.userUserNewMessageAnimation = false;
                        setTimeout(() => {
                            document.getElementById("userUserCheck")!.scrollIntoView();
                        }, 1);
                    }
                    this.setScrollLeft(this.userUserChatField.imagePreUpload.scrollLeft);
                }, 10);
            }
        }
    }

    startSocket() {
        this.hub = new HubConnectionBuilder().withUrl(SocketConstants.chatHub).build();
        this.hub.start();
    }

    addChatShopListener() {
        this.hub.on("chatShop", (res: any) => {
            if(res) {
                if(!res.daXem) {
                    if(res.userId == this.loggedUser.userId && res.userId != res.nguoiGuiId) {
                        if(this.listShopUser.length <= 0) {
                            let item = {
                                userId: this.loggedUser.userId,
                                shopId: res.nguoiGuiId
                            }
                            this.getShopByShopId(item, 1, 1);
                            this.listShopUser.unshift(item);
                            this.userShopOpenChatField(item);
                            this.activeShopIdUser = res.nguoiGuiId;
                        }
                        else {
                            for(let index in this.listShopUser) {
                                if(this.listShopUser[index].shopId == res.nguoiGuiId) {
                                    if(this.activeShopIdUser == res.nguoiGuiId) {
                                        this.listShopUser[index].moreNhanTinShop = true;
                                        if (res.isImage) {
                                            res.noiDungHienThi = environment.apiUrl + "/api/" + res.noiDung.replaceAll("\\", "/");
                                        }
                                        res.createdAt = this.getTimeNow();
                                        this.userShopListNhanTin.push(res);
                                        this.userShopMessageCount++;
                                        this.listShopUser[index].listChuaXem.push(res);
                                        this.listShopUser[index].chuaXemCount++;
                                        this.userChuaXemCount++;
                                        this.userShopChuaXemCount++;
                                        this.totalChuaXemCount++;
                                        if(this.tabIndex == 0 && this.collapseTabIndex == 0) {
                                            setTimeout(() => {
                                                let messageElement = document.getElementsByName("userShopMessageElement")[document.getElementsByName("userShopMessageElement").length - 3];
                                                let elementTop = messageElement.offsetTop;
                                                let elementBottom = elementTop + messageElement.clientHeight;
                                                let fieldTop = this.userShopMessageField.nativeElement.scrollTop;
                                                let fieldBottom = fieldTop + this.userShopMessageField.nativeElement.clientHeight;
                                                if(elementBottom <= fieldBottom) {
                                                    setTimeout(() => {
                                                        document.getElementById("userShopCheck")!.scrollIntoView();
                                                    }, 1);
                                                    if(this.animation) {
                                                        this.updateNhanTinShop(this.listShopUser[index], 1);
                                                    }
                                                }
                                                else {
                                                    this.userShopNewMessageAnimation = true;
                                                }
                                            }, 10);
                                        }
                                    }
                                    else {
                                        this.listShopUser[index].listChuaXem.push(res);
                                        this.listShopUser[index].chuaXemCount++;
                                        this.userChuaXemCount++;
                                        this.userShopChuaXemCount++;
                                        this.totalChuaXemCount++;
                                    }
                                    this.listShopUser.unshift(this.listShopUser.splice(index, 1)[0]);
                                    break;
                                }
                                if(parseInt(index) == this.listShopUser.length - 1) {
                                    let item = {
                                        userId: this.loggedUser.userId,
                                        shopId: res.nguoiGuiId
                                    }
                                    this.getShopByShopId(item, 1, 1);
                                    this.listShopUser.unshift(item);
                                    this.userShopOpenChatField(item);
                                    this.activeShopIdUser = res.nguoiGuiId;
                                }
                            }
                        }
                    }
                    if(this.shop && this.shop.id == res.shopId && res.shopId != res.nguoiGuiId) {
                        if(this.listUser.length <= 0) {
                            let item = {
                                userId: res.nguoiGuiId,
                                shopId: this.shop.id
                            }
                            this.getUserInfoByUserId(item, 1, 1);
                            this.listUser.unshift(item);
                            this.shopOpenChatField(item);
                            this.activeUserId = res.nguoiGuiId;
                        }
                        else {
                            for(let index in this.listUser) {
                                if(this.listUser[index].userId == res.nguoiGuiId) {
                                    if(this.activeUserId == res.nguoiGuiId) {
                                        this.listUser[index].moreNhanTinShop = true;
                                        if (res.isImage) {
                                            res.noiDungHienThi = environment.apiUrl + "/api/" + res.noiDung.replaceAll("\\", "/");
                                        }
                                        res.createdAt = this.getTimeNow();
                                        this.shopListNhanTin.push(res);
                                        this.shopMessageCount++;
                                        this.listUser[index].listChuaXem.push(res);
                                        this.listUser[index].chuaXemCount++;
                                        this.shopChuaXemCount++;
                                        this.totalChuaXemCount++;
                                        if(this.tabIndex == 1) {
                                            setTimeout(() => {
                                                let messageElement = document.getElementsByName("shopMessageElement")[document.getElementsByName("shopMessageElement").length - 3];
                                                let elementTop = messageElement.offsetTop;
                                                let elementBottom = elementTop + messageElement.clientHeight;
                                                let fieldTop = this.shopMessageField.nativeElement.scrollTop;
                                                let fieldBottom = fieldTop + this.shopMessageField.nativeElement.clientHeight;
                                                if(elementBottom <= fieldBottom) {
                                                    setTimeout(() => {
                                                        document.getElementById("shopCheck")!.scrollIntoView();
                                                    }, 1);
                                                    if(this.animation) {
                                                        this.updateNhanTinShop(this.listUser[index], 2);
                                                    }
                                                }
                                                else {
                                                    this.shopNewMessageAnimation = true;
                                                }
                                            }, 10);
                                        }
                                    }
                                    else {
                                        this.listUser[index].listChuaXem.push(res);
                                        this.listUser[index].chuaXemCount++;
                                        this.shopChuaXemCount++;
                                        this.totalChuaXemCount++;
                                    }
                                    this.listUser.unshift(this.listUser.splice(index, 1)[0]);
                                    break;
                                }
                                if(parseInt(index) == this.listUser.length - 1) {
                                    let item = {
                                        userId: res.nguoiGuiId,
                                        shopId: this.shop.id
                                    }
                                    this.getUserInfoByUserId(item, 1, 1);
                                    this.listUser.unshift(item);
                                    this.shopOpenChatField(item);
                                    this.activeUserId = res.nguoiGuiId;
                                }
                            }
                        }
                    }
                }
                else {
                    if(this.activeShopIdUser == res.shopId && res.userId == this.loggedUser.userId) {
                        this.userShopViewed = true;
                    }
                    if(this.activeUserId == res.userId && this.shop && this.shop.id == res.shopId) {
                        this.shopViewed = true;
                    }
                }
            }
        });
    }

    addChatUserListener() {
        this.hub.on("chatUser", (res: any) => {
            if(res) {
                if(!res.daXem) {
                    if(res.userNhanId == this.loggedUser.userId) {
                        if(this.listUserUser.length <= 0) {
                            let item = {
                                userGuiId: res.userNhanId,
                                userNhanId: res.userGuiId
                            }
                            this.getUserNhanByUserId(item, 1, 1);
                            this.listUserUser.unshift(item);
                            this.userUserOpenChatField(item);
                            this.activeUserIdUser = res.userGuiId;
                        }
                        else {
                            for(let index in this.listUserUser) {
                                if(this.listUserUser[index].userNhanId == res.userGuiId) {
                                    if(this.activeUserIdUser == res.userGuiId) {
                                        this.listUserUser[index].moreNhanTinUser = true;
                                        if (res.isImage) {
                                            res.noiDungHienThi = environment.apiUrl + "/api/" + res.noiDung.replaceAll("\\", "/");
                                        }
                                        res.createdAt = this.getTimeNow();
                                        this.userUserListNhanTin.push(res);
                                        this.userUserMessageCount++;
                                        this.listUserUser[index].listChuaXem.push(res);
                                        this.listUserUser[index].chuaXemCount++;
                                        this.userChuaXemCount++;
                                        this.userUserChuaXemCount++;
                                        this.totalChuaXemCount++;
                                        if(this.tabIndex == 0 && this.collapseTabIndex == 1) {
                                            setTimeout(() => {
                                                let messageElement = document.getElementsByName("userUserMessageElement")[document.getElementsByName("userUserMessageElement").length - 3];
                                                let elementTop = messageElement.offsetTop;
                                                let elementBottom = elementTop + messageElement.clientHeight;
                                                let fieldTop = this.userUserMessageField.nativeElement.scrollTop;
                                                let fieldBottom = fieldTop + this.userUserMessageField.nativeElement.clientHeight;
                                                if(elementBottom <= fieldBottom) {
                                                    setTimeout(() => {
                                                        document.getElementById("userUserCheck")!.scrollIntoView();
                                                    }, 1);
                                                    if(this.animation) {
                                                        this.updateNhanTinUser(this.listUserUser[index]);
                                                    }
                                                }
                                                else {
                                                    this.userUserNewMessageAnimation = true;
                                                }
                                            }, 10);
                                        }
                                    }
                                    else {
                                        this.listUserUser[index].listChuaXem.push(res);
                                        this.listUserUser[index].chuaXemCount++;
                                        this.userChuaXemCount++;
                                        this.userUserChuaXemCount++;
                                        this.totalChuaXemCount++;
                                    }
                                    this.listUserUser.unshift(this.listUserUser.splice(index, 1)[0]);
                                    break;
                                }
                                if(parseInt(index) == this.listUserUser.length - 1) {
                                    let item = {
                                        userGuiId: res.userNhanId,
                                        userNhanId: res.userGuiId
                                    }
                                    this.getUserNhanByUserId(item, 1, 1);
                                    this.listUserUser.unshift(item);
                                    this.userUserOpenChatField(item);
                                    this.activeUserIdUser = res.userGuiId;
                                }
                            }
                        }
                    }
                }
                else {
                    if(this.activeUserIdUser == res.userNhanId && this.loggedUser.userId == res.userGuiId) {
                        this.userUserViewed = true;
                    }
                }
            }
        });
    }

    returnNewestMessage() {
        if(this.tabIndex == 0 && this.collapseTabIndex == 0) {
            setTimeout(() => {
                document.getElementById("userShopCheck")!.scrollIntoView({
                    behavior: 'smooth',
                    block: 'start',
                    inline: 'nearest'
                });
                this.userShopNewMessageAnimation = false;
            }, 1);
            this.updateNhanTinShop(this.userShopChatField, 1);
        }
        else if(this.tabIndex == 0 && this.collapseTabIndex == 1) {
            setTimeout(() => {
                document.getElementById("userUserCheck")!.scrollIntoView({
                    behavior: 'smooth',
                    block: 'start',
                    inline: 'nearest'
                });
                this.userUserNewMessageAnimation = false;
            }, 1);
            this.updateNhanTinUser(this.userUserChatField);
        }
        else if(this.tabIndex == 1) {
            setTimeout(() => {
                document.getElementById("shopCheck")!.scrollIntoView({
                    behavior: 'smooth',
                    block: 'start',
                    inline: 'nearest'
                });
                this.shopNewMessageAnimation = false;
            }, 1);
            this.updateNhanTinShop(this.shopChatField, 2);
        }
    }

    setNewWidth(event: any) {
        this.widthPopup = event;
        if(this.tabIndex == 0 && this.collapseTabIndex == 0) {
            this.userShopChatField.imagePreUpload.scrollLeft = this.userShopImageField.nativeElement.scrollLeft;
        }
        else if(this.tabIndex == 0 && this.collapseTabIndex == 1) {
            this.userUserChatField.imagePreUpload.scrollLeft = this.userUserImageField.nativeElement.scrollLeft;
        }
        else if(this.tabIndex == 1) {
            this.shopChatField.imagePreUpload.scrollLeft = this.shopImageField.nativeElement.scrollLeft;
        }
        this.checkScrollable();
    }

    setNewHeight(event: any) {
        let gap = parseInt(event) - parseInt(this.heightPopup);
        this.heightPopup = event;
        this.contentHeight += gap;
        this.userShopPartnerField.nativeElement.style.transition = null;
        this.userUserPartnerField.nativeElement.style.transition = null;
        if(this.collapseTabIndex == 0) {
            this.userShopPartnerField.nativeElement.style.height = this.contentHeight + "px";
        }
        else {
            this.userUserPartnerField.nativeElement.style.height = this.contentHeight + "px";
        }
        if(this.tabIndex == 0 && this.collapseTabIndex == 0) {
            this.userShopChatField.imagePreUpload.scrollLeft = this.userShopImageField.nativeElement.scrollLeft;
        }
        else if(this.tabIndex == 0 && this.collapseTabIndex == 1) {
            this.userUserChatField.imagePreUpload.scrollLeft = this.userUserImageField.nativeElement.scrollLeft;
        }
        else if(this.tabIndex == 1) {
            this.shopChatField.imagePreUpload.scrollLeft = this.shopImageField.nativeElement.scrollLeft;
        }
        this.checkScrollable();
    }

    getAllTinNhanShopChuaXem(item: any, type: any) {
        let param = {
            userId: item.userId,
            shopId: item.shopId,
            daXem: false
        }
        this.chatShopService.getAllNhanTinShop(param).subscribe({
            next: (res: any) => {
                if(res.data && res.data.length) {
                    if(type == 1) {
                        for(let index in res.data) {
                            if(res.data[index].nguoiGuiId == res.data[index].userId) {
                                res.data.splice(index, 1);
                                index = (parseInt(index) - 1).toString();
                            }
                            else {
                                item.listChuaXem.push(res.data[index]);
                                item.chuaXemCount++;
                                this.userChuaXemCount++;
                                this.userShopChuaXemCount++;
                                this.totalChuaXemCount++;
                            }
                        }
                    }
                    else if(type == 2) {
                        for(let index in res.data) {
                            if(res.data[index].nguoiGuiId == res.data[index].shopId) {
                                res.data.splice(index, 1);
                                index = (parseInt(index) - 1).toString();
                            }
                            else {
                                item.listChuaXem.push(res.data[index]);
                                item.chuaXemCount++;
                                this.shopChuaXemCount++;
                                this.totalChuaXemCount++;
                            }
                        }
                    }
                }
            }
        });
    }

    getAllTinNhanUserChuaXem(item: any) {
        let param = {
            userGuiId: item.userNhanId,
            userNhanId: item.userGuiId,
            daXem: false
        }
        this.chatUserService.getAllNhanTinUser(param).subscribe({
            next: (res: any) => {
                if(res.data && res.data.length) {
                    for(let index in res.data) {
                        if(res.data[index].userNhanId == this.loggedUser.userId) {
                            item.listChuaXem.push(res.data[index]);
                            item.chuaXemCount++;
                            this.userChuaXemCount++;
                            this.userUserChuaXemCount++;
                            this.totalChuaXemCount++;
                        }
                    }
                }
            }
        });
    }

    updateNhanTinShop(item: any, type: any) {
        for(let index in item.listChuaXem) {
            item.listChuaXem[index].daXem = true;
            if(parseInt(index) == item.listChuaXem.length - 1) {
                this.chatShopService.updateNhanTinShop(this.loggedUser.userId, item.listChuaXem).subscribe({
                    next: (res: any) => {
                        if(res.statusCode == 200) {
                            if(type == 1) {
                                this.userShopChuaXemCount -= item.chuaXemCount;
                                this.userChuaXemCount -= item.chuaXemCount;
                            }
                            else if(type == 2) {
                                this.shopChuaXemCount -= item.chuaXemCount;
                            }
                            this.totalChuaXemCount -= item.chuaXemCount;
                            item.chuaXemCount = 0;
                            item.listChuaXem = [];
                        }
                    }
                });
            }
        }
    }

    updateNhanTinUser(item: any) {
        for(let index in item.listChuaXem) {
            item.listChuaXem[index].daXem = true;
            if(parseInt(index) == item.listChuaXem.length - 1) {
                this.chatUserService.updateNhanTinUser(this.loggedUser.userId, item.listChuaXem).subscribe({
                    next: (res: any) => {
                        if(res.statusCode == 200) {
                            this.userUserChuaXemCount -= item.chuaXemCount;
                            this.userChuaXemCount -= item.chuaXemCount;
                            this.totalChuaXemCount -= item.chuaXemCount;
                            item.chuaXemCount = 0;
                            item.listChuaXem = [];
                        }
                    }
                });
            }
        }
    }

    checkFileType(element: any) {
        let correctType = true;
        for(let file of element.files) {
            if(["avif", "bmp", "gif", "ico", "jpeg", "jpg", "png", "svg", "webp"].includes(file.name.split(".")[file.name.split(".").length - 1])) {
                file.isImage = true;
                file.isFile = false;
            }
            else if(["avi", "bz", "bz2", "cda", "csv", "doc", "docx", "epub", "gz", "mid", "midi", "mp3", "mp4",
                    "mpeg", "odp", "ods", "odt", "oga", "ogv", "ogx", "opus", "pdf", "ppt", "pptx", "rar", "rtf",
                    "tar", "ts", "txt", "wav", "vsd", "weba", "webm", "xls", "xlsx", "zip", "7z"].includes(file.name.split(".")[file.name.split(".").length - 1])) {
                        file.isImage = false;
                        file.isFile = true;
                    }
            else {
                correctType = false;
            }
            if(!correctType) {
                element.value = "";
                this.popUpService.open(MessageConstants.FILE_TYPE_IS_NOT_SUPPORTED, mType.info);
                break;
            }
            if(Array.from(element.files).indexOf(file) == element.files.length - 1) {
                this.checkFileSize(element);
            }
        }
    }

    checkFileSize(element: any) {
        let correctSize = true;
        for(let file of element.files) {
            if(file.size >= 2147483648) {
                correctSize = false;
            }
            if(!correctSize) {
                element.value = "";
                this.popUpService.open(MessageConstants.TOO_LARGE_FILE_SIZE, mType.info);
                break;
            }
            if(Array.from(element.files).indexOf(file) == element.files.length - 1) {
                this.uploadFile(element);
            }
        }
    }

    uploadFile(element: any) {
        for(let file of element.files) {
            let formData = new FormData();
            let fileName = file.name;
            formData.append('file', file, file.name);
            let item: any = {
                noiDung: "",
                createdAt: this.getTimeNow(),
                sendError: false,
                isFile: file.isFile,
                isImage: file.isImage,
                fileName: fileName,
                userLogId: this.loggedUser.userId
            }
            if(this.tabIndex == 0 && this.collapseTabIndex == 0) {
                item.userId = this.userShopChatField.userId;
                item.shopId = this.userShopChatField.shopId;
                item.nguoiGuiId = this.userShopChatField.userId;
                let once = true;
                let index = -1;
                this.updateNhanTinShop(this.userShopChatField, 1);
                this.userShopViewed = false;
                this.userShopSending = true;
                this.userShopSendError = false;
                this.chatShopService.uploadFile(item, formData).subscribe({
                    next: (res: any) => {
                        if(once) {
                            once = false;
                            let index2 = this.listShopUser.indexOf(this.userShopChatField);
                            if(index2 != -1) {
                                this.listShopUser.unshift(this.listShopUser.splice(index2, 1)[0]);
                            }
                            this.userShopListNhanTin.push(item);
                            index = this.userShopListNhanTin.indexOf(item);
                            if (index != -1 && this.userShopListNhanTin[index].isImage) {
                                this.displayImageFromFile(file, this.userShopListNhanTin[index]);
                            }
                        }
                        setTimeout(() => {
                            document.getElementById("userShopCheck")!.scrollIntoView();
                        }, 1);
                        if(res.type === HttpEventType.UploadProgress && index != -1) {
                            this.userShopListNhanTin[index].progress = Math.round(100 * res.loaded / res.total);
                        }
                        else if (res.type === HttpEventType.Response) {
                            if(res.body && res.body.statusCode == 200) {
                                this.userShopMessageCount++;
                                this.userShopSending = false;
                                this.userShopSendError = false;
                                if (index != -1 && this.userShopListNhanTin[index].isImage && res.body.noiDung) {
                                    this.userShopListNhanTin[index].noiDungHienThi = environment.apiUrl + "/api/" + res.body.noiDung.replaceAll("\\", "/");
                                    this.userShopListNhanTin[index].createdAt = this.getTimeNow();
                                }
                            }
                            else {
                                this.userShopSending = false;
                                this.userShopSendError = true;
                                if(index != -1) {
                                    this.userShopListNhanTin[index].sendError = true;
                                }
                            }
                        }
                    },
                    error: (res: any) => {
                        this.userShopSending = false;
                        this.userShopSendError = true;
                        if(index != -1) {
                            this.userShopListNhanTin[index].sendError = true;
                        }
                    }
                });
            }
            else if(this.tabIndex == 0 && this.collapseTabIndex == 1) {
                item.userGuiId = this.loggedUser.userId;
                item.userNhanId = this.userUserChatField.userNhanId;
                let once = true;
                let index = -1;
                this.updateNhanTinUser(this.userUserChatField);
                this.userUserViewed = false;
                this.userUserSending = true;
                this.userUserSendError = false;
                this.chatUserService.uploadFile(item, formData).subscribe({
                    next: (res: any) => {
                        if(once) {
                            once = false;
                            let index2 = this.listUserUser.indexOf(this.userUserChatField);
                            if(index2 != -1) {
                                this.listUserUser.unshift(this.listUserUser.splice(index2, 1)[0]);
                            }
                            this.userUserListNhanTin.push(item);
                            index = this.userUserListNhanTin.indexOf(item);
                            if (index != -1 && this.userUserListNhanTin[index].isImage) {
                                this.displayImageFromFile(file, this.userUserListNhanTin[index]);
                            }
                        }
                        setTimeout(() => {
                            document.getElementById("userUserCheck")!.scrollIntoView();
                        }, 1);
                        if(res.type === HttpEventType.UploadProgress && index != -1) {
                            this.userUserListNhanTin[index].progress = Math.round(100 * res.loaded / res.total);
                        }
                        else if (res.type === HttpEventType.Response) {
                            if(res.body && res.body.statusCode == 200) {
                                this.userUserMessageCount++;
                                this.userUserSending = false;
                                this.userUserSendError = false;
                                if (index != -1 && this.userUserListNhanTin[index].isImage && res.body.noiDung) {
                                    this.userUserListNhanTin[index].noiDungHienThi = environment.apiUrl + "/api/" + res.body.noiDung.replaceAll("\\", "/");
                                    this.userUserListNhanTin[index].createdAt = this.getTimeNow();
                                }
                            }
                            else {
                                this.userUserSending = false;
                                this.userUserSendError = true;
                                if(index != -1) {
                                    this.userUserListNhanTin[index].sendError = true;
                                }
                            }
                        }
                    },
                    error: (res: any) => {
                        this.userUserSending = false;
                        this.userUserSendError = true;
                        if(index != -1) {
                            this.userUserListNhanTin[index].sendError = true;
                        }
                    }
                });
            }
            else if(this.tabIndex == 1) {
                item.userId = this.shopChatField.userId;
                item.shopId = this.shopChatField.shopId;
                item.nguoiGuiId = this.shopChatField.shopId;
                let once = true;
                let index = -1;
                this.updateNhanTinShop(this.userShopChatField, 1);
                this.shopViewed = false;
                this.shopSending = true;
                this.shopSendError = false;
                this.chatShopService.uploadFile(item, formData).subscribe({
                    next: (res: any) => {
                        if(once) {
                            once = false;
                            let index2 = this.listUser.indexOf(this.shopChatField);
                            if(index2 != -1) {
                                this.listUser.unshift(this.listUser.splice(index2, 1)[0]);
                            }
                            this.shopListNhanTin.push(item);
                            index = this.shopListNhanTin.indexOf(item);
                            if (index != -1 && this.shopListNhanTin[index].isImage) {
                                this.displayImageFromFile(file, this.shopListNhanTin[index]);
                            }
                        }
                        setTimeout(() => {
                            document.getElementById("shopCheck")!.scrollIntoView();
                        }, 1);
                        if(res.type === HttpEventType.UploadProgress && index != -1) {
                            this.shopListNhanTin[index].progress = Math.round(100 * res.loaded / res.total);
                        }
                        else if (res.type === HttpEventType.Response) {
                            if(res.body && res.body.statusCode == 200) {
                                this.shopMessageCount++;
                                this.shopSending = false;
                                this.shopSendError = false;
                                if (index != -1 && this.shopListNhanTin[index].isImage && res.body.noiDung) {
                                    this.shopListNhanTin[index].noiDungHienThi = environment.apiUrl + "/api/" + res.body.noiDung.replaceAll("\\", "/");
                                    this.shopListNhanTin[index].createdAt = this.getTimeNow();
                                }
                                element.value = "";
                            }
                            else {
                                this.shopSending = false;
                                this.shopSendError = true;
                                if(index != -1) {
                                    this.shopListNhanTin[index].sendError = true;
                                }
                            }
                        }
                    },
                    error: (res: any) => {
                        this.shopSending = false;
                        this.shopSendError = true;
                        if(index != -1) {
                            this.shopListNhanTin[index].sendError = true;
                        }
                    }
                });
            }
            if(Array.from(element.files).indexOf(file) == element.files.length - 1) {
                if(Array.prototype.isPrototypeOf(element.files)) {
                    element.files = [];
                    element.closed = false;
                    element.scrollableLeft = false;
                    element.scrollableRight = false;
                }
                element.value = "";
            }
        }
    }

    downloadFile(fileLink: any, fileName: any) {
        if(fileLink) {
            fileLink = environment.apiUrl + "/api/" + fileLink.replaceAll("\\", "/");
            let link = document.createElement("a");
            link.setAttribute("href", fileLink);
            link.setAttribute("download", `${fileName}`);
            link.setAttribute("target", "_blank");
            document.body.appendChild(link);
            link.click();
            link.remove();
        }
    }

    viewImage(imageSource: string) {
        this.viewImageService.open(imageSource);
    }

    displayImageFromFile(file: any, item: any) {
        let reader = new FileReader();
        reader.onload = (e: any) => {
            item.noiDungHienThi = this.sanitizer.bypassSecurityTrustUrl(e.target.result);
        }
        reader.readAsDataURL(file);
    }

    scrollToRight() {
        if(this.mouse) {
            if(this.tabIndex == 0 && this.collapseTabIndex == 0) {
                this.userShopImageField.nativeElement.scrollLeft += 1;
                this.userShopChatField.imagePreUpload.scrollLeft = this.userShopImageField.nativeElement.scrollLeft;
                this.userShopChatField.imagePreUpload.scrollableLeft = true;
                if(this.userShopImageField.nativeElement.scrollLeft + this.userShopImageField.nativeElement.clientWidth < this.userShopImageField.nativeElement.scrollWidth) {
                    this.userShopChatField.imagePreUpload.scrollableRight = true;
                    setTimeout(() => {
                        this.scrollToRight();
                    }, 1);
                }
                else {
                    this.userShopChatField.imagePreUpload.scrollableRight = false;
                }
            }
            else if(this.tabIndex == 0 && this.collapseTabIndex == 1) {
                this.userUserImageField.nativeElement.scrollLeft += 1;
                this.userUserChatField.imagePreUpload.scrollLeft = this.userUserImageField.nativeElement.scrollLeft;
                this.userUserChatField.imagePreUpload.scrollableLeft = true;
                if(this.userUserImageField.nativeElement.scrollLeft + this.userUserImageField.nativeElement.clientWidth < this.userUserImageField.nativeElement.scrollWidth) {
                    this.userUserChatField.imagePreUpload.scrollableRight = true;
                    setTimeout(() => {
                        this.scrollToRight();
                    }, 1);
                }
                else {
                    this.userUserChatField.imagePreUpload.scrollableRight = false;
                }
            }
            else if(this.tabIndex == 1) {
                this.shopImageField.nativeElement.scrollLeft += 1;
                this.shopChatField.imagePreUpload.scrollLeft = this.shopImageField.nativeElement.scrollLeft;
                this.shopChatField.imagePreUpload.scrollableLeft = true;
                if(this.shopImageField.nativeElement.scrollLeft + this.shopImageField.nativeElement.clientWidth < this.shopImageField.nativeElement.scrollWidth) {
                    this.shopChatField.imagePreUpload.scrollableRight = true;
                    setTimeout(() => {
                        this.scrollToRight();
                    }, 1);
                }
                else {
                    this.shopChatField.imagePreUpload.scrollableRight = false;
                }
            }
        }
        else {
            return;
        }
    }

    scrollToLeft() {
        if(this.mouse) {
            if(this.tabIndex == 0 && this.collapseTabIndex == 0) {
                this.userShopImageField.nativeElement.scrollLeft -= 1;
                this.userShopChatField.imagePreUpload.scrollLeft = this.userShopImageField.nativeElement.scrollLeft;
                this.userShopChatField.imagePreUpload.scrollableRight = true;
                if(this.userShopImageField.nativeElement.scrollLeft > 0) {
                    this.userShopChatField.imagePreUpload.scrollableLeft = true;
                    this.checkScrollable();
                    setTimeout(() => {
                        this.scrollToLeft();
                    }, 1);
                }
                else {
                    this.userShopChatField.imagePreUpload.scrollableLeft = false;
                }
            }
            else if(this.tabIndex == 0 && this.collapseTabIndex == 1) {
                this.userUserImageField.nativeElement.scrollLeft -= 1;
                this.userUserChatField.imagePreUpload.scrollLeft = this.userUserImageField.nativeElement.scrollLeft;
                this.userUserChatField.imagePreUpload.scrollableRight = true;
                if(this.userUserImageField.nativeElement.scrollLeft > 0) {
                    this.userUserChatField.imagePreUpload.scrollableLeft = true;
                    this.checkScrollable();
                    setTimeout(() => {
                        this.scrollToLeft();
                    }, 1);
                }
                else {
                    this.userUserChatField.imagePreUpload.scrollableLeft = false;
                }
            }
            else if(this.tabIndex == 1) {
                this.shopImageField.nativeElement.scrollLeft -= 1;
                this.shopChatField.imagePreUpload.scrollableRight = true;
                this.shopChatField.imagePreUpload.scrollLeft = this.shopImageField.nativeElement.scrollLeft;
                if(this.shopImageField.nativeElement.scrollLeft > 0) {
                    this.shopChatField.imagePreUpload.scrollableLeft = true;
                    this.checkScrollable();
                    setTimeout(() => {
                        this.scrollToLeft();
                    }, 1);
                }
                else {
                    this.shopChatField.imagePreUpload.scrollableLeft = false;
                }
            }
        }
        else {
            return;
        }
    }

    setScrollLeft(pixel: any) {
        if(this.tabIndex == 0 && this.collapseTabIndex == 0) {
            this.userShopImageField.nativeElement.scrollLeft = pixel;
        }
        else if(this.tabIndex == 0 && this.collapseTabIndex == 1) {
            this.userUserImageField.nativeElement.scrollLeft = pixel;
        }
        else if(this.tabIndex == 1) {
            this.shopImageField.nativeElement.scrollLeft = pixel;
        }
    }

    pasteImage(event: any) {
        let item = event.clipboardData.files[0];
        if(item && item.type.startsWith("image/")) {
            item.isFile = false;
            item.isImage = true;
            this.displayImageFromFile(item, item);
            if(this.tabIndex == 0 && this.collapseTabIndex == 0) {
                this.userShopChatField.imagePreUpload.files.push(item);
                this.userShopChatField.imagePreUpload.closed = false;
                setTimeout(() => {
                    this.userShopImageField.nativeElement.scrollTo({
                        left: this.userShopImageField.nativeElement.scrollWidth,
                        behavior: "smooth"
                    });
                    if(this.userShopImageField.nativeElement.clientWidth < this.userShopImageField.nativeElement.scrollWidth) {
                        this.userShopChatField.imagePreUpload.scrollableRight = false;
                        this.userShopChatField.imagePreUpload.scrollableLeft = true;
                    }
                    this.userShopChatField.imagePreUpload.scrollLeft = this.userShopImageField.nativeElement.scrollWidth;
                }, 10);
            }
            else if(this.tabIndex == 0 && this.collapseTabIndex == 1) {
                this.userUserChatField.imagePreUpload.files.push(item);
                this.userUserChatField.imagePreUpload.closed = false;
                setTimeout(() => {
                    this.userUserImageField.nativeElement.scrollTo({
                        left: this.userUserImageField.nativeElement.scrollWidth,
                        behavior: "smooth"
                    });
                    if(this.userUserImageField.nativeElement.clientWidth < this.userUserImageField.nativeElement.scrollWidth) {
                        this.userUserChatField.imagePreUpload.scrollableRight = false;
                        this.userUserChatField.imagePreUpload.scrollableLeft = true;
                    }
                    this.userUserChatField.imagePreUpload.scrollLeft = this.userUserImageField.nativeElement.scrollWidth;
                }, 10);
            }
            else if(this.tabIndex == 1) {
                this.shopChatField.imagePreUpload.files.push(item);
                this.shopChatField.imagePreUpload.closed = false;
                setTimeout(() => {
                    this.shopImageField.nativeElement.scrollTo({
                        left: this.shopImageField.nativeElement.scrollWidth,
                        behavior: "smooth"
                    });
                    if(this.shopImageField.nativeElement.clientWidth < this.shopImageField.nativeElement.scrollWidth) {
                        this.shopChatField.imagePreUpload.scrollableRight = false;
                        this.shopChatField.imagePreUpload.scrollableLeft = true;
                    }
                    this.shopChatField.imagePreUpload.scrollLeft = this.shopImageField.nativeElement.scrollWidth;
                }, 10);
            }
        }
    }

    removeImagePreUpload(index: any) {
        if(this.tabIndex == 0 && this.collapseTabIndex == 0) {
            this.userShopChatField.imagePreUpload.files.splice(index, 1);
        }
        else if(this.tabIndex == 0 && this.collapseTabIndex == 1) {
            this.userUserChatField.imagePreUpload.files.splice(index, 1);
        }
        else if(this.tabIndex == 1) {
            this.shopChatField.imagePreUpload.files.splice(index, 1);
        }
        this.checkScrollable();
    }

    checkScrollable() {
        setTimeout(() => {
            if(this.tabIndex == 0 && this.collapseTabIndex == 0) {
                if(this.userShopImageField.nativeElement.scrollLeft + this.userShopImageField.nativeElement.clientWidth < this.userShopImageField.nativeElement.scrollWidth) {
                    this.userShopChatField.imagePreUpload.scrollableRight = true;
                }
                else {
                    this.userShopChatField.imagePreUpload.scrollableRight = false;
                }
                if(this.userShopImageField.nativeElement.scrollLeft > 0) {
                    this.userShopChatField.imagePreUpload.scrollableLeft = true;
                }
                else {
                    this.userShopChatField.imagePreUpload.scrollableLeft = false;
                }
            }
            else if(this.tabIndex == 0 && this.collapseTabIndex == 1) {
                if(this.userUserImageField.nativeElement.scrollLeft + this.userUserImageField.nativeElement.clientWidth < this.userUserImageField.nativeElement.scrollWidth) {
                    this.userUserChatField.imagePreUpload.scrollableRight = true;
                }
                else {
                    this.userUserChatField.imagePreUpload.scrollableRight = false;
                }
                if(this.userUserImageField.nativeElement.scrollLeft > 0) {
                    this.userUserChatField.imagePreUpload.scrollableLeft = true;
                }
                else {
                    this.userUserChatField.imagePreUpload.scrollableLeft = false;
                }
            }
            else if(this.tabIndex == 1) {
                if(this.shopImageField.nativeElement.scrollLeft + this.shopImageField.nativeElement.clientWidth < this.shopImageField.nativeElement.scrollWidth) {
                    this.shopChatField.imagePreUpload.scrollableRight = true;
                }
                else {
                    this.shopChatField.imagePreUpload.scrollableRight = false;
                }
                if(this.shopImageField.nativeElement.scrollLeft > 0) {
                    this.shopChatField.imagePreUpload.scrollableLeft = true;
                }
                else {
                    this.shopChatField.imagePreUpload.scrollableLeft = false;
                }
            }
        }, 10);
    }

    openDialogBaoCao(userId: any, shopId: any, name: any) {
        let item: any = {
            nguoiBaoCaoId: this.loggedUser.userId,
            userId: userId,
            shopId: shopId,
            lyDoBaoCao: null,
            noiGuiBaoCao: null
        }
        let dialogRef = this.dialog.open(ReportComponent, {
            width: "48rem",
            data: {
                name: name,
                item: item
            }
        });
        dialogRef.afterClosed().subscribe((res: any) => {
            if(res) {
                this.chatShopService.createBaoCao(this.loggedUser.userId, item).subscribe({
                    next: (ress: any) => {
                        if(ress.statusCode == 200) {

                        }
                        else {
                            this.popUpService.open(MessageConstants.SYSTEM_ERROR, mType.error);
                        }
                    },
                    error: (ress: any) => {
                        this.popUpService.open(MessageConstants.SYSTEM_ERROR, mType.error);
                    }
                });
            }
        });
    }
}
