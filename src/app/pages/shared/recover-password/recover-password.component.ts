import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MessageConstants } from 'src/app/shared/constants/message.constants';
import { SendMailConstants } from 'src/app/shared/constants/send-mail.constants';
import { LogService } from 'src/app/shared/services/main/log.service';

@Component({
    selector: 'app-recover-password',
    templateUrl: './recover-password.component.html',
    styleUrls: ['./recover-password.component.scss']
})
export class RecoverPasswordComponent implements OnInit {
    formControl: FormGroup;
    sent: any;
    error: any;
    spinner: any = false;

    constructor(private formBuilder: FormBuilder, private dialogRef: MatDialogRef<RecoverPasswordComponent>, private logService: LogService,
        @Inject(MAT_DIALOG_DATA) public data: any) { }

    ngOnInit(): void {
        this.formControl = this.formBuilder.group({
            email: ['', [Validators.required, Validators.pattern("^[a-zA-Z]([.]?[a-zA-Z0-9])*@[a-zA-Z0-9]{2,}(.[a-zA-Z0-9]{2,})+$")]]
        });
    }

    getRecoverPassword() {
        this.formControl.markAllAsTouched();
        if(this.formControl.invalid) {
            return;
        }
        else {
            this.spinner = true;
            let item: any = {
                email: this.data.item.email,
                urlClient: SendMailConstants.urlForgotPassword
            }
            this.logService.getRecoverPassword(item).subscribe({
                next: (res: any) => {
                    if(res.statusCode == 200) {
                        this.sent = MessageConstants.SENT_RECOVER_PASSWORD_EMAIL;
                        this.error = null;
                        this.spinner = false;
                    }
                    else if(res.statusCode == 404) {
                        this.sent = null;
                        this.error = MessageConstants.NOT_EXISTED_EMAIL;
                        this.spinner = false;
                    }
                    else {
                        this.sent = null;
                        this.error = MessageConstants.SYSTEM_ERROR;
                        this.spinner = false;
                    }
                },
                error: (res: any) => {
                    this.sent = null;
                    this.error = MessageConstants.SYSTEM_ERROR;
                    this.spinner = false;
                }
            });
        }
    }

    openDialogLogin() {
        this.dialogRef.close("login");
    }

    get f() {
        return this.formControl.controls;
    }

    keyCapture(event: any) {
        let e = <KeyboardEvent>event;
        if(e.keyCode === 13) {
            e.preventDefault();
            this.getRecoverPassword();
        }
    }
}
