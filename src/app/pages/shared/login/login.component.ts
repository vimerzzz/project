import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MessageConstants } from 'src/app/shared/constants/message.constants';
import { mType, PopUpService } from 'src/app/shared/services/base/pop-up.service';
import { LogService } from 'src/app/shared/services/main/log.service';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
    formControl: FormGroup;
    error: any;
    spinner: any = false;
    passwordType: "password" | "text";

    constructor(private formBuilder: FormBuilder, private dialogRef: MatDialogRef<LoginComponent>, private logService: LogService, private popUpService: PopUpService,
        @Inject(MAT_DIALOG_DATA) public data: any) {}

    ngOnInit(): void {
        this.formControl = this.formBuilder.group({
            userName: ["", [Validators.required, Validators.pattern("^[a-zA-Z0-9-_]+$"), Validators.minLength(5)]],
            password: ["", [Validators.required, Validators.pattern("^[a-zA-Z0-9-_]+$"), Validators.minLength(5)]]
        });
        this.passwordType = "password";
    }

    openDialogSignup() {
        this.dialogRef.close("signup");
    }

    openDialogRecoverPassword() {
        this.dialogRef.close("recoverPassword");
    }

    checkUserName() {
        this.formControl.markAllAsTouched();
        if(this.formControl.invalid) {
            return;
        }
        else {
            this.spinner = true;
            this.logService.checkExistedUserName(this.data.item).subscribe({
                next: (res: any) => {
                    if(res.statusCode == 200) {
                        this.checkPassword();
                    }
                    else if(res.statusCode == 404) {
                        this.error = MessageConstants.NOT_EXISTED_USERNAME;
                        this.spinner = false;
                        this.popUpService.open(MessageConstants.LOGIN_FAILED, mType.error);
                    }
                    else if(res.statusCode == 406) {
                        this.error = MessageConstants.ACCOUNT_WAS_BANNED;
                        this.spinner = false;
                        this.popUpService.open(MessageConstants.LOGIN_FAILED, mType.error);
                    }
                    else {
                        this.error = MessageConstants.SYSTEM_ERROR;
                        this.spinner = false;
                        this.popUpService.open(MessageConstants.LOGIN_FAILED, mType.error);
                    }
                },
                error: (res: any) => {
                    this.error = MessageConstants.SYSTEM_ERROR;
                    this.spinner = false;
                    this.popUpService.open(MessageConstants.LOGIN_FAILED, mType.error);
                }
            });
        }
    }

    checkPassword() {
        this.logService.checkPassword(this.data.item).subscribe({
            next: (res: any) => {
                if(res.statusCode == 200) {
                    this.close();
                }
                else if(res.statusCode == 401) {
                    this.error = MessageConstants.WRONG_PASSWORD;
                    this.spinner = false;
                    this.popUpService.open(MessageConstants.LOGIN_FAILED, mType.error);
                }
                else {
                    this.error = MessageConstants.SYSTEM_ERROR;
                    this.spinner = false;
                    this.popUpService.open(MessageConstants.LOGIN_FAILED, mType.error);
                }
            },
            error: (res: any) => {
                this.error = MessageConstants.SYSTEM_ERROR;
                this.spinner = false;
                this.popUpService.open(MessageConstants.LOGIN_FAILED, mType.error);
            }
        });
    }

    close() {
        this.error = null;
        this.spinner = false;
        this.dialogRef.close(true);
    }

    passwordToggle() {
        if(this.passwordType == "password") {
            this.passwordType = "text";
        }
        else if(this.passwordType == "text") {
            this.passwordType = "password";
        }
    }

    get f() {
        return this.formControl.controls;
    }

    keyCapture(event: any) {
        let e = <KeyboardEvent>event;
        if(e.keyCode === 13) {
            e.preventDefault();
            this.checkUserName();
        }
    }
}
