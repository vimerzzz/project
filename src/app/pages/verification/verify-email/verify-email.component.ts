import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';
import { BaseAuth } from 'src/app/shared/auth/base.auth';
import { MessageConstants } from 'src/app/shared/constants/message.constants';
import { VerifyEmailService } from 'src/app/shared/services/main/verification/verify-email.service';

@Component({
    selector: 'app-verify-email',
    templateUrl: './verify-email.component.html',
    styleUrls: ['./verify-email.component.scss']
})
export class VerifyEmailComponent implements OnInit {
    loaded: Promise<boolean>;
    result: any;

    constructor(private titleService: Title, private base: BaseAuth, private verifyEmailService: VerifyEmailService, private router: ActivatedRoute) { }

    ngOnInit(): void {
        this.titleService.setTitle("Xác thực email");
        this.base.checkExpire();
        this.verifyEmail();
    }

    verifyEmail() {
        let item: any = {};
        this.router.queryParams.subscribe((params: any) => {
            if(params) {
                item.email = encodeURI(params.email);
                item.token = encodeURI(params.token);

                this.verifyEmailService.verifyEmail(item).subscribe({
                    next: (res: any) => {
                        if(res.statusCode == 200) {
                            this.result = MessageConstants.VERIFY_EMAIL_SUCCESS;
                            this.loaded = Promise.resolve(true);
                        }
                        else {
                            this.result = MessageConstants.VERIFY_EMAIL_FAILED;
                            this.loaded = Promise.resolve(true);
                        }
                    },
                    error: (res: any) => {
                        this.result = MessageConstants.VERIFY_EMAIL_FAILED;
                        this.loaded = Promise.resolve(true);
                    }
                });
            }
        });
    }
}
