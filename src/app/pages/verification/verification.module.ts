import { NgModule } from '@angular/core';
import { MaterialModule } from 'src/app/material.module';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { VerificationRoutingModule } from './verification-routing.module';
import { VerifyEmailComponent } from './verify-email/verify-email.component';

@NgModule({
    declarations: [
        VerifyEmailComponent,
        ForgotPasswordComponent
    ],
    imports: [
        MaterialModule,
        VerificationRoutingModule
    ]
})
export class VerificationModule { }
