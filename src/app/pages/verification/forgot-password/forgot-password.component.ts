import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';
import { BaseAuth } from 'src/app/shared/auth/base.auth';
import { MessageConstants } from 'src/app/shared/constants/message.constants';
import { RecoverPasswordService } from 'src/app/shared/services/main/verification/recover-password.service';

@Component({
    selector: 'app-forgot-password',
    templateUrl: './forgot-password.component.html',
    styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent implements OnInit {
    formControl: FormGroup;
    passwordItem: any;
    result: any;
    passwordType: "password" | "text";
    rePasswordType: "password" | "text";
    spinner: any = false;
    @ViewChild("password", {read: ElementRef}) password: ElementRef;
    @ViewChild("rePassword", {read: ElementRef}) rePassword: ElementRef;

    constructor(private titleService: Title, private base: BaseAuth, private formBuilder: FormBuilder, private recoverPasswordService: RecoverPasswordService, private router: ActivatedRoute) { }

    ngOnInit(): void {
        this.titleService.setTitle("Lấy lại mật khẩu");
        this.base.checkExpire();
        this.formControl = this.formBuilder.group({
            password: ['', [Validators.required, Validators.pattern("^[a-zA-Z0-9-_]+$"), Validators.minLength(5)]],
            rePassword: ['', [Validators.required, Validators.pattern("^[a-zA-Z0-9-_]+$"), Validators.minLength(5)]],
        });
        this.passwordType = "password";
        this.rePasswordType = "password";
    }

    unMatchedPassword(control: AbstractControl): {[key: string]: any} {
        return {"notMatched": true};
    }

    checkPassword() {
        if(this.rePassword.nativeElement.value == "") {
            this.f.rePassword.setValidators([Validators.required, Validators.pattern("^[a-zA-Z0-9-_]+$"), Validators.minLength(5)]);
            if(this.f.rePassword.dirty) {
                this.f.rePassword.reset();
            }
        }
        else {
            if(this.password.nativeElement.value == this.rePassword.nativeElement.value) {
                this.f.rePassword.clearValidators();
                let tmp = this.rePassword.nativeElement.value;
                this.f.rePassword.reset();
                this.f.rePassword.setValidators([Validators.pattern("^[a-zA-Z0-9-_]+$"), Validators.minLength(5)]);
                this.f.rePassword.setValue(tmp);
            }
            else {
                this.f.rePassword.setValidators([this.unMatchedPassword, Validators.pattern("^[a-zA-Z0-9-_]+$"), Validators.minLength(5)]);
                let tmp = this.rePassword.nativeElement.value;
                this.f.rePassword.reset();
                this.f.rePassword.setValue(tmp);
            }
        }
    }

    get f() {
        return this.formControl.controls;
    }
    recoverPassword() {
        this.formControl.markAllAsTouched();

        if (this.formControl.invalid) {
            return;
        }
        else {
            this.spinner = true;
            let item: any = {
                password: this.passwordItem
            }
            let params = this.router.queryParams.subscribe((params: any) => {
                if(params) {
                    item.email = encodeURI(params.email);
                    item.token = encodeURI(params.token);
                    this.recoverPasswordService.recoverPassword(item).subscribe({
                        next: (res: any) => {
                            if(res.statusCode == 200) {
                                this.result = MessageConstants.RECOVER_PASSWORD_SUCCESS;
                                this.spinner = false;
                            }
                            else {
                                this.result = MessageConstants.RECOVER_PASSWORD_FAILED;
                                this.spinner = false;
                            }
                        },
                        error: (res: any) => {
                            this.result = MessageConstants.RECOVER_PASSWORD_FAILED;
                            this.spinner = false;
                        }
                    });
                }
            });
        }
    }

    passwordToggle() {
        if(this.passwordType == "password") {
            this.passwordType = "text";
        }
        else if(this.passwordType == "text") {
            this.passwordType = "password";
        }
    }

    rePasswordToggle() {
        if(this.rePasswordType == "password") {
            this.rePasswordType = "text";
        }
        else if(this.rePasswordType == "text") {
            this.rePasswordType = "password";
        }
    }
}
