import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { BaseAuth } from 'src/app/shared/auth/base.auth';
import { NavigationService } from 'src/app/shared/services/base/navigation.service';

@Component({
    selector: 'app-forbidden',
    templateUrl: './forbidden.component.html',
    styleUrls: ['./forbidden.component.scss']
})
export class ForbiddenComponent implements OnInit {

    constructor(private titleService: Title, private base: BaseAuth, private navigationService: NavigationService) { }

    ngOnInit() {
        this.titleService.setTitle("Error");
        this.base.checkExpire();
    }

    back() {
        this.navigationService.back();
    }
}
