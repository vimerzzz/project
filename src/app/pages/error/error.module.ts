import { NgModule } from '@angular/core';
import { ErrorRoutingModule } from './error-routing.module';
import { NotFoundComponent } from './not-found/not-found.component';
import { MaterialModule } from 'src/app/material.module';
import { UnauthorizedComponent } from './unauthorized/unauthorized.component';
import { ForbiddenComponent } from './forbidden/forbidden.component';


@NgModule({
    declarations: [
        NotFoundComponent,
        UnauthorizedComponent,
        ForbiddenComponent
    ],
    imports: [
        ErrorRoutingModule,
        MaterialModule
    ]
})
export class ErrorModule { }
