import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ForbiddenComponent } from './forbidden/forbidden.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { UnauthorizedComponent } from './unauthorized/unauthorized.component';

const routes: Routes = [
    {
        path: '',
        children: [
            {
                path: "not-found",
                component: NotFoundComponent
            },
            {
                path: "unauthorized",
                component: UnauthorizedComponent
            },
            {
                path: "forbidden",
                component: ForbiddenComponent
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ErrorRoutingModule { }
