import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Title } from '@angular/platform-browser';
import { BaseAuth } from 'src/app/shared/auth/base.auth';
import { MessageConstants } from 'src/app/shared/constants/message.constants';
import { NavigationService } from 'src/app/shared/services/base/navigation.service';
import { mType, PopUpService } from 'src/app/shared/services/base/pop-up.service';
import { LoginComponent } from '../../shared/login/login.component';
import { HeaderService } from 'src/app/shared/services/main/header.service';
import { SignupComponent } from '../../shared/signup/signup.component';
import { RecoverPasswordComponent } from '../../shared/recover-password/recover-password.component';

@Component({
    selector: 'app-unauthorized',
    templateUrl: './unauthorized.component.html',
    styleUrls: ['./unauthorized.component.scss']
})
export class UnauthorizedComponent implements OnInit {
    constructor(private dialog: MatDialog, private popUpService: PopUpService, private headerService: HeaderService, private titleService: Title, private base: BaseAuth, private navigationService: NavigationService) { }

    ngOnInit() {
        this.titleService.setTitle("Error");
        this.base.checkExpire();
        this.checkLoggedUser();
    }

    checkLoggedUser() {
        let object = localStorage.getItem("loggedUser");
        if(object) {
            this.navigationService.back();
        }
    }

    openDialogLogin() {
        let item: any = {
            userName: null,
            password: null
        };
        let dialogRef = this.dialog.open(LoginComponent, {
            width: "32rem",
            data: {
                item
            }
        });
        dialogRef.afterClosed().subscribe((resp: any) => {
            if (resp == "signup") {
                this.openDialogSignup();
            }
            else if (resp == "recoverPassword") {
                this.openDialogRecoverPassword();
            }
            else if(resp == true) {
                this.headerService.login(item).subscribe({
                    next: (res: any) => {
                        if(res.statusCode == 200) {
                            this.popUpService.open(MessageConstants.LOGIN_SUCCESS, mType.success);
                            this.saveUser(res);
                        }
                        else {
                            this.popUpService.open(MessageConstants.LOGIN_FAILED, mType.error);
                        }
                    },
                    error: (res: any) => {
                        this.popUpService.open(MessageConstants.LOGIN_FAILED, mType.error);
                    }
                });
            }
        });
    }

    openDialogSignup() {
        let item = {
            userName: null,
            email: null,
            password: null,
            rePassword: null
        }
        let dialogRef = this.dialog.open(SignupComponent, {
            width: "32rem",
            data: {
                item
            }
        });
        dialogRef.afterClosed().subscribe((resp: any) => {
            if (resp == "login") {
                this.openDialogLogin();
            }
            else if (resp == true) {
                this.headerService.createUser(item).subscribe({
                    next: (res: any) => {
                        if (res.statusCode == 200) {
                            this.headerService.login(item).subscribe({
                                next: (ress: any) => {
                                    if (ress.statusCode == 200) {
                                        this.popUpService.open(MessageConstants.SIGNUP_SUCCESS, mType.success);
                                        this.saveUser(ress);
                                    }
                                    else {
                                        this.popUpService.open(MessageConstants.SIGNUP_FAILED, mType.error);
                                    }
                                },
                                error: (ress: any) => {
                                    this.popUpService.open(MessageConstants.SIGNUP_FAILED, mType.error);
                                }
                            });
                        }
                        else {
                            this.popUpService.open(MessageConstants.SIGNUP_FAILED, mType.error);
                        }
                    },
                    error: (res: any) => {
                        this.popUpService.open(MessageConstants.SIGNUP_FAILED, mType.error);
                    }
                });
            }
        });
    }

    openDialogRecoverPassword() {
        let item = {
            email: null
        }
        let dialogRef = this.dialog.open(RecoverPasswordComponent, {
            width: "32rem",
            data: {
                item
            }
        });
        dialogRef.afterClosed().subscribe((resp: any) => {
            if (resp == "login") {
                this.openDialogLogin();
            }
        });
    }

    saveUser(res: any) {
        let user: any = {
            userId: res.userId,
            roleId: res.roleId,
            token: res.token,
            timeExpire: new Date().getTime() + 15*24*60*60*1000
        };
        localStorage.setItem("loggedUser", JSON.stringify(user));
        this.base.setLoggedUser(true);
        this.navigationService.back();
    }
}
