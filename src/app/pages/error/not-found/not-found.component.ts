import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { BaseAuth } from 'src/app/shared/auth/base.auth';

@Component({
    selector: 'app-not-found',
    templateUrl: './not-found.component.html',
    styleUrls: ['./not-found.component.scss']
})
export class NotFoundComponent implements OnInit {

    constructor(private titleService: Title, private base: BaseAuth) { }

    ngOnInit() {
        this.titleService.setTitle("Error");
        this.base.checkExpire();
    }
}
