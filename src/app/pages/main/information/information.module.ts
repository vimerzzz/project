import { NgModule } from '@angular/core';
import { MaterialModule } from 'src/app/material.module';
import { DirectivesModule } from 'src/app/shared/directives/directives.module';
import { AddressComponent } from './address/address.component';
import { DialogAddAddressComponent } from './address/dialog-add-address/dialog-add-address.component';
import { FollowComponent } from './follow/follow.component';
import { InformationRoutingModule } from './information-routing.module';
import { InformationSidenavComponent } from './information-sidenav/information-sidenav.component';
import { MyInformationComponent } from './my-information/my-information.component';
import { NotificationComponent } from './notification/notification.component';
import { OrderComponent } from './order/order.component';
import { ViewCouponComponent } from './order/view-detail/view-coupon/view-coupon.component';
import { ViewDetailComponent } from './order/view-detail/view-detail.component';
import { PaymentComponent } from './payment/payment.component';
import { SettingComponent } from './setting/setting.component';

@NgModule({
    declarations: [
        MyInformationComponent,
        InformationSidenavComponent,
        SettingComponent,
        FollowComponent,
        PaymentComponent,
        AddressComponent,
        DialogAddAddressComponent,
        NotificationComponent,
        OrderComponent,
        ViewDetailComponent,
        ViewCouponComponent
    ],
    imports: [
        InformationRoutingModule,
        MaterialModule,
        DirectivesModule
    ]
})
export class InformationModule { }
