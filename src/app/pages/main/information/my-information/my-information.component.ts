import { formatDate } from '@angular/common';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { DomSanitizer, Title } from '@angular/platform-browser';
import { Observable } from 'rxjs';
import { BaseAuth } from 'src/app/shared/auth/base.auth';
import { MessageConstants } from 'src/app/shared/constants/message.constants';
import { ImageCropService } from 'src/app/shared/services/base/image-crop.service';
import { mType, PopUpService } from 'src/app/shared/services/base/pop-up.service';
import { MyInformationService } from 'src/app/shared/services/main/information/my-information.service';
import { ResetAvatar } from 'src/app/shared/utilities/reset-status.utility';
import { environment } from 'src/environments/environment';

@Component({
    selector: 'app-my-information',
    templateUrl: './my-information.component.html',
    styleUrls: ['./my-information.component.scss']
})
export class MyInformationComponent implements OnInit {
    loaded: Promise<boolean>;
    formControl: FormGroup;
    loggedUser: any;
    spinner: any = false;
    userInfo: any;
    dayNow: any;
    monthNow: any;
    yearNow: any;
    maxDate: any;
    avatarImage: any;
    correctType: any = true;
    @ViewChild("avatarUpload") avatarUpload: ElementRef;

    constructor(private titleService: Title, private base: BaseAuth, private formBuilder: FormBuilder, private popUpService: PopUpService, private myInfomationService: MyInformationService, private resetAvatar: ResetAvatar, private imageCropService: ImageCropService, private sanitizer: DomSanitizer) { }

    ngOnInit(): void {
        this.titleService.setTitle("Thông tin cá nhân");
        this.base.checkExpire();
        this.formControl = this.formBuilder.group({
            firstName: [""],
            middleName: [""],
            lastName: [""],
            gender: [""],
            phoneNumber: [""],
            birthday: [""]
        });
        this.getDateNow();
        this.getLoggedUser();
    }

    getDateNow() {
        let now = new Date();
        this.dayNow = now.getDate();
        this.monthNow = now.getMonth() + 1;
        this.yearNow = now.getFullYear();
        if (this.dayNow < 10) {
            if (this.monthNow < 10) {
                this.maxDate = this.yearNow + "-" + "0" + this.monthNow + "-" + "0" + this.dayNow;
            }
            else {
                this.maxDate = this.yearNow + "-" + this.monthNow + "-" + "0" + this.dayNow;
            }
        }
        else {
            if (this.monthNow < 10) {
                this.maxDate = this.yearNow + "-" + "0" + this.monthNow + "-" + this.dayNow;
            }
            else {
                this.maxDate = this.yearNow + "-" + this.monthNow + "-" + this.dayNow;
            }
        }
    }

    getLoggedUser() {
        let object = localStorage.getItem("loggedUser");
        if (object) {
            this.loggedUser = JSON.parse(object);
            this.getUserInfoByUserId(this.loggedUser.userId);
        }
    }

    getUserInfoByUserId(id: any) {
        this.myInfomationService.getUserInfoByUserId(id).subscribe({
            next: (res: any) => {
                if (res) {
                    this.userInfo = res;
                    this.avatarImage = this.userInfo.anhDaiDien;
                    this.resetAvatar.setAvatarUrl(this.userInfo.anhDaiDien);
                    if (this.avatarImage != null) {
                        this.avatarImage = environment.apiUrl + "/api/" + this.avatarImage.replaceAll("\\", "/");
                    }
                    this.loaded = Promise.resolve(true);
                }
            }
        });
    }

    updateUserInfo() {
        this.formControl.markAllAsTouched();
        if (this.formControl.invalid) {
            return;
        }
        else {
            this.spinner = true;
            this.userInfo.ngaySinh = formatDate(this.userInfo.ngaySinh, "yyyy-MM-dd", "en-GB");
            this.myInfomationService.updateUserInfo(this.userInfo).subscribe({
                next: (res: any) => {
                    if (res.statusCode == 200) {
                        this.uploadAnhDaiDien();
                    }
                    else {
                        this.popUpService.open(MessageConstants.UPDATE_FAILED, mType.error);
                        this.spinner = false;
                    }
                },
                error: (res: any) => {
                    this.popUpService.open(MessageConstants.UPDATE_FAILED, mType.error);
                    this.spinner = false;
                }
            });
        }
    }

    uploadAnhDaiDien() {
        let file: File = this.avatarUpload.nativeElement.files[0];
        if (file && this.correctType) {
            let formData = new FormData();
            formData.append('file', file, file.name);
            this.myInfomationService.uploadAnhDaiDien(this.userInfo.userId, formData).subscribe({
                next: (res: any) => {
                    if (res.statusCode == 200) {
                        this.popUpService.open(MessageConstants.UPDATE_SUCCESS, mType.success);
                        this.spinner = false;
                        this.getLoggedUser();
                    }
                    else {
                        this.popUpService.open(MessageConstants.UPDATE_FAILED, mType.error);
                        this.spinner = false;
                    }
                },
                error: (res: any) => {
                    this.popUpService.open(MessageConstants.UPDATE_FAILED, mType.error);
                    this.spinner = false;
                }
            });
        }
        else {
            this.popUpService.open(MessageConstants.UPDATE_SUCCESS, mType.success);
            this.spinner = false;
            this.getLoggedUser();
        }
    }

    checkFileType() {
        if(this.avatarUpload.nativeElement.files[0].type != "image/png" &&
            this.avatarUpload.nativeElement.files[0].type != "image/jpeg" &&
            this.avatarUpload.nativeElement.files[0].type != "image/webp") {
                this.popUpService.open(MessageConstants.WRONG_FILE_TYPE, mType.warn);
                this.correctType = false;
            }
        else {
            this.correctType = true;
            this.openImageCrop();
        }
    }

    openImageCrop() {
        let reader = new FileReader();
        let files: string[] = [];
        let names: string[] = [];
        reader.onload = (e: any) => {
            files.push(e.target.result);
            names.push(this.avatarUpload.nativeElement.files[0].name);
            this.imageCropService.open(files, 1, 1, 160, names, 1);
            this.imageCropService.image.subscribe((res: any) => {
                if(res == "") {
                    this.avatarUpload.nativeElement.value = "";
                }
                else {
                    let list = new DataTransfer();
                    for(let item of res) {
                        list.items.add(item);
                    }
                    this.avatarUpload.nativeElement.files = list.files;
                    this.displayImage();
                }
            });
        }
        reader.readAsDataURL(this.avatarUpload.nativeElement.files[0]);
    }


    displayImage() {
        let reader = new FileReader();
        reader.onload = (e: any) => {
            this.avatarImage = this.sanitizer.bypassSecurityTrustUrl(e.target.result);
        }
        reader.readAsDataURL(this.avatarUpload.nativeElement.files[0]);
    }

    get f() {
        return this.formControl.controls;
    }
}
