import { HttpEventType } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Title } from '@angular/platform-browser';
import { BaseAuth } from 'src/app/shared/auth/base.auth';
import { MessageConstants } from 'src/app/shared/constants/message.constants';
import { SendMailConstants } from 'src/app/shared/constants/send-mail.constants';
import { mType, PopUpService } from 'src/app/shared/services/base/pop-up.service';
import { SettingService } from 'src/app/shared/services/main/information/setting.service';

@Component({
    selector: 'app-setting',
    templateUrl: './setting.component.html',
    styleUrls: ['./setting.component.scss']
})
export class SettingComponent implements OnInit {
    loaded: Promise<boolean>;
    user: any;
    loggedUser: any;
    initialEmail: any;
    formControl: FormGroup;
    error: any;
    errorVerify: any;
    informationVerify: any;
    verifySpinner: any;
    updateSpinner: any;
    changePasswordPanel: any = false;
    oldPasswordType: "password" | "text";
    newPasswordType: "password" | "text";
    reNewPasswordType: "password" | "text";

    constructor(private formBuilder: FormBuilder, private titleService: Title, private base: BaseAuth, private settingService: SettingService, private popUpService: PopUpService) { }

    ngOnInit(): void {
        this.titleService.setTitle("Cài đặt tài khoản");
        this.base.checkExpire();
        this.formControl = this.formBuilder.group({
            userName: ['', [Validators.required, Validators.pattern("^[a-zA-Z0-9-_]+$"), Validators.minLength(5)]],
            oldPassword: [''],
            newPassword: [''],
            reNewPassword: [''],
            email: ['', [Validators.required, Validators.pattern("^[a-zA-Z]([.]?[a-zA-Z0-9])*@[a-zA-Z0-9]{2,}(.[a-zA-Z0-9]{2,})+$")]]
        });
        this.getLoggedUser();
        this.oldPasswordType = "password";
        this.newPasswordType = "password";
        this.reNewPasswordType = "password";
    }

    unMatchedPassword(control: AbstractControl): {[key: string]: any} {
        return {"notMatched": true};
    }

    getLoggedUser() {
        let object = localStorage.getItem("loggedUser");
        if(object) {
            this.loggedUser = JSON.parse(object);
            this.getUserById(this.loggedUser.userId);
        }
    }

    getUserById(id: any) {
        this.settingService.getUserById(id).subscribe({
            next: (res: any) => {
                if(res) {
                    this.user = res;
                    this.user.oldPassword = null;
                    this.user.newPassword = null;
                    this.user.reNewPassword = null;
                    this.initialEmail = this.user.email;
                    this.loaded = Promise.resolve(true);
                }
            }
        });
    }

    checkPassword() {
        if(this.user.reNewPassword == "") {
            this.f.reNewPassword.setValidators([Validators.required, Validators.pattern("^[a-zA-Z0-9-_]+$"), Validators.minLength(5)]);
            if(this.f.reNewPassword.dirty) {
                this.f.reNewPassword.reset();
            }
        }
        else {
            if(this.user.newPassword == this.user.reNewPassword) {
                this.f.reNewPassword.clearValidators();
                let tmp = this.user.reNewPassword;
                this.f.reNewPassword.reset();
                this.f.reNewPassword.setValidators([Validators.pattern("^[a-zA-Z0-9-_]+$"), Validators.minLength(5)]);
                this.f.reNewPassword.setValue(tmp);
            }
            else {
                this.f.reNewPassword.setValidators([this.unMatchedPassword, Validators.pattern("^[a-zA-Z0-9-_]+$"), Validators.minLength(5)]);
                let tmp = this.user.reNewPassword;
                this.f.reNewPassword.reset();
                this.f.reNewPassword.setValue(tmp);
            }
        }
    }

    checkUserName() {
        this.formControl.markAllAsTouched();

        if (this.formControl.invalid) {
            return;
        }
        else {
            this.updateSpinner = true;
            this.settingService.checkExistedUserName(this.user).subscribe({
                next: (res: any) => {
                    if(res.statusCode == 200) {
                        this.error = MessageConstants.EXISTED_USERNAME;
                        this.popUpService.open(MessageConstants.UPDATE_FAILED, mType.error);
                        this.updateSpinner = false;
                    }
                    else {
                        this.checkEmail();
                    }
                },
                error: (res: any) => {
                    this.error = MessageConstants.SYSTEM_ERROR;
                    this.popUpService.open(MessageConstants.UPDATE_FAILED, mType.error);
                    this.updateSpinner = false;
                }
            });
        }
    }

    checkEmail() {
        this.settingService.checkExistedEmail(this.user).subscribe({
            next: (res: any) => {
                if(res.statusCode == 200) {
                    this.error = MessageConstants.EXISTED_EMAIL;
                    this.popUpService.open(MessageConstants.UPDATE_FAILED, mType.error);
                    this.updateSpinner = false;
                }
                else {
                    if(this.changePasswordPanel) {
                        this.checkOldPassword();
                    }
                    else {
                        this.updateUser();
                    }
                }
            },
            error: (res: any) => {
                this.error = MessageConstants.SYSTEM_ERROR;
                this.popUpService.open(MessageConstants.UPDATE_FAILED, mType.error);
                this.updateSpinner = false;
            }
        });
    }

    checkOldPassword() {
        this.user.password = this.user.oldPassword;
        this.settingService.checkPassword(this.user).subscribe({
            next: (res: any) => {
                if(res.statusCode == 401) {
                    this.error = MessageConstants.WRONG_PASSWORD;
                    this.popUpService.open(MessageConstants.UPDATE_FAILED, mType.error);
                    this.updateSpinner = false;
                }
                else if(res.statusCode == 200) {
                    this.updateUser();
                }
                else {
                    this.error = MessageConstants.SYSTEM_ERROR;
                    this.popUpService.open(MessageConstants.UPDATE_FAILED, mType.error);
                    this.updateSpinner = false;
                }
            },
            error: (res: any) => {
                this.error = MessageConstants.SYSTEM_ERROR;
                this.popUpService.open(MessageConstants.UPDATE_FAILED, mType.error);
                this.updateSpinner = false;
            }
        });
    }

    changePasswordToggle() {
        if(this.changePasswordPanel) {
            this.changePasswordPanel = false;
            this.f.oldPassword.clearValidators();
            this.f.newPassword.clearValidators();
            this.f.reNewPassword.clearValidators();
            this.f.oldPassword.reset();
            this.f.newPassword.reset();
            this.f.reNewPassword.reset();
        }
        else {
            this.changePasswordPanel = true;
            this.f.oldPassword.setValidators([Validators.required, Validators.pattern("^[a-zA-Z0-9-_]+$"), Validators.minLength(5)]);
            this.f.newPassword.setValidators([Validators.required, Validators.pattern("^[a-zA-Z0-9-_]+$"), Validators.minLength(5)]);
            this.f.reNewPassword.setValidators([Validators.required, Validators.pattern("^[a-zA-Z0-9-_]+$"), Validators.minLength(5)]);
            this.f.oldPassword.reset();
            this.f.newPassword.reset();
            this.f.reNewPassword.reset();
        }
    }

    oldPasswordToggle() {
        if(this.oldPasswordType == "password") {
            this.oldPasswordType = "text";
        }
        else if(this.oldPasswordType == "text") {
            this.oldPasswordType = "password";
        }
    }

    newPasswordToggle() {
        if(this.newPasswordType == "password") {
            this.newPasswordType = "text";
        }
        else if(this.newPasswordType == "text") {
            this.newPasswordType = "password";
        }
    }

    reNewPasswordToggle() {
        if(this.reNewPasswordType == "password") {
            this.reNewPasswordType = "text";
        }
        else if(this.reNewPasswordType == "text") {
            this.reNewPasswordType = "password";
        }
    }

    verifyEmail() {
        this.verifySpinner = true;
        this.user.urlClient = SendMailConstants.urlVerifyEmail;
        this.settingService.getEmailVerification(this.user).subscribe({
            next: (res: any) => {
                if(res.statusCode == 200) {
                    this.errorVerify = null;
                    this.informationVerify = MessageConstants.SENT_EMAIL_VERIFICATION + this.initialEmail;
                    this.verifySpinner = false;
                }
                else if(res.statusCode == 404) {
                    this.errorVerify = MessageConstants.NOT_EXISTED_EMAIL;
                    this.informationVerify = null;
                    this.verifySpinner = false;
                }
                else {
                    this.errorVerify = MessageConstants.SYSTEM_ERROR;
                    this.informationVerify = null;
                    this.verifySpinner = false;
                }
            },
            error: (res: any) => {
                this.errorVerify = MessageConstants.SYSTEM_ERROR;
                this.informationVerify = null;
                this.verifySpinner = false;
            }
        })
    }

    updateUser() {
        if(this.initialEmail != this.user.email) {
            this.user.emailConfirmed = false;
        }
        this.user.password = this.user.newPassword;
        this.settingService.updateUser(this.user).subscribe({
            next: (res: any) => {
                if(res.statusCode == 200) {
                    this.error = null;
                    this.initialEmail = this.user.email;
                    this.popUpService.open(MessageConstants.UPDATE_SUCCESS, mType.success);
                    this.updateSpinner = false;
                    this.getLoggedUser();
                }
                else {
                    this.error = MessageConstants.SYSTEM_ERROR;
                    this.popUpService.open(MessageConstants.UPDATE_FAILED, mType.error);
                    this.updateSpinner = false;
                }
            },
            error: (res: any) => {
                this.error = MessageConstants.SYSTEM_ERROR;
                this.popUpService.open(MessageConstants.UPDATE_FAILED, mType.error);
                this.updateSpinner = false;
            }
        });
    }

    get f() {
        return this.formControl.controls;
    }
}
