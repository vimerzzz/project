import { Component, OnInit, Sanitizer } from '@angular/core';
import { DomSanitizer, Title } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { HubConnection, HubConnectionBuilder } from '@microsoft/signalr';
import { BaseAuth } from 'src/app/shared/auth/base.auth';
import { MessageConstants } from 'src/app/shared/constants/message.constants';
import { SocketConstants } from 'src/app/shared/constants/socket.constants';
import { mType, PopUpService } from 'src/app/shared/services/base/pop-up.service';
import { NotificationService } from 'src/app/shared/services/main/information/notification.service';
import { ResetThongBao } from 'src/app/shared/utilities/reset-status.utility';

@Component({
    selector: 'app-notification',
    templateUrl: './notification.component.html',
    styleUrls: ['./notification.component.scss']
})
export class NotificationComponent implements OnInit {
    listThongBao: any;
    loggedUser: any;
    loaded: Promise<boolean>;
    totalRecord: any = 0;
    lastPage: any;
    hub: HubConnection;
    params: any = {
        pageIndex: 1,
        pageSize: 10,
        userId: "",
        daDoc: ""
    }

    constructor(private titleService: Title, private base: BaseAuth, private resetThongBao: ResetThongBao, private nottificationService: NotificationService, private popUpService: PopUpService, private sanitizer: DomSanitizer, private router: Router) { }

    ngOnInit(): void {
        this.titleService.setTitle("Thông báo");
        this.base.checkExpire();
        this.getLoggedUser();
    }

    getLoggedUser() {
        let object = localStorage.getItem("loggedUser");
        if(object) {
            this.loggedUser = JSON.parse(object);
            this.params.userId = this.loggedUser.userId;
            this.getAllThongBao(1);
            this.startSocket();
            this.addThongBao();
        }
    }

    getAllThongBao(index: any) {
        this.params.pageIndex = index;
        this.nottificationService.getAllThongBao(this.params).subscribe({
            next: (res: any) => {
                this.listThongBao = res.data ? res.data : [];
                for(let item of this.listThongBao) {
                    item.noiDungThongBaoHTML = this.sanitizer.bypassSecurityTrustHtml(item.noiDungThongBao);
                    item.done = item.daDoc;
                    item.inProgressDone = false;
                    item.close = false;
                    item.inProgressClose = false;
                }
                this.totalRecord = res.totalRecord;
                this.lastPage = res.totalRecord % this.params.pageSize == 0 ? Math.floor(res.totalRecord / this.params.pageSize) : Math.floor(res.totalRecord / this.params.pageSize) + 1;
                this.loaded = Promise.resolve(true);
                this.setClass();
                this.updateDaXem();
            }
        });
    }

    setClass() {
        setTimeout(() => {
            let notifyLink = document.getElementsByName("notify-link");
            for(let index = 0; index < notifyLink.length; index++) {
                notifyLink[index].style.color = "rgb(71, 134, 145)";
                notifyLink[index].style.cursor = "pointer";
                notifyLink[index].addEventListener("click", () => {
                    let url = notifyLink[index].getAttribute("href");
                    let root = window.location.href.replace("http://", "").replace("https://", "").split("/")[0];
                    if(url != null) {
                        let rootUrl = url.replace("http://", "").replace("https://", "").split("/")[0];
                        if(rootUrl == root && notifyLink[index].getAttribute("target") != "_blank") {
                            url = url.replace("http://", "").replace("https://", "").split(rootUrl)[1];
                            notifyLink[index].removeAttribute("href");
                            this.router.navigateByUrl(url);
                        }
                    }
                });
            }
        }, 1);
    }

    updateDaXem() {
        if(this.listThongBao.length) {
            let count = this.totalRecord - this.params.pageSize * (this.params.pageIndex - 1);
            count = count > this.params.pageSize ? this.params.pageSize : count;
            for(let index in this.listThongBao) {
                if(this.listThongBao[index].daXem) {
                    count--;
                }
                else {
                    this.listThongBao[index].daXem = true;
                }
                if(parseInt(index) == this.listThongBao.length - 1) {
                    if(count) {
                        this.nottificationService.updateThongBao(this.listThongBao).subscribe({
                            next: (res: any) => {
                                if(res.statusCode == 200) {
                                    let tmp = this.resetThongBao.getThongBaoCount();
                                    if(tmp >= count) {
                                        this.resetThongBao.setThongBaoCount(tmp - count);
                                    }
                                }
                                else {
                                    this.popUpService.open(MessageConstants.SYSTEM_ERROR, mType.error);
                                }
                            },
                            error: (res: any) => {
                                this.popUpService.open(MessageConstants.SYSTEM_ERROR, mType.error);
                            }
                        });
                    }
                }
            }
        }
    }

    updateDaDoc(item: any) {
        if(!item.done) {
            item.inProgressDone = true;
            item.daDoc = true;
            let tmp = [];
            tmp.push(item);
            this.nottificationService.updateThongBao(tmp).subscribe({
                next: (res: any) => {
                    if(res.statusCode == 200) {
                        item.inProgressDone = false;
                        item.done = true;
                        this.getAllThongBao(this.params.pageIndex);
                    }
                    else {
                        this.popUpService.open(MessageConstants.SYSTEM_ERROR, mType.error);
                    }
                },
                error: (res: any) => {
                    this.popUpService.open(MessageConstants.SYSTEM_ERROR, mType.error);
                }
            });
        }
    }

    updateDaDocAll() {
        if(this.listThongBao.length) {
            for(let item of this.listThongBao) {
                item.inProgressDone = true;
                item.daDoc = true;
            }
            this.nottificationService.updateThongBao(this.listThongBao).subscribe({
                next: (res: any) => {
                    if(res.statusCode == 200) {
                        for(let item of this.listThongBao) {
                            item.inProgressDone = false;
                            item.done = true;
                        }
                        this.getAllThongBao(this.params.pageIndex);
                    }
                    else {
                        this.popUpService.open(MessageConstants.SYSTEM_ERROR, mType.error);
                    }
                },
                error: (res: any) => {
                    this.popUpService.open(MessageConstants.SYSTEM_ERROR, mType.error);
                }
            });
        }
    }

    deleteThongBao(item: any) {
        if(!item.close) {
            item.inProgressClose = true;
            let id = [];
            id.push(item.id);
            this.nottificationService.deleteThongBao(this.loggedUser.userId, id).subscribe({
                next: (res: any) => {
                    if(res.statusCode == 200) {
                        item.inProgressClose = false;
                        item.close = true;
                        this.getAllThongBao(this.params.pageIndex);
                    }
                    else {
                        this.popUpService.open(MessageConstants.SYSTEM_ERROR, mType.error);
                    }
                },
                error: (res: any) => {
                    this.popUpService.open(MessageConstants.SYSTEM_ERROR, mType.error);
                }
            });
        }
    }

    deleteAll() {
        if(this.listThongBao.length) {
            let id = [];
            for(let item of this.listThongBao) {
                item.inProgressDone = true;
                item.daDoc = true;
                id.push(item.id);
            }
            this.nottificationService.deleteThongBao(this.loggedUser.userId, id).subscribe({
                next: (res: any) => {
                    if(res.statusCode == 200) {
                        for(let item of this.listThongBao) {
                            item.close = true;
                            item.inProgressClose = false;
                        }
                        this.getAllThongBao(this.params.pageIndex);
                    }
                    else {
                        this.popUpService.open(MessageConstants.SYSTEM_ERROR, mType.error);
                    }
                },
                error: (res: any) => {
                    this.popUpService.open(MessageConstants.SYSTEM_ERROR, mType.error);
                }
            });
        }
    }

    startSocket() {
        this.hub = new HubConnectionBuilder().withUrl(SocketConstants.thongBaoHub).build();
        this.hub.start();
    }

    addThongBao() {
        this.hub.on("thongBao", (res: any) => {
            if(res && res.length) {
                for(let item of res) {
                    if(item.userId == this.loggedUser.userId && this.params.daDoc != true && this.params.pageIndex == 1) {
                        if(this.listThongBao.length == this.params.pageSize) {
                            this.listThongBao.pop();
                        }
                        if(this.totalRecord % this.params.pageSize == 0) {
                            this.lastPage++;
                        }
                        item.noiDungThongBaoHTML = this.sanitizer.bypassSecurityTrustHtml(item.noiDungThongBao);
                        item.done = item.daDoc;
                        item.inProgressDone = false;
                        item.close = false;
                        item.inProgressClose = false;
                        this.listThongBao.unshift(item);
                        this.setClass();
                        this.updateDaXem();
                        this.totalRecord++;
                    }
                }
            }
        });
    }
}
