import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { Sort } from '@angular/material/sort';
import { Title } from '@angular/platform-browser';
import { BaseAuth } from 'src/app/shared/auth/base.auth';
import { ConfirmDeleteComponent } from 'src/app/shared/components/confirm-delete/confirm-delete.component';
import { MessageConstants } from 'src/app/shared/constants/message.constants';
import { mType, PopUpService } from 'src/app/shared/services/base/pop-up.service';
import { AddressService } from 'src/app/shared/services/main/information/address.service';
import { DialogAddAddressComponent } from './dialog-add-address/dialog-add-address.component';

@Component({
    selector: 'app-address',
    templateUrl: './address.component.html',
    styleUrls: ['./address.component.scss']
})
export class AddressComponent implements OnInit {
    loaded: Promise<boolean>;
    listAddress: any;
    loggedUser: any;
    totalRecord: any;
    stt: any = 1;
    displayedColumns = ["stt", "diaChi", "thaoTac"];
    sortActive: any;
    sortDirection: any;
    params: any = {
        pageIndex: 1,
        pageSize: 10,
        searchString: null,
        order: null,
        orderBy: null,
        userId: null
    }
    paramsChange: any = {
        searchString: null
    }
    @ViewChild("paginator") paginator: MatPaginator;

    constructor(private titleService: Title, private base: BaseAuth, private popUpService: PopUpService, private addressService: AddressService, private dialog: MatDialog) { }

    ngOnInit(): void {
        this.titleService.setTitle("Sổ địa chỉ");
        this.base.checkExpire();
        this.getLoggedUser();
    }

    getLoggedUser() {
        let object = localStorage.getItem("loggedUser");
        if(object) {
            this.loggedUser = JSON.parse(object);
            this.params.userId = this.loggedUser.userId;
            this.getAllDiaChi();
        }
    }

    getAllDiaChi(e?: any) {
        if (e && e.pageIndex !== "" && e.pageIndex !== undefined) {
            this.params.pageIndex = e.pageIndex + 1;
        }
        if (e && e.pageSize !== "" && e.pageSize !== undefined) {
            this.params.pageSize = e.pageSize;
        }
        this.addressService.getAllDiaChi(this.params).subscribe({
            next: (res: any) => {
                this.listAddress = res.data ? res.data : [];
                for(let diaChi of this.listAddress) {
                    diaChi.stt = this.stt;
                    this.stt++;
                }
                this.stt = 1;
                this.totalRecord = res.totalRecord;
                this.loaded = Promise.resolve(true);
                this.sortActive = this.params.orderBy;
                this.sortDirection = this.params.order;
            }
        });
    }

    openDialogAddDiaChi() {
        let item: any = {
            diaChi: null,
            userId: this.loggedUser.userId
        }
        let dialogRef = this.dialog.open(DialogAddAddressComponent, {
            width: "48rem",
            data: {
                item
            }
        });
        dialogRef.afterClosed().subscribe((resp: any) => {
            if(resp == true) {
                this.addressService.createDiaChi(item).subscribe({
                    next: (res: any) => {
                        if(res.statusCode == 200) {
                            this.popUpService.open(MessageConstants.CREATE_SUCCESS, mType.success);
                            this.getAllDiaChi();
                        }
                        else {
                            this.popUpService.open(MessageConstants.CREATE_FAILED, mType.error);
                        }
                    },
                    error: (res: any) => {
                        this.popUpService.open(MessageConstants.CREATE_FAILED, mType.error);
                    }
                });
            }
        });
    }

    openDialogUpdateDiaChi(item: any) {
        let dialogRef = this.dialog.open(DialogAddAddressComponent, {
            width: "48rem",
            data: {
                item
            }
        });
        dialogRef.afterClosed().subscribe((resp: any) => {
            if(resp == true) {
                this.addressService.updateDiaChi(item).subscribe({
                    next: (res: any) => {
                        if(res.statusCode == 200) {
                            this.popUpService.open(MessageConstants.UPDATE_SUCCESS, mType.success);
                            this.getAllDiaChi();
                        }
                        else {
                            this.popUpService.open(MessageConstants.UPDATE_FAILED, mType.error);
                        }
                    },
                    error: (res: any) => {
                        this.popUpService.open(MessageConstants.UPDATE_FAILED, mType.error);
                    }
                });
            }
        });
    }

    deleteDiaChi(element: any) {
        let item: any = {
            id: element.id,
            userId: element.userId
        }
        let dialogRef = this.dialog.open(ConfirmDeleteComponent, {
            width: "32rem"
        });
        dialogRef.afterClosed().subscribe((resp: any) => {
            if(resp == true) {
                this.addressService.deleteDiaChi(item).subscribe({
                    next: (res: any) => {
                        if(res.statusCode == 200) {
                            this.popUpService.open(MessageConstants.DELETE_SUCCESS, mType.success);
                            this.getAllDiaChi();
                        }
                        else {
                            this.popUpService.open(MessageConstants.DELETE_FAILED, mType.error);
                        }
                    },
                    error: (res: any) => {
                        this.popUpService.open(MessageConstants.DELETE_FAILED, mType.error);
                    }
                });
            }
        });
    }

    sortData(sort: Sort) {
        this.params.order = sort.direction;
        this.params.orderBy = sort.active;
        this.getAllDiaChi();
    }

    toHeadPage() {
        if(this.params.searchString != this.paramsChange.searchString) {
            this.paginator.firstPage();
            this.paramsChange.searchString = this.params.searchString;
        }
    }
}
