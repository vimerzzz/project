import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
    selector: 'app-dialog-add-address',
    templateUrl: './dialog-add-address.component.html',
    styleUrls: ['./dialog-add-address.component.scss']
})
export class DialogAddAddressComponent implements OnInit {
    formControl: FormGroup;

    constructor(private formBuilder: FormBuilder, private dialogRef: MatDialogRef<DialogAddAddressComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any) { }

    ngOnInit(): void {
        this.formControl = this.formBuilder.group({
            address: ["", Validators.required]
        });
    }

    get f() {
        return this.formControl.controls;
    }

    save() {
        this.formControl.markAllAsTouched();
        if(this.formControl.invalid) {
            return;
        }
        else {
            this.dialogRef.close(true);
        }
    }
}
