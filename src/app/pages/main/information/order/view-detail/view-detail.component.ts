import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { SuccessColor } from 'src/app/shared/animations/text-color.animations';
import { BaseAuth } from 'src/app/shared/auth/base.auth';
import { ConfirmDeleteComponent } from 'src/app/shared/components/confirm-delete/confirm-delete.component';
import { GuidConstants } from 'src/app/shared/constants/guid.constants';
import { MessageConstants } from 'src/app/shared/constants/message.constants';
import { mType, PopUpService } from 'src/app/shared/services/base/pop-up.service';
import { OrderService } from 'src/app/shared/services/main/information/order.service';
import { ResetGioHang } from 'src/app/shared/utilities/reset-status.utility';
import { environment } from 'src/environments/environment';
import { ViewCouponComponent } from './view-coupon/view-coupon.component';

@Component({
    selector: 'app-view-detail',
    templateUrl: './view-detail.component.html',
    styleUrls: ['./view-detail.component.scss'],
    animations: [
        SuccessColor
    ]
})
export class ViewDetailComponent implements OnInit {
    formControl: FormGroup;
    loggedUser: any;
    loaded: Promise<boolean>;
    donHang: any;
    acceptingStatus = GuidConstants.ONE;
    acceptedStatus = GuidConstants.TWO;
    canceledStatus = GuidConstants.FIVE;
    // displayedColumns = ["hinhAnh", "tenSanPham", "donGia", "soLuong", "maGiamGia", "tongTien"];
    displayedColumns = ["hinhAnh", "tenSanPham", "donGia", "soLuong", "tongTien"];
    editable: any = false;
    dauGia: any = false;
    info = {
        tenDayDu: "",
        tenDayDuNguoiDat: "",
        soDienThoai: "",
        soDienThoaiNguoiDat: "",
        diaChi: "",
        xuatHoaDon: null,
        ghiChu: "",
    }

    constructor(private formBuilder: FormBuilder, private dialog: MatDialog, private popUpService: PopUpService, private resetGioHang: ResetGioHang, private base: BaseAuth, private titleService: Title, private router: Router, private activeRouter: ActivatedRoute, private orderService: OrderService) { }

    ngOnInit(): void {
        this.titleService.setTitle("Chi tiết đơn hàng");
        this.base.checkExpire();
        this.getLoggedUser();
        this.formControl = this.formBuilder.group({
            hoVaTenDat: ["", Validators.required],
            sdtDat: ["", Validators.required],
            hoVaTen: ["", Validators.required],
            sdt: ["", Validators.required],
            diaChi: ["", Validators.required],
            ghiChu: [""],
        });
    }

    back() {
        this.router.navigateByUrl("/information/order");
    }

    get f() {
        return this.formControl.controls;
    }

    getLoggedUser() {
        let object = localStorage.getItem("loggedUser");
        if(object) {
            this.loggedUser = JSON.parse(object);
            this.getDonHangByMaDonHang();
        }
    }

    getDonHangByMaDonHang() {
        this.activeRouter.queryParams.subscribe((params) => {
            if(params.maDonHang) {
                this.orderService.getDonHangByMaDonHang(this.loggedUser.userId, params.maDonHang).subscribe({
                    next: (res: any) => {
                        this.donHang = res ? res : null;
                        if(this.donHang) {
                            this.info = {
                                diaChi: this.donHang.diaChi,
                                tenDayDu: this.donHang.tenDayDu,
                                tenDayDuNguoiDat: this.donHang.tenDayDuNguoiDat,
                                soDienThoai: this.donHang.soDienThoai,
                                soDienThoaiNguoiDat: this.donHang.soDienThoaiNguoiDat,
                                xuatHoaDon: this.donHang.xuatHoaDon,
                                ghiChu: this.donHang.ghiChu
                            }
                        }
                        for(let item of this.donHang.donHangSanPham) {
                            if(item.hinhAnhSanPham) {
                                item.hinhAnhSanPham = environment.apiUrl + "/api/" + item.hinhAnhSanPham.replaceAll("\\", "/");
                            }
                            item.linkAnimation = false;
                            item.giamGiaAnimation = false;
                            item.giaGiam = 0;
                            if(item.sanPhamDauGia) {
                                this.dauGia = true;
                            }
                            // if(item.coMaGiamGia) {
                            //     if(item.giamGia * item.soLuong < item.dieuKienGiamGia) {
                            //         item.giaGiam = 0;
                            //     }
                            //     else {
                            //         item.giaGiam = item.giamGia * item.soLuong * item.phanTramGiamGia / 100;
                            //         item.giaGiam = item.giaGiam < item.giamGiaToiDa ? item.giaGiam : item.giamGiaToiDa;
                            //         item.giaGiam = item.giaGiam < item.giamGia * item.soLuong ? item.giaGiam : item.giamGia * item.soLuong;
                            //     }
                            // }
                        }
                        this.loaded = Promise.resolve(true);
                    }
                });
            }
            else {
                this.router.navigateByUrl("/information/order");
            }
        });
    }

    viewMaGiamGia(item: any) {
        this.dialog.open(ViewCouponComponent, {
            width: "32rem",
            data: {
                item
            }
        });
    }

    datHangLai() {
        for(let index in this.donHang.donHangSanPham) {
            this.orderService.getSanPhamById(this.donHang.donHangSanPham[index].sanPhamGocId).subscribe({
                next: (res: any) => {
                    if(res) {
                        let item = {
                            userId: this.loggedUser.userId,
                            sanPhamId: this.donHang.donHangSanPham[index].sanPhamGocId,
                            soLuong: this.donHang.donHangSanPham[index].soLuong,
                            donGia: res.donGia,
                            giamGia: res.giamGia
                        }
                        this.orderService.createGioHang(item).subscribe({
                            next: (res: any) => {
                                if(res.statusCode == 200) {
                                    this.getAllGioHang(index);
                                }
                                else {
                                    this.popUpService.open(MessageConstants.CREATE_FAILED, mType.error);
                                }
                            },
                            error: (res: any) => {
                                this.popUpService.open(MessageConstants.CREATE_FAILED, mType.error);
                            }
                        });
                    }
                    else {
                        this.popUpService.open(MessageConstants.SYSTEM_ERROR, mType.error);
                    }
                },
                error: (res: any) => {
                    this.popUpService.open(MessageConstants.SYSTEM_ERROR, mType.error);
                }
            });
        }
    }

    getAllGioHang(index: any) {
        this.orderService.getAllGioHang(this.loggedUser.userId).subscribe({
            next: (res: any) => {
                if(res) {
                    let soGioHang = 0;
                    for(let item of res) {
                        soGioHang += item.listSanPhamInGioHang && item.listSanPhamInGioHang.length ? item.listSanPhamInGioHang.length : 0;
                    }
                    this.resetGioHang.setGioHangCount(soGioHang);
                    if(parseInt(index) == this.donHang.donHangSanPham.length - 1) {
                        this.popUpService.open(MessageConstants.CREATE_SUCCESS, mType.success);
                        this.router.navigateByUrl("/cart");
                    }
                }
            }
        });
    }

    huyDonHang(item: any) {
        let title = "Hủy đơn hàng?";
        let dialogRef  = this.dialog.open(ConfirmDeleteComponent, {
            width: "32rem",
            data: {
                title
            }
        });
        dialogRef.afterClosed().subscribe((res: any) => {
            if(res) {
                item.tinhTrangDonHangId = this.canceledStatus;
                this.orderService.updateTinhTrangDonHang(this.loggedUser.userId, item).subscribe({
                    next: (ress: any) => {
                        if(ress.statusCode == 200) {
                            this.popUpService.open(MessageConstants.UPDATE_SUCCESS, mType.success);
                            this.getDonHangByMaDonHang();
                        }
                        else {
                            this.popUpService.open(MessageConstants.UPDATE_FAILED, mType.error);
                        }
                    },
                    error: (res: any) => {
                        this.popUpService.open(MessageConstants.UPDATE_FAILED, mType.error);
                    }
                });
            }
        });
    }

    updateDonHang() {
        this.orderService.updateDonHang(this.donHang).subscribe({
            next: (res: any) => {
                if(res.statusCode == 200) {
                    this.popUpService.open(MessageConstants.UPDATE_SUCCESS, mType.success);
                    this.getDonHangByMaDonHang();
                }
                else {
                    this.popUpService.open(MessageConstants.UPDATE_FAILED, mType.error);
                }
            },
            error: (res: any) => {
                this.popUpService.open(MessageConstants.UPDATE_FAILED, mType.error);
            }
        });
    }
}
