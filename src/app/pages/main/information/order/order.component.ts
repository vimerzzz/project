import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { Sort } from '@angular/material/sort';
import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { BaseAuth } from 'src/app/shared/auth/base.auth';
import { ConfirmDeleteComponent } from 'src/app/shared/components/confirm-delete/confirm-delete.component';
import { GuidConstants } from 'src/app/shared/constants/guid.constants';
import { MessageConstants } from 'src/app/shared/constants/message.constants';
import { mType, PopUpService } from 'src/app/shared/services/base/pop-up.service';
import { OrderService } from 'src/app/shared/services/main/information/order.service';

@Component({
    selector: 'app-order',
    templateUrl: './order.component.html',
    styleUrls: ['./order.component.scss']
})
export class OrderComponent implements OnInit {
    tabIndex: any = 0;
    loaded: Promise<boolean>;
    init: any = true;
    loggedUser: any;
    currentTab: any = 0;
    totalRecord: any;
    stt: any = 1;
    displayedColumns = ["stt", "maDonHang", "soSanPham", "createdAt", "tinhTrangDonHangId", "thanhTien", "thaoTac"];
    sortActive: any;
    sortDirection: any;
    listDonHang: any;
    acceptingStatus = GuidConstants.ONE;
    acceptedStatus = GuidConstants.TWO;
    deliveringStatus = GuidConstants.THREE;
    deliveredStatus = GuidConstants.FOUR;
    canceledStatus = GuidConstants.FIVE;
    listTab = [
        { name: "Tất cả" },
        { name: "Đang xác nhận" },
        { name: "Đã xác nhận" },
        { name: "Đang giao hàng" },
        { name: "Đã hoàn tất" },
        { name: "Đã hủy" }
    ]
    params: any = {
        pageIndex: 1,
        pageSize: 10,
        order: null,
        orderBy: null,
        userId: null,
        tinhTrangDonHangId: null,
        maDonHang: null
    }
    paramsChange: any = {
        maDonHang: null
    }
    @ViewChild("paginator") paginator: MatPaginator;

    constructor(private titleService: Title, private base: BaseAuth, private dialog: MatDialog, private router: Router, private popUpService: PopUpService, private orderService: OrderService) { }

    ngOnInit(): void {
        this.titleService.setTitle("Đơn hàng");
        this.base.checkExpire();
        this.getLoggedUser();
    }

    getLoggedUser() {
        let object = localStorage.getItem("loggedUser");
        if(object) {
            this.loggedUser = JSON.parse(object);
            this.params.userId = this.loggedUser.userId;
            this.changeTab();
        }
    }

    changeTab(e?: any) {
        if(this.currentTab == this.tabIndex) {
            if (e && e.pageIndex !== "" && e.pageIndex !== undefined) {
                this.params.pageIndex = e.pageIndex + 1;
            }
            if (e && e.pageSize !== "" && e.pageSize !== undefined) {
                this.params.pageSize = e.pageSize;
            }
        }
        else {
            this.currentTab = this.tabIndex;
            this.params.pageIndex = 1;
            this.params.pageSize = 10;
            this.params.order = null;
            this.params.orderBy = null;
            this.params.maDonHang = null;
            this.paramsChange.maDonHang = null;
            this.init = true;
        }
        if(this.tabIndex == 0) {
            this.params.tinhTrangDonHangId = null;
        }
        else if(this.tabIndex == 1) {
            this.params.tinhTrangDonHangId = this.acceptingStatus;
        }
        else if(this.tabIndex == 2) {
            this.params.tinhTrangDonHangId = this.acceptedStatus;
        }
        else if(this.tabIndex == 3) {
            this.params.tinhTrangDonHangId = this.deliveringStatus;
        }
        else if(this.tabIndex == 4) {
            this.params.tinhTrangDonHangId = this.deliveredStatus;
        }
        else if(this.tabIndex == 5) {
            this.params.tinhTrangDonHangId = this.canceledStatus;
        }
        this.orderService.getAllDonHang(this.params).subscribe({
            next: (res: any) => {
                this.listDonHang = res.data ? res.data : [];
                for(let item of this.listDonHang) {
                    item.stt = this.stt;
                    this.stt++;
                }
                this.stt = 1;
                this.totalRecord = res.totalRecord;
                if(this.init) {
                    this.loaded = Promise.resolve(true);
                    this.init = false;
                }
                this.sortActive = this.params.orderBy;
                this.sortDirection = this.params.order;
            }
        });
    }

    sortData(sort: Sort) {
        this.params.order = sort.direction;
        this.params.orderBy = sort.active;
        this.changeTab();
    }

    huyDonHang(item: any) {
        let title = "Hủy đơn hàng?";
        let dialogRef  = this.dialog.open(ConfirmDeleteComponent, {
            width: "32rem",
            data: {
                title
            }
        });
        dialogRef.afterClosed().subscribe((res: any) => {
            if(res) {
                item.tinhTrangDonHangId = this.canceledStatus;
                this.orderService.updateTinhTrangDonHang(this.loggedUser.userId, item).subscribe({
                    next: (ress: any) => {
                        if(ress.statusCode == 200) {
                            this.popUpService.open(MessageConstants.UPDATE_SUCCESS, mType.success);
                            this.changeTab();
                        }
                        else {
                            this.popUpService.open(MessageConstants.UPDATE_FAILED, mType.error);
                        }
                    },
                    error: (res: any) => {
                        this.popUpService.open(MessageConstants.UPDATE_FAILED, mType.error);
                    }
                });
            }
        });
    }

    viewDonHang(maDonHang: any) {
        this.router.navigate(["/information/order/detail"], {
            queryParams: {
                maDonHang: maDonHang
            }
        })
    }

    toHeadPage() {
        if(this.params.maDonHang != this.paramsChange.maDonHang) {
            this.paginator.firstPage();
            this.paramsChange.maDonHang = this.params.maDonHang;
        }
    }
}
