import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AdminAuth } from "src/app/shared/auth/admin.auth";
import { CustomerAuth } from "src/app/shared/auth/customer.auth";
import { DeliverAuth } from "src/app/shared/auth/deliver.auth";
import { AddressComponent } from "./address/address.component";
import { FollowComponent } from "./follow/follow.component";
import { InformationSidenavComponent } from "./information-sidenav/information-sidenav.component";
import { MyInformationComponent } from "./my-information/my-information.component";
import { NotificationComponent } from "./notification/notification.component";
import { OrderComponent } from "./order/order.component";
import { ViewDetailComponent } from "./order/view-detail/view-detail.component";
import { SettingComponent } from "./setting/setting.component";

const routes: Routes = [
    {
        path: "",
        children: [
            {
                path: "",
                redirectTo: "/information/my-information",
                pathMatch: "full"
            },
            {
                path: "",
                component: InformationSidenavComponent,
                children: [
                    {
                        path: "my-information",
                        component: MyInformationComponent,
                        canActivate: [CustomerAuth]
                    },
                    {
                        path: "setting",
                        component: SettingComponent,
                        canActivate: [CustomerAuth]
                    },
                    {
                        path: "follow",
                        component: FollowComponent,
                        canActivate: [CustomerAuth]
                    },
                    {
                        path: "address",
                        component: AddressComponent,
                        canActivate: [CustomerAuth]
                    },
                    {
                        path: "notification",
                        component: NotificationComponent,
                        canActivate: [CustomerAuth]
                    },
                    {
                        path: "order",
                        component: OrderComponent,
                        canActivate: [CustomerAuth]
                    },
                    {
                        path: "order/detail",
                        component: ViewDetailComponent,
                        canActivate: [CustomerAuth]
                    }
                ]
            }
        ]
    }
]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
  })
  export class InformationRoutingModule { }
