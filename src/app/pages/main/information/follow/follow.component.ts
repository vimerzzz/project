import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { GrayShadow } from 'src/app/shared/animations/box-shadow.animations';
import { PrimaryButton, WarnButton } from 'src/app/shared/animations/button-fade.animations';
import { SuccessColor } from 'src/app/shared/animations/text-color.animations';
import { BaseAuth } from 'src/app/shared/auth/base.auth';
import { MessageConstants } from 'src/app/shared/constants/message.constants';
import { mType, PopUpService } from 'src/app/shared/services/base/pop-up.service';
import { FollowService } from 'src/app/shared/services/main/information/follow.service';
import { environment } from 'src/environments/environment';

@Component({
    selector: 'app-follow',
    templateUrl: './follow.component.html',
    styleUrls: ['./follow.component.scss'],
    animations: [
        PrimaryButton,
        WarnButton,
        SuccessColor,
        GrayShadow
    ]
})
export class FollowComponent implements OnInit {
    loggedUser: any;
    loaded: Promise<boolean>;
    listTheoDoi: any;
    endContent: any = false;
    params: any = {
        pageIndex: 1,
        pageSize: 10,
        userId: null,
        searchString: null
    }

    constructor(private titleService: Title, private base: BaseAuth, private followService: FollowService, private popUpService: PopUpService) { }

    ngOnInit(): void {
        this.titleService.setTitle("Sổ theo dõi");
        this.base.checkExpire();
        this.getLoggedUser();
    }

    getLoggedUser() {
        let object = localStorage.getItem("loggedUser");
        if(object) {
            this.loggedUser = JSON.parse(object);
            this.params.userId = this.loggedUser.userId;
            this.getAllSoTheoDoi();
        }
    }

    getAllSoTheoDoi() {
        this.params.pageIndex = 1;
        this.followService.getAllSoTheoDoi(this.params).subscribe({
            next: (res: any) => {
                this.listTheoDoi = res.data ? res.data : [];
                for(let item of this.listTheoDoi) {
                    item.followed = true;
                    item.loading = false;
                    item.followButtonAnimation = false;
                    item.unfollowButtonAnimation = false;
                    item.seeMoreButtonAnimation = false;
                    item.linkAnimation = false;
                    item.grayShadowAnimation = [];
                    item.avatar = item.shop.anhDaiDien;
                    if (item.avatar != null) {
                        item.avatar = environment.apiUrl + "/api/" + item.avatar.replaceAll("\\", "/");
                    }
                    this.getAllSanPham(item);
                }
                this.loaded = Promise.resolve(true);
            }
        });
    }

    getAllSanPham(item: any) {
        let params = {
            shopId: item.shop.id,
            pageIndex: 1,
            pageSize: 5
        }
        this.followService.getNewestAllSanPhamWithHinhAnhByShopId(params).subscribe({
            next: (res: any) => {
                item.sanPham = res.data ? res.data : [];
                for(let sanPham of item.sanPham) {
                    item.grayShadowAnimation.push(false);
                    sanPham.avatar = sanPham.hinhAnhSanPham[0].hinhAnh;
                    if (sanPham.avatar != null) {
                        sanPham.avatar = environment.apiUrl + "/api/" + sanPham.avatar.replaceAll("\\", "/");
                    }
                }
                if(this.params.pageIndex != 1) {
                    this.listTheoDoi.push(item);
                }
            }
        });
    }

    addSoTheoDoi(item: any) {
        item.loading = true;
        let param = {
            userId: this.loggedUser.userId,
            shopId: item.shop.id
        }
        this.followService.createSoTheoDoi(param).subscribe({
            next: (res: any) => {
                if(res.statusCode == 200) {
                    item.followed = true;
                    item.loading = false;
                    item.followButtonAnimation = false;
                    item.unfollowButtonAnimation = false;
                    this.popUpService.open(MessageConstants.FOLLOWED, mType.success);
                }
                else {
                    this.popUpService.open(MessageConstants.SYSTEM_ERROR, mType.error);
                }
            },
            error: (res: any) => {
                this.popUpService.open(MessageConstants.SYSTEM_ERROR, mType.error);
            }
        });
    }

    deleteSoTheoDoi(item: any) {
        item.loading = true;
        let param = {
            userId: this.loggedUser.userId,
            shopId: item.shop.id,
            id: item.id
        }
        this.followService.deleteSoTheoDoi(param).subscribe({
            next: (res: any) => {
                if(res.statusCode == 200) {
                    item.followed = false;
                    item.loading = false;
                    item.followButtonAnimation = false;
                    item.unfollowButtonAnimation = false;
                    this.popUpService.open(MessageConstants.UNFOLLOWED, mType.success);
                }
                else {
                    this.popUpService.open(MessageConstants.SYSTEM_ERROR, mType.error);
                }
            },
            error: (res: any) => {
                this.popUpService.open(MessageConstants.SYSTEM_ERROR, mType.error);
            }
        });
    }

    getMoreSoTheoDoi() {
        this.params.pageIndex++;
        this.followService.getAllSoTheoDoi(this.params).subscribe({
            next: (res: any) => {
                if(res.data && res.data.length) {
                    for(let item of res.data) {
                        item.followed = true;
                        item.loading = false;
                        item.followButtonAnimation = false;
                        item.unfollowButtonAnimation = false;
                        item.linkAnimation = false;
                        item.seeMoreButtonAnimation = false;
                        item.grayShadowAnimation = [];
                        item.avatar = item.shop.anhDaiDien;
                        if (item.avatar != null) {
                            item.avatar = environment.apiUrl + "/api/" + item.avatar.replaceAll("\\", "/");
                        }
                        this.getAllSanPham(item);
                    }
                }
                else {
                    this.endContent = true;
                }
            }
        });
    }
}
