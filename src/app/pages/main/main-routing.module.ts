import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { HomeComponent } from "./home/home.component";

const routes: Routes = [
    {
        path: "",
        children: [
            {
                path: "",
                redirectTo: "/home",
                pathMatch: "full"
            },
            {
                path: "home",
                component: HomeComponent
            },
            {
                path: "information",
                loadChildren: () => import('./information/information.module').then(m => m.InformationModule)
            },
            {
                path: "shop",
                loadChildren: () => import('./shop/shop.module').then(m => m.ShopModule)
            },
            {
                path: "cart",
                loadChildren: () => import('./cart/cart.module').then(m => m.CartModule)
            },
            {
                path: "admin",
                loadChildren: () => import('./admin/admin.module').then(m => m.AdminModule)
            },
            // {
            //     path: "deliver",
            //     loadChildren: () => import('./deliver/deliver.module').then(m => m.DeliverModule)
            // }
        ]
    }
]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
  })
  export class MainRoutingModule { }
