import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Title } from '@angular/platform-browser';
import { HubConnection, HubConnectionBuilder } from '@microsoft/signalr';
import { SuccessColor } from 'src/app/shared/animations/text-color.animations';
import { BaseAuth } from 'src/app/shared/auth/base.auth';
import { MessageConstants } from 'src/app/shared/constants/message.constants';
import { SocketConstants } from 'src/app/shared/constants/socket.constants';
import { mType, PopUpService } from 'src/app/shared/services/base/pop-up.service';
import { ReportManagementService } from 'src/app/shared/services/main/admin/report-management.service';
import { convertViToEn } from 'src/app/shared/utilities/convert-language.utility';
import { ResetChat } from 'src/app/shared/utilities/reset-status.utility';
import { environment } from 'src/environments/environment';

@Component({
    selector: 'app-report-management',
    templateUrl: './report-management.component.html',
    styleUrls: ['./report-management.component.scss'],
    animations: [
        SuccessColor
    ]
})
export class ReportManagementComponent implements OnInit {
    loaded: Promise<boolean>;
    listBaoCao: any;
    loggedUser: any;
    nguoiBaoCaoSearch: any;
    nguoiBiBaoCaoSearch: any;
    listNguoiBaoCao: any;
    listNguoiBaoCaoFilter: any;
    listNguoiBiBaoCao: any;
    listNguoiBiBaoCaoFilter: any;
    listShopBiBaoCao: any;
    listShopBiBaoCaoFilter: any;
    lastPage: any;
    hub: HubConnection;
    totalRecord: any = 0;
    params: any = {
        order: "desc",
        orderBy: "createdAt",
        pageIndex: 1,
        pageSize: 10,
        searchString: null,
        nguoiBaoCaoId: "",
        userId: "",
        shopId: "",
        userShopId: "",
        daXem: ""
    }

    constructor(private titleService: Title, private base: BaseAuth, private resetChat: ResetChat, private popUpService: PopUpService, private reportManagementService: ReportManagementService) {}

    ngOnInit(): void {
        this.titleService.setTitle("Quản lý báo cáo");
        this.base.checkExpire();
        this.search();
        this.getLoggedUser();
        this.getAllBaoCao(1);
        this.getAllNguoiBaoCao();
        this.getAllNguoiBiBaoCao();
        this.getAllShopBiBaoCao();
        this.startSocket();
        this.addBaoCaoListener();
    }

    search() {
        this.nguoiBaoCaoSearch = new FormControl("");
        this.nguoiBaoCaoSearch.valueChanges.subscribe((value: any) => {
            this.listNguoiBaoCaoFilter = this.listNguoiBaoCao.filter((item: any) => convertViToEn(item.name).includes(convertViToEn(value)));
        });
        this.nguoiBiBaoCaoSearch = new FormControl("");
        this.nguoiBiBaoCaoSearch.valueChanges.subscribe((value: any) => {
            this.listNguoiBiBaoCaoFilter = this.listNguoiBiBaoCao.filter((item: any) => convertViToEn(item.name).includes(convertViToEn(value)));
            this.listShopBiBaoCaoFilter = this.listShopBiBaoCao.filter((item: any) => convertViToEn(item.name).includes(convertViToEn(value)));
        });
    }

    getLoggedUser() {
        let object = localStorage.getItem("loggedUser");
        if(object) {
            this.loggedUser = JSON.parse(object);
        }
    }

    setInitialValue(list: any) {
        for(let item of list) {
            if(item.nguoiBaoCao && item.nguoiBaoCaoId) {
                if(item.nguoiBaoCao.userInfo.anhDaiDien != null) {
                    item.nguoiBaoCao.userInfo.anhDaiDien = environment.apiUrl + "/api/" + item.nguoiBaoCao.userInfo.anhDaiDien.replaceAll("\\", "/");
                }
                if((item.nguoiBaoCao.userInfo.ho && item.nguoiBaoCao.userInfo.ho.trim()) &&
                    ((item.nguoiBaoCao.userInfo.tenDem && item.nguoiBaoCao.userInfo.tenDem.trim()) ||
                    (item.nguoiBaoCao.userInfo.ten && item.nguoiBaoCao.userInfo.ten.trim()))) {
                        item.nguoiBaoCao.name = "";
                        if(item.nguoiBaoCao.userInfo.ho && item.nguoiBaoCao.userInfo.ho.trim()) {
                            item.nguoiBaoCao.name += item.nguoiBaoCao.userInfo.ho + " ";
                        }
                        if(item.nguoiBaoCao.userInfo.tenDem && item.nguoiBaoCao.userInfo.tenDem.trim()) {
                            item.nguoiBaoCao.name += item.nguoiBaoCao.userInfo.tenDem + " ";
                        }
                        if(item.nguoiBaoCao.userInfo.ten && item.nguoiBaoCao.userInfo.ten.trim()) {
                            item.nguoiBaoCao.name += item.nguoiBaoCao.userInfo.ten + " ";
                        }
                }
                else {
                    item.nguoiBaoCao.name = item.nguoiBaoCao.userName;
                }
            }
            if(item.user && item.userId) {
                if(item.user.userInfo.anhDaiDien != null) {
                    item.user.userInfo.anhDaiDien = environment.apiUrl + "/api/" + item.user.userInfo.anhDaiDien.replaceAll("\\", "/");
                }
                if((item.user.userInfo.ho && item.user.userInfo.ho.trim()) &&
                    ((item.user.userInfo.tenDem && item.user.userInfo.tenDem.trim()) ||
                    (item.user.userInfo.ten && item.user.userInfo.ten.trim()))) {
                        item.user.name = "";
                        if(item.user.userInfo.ho && item.user.userInfo.ho.trim()) {
                            item.user.name += item.user.userInfo.ho + " ";
                        }
                        if(item.user.userInfo.tenDem && item.user.userInfo.tenDem.trim()) {
                            item.user.name += item.user.userInfo.tenDem + " ";
                        }
                        if(item.user.userInfo.ten && item.user.userInfo.ten.trim()) {
                            item.user.name += item.user.userInfo.ten + " ";
                        }
                }
                else {
                    item.user.name = item.user.userName;
                }
            }
            if(item.shop && item.shopId) {
                if(item.shop.anhDaiDien != null) {
                    item.shop.anhDaiDien = environment.apiUrl + "/api/" + item.shop.anhDaiDien.replaceAll("\\", "/");
                }
            }
            if(item.noiGuiBaoCao) {
                let query = "?" + decodeURI(item.noiGuiBaoCao).split("?")[1];
                item.noiGuiBaoCao = decodeURI(item.noiGuiBaoCao).split("?")[0];
                let obj: any = {};
                let urlParams = new URLSearchParams(query);
                urlParams.forEach((value: any, key: any) => {
                    obj[key] = value;
                });
                item.queryParams = {...obj};
            }
            item.baoCaoLinkAnimation = false;
            item.biBaoCaoLinkAnimation = false;
            item.inProgressDone = false;
            item.inProgressClose = false;
            item.close = false;
            item.done = item.daXem;
        }
    }

    getAllBaoCao(index: any) {
        this.params.pageIndex = index;
        this.reportManagementService.getAllBaoCao(this.params).subscribe({
            next: (res: any) => {
                if(res) {
                    this.listBaoCao = res.data ? res.data : [];
                    this.setInitialValue(this.listBaoCao);
                    this.lastPage = res.totalRecord % this.params.pageSize == 0 ? Math.floor(res.totalRecord / this.params.pageSize) : Math.floor(res.totalRecord / this.params.pageSize) + 1;
                    this.totalRecord = res.totalRecord;
                    this.loaded = Promise.resolve(true);
                }
            }
        });
    }

    getAllNguoiBaoCao() {
        this.reportManagementService.getAllNguoiBaoCao().subscribe({
            next: (res: any) => {
                this.listNguoiBaoCao = res ? res : [];
                this.setInitialValue(this.listNguoiBaoCao);
                this.listNguoiBaoCaoFilter = this.listNguoiBaoCao;
            }
        });
    }

    getAllNguoiBiBaoCao() {
        this.reportManagementService.getAllNguoiBiBaoCao().subscribe({
            next: (res: any) => {
                this.listNguoiBiBaoCao = res ? res : [];
                this.setInitialValue(this.listNguoiBiBaoCao);
                this.listNguoiBiBaoCaoFilter = this.listNguoiBiBaoCao;
            }
        });
    }

    getAllShopBiBaoCao() {
        this.reportManagementService.getAllShopBiBaoCao().subscribe({
            next: (res: any) => {
                this.listShopBiBaoCao = res ? res : [];
                this.setInitialValue(this.listShopBiBaoCao);
                this.listShopBiBaoCaoFilter = this.listShopBiBaoCao;
            }
        });
    }

    updateBaoCao(element: any) {
        if(!element.done) {
            element.inProgressDone = true;
            element.daXem = true;
            this.reportManagementService.updateBaoCao(element).subscribe({
                next: (res: any) => {
                    if(res.statusCode == 200) {
                        element.inProgressDone = false;
                        element.done = true;
                    }
                    else {
                        this.popUpService.open(MessageConstants.SYSTEM_ERROR, mType.error);
                    }
                },
                error: (res: any) => {
                    this.popUpService.open(MessageConstants.SYSTEM_ERROR, mType.error);
                }
            });
        }
    }

    updateAllBaoCao() {
        for(let item of this.listBaoCao) {
            this.updateBaoCao(item);
        }
    }

    deleteBaoCao(element: any) {
        if(!element.close) {
            element.inProgressClose = true;
            this.reportManagementService.deleteBaoCao(element.id).subscribe({
                next: (res: any) => {
                    if(res.statusCode == 200) {
                        element.inProgressClose = false;
                        element.close = true;
                    }
                    else {
                        this.popUpService.open(MessageConstants.SYSTEM_ERROR, mType.error);
                    }
                },
                error: (res: any) => {
                    this.popUpService.open(MessageConstants.SYSTEM_ERROR, mType.error);
                }
            });
        }
    }

    deleteAllBaoCao() {
        for(let item of this.listBaoCao) {
            this.deleteBaoCao(item);
        }
    }

    checkuserShop() {
        if(this.params.userShopId.split("user").length == 2) {
            this.params.userId = this.params.userShopId.split("user")[1];
            this.params.shopId = "";
        }
        else if(this.params.userShopId.split("shop").length == 2) {
            this.params.shopId = this.params.userShopId.split("shop")[1];
            this.params.userId = "";
        }
        else if(this.params.userShopId == "") {
            this.params.shopId = "";
            this.params.userId = "";
        }
    }

    openChatShop(shopId: any) {
        this.resetChat.setUserShopChatStatus(true);
        this.resetChat.setChatStatus(true);
        this.resetChat.setChatShopId(shopId);
    }

    openChatUser(userId: any) {
        if(this.loggedUser.userId != userId) {
            this.resetChat.setUserUserChatStatus(true);
            this.resetChat.setChatStatus(true);
            this.resetChat.setChatUserId(userId);
        }
    }

    startSocket() {
        this.hub = new HubConnectionBuilder().withUrl(SocketConstants.baoCaoHub).build();
        this.hub.start();
    }

    addBaoCaoListener() {
        this.hub.on("baoCao", (res: any) => {
            if(res) {
                if(this.params.pageIndex == 1 && this.params.daXem != true &&
                    (res.nguoiBaoCaoId && (this.params.nguoiBaoCaoId == "" || this.params.nguoiBaoCaoId == res.nguoiBaoCaoId)) &&
                    ((res.userId && (this.params.userShopId == "" || this.params.userId == res.userId)) ||
                    (res.shopId && (this.params.userShopId == "" || this.params.shopId == res.shopId)))) {
                    if(this.listBaoCao.length == this.params.pageSize) {
                        this.listBaoCao.pop();
                    }
                    if(this.totalRecord % this.params.pageSize == 0) {
                        this.lastPage++;
                    }
                    if(res.nguoiBaoCao && res.nguoiBaoCaoId) {
                        if(res.nguoiBaoCao.userInfo.anhDaiDien != null) {
                            res.nguoiBaoCao.userInfo.anhDaiDien = environment.apiUrl + "/api/" + res.nguoiBaoCao.userInfo.anhDaiDien.replaceAll("\\", "/");
                        }
                        if((res.nguoiBaoCao.userInfo.ho && res.nguoiBaoCao.userInfo.ho.trim()) &&
                            ((res.nguoiBaoCao.userInfo.tenDem && res.nguoiBaoCao.userInfo.tenDem.trim()) ||
                            (res.nguoiBaoCao.userInfo.ten && res.nguoiBaoCao.userInfo.ten.trim()))) {
                                res.nguoiBaoCao.name = "";
                                if(res.nguoiBaoCao.userInfo.ho && res.nguoiBaoCao.userInfo.ho.trim()) {
                                    res.nguoiBaoCao.name += res.nguoiBaoCao.userInfo.ho + " ";
                                }
                                if(res.nguoiBaoCao.userInfo.tenDem && res.nguoiBaoCao.userInfo.tenDem.trim()) {
                                    res.nguoiBaoCao.name += res.nguoiBaoCao.userInfo.tenDem + " ";
                                }
                                if(res.nguoiBaoCao.userInfo.ten && res.nguoiBaoCao.userInfo.ten.trim()) {
                                    res.nguoiBaoCao.name += res.nguoiBaoCao.userInfo.ten + " ";
                                }
                        }
                        else {
                            res.nguoiBaoCao.name = res.nguoiBaoCao.userName;
                        }
                        if(!this.listNguoiBaoCao.find((s: any) => s.nguoiBaoCaoId == res.nguoiBaoCaoId)) {
                            this.listNguoiBaoCao.unshift(res);
                        }
                    }
                    if(res.user && res.userId) {
                        if(res.user.userInfo.anhDaiDien != null) {
                            res.user.userInfo.anhDaiDien = environment.apiUrl + "/api/" + res.user.userInfo.anhDaiDien.replaceAll("\\", "/");
                        }
                        if((res.user.userInfo.ho && res.user.userInfo.ho.trim()) &&
                            ((res.user.userInfo.tenDem && res.user.userInfo.tenDem.trim()) ||
                            (res.user.userInfo.ten && res.user.userInfo.ten.trim()))) {
                                res.user.name = "";
                                if(res.user.userInfo.ho && res.user.userInfo.ho.trim()) {
                                    res.user.name += res.user.userInfo.ho + " ";
                                }
                                if(res.user.userInfo.tenDem && res.user.userInfo.tenDem.trim()) {
                                    res.user.name += res.user.userInfo.tenDem + " ";
                                }
                                if(res.user.userInfo.ten && res.user.userInfo.ten.trim()) {
                                    res.user.name += res.user.userInfo.ten + " ";
                                }
                        }
                        else {
                            res.user.name = res.user.userName;
                        }
                        if(!this.listNguoiBiBaoCao.find((s: any) => s.userId == res.userId)) {
                            this.listNguoiBiBaoCao.unshift(res);
                        }
                    }
                    if(res.shop && res.shopId) {
                        if(res.shop.anhDaiDien != null) {
                            res.shop.anhDaiDien = environment.apiUrl + "/api/" + res.shop.anhDaiDien.replaceAll("\\", "/");
                        }
                        if(!this.listShopBiBaoCao.find((s: any) => s.shopId == res.shopId)) {
                            this.listShopBiBaoCao.unshift(res);
                        }
                    }
                    this.listBaoCao.unshift(res);
                    this.search();
                    this.totalRecord++;
                }
            }
        });
    }
}
