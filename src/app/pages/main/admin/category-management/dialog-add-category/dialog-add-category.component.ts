import { CdkDragDrop } from '@angular/cdk/drag-drop';
import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSelectChange } from '@angular/material/select';
import { MatTable } from '@angular/material/table';
import { BehaviorSubject } from 'rxjs';
import { MessageConstants } from 'src/app/shared/constants/message.constants';
import { mType, PopUpService } from 'src/app/shared/services/base/pop-up.service';
import { AttributeCategoryService } from 'src/app/shared/services/main/admin/attribute-category.service';
import { convertViToEn } from 'src/app/shared/utilities/convert-language.utility';

@Component({
    selector: 'app-dialog-add-category',
    templateUrl: './dialog-add-category.component.html',
    styleUrls: ['./dialog-add-category.component.scss']
})
export class DialogAddCategoryComponent implements OnInit {
    formControl: FormGroup;
    loaded: Promise<boolean>;
    listAttribute: any;
    listInitAttribute: any;
    listAttributeFilter: any;
    listNewAttribute: any = [];
    displayedColumns = ["stt", "tenThuocTinh", "thuocTinhMacDinh", "thaoTac"];
    displayedNewColumns = ["stt", "tenThuocTinh", "thuocTinhMacDinh", "thaoTac"];
    spinner: any = false;
    rowNumber: any = 0;
    searchString: any = "";
    @ViewChild("table") table: MatTable<Element>;
    @ViewChild("newTable") newTable: MatTable<Element>;
    status = new BehaviorSubject<any>("");
    initStatus: any = true;
    dsThuTu: any = [];
    dsNewThuTu: any = [];
    dragDisabled: any = true;

    constructor(private formBuilder: FormBuilder, private dialogRef: MatDialogRef<DialogAddCategoryComponent>, private attributeService: AttributeCategoryService, private popUpService: PopUpService,
        @Inject(MAT_DIALOG_DATA) public data: any) { }

    ngOnInit(): void {
        this.formControl = this.formBuilder.group({
            tenLoaiSanPham: ["", Validators.required],
            moTaLoaiSanPham: [""]
        });
        // if(this.data.item.id) {
        //     this.getAllThuocTinh();
        // }
    }

    // getAllThuocTinh() {
    //     this.attributeService.getAllThuocTinh(this.data.item.id).subscribe({
    //         next: (res: any) => {
    //             this.listAttribute = res ? res : [];
    //             this.listAttributeFilter = this.listAttribute;
    //             this.dsThuTu = [];
    //             for(let item of this.listAttribute) {
    //                 this.dsThuTu.push(item.soThuTu);
    //             }
    //             this.listInitAttribute = res ? [...res] : [];
    //             this.loaded = Promise.resolve(true);
    //         }
    //     });
    // }

    addNewRow() {
        this.rowNumber++;
        let item: any = {
            soThuTu: this.rowNumber,
            tenThuocTinh: null,
            thuocTinhMacDinh: null,
            loaiSanPhamId: this.data.item.id
        }
        this.listNewAttribute.push(item);
        this.dsNewThuTu.push(this.rowNumber);
        this.newTable.renderRows();
    }

    deleteRow(item: any) {
        let index = this.listAttribute.indexOf(item);
        this.listAttribute.splice(index, 1);
        if(this.searchString != "") {
            let index2 = this.listAttributeFilter.indexOf(item);
            this.listAttributeFilter.splice(index2, 1);
        }
        this.table.renderRows();
        for(let i = index; i < this.listAttribute.length; i++) {
            this.listAttribute[i].soThuTu--;
        }
        this.dsThuTu.pop();
    }

    deleteNewRow(item: any) {
        let index = this.listNewAttribute.indexOf(item);
        this.listNewAttribute.splice(index, 1);
        this.newTable.renderRows();
        for(let i = index; i < this.listNewAttribute.length; i++) {
            this.listNewAttribute[i].soThuTu--;
        }
        this.rowNumber--;
        this.dsNewThuTu.pop();
    }

    filter() {
        this.listAttributeFilter = this.listAttribute.filter((item: any) => convertViToEn(item.tenThuocTinh).includes(convertViToEn(this.searchString)) || convertViToEn(item.thuocTinhMacDinh).includes(convertViToEn(this.searchString)));
    }

    clearFilter() {
        this.searchString = "";
        this.listAttributeFilter = this.listAttribute;
    }

    dropTable(event: CdkDragDrop<Element[]>) {
        this.dragDisabled = true;
        let soThuTu = JSON.parse(JSON.stringify(event.container.data[event.currentIndex])).soThuTu;
        this.listAttribute = this.saveSapXep(event.item.data, soThuTu, this.listAttribute);
        this.table.renderRows();
        if(this.searchString != "") {
            this.filter();
        }
    }

    dropNewTable(event: CdkDragDrop<Element[]>) {
        this.dragDisabled = true;
        let soThuTu = JSON.parse(JSON.stringify(event.container.data[event.currentIndex])).soThuTu;
        this.listNewAttribute = this.saveSapXep(event.item.data, soThuTu, this.listNewAttribute);
        this.newTable.renderRows();
    }

    changeSoThuTu(element: any, event: MatSelectChange) {
        this.listAttribute = this.saveSapXep(element, event.value, this.listAttribute);
        this.table.renderRows();
        if(this.searchString != "") {
            this.filter();
        }
    }

    changeNewSoThuTu(element: any, event: MatSelectChange) {
        this.listNewAttribute = this.saveSapXep(element, event.value, this.listNewAttribute);
        this.newTable.renderRows();
    }

    saveSapXep(item: any, soThuTu: any, list: any): any {
        let start = list.indexOf(item);
        let finish = 0;
        for(let index in list) {
            if(list[index].soThuTu == soThuTu) {
                finish = parseInt(index);
            }
        }
        list.splice(start, 1);
        list.splice(finish, 0, item);
        if(start < finish) {
            for(let index = start; index < finish; index++) {
                list[index].soThuTu--;
            }
            list[finish].soThuTu = soThuTu;
        }
        else if(start > finish) {
            for(let index = start; index > finish; index--) {
                list[index].soThuTu++;
            }
            list[finish].soThuTu = soThuTu;
        }
        return list;
    }

    saveAttribute() {
        this.formControl.markAllAsTouched();
        if(this.formControl.invalid) {
            return;
        }
        else {
            this.spinner = true;
            let checkTenThuocTinh: any;
            for(let index = 0; index < this.listNewAttribute.length; index++) {
                checkTenThuocTinh = this.listNewAttribute[index].tenThuocTinh ? this.listNewAttribute[index].tenThuocTinh.trim() : "";
                if(checkTenThuocTinh == "") {
                    this.deleteNewRow(this.listNewAttribute[index]);
                    index--;
                }
            }
            if(!this.status.isStopped) {
                this.status.next("create");
            }
            else {
                this.status = new BehaviorSubject<any>("create");
            }
            this.status.subscribe((res: any) => {
                switch(res) {
                    case "create": {
                        for(let item of this.listNewAttribute) {
                            item.soThuTu += this.listAttribute.length;
                        }
                        if(this.listNewAttribute.length) {
                            this.createThuocTinh(this.listNewAttribute);
                        }
                        else {
                            this.status.next("update");
                        }
                        break;
                    }
                    case "update": {
                        if(this.listAttribute.length) {
                            this.updateThuocTinh(this.listAttribute);
                        }
                        else {
                            this.status.next("delete");
                        }
                        break;
                    }
                    case "delete": {
                        if(this.listInitAttribute.length) {
                            if(this.listAttribute.length != this.listInitAttribute.length) {
                                let id: any = [];
                                for(let item of this.listInitAttribute) {
                                    if(this.listAttribute.length) {
                                        for(let index in this.listAttribute) {
                                            if(this.listAttribute[index].id == item.id) {
                                                break;
                                            }
                                            if(parseInt(index) == this.listAttribute.length - 1) {
                                                id.push(item.id);
                                            }
                                        }
                                    }
                                    else {
                                        id.push(item.id);
                                    }
                                }
                                this.deleteThuocTinh(id);
                            }
                            else {
                                this.status.next("done");
                            }
                        }
                        else {
                            this.status.next("done");
                        }
                        break;
                    }
                    case "done": {
                        this.spinner = false;
                        this.status.complete();
                        // this.getAllThuocTinh();
                        this.popUpService.open(MessageConstants.UPDATE_SUCCESS, mType.success);
                        break;
                    }
                    default: {
                        this.spinner = false;
                        this.status.complete();
                        // this.getAllThuocTinh();
                        this.popUpService.open(MessageConstants.UPDATE_FAILED, mType.error);
                        break;
                    }
                }
            });
        }
    }

    createThuocTinh(item: any) {
        this.attributeService.createThuocTinh(item).subscribe({
            next: (res: any) => {
                if(res.statusCode == 200) {
                    this.listNewAttribute = [];
                    this.rowNumber = 0;
                    this.dsNewThuTu = [];
                    this.status.next("update");
                }
                else {
                    this.status.next("error");
                }
            },
            error: (res: any) => {
                this.status.next("error");
            }
        });
    }

    updateThuocTinh(item: any) {
        this.attributeService.updateThuocTinh(item).subscribe({
            next: (res: any) => {
                if(res.statusCode == 200) {
                    this.status.next("delete");
                }
                else {
                    this.status.next("error");
                }
            },
            error: (res: any) => {
                this.status.next("error");
            }
        });
    }

    deleteThuocTinh(id: any) {
        this.attributeService.deleteThuocTinh(id).subscribe({
            next: (res: any) => {
                if(res.statusCode == 200) {
                    this.status.next("done");
                }
                else {
                    this.status.next("error");
                }
            },
            error: (res: any) => {
                this.status.next("error");
            }
        });
    }

    get f() {
        return this.formControl.controls;
    }

    save() {
        this.formControl.markAllAsTouched();
        if(this.formControl.invalid) {
            return;
        }
        else {
            this.dialogRef.close(true);
        }
    }
}
