import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { Sort } from '@angular/material/sort';
import { Title } from '@angular/platform-browser';
import { BaseAuth } from 'src/app/shared/auth/base.auth';
import { ConfirmDeleteComponent } from 'src/app/shared/components/confirm-delete/confirm-delete.component';
import { MessageConstants } from 'src/app/shared/constants/message.constants';
import { mType, PopUpService } from 'src/app/shared/services/base/pop-up.service';
import { CategoryManagementService } from 'src/app/shared/services/main/admin/category-management.service';
import { DialogAddCategoryComponent } from './dialog-add-category/dialog-add-category.component';

@Component({
    selector: 'app-category-management',
    templateUrl: './category-management.component.html',
    styleUrls: ['./category-management.component.scss']
})
export class CategoryManagementComponent implements OnInit {
    loaded: Promise<boolean>;
    listCategory: any;
    totalRecord: any;
    stt: any = 1;
    //displayedColumns = ["stt", "tenLoaiSanPham", "moTaLoaiSanPham", "soThuocTinh", "thaoTac"];
    displayedColumns = ["stt", "tenLoaiSanPham", "moTaLoaiSanPham", "thaoTac"];
    sortActive: any;
    sortDirection: any;
    params: any = {
        pageIndex: 1,
        pageSize: 10,
        searchString: null,
        order: null,
        orderBy: null
    }
    paramsChange: any = {
        searchString: null
    }
    @ViewChild("paginator") paginator: MatPaginator;

    constructor(private titleService: Title, private base: BaseAuth, private popUpService: PopUpService, private categoryManagementService: CategoryManagementService, private dialog: MatDialog) { }

    ngOnInit(): void {
        this.titleService.setTitle("Quản lý loại sản phẩm");
        this.base.checkExpire();
        this.getAllLoaiSanPham();
    }

    getAllLoaiSanPham(e?: any) {
        if (e && e.pageIndex !== "" && e.pageIndex !== undefined) {
            this.params.pageIndex = e.pageIndex + 1;
        }
        if (e && e.pageSize !== "" && e.pageSize !== undefined) {
            this.params.pageSize = e.pageSize;
        }
        this.categoryManagementService.getAllLoaiSanPham(this.params).subscribe({
            next: (res: any) => {
                this.listCategory = res.data ? res.data : [];
                for(let category of this.listCategory) {
                    category.stt = this.stt;
                    this.stt++;
                }
                this.stt = 1;
                this.totalRecord = res.totalRecord;
                this.loaded = Promise.resolve(true);
                this.sortActive = this.params.orderBy;
                this.sortDirection = this.params.order;
            }
        });
    }

    openDialogAddLoaiSanPham() {
        let item = {
            tenLoaiSanPham: null,
            moTaLoaiSanPham: null
        }
        let dialogRef = this.dialog.open(DialogAddCategoryComponent, {
            //width: "100%",
            width: "48rem",
            data: {
                item
            }
        });
        dialogRef.afterClosed().subscribe((resp: any) => {
            if(resp == true) {
                this.categoryManagementService.createLoaiSanPham(item).subscribe({
                    next: (res: any) => {
                        if(res.statusCode == 200) {
                            this.popUpService.open(MessageConstants.CREATE_SUCCESS, mType.success);
                            this.getAllLoaiSanPham();
                        }
                        else {
                            this.popUpService.open(MessageConstants.CREATE_FAILED, mType.error);
                        }
                    },
                    error: (res: any) => {
                        this.popUpService.open(MessageConstants.CREATE_FAILED, mType.error);
                    }
                });
            }
        });
    }

    openDialogUpdateLoaiSanPham(item: any) {
        let dialogRef = this.dialog.open(DialogAddCategoryComponent, {
            //width: "100%",
            width: "48rem",
            data: {
                item
            }
        });
        dialogRef.afterClosed().subscribe((resp: any) => {
            if(resp == true) {
                this.categoryManagementService.updateLoaiSanPham(item).subscribe({
                    next: (res: any) => {
                        if(res.statusCode == 200) {
                            this.popUpService.open(MessageConstants.UPDATE_SUCCESS, mType.success);
                            this.getAllLoaiSanPham();
                        }
                        else {
                            this.popUpService.open(MessageConstants.UPDATE_FAILED, mType.error);
                        }
                    },
                    error: (res: any) => {
                        this.popUpService.open(MessageConstants.UPDATE_FAILED, mType.error);
                    }
                });
            }
        });
    }

    deleteLoaiSanPham(id: any) {
        let dialogRef = this.dialog.open(ConfirmDeleteComponent, {
            width: "32rem"
        });
        dialogRef.afterClosed().subscribe((resp: any) => {
            if(resp == true) {
                this.categoryManagementService.deleteLoaiSanPham(id).subscribe({
                    next: (res: any) => {
                        if(res.statusCode == 200) {
                            this.popUpService.open(MessageConstants.DELETE_SUCCESS, mType.success);
                            this.getAllLoaiSanPham();
                        }
                        else {
                            this.popUpService.open(MessageConstants.DELETE_FAILED, mType.error);
                        }
                    },
                    error: (res: any) => {
                        this.popUpService.open(MessageConstants.DELETE_FAILED, mType.error);
                    }
                });
            }
        });
    }

    sortData(sort: Sort) {
        this.params.order = sort.direction;
        this.params.orderBy = sort.active;
        this.getAllLoaiSanPham();
    }

    toHeadPage() {
        if(this.params.searchString != this.paramsChange.searchString) {
            this.paginator.firstPage();
            this.paramsChange.searchString = this.params.searchString;
        }
    }
}
