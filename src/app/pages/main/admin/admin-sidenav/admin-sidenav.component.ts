import { AfterViewInit, Component, OnDestroy, OnInit } from '@angular/core';
import { HubConnection, HubConnectionBuilder } from '@microsoft/signalr';
import { fromEvent, Subscription } from 'rxjs';
import { SocketConstants } from 'src/app/shared/constants/socket.constants';
import { ReportManagementService } from 'src/app/shared/services/main/admin/report-management.service';

@Component({
  selector: 'app-admin-sidenav',
  templateUrl: './admin-sidenav.component.html',
  styleUrls: ['./admin-sidenav.component.scss']
})
export class AdminSidenavComponent implements OnInit, AfterViewInit, OnDestroy {
    roleId: any;
    opened: any = true;
    mode: "side" | "over" | "push";
    toggleButton: any = true;
    resizeSub: Subscription;
    widthBreakpoint: any = 1000;
    soBaoCao: any = 0;
    hub: HubConnection;

    constructor(private reportManagementService: ReportManagementService) { }

    ngOnInit() {
        let object = localStorage.getItem("loggedUser");
        if(object) {
            this.roleId = JSON.parse(object).roleId;
        }
        this.mode = "side";
        this.sideNavCollapse(window.innerWidth);
        this.getAllBaoCao();
        this.startSocket();
        this.addBaoCaoListener();
    }

    ngAfterViewInit() {
        this.resizeSub = fromEvent(window, "resize").subscribe(() => {
            this.sideNavCollapse(window.innerWidth);
        });
    }

    sideNavCollapse(width: number) {
        if(width < this.widthBreakpoint) {
            this.opened = false;
            this.mode = "push";
            this.toggleButton = false;
        }
        else {
            this.opened = true;
            this.mode = "side";
            this.toggleButton = true;
        }
    }

    getAllBaoCao() {
        let params: any = {
            pageSize: 0,
            pageIndex: 0,
            daXem: false,
        }
        this.reportManagementService.getAllBaoCao(params).subscribe({
            next: (res: any) => {
                this.soBaoCao = res.totalRecord;
            }
        });
    }

    startSocket() {
        this.hub = new HubConnectionBuilder().withUrl(SocketConstants.baoCaoHub).build();
        this.hub.start();
    }

    addBaoCaoListener() {
        this.hub.on("baoCaoChuaXem", (res: any) => {
            if(res) {
                this.soBaoCao = res.totalRecord;
            }
        });
    }

    ngOnDestroy() {
        this.resizeSub.unsubscribe();
    }
}
