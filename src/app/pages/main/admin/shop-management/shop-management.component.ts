import { SelectionModel } from '@angular/cdk/collections';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { Sort } from '@angular/material/sort';
import { Title } from '@angular/platform-browser';
import { BaseAuth } from 'src/app/shared/auth/base.auth';
import { ShopManagementService } from 'src/app/shared/services/main/admin/shop-management.service';
import { ResetChat } from 'src/app/shared/utilities/reset-status.utility';
import { DialogShopDetailComponent } from './dialog-shop-detail/dialog-shop-detail.component';

@Component({
    selector: 'app-shop-management',
    templateUrl: './shop-management.component.html',
    styleUrls: ['./shop-management.component.scss']
})
export class ShopManagementComponent implements OnInit {
    loaded: Promise<boolean>;
    listShop: any;
    stt: any = 1;
    displayedColumns = ["select", "stt", "tenShop", "createdAt", "taiKhoanChuShop", "hoatDong", "thaoTac"];
    selection = new SelectionModel<any>(true, []);
    sortActive: any;
    sortDirection: any;
    totalRecord: any = 0;
    activateSpinner: any = false;
    banSpinner: any = false;
    loggedUser: any;
    isUser: any = false;
    params: any = {
        pageIndex: 1,
        pageSize: 10,
        searchString: null,
        order: null,
        orderBy: null,
        hoatDong: ""
    }
    paramsChange: any = {
        searchString: null,
        hoatDong: ""
    }
    @ViewChild("paginator") paginator: MatPaginator;

    constructor(private titleService: Title, private base: BaseAuth, private resetChat: ResetChat, private shopManagementService: ShopManagementService, private dialog: MatDialog) { }

    ngOnInit(): void {
        this.titleService.setTitle("Quản lý shop");
        this.base.checkExpire();
        this.getLoggedUser();
        this.getAllShop();
    }

    getLoggedUser() {
        let object = localStorage.getItem("loggedUser");
        if(object) {
            this.loggedUser = JSON.parse(object);
        }
    }

    getAllShop(e?: any) {
        if (e && e.pageIndex !== "" && e.pageIndex !== undefined) {
            this.params.pageIndex = e.pageIndex + 1;
        }
        if (e && e.pageSize !== "" && e.pageSize !== undefined) {
            this.params.pageSize = e.pageSize;
        }
        this.shopManagementService.getAllShop(this.params).subscribe({
            next: (res: any) => {
                this.isUser = false;
                this.listShop = res.data ? res.data : [];
                for(let shop of this.listShop) {
                    shop.stt = this.stt;
                    this.stt++;
                    if(shop.userId == this.loggedUser.userId) {
                        this.isUser = true;
                    }
                }
                this.stt = 1;
                this.totalRecord = res.totalRecord;
                this.loaded = Promise.resolve(true);
                this.selection.clear();
                this.sortActive = this.params.orderBy;
                this.sortDirection = this.params.order;
                this.activateSpinner = false;
                this.banSpinner = false;
            }
        });
    }

    activateShops(element: any) {
        if(element[0]) {
            this.activateSpinner = true;
            let index = 0;
            for(let obj of element) {
                obj.hoatDong = true;
                this.shopManagementService.updateStatusShop(obj).subscribe({
                    next: (res: any) => {
                        index++;
                        if(index == element.length) {
                            this.getAllShop();
                        }
                    },
                    error: (res: any) => {
                        index++;
                    }
                });
            }
        }
    }

    banShops(element: any) {
        if(element[0]) {
            this.banSpinner = true;
            let index = 0;
            for(let obj of element) {
                obj.hoatDong = false;
                this.shopManagementService.updateStatusShop(obj).subscribe({
                    next: (res: any) => {
                        index++;
                        if(index == element.length) {
                            this.getAllShop();
                        }
                    },
                    error: (res: any) => {
                        index++;
                    }
                });
            }
        }
    }

    sortData(sort: Sort) {
        this.params.order = sort.direction;
        this.params.orderBy = sort.active;
        this.getAllShop();
    }

    isAllSelected() {
        let numSelected = this.isUser ? this.selection.selected.length + 1 : this.selection.selected.length;
        let numRows = this.listShop.length;
        return numSelected === numRows;
    }

    masterToggle() {
        if (this.isAllSelected()) {
            this.selection.clear();
            return;
        }
        let temp = this.listShop.slice();
        if(this.isUser) {
            for(let index in temp) {
                if(temp[index].userId == this.loggedUser.userId) {
                    temp.splice(index, 1);
                    break;
                }
            }
        }
        this.selection.select(...temp);
    }

    openChat(id: any) {
        this.resetChat.setUserShopChatStatus(true);
        this.resetChat.setChatStatus(true);
        this.resetChat.setChatShopId(id);
    }

    openDialogShopDetail(item: any) {
        this.dialog.open(DialogShopDetailComponent, {
            width: "48rem",
            data: {
                item
            }
        });
    }

    toHeadPage() {
        if(this.params.searchString != this.paramsChange.searchString) {
            this.paginator.firstPage();
            this.paramsChange.searchString = this.params.searchString;
        }
        if(this.params.hoatDong != this.paramsChange.hoatDong) {
            this.paginator.firstPage();
            this.paramsChange.hoatDong = this.params.hoatDong;
        }
    }
}
