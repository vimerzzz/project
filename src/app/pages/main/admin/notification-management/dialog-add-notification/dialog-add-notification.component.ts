import { SelectionModel } from '@angular/cdk/collections';
import { formatDate } from '@angular/common';
import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatCheckboxChange } from '@angular/material/checkbox';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Sort } from '@angular/material/sort';
import { HtmlEditorService, ImageService, LinkService, RichTextEditorComponent, ToolbarService } from '@syncfusion/ej2-angular-richtexteditor';
import { GuidConstants } from 'src/app/shared/constants/guid.constants';
import { NotificationManagementService } from 'src/app/shared/services/main/admin/notification-management.service';
import { convertViToEn } from 'src/app/shared/utilities/convert-language.utility';

@Component({
    selector: 'app-dialog-add-notification',
    templateUrl: './dialog-add-notification.component.html',
    styleUrls: ['./dialog-add-notification.component.scss'],
    providers: [ToolbarService, LinkService, ImageService, HtmlEditorService]
})
export class DialogAddNotificationComponent implements OnInit {
    formControl: FormGroup;
    dayNow: any;
    monthNow: any;
    yearNow: any;
    minDate: any;
    moiNguoiDung: any = true;
    adminRole = GuidConstants.ONE;
    customerRole = GuidConstants.TWO;
    deliverRole = GuidConstants.THREE;
    expertRole = GuidConstants.FOUR;
    adminCount: any = 0;
    deliverCount: any = 0;
    customerCount: any = 0;
    expertCount: any = 0;
    adminSelectionCount: any = 0;
    deliverSelectionCount: any = 0;
    customerSelectionCount: any = 0;
    expertSelectionCount: any = 0;
    chooseUser: any = false;
    searchString: any = "";
    listUser: any;
    listUserFilter: any;
    displayedColumns = ["select", "userName", "email"];
    sortActive: any;
    sortDirection: any;
    listUserId: any = [];
    init: any = true;
    selection = new SelectionModel<any>(true, []);
    params: any = {
        order: null,
        orderBy: null,
        hoatDong: true
    }
    tools: object = {
        items: ['Undo', 'Redo', '|',
            'Bold', 'Italic', 'Underline', 'StrikeThrough', '|',
            'FontName', 'FontSize', 'FontColor', '|',
            'Alignments', '|', 'OrderedList', 'UnorderedList', '|',
            'CreateLink', 'RemoveLink', '|', 'ClearFormat']
    };
    @ViewChild("formRTE") rte: RichTextEditorComponent;

    constructor(private formBuilder: FormBuilder, private dialogRef: MatDialogRef<DialogAddNotificationComponent>, private notificationService: NotificationManagementService,
        @Inject(MAT_DIALOG_DATA) public data: any) { }

    ngOnInit(): void {
        this.formControl = this.formBuilder.group({
            tieuDeThongBao: ["", Validators.required],
            noiDungThongBao: ["", Validators.required],
            thoiGianHen: [""],
            thoiGianLenLich: [""],
            moiNguoiDung: [""],
            timKiem: [""]
        });
        if(this.data.item.idThongBao && this.data.item.idThongBao.length) {
            this.f.thoiGianLenLich.setValue(this.data.item.thoiGianHen.split("T")[1].split(":")[0] + ":" + this.data.item.thoiGianHen.split("T")[1].split(":")[1]);
        }
        else {
            this.f.thoiGianLenLich.setValue("00:00");
        }
        this.getDateNow();
        this.getAllUser();
    }

    getDateNow() {
        let now = new Date();
        this.dayNow = now.getDate();
        this.monthNow = now.getMonth() + 1;
        this.yearNow = now.getFullYear();
        if(this.dayNow < 10) {
            if(this.monthNow < 10) {
                this.minDate = this.yearNow + "-" + "0" + this.monthNow + "-" + "0" + this.dayNow;
            }
            else {
                this.minDate = this.yearNow + "-" + this.monthNow + "-" + "0" + this.dayNow;
            }
        }
        else {
            if(this.monthNow < 10) {
                this.minDate = this.yearNow + "-" + "0" + this.monthNow + "-" + this.dayNow;
            }
            else {
                this.minDate = this.yearNow + "-" + this.monthNow + "-" + this.dayNow;
            }
        }
    }

    getAllUser() {
        this.notificationService.getAllUser(this.params).subscribe({
            next: (res: any) => {
                this.listUser = res.data ? res.data : [];
                this.adminCount = 0;
                this.deliverCount = 0;
                this.customerCount = 0;
                for(let item of this.listUser) {
                    if(item.roleId == this.adminRole) {
                        this.adminCount++;
                    }
                    else if(item.roleId == this.deliverRole) {
                        this.deliverCount++;
                    }
                    else if(item.roleId == this.customerRole) {
                        this.customerCount++;
                    }
                    else if(item.roleId == this.expertRole) {
                        this.expertCount++;
                    }
                }
                this.listUserFilter = this.listUser;
                this.sortActive = this.params.orderBy;
                this.sortDirection = this.params.order;
                this.filter();
                if(this.init) {
                    if(this.data.item.idThongBao && this.data.item.idThongBao.length) {
                        this.checkUser();
                    }
                }
            }
        });
    }

    checkUser() {
        if(this.listUser.length != this.data.item.idThongBao.length) {
            this.moiNguoiDung = false;
            for(let item of this.data.item.idThongBao) {
                this.selection.select(item.userId);
                if(item.roleId == this.adminRole) {
                    this.adminSelectionCount++;
                }
                else if(item.roleId == this.deliverRole) {
                    this.deliverSelectionCount++;
                }
                else if(item.roleId == this.customerRole) {
                    this.customerSelectionCount++;
                }
                else if(item.roleId == this.expertRole) {
                    this.expertSelectionCount++;
                }
            }
            if((this.adminCount != this.adminSelectionCount && this.adminSelectionCount) ||
                (this.deliverCount != this.deliverSelectionCount && this.deliverSelectionCount) ||
                (this.customerCount != this.customerSelectionCount && this.customerSelectionCount)) {
                this.chooseUser = true;
            }
        }
    }

    checkGuiNgay() {
        if(this.data.item.coLichHen) {
            this.data.item.coLichHen = false;
            this.f.thoiGianHen.clearValidators();
            this.f.thoiGianHen.reset();
        }
        else {
            this.data.item.coLichHen = true;
            this.f.thoiGianHen.setValidators(Validators.required);
            this.f.thoiGianHen.reset();
        }
    }

    selectRole(event: MatCheckboxChange) {
        if(event.checked) {
            for(let item of this.listUser) {
                if(item.roleId == event.source.value) {
                    if(!this.selection.selected.includes(item.id)) {
                        this.selection.select(item.id);
                        if(item.roleId == this.adminRole) {
                            this.adminSelectionCount++;
                        }
                        else if(item.roleId == this.deliverRole) {
                            this.deliverSelectionCount++;
                        }
                        else if(item.roleId == this.customerRole) {
                            this.customerSelectionCount++;
                        }
                        else if(item.roleId == this.expertRole) {
                            this.expertSelectionCount++;
                        }
                    }
                }
            }
        }
        else {
            for(let item of this.listUser) {
                if(item.roleId == event.source.value) {
                    this.selection.deselect(item.id);
                    if(item.roleId == this.adminRole) {
                        this.adminSelectionCount = 0;
                    }
                    else if(item.roleId == this.deliverRole) {
                        this.deliverSelectionCount = 0;
                    }
                    else if(item.roleId == this.customerRole) {
                        this.customerSelectionCount = 0;
                    }
                    else if(item.roleId == this.expertRole) {
                        this.expertSelectionCount = 0;
                    }
                }
            }
        }
    }

    selectUser(element: any) {
        if(this.selection.selected.includes(element.id)) {
            if(element.roleId == this.adminRole) {
                this.adminSelectionCount++;
            }
            else if(element.roleId == this.deliverRole) {
                this.deliverSelectionCount++;
            }
            else if(element.roleId == this.customerRole) {
                this.customerSelectionCount++;
            }
            else if(element.roleId == this.expertRole) {
                this.expertSelectionCount++;
            }
        }
        else {
            if(element.roleId == this.adminRole) {
                this.adminSelectionCount--;
            }
            else if(element.roleId == this.deliverRole) {
                this.deliverSelectionCount--;
            }
            else if(element.roleId == this.customerRole) {
                this.customerSelectionCount--;
            }
            else if(element.roleId == this.expertRole) {
                this.expertSelectionCount--;
            }
        }
    }

    filter() {
        this.listUserFilter = this.listUser.filter((item: any) => (convertViToEn(item.userName).includes(convertViToEn(this.searchString)) || convertViToEn(item.email).includes(convertViToEn(this.searchString))));
    }

    clearFilter() {
        this.searchString = "";
        this.listUserFilter = this.listUser;
    }

    get f() {
        return this.formControl.controls;
    }

    setTime() {
        if(!this.data.item.coLichHen) {
            this.data.item.thoiGianHen = this.minDate;
            let date = new Date();
            let hour = date.getHours();
            let minute = date.getMinutes();
            if(hour < 10) {
                if(minute < 10) {
                    this.f.thoiGianLenLich.setValue("0" + hour + ":0" + minute);
                }
                else {
                    this.f.thoiGianLenLich.setValue("0" + hour + ":" + minute);
                }
            }
            else {
                if(minute < 10) {
                    this.f.thoiGianLenLich.setValue(hour + ":0" + minute);
                }
                else {
                    this.f.thoiGianLenLich.setValue(hour + ":" + minute);
                }
            }
        }
        this.data.item.thoiGianHen = formatDate(this.data.item.thoiGianHen, "yyyy-MM-dd", "en-GB") + "T" + this.f.thoiGianLenLich.value + ":00";
        this.setListItem();
    }

    setListItem() {
        this.formControl.markAllAsTouched();
        if(this.formControl.invalid) {
            return;
        }
        else {
            if(!this.moiNguoiDung && !this.selection.selected.length) {
                return;
            }
            else {
                let temp = this.data.item.noiDungThongBao.split("<a");
                this.data.item.noiDungThongBao = temp[0];
                for(let index = 1; index < temp.length; index++) {
                    this.data.item.noiDungThongBao += "<a name=\"notify-link\"" + temp[index];
                }
                this.data.item.coLichHen = true;
                if(this.moiNguoiDung) {
                    for(let item of this.listUser) {
                        this.data.item.userId = item.id;
                        this.data.listItem.push(Object.assign({}, this.data.item));
                    }
                }
                else {
                    for(let item of this.selection.selected) {
                        this.data.item.userId = item;
                        this.data.listItem.push(Object.assign({}, this.data.item));
                    }
                }
                if(this.data.item.idThongBao && this.data.item.idThongBao.length) {
                    for(let inx in this.data.item.idThongBao) {
                        this.data.listDeleteItem.push(this.data.item.idThongBao[inx].id);
                        for(let index in this.data.listItem) {
                            if(this.data.listItem[index].userId == this.data.item.idThongBao[inx].userId) {
                                this.data.listItem[index].id = this.data.item.idThongBao[inx].id;
                                this.data.listUpdateItem.push(this.data.listItem.splice(index, 1)[0]);
                                this.data.listDeleteItem.pop();
                                break;
                            }
                        }
                        if(parseInt(inx) == this.data.item.idThongBao.length - 1) {
                            this.save();
                        }
                    }
                }
                else {
                    this.save();
                }
            }
        }
    }

    save() {
        this.dialogRef.close(true);
    }

    sortData(sort: Sort) {
        this.params.order = sort.direction;
        this.params.orderBy = sort.active;
        this.getAllUser();
    }

    isAllSelected() {
        for(let item of this.listUserFilter) {
            if(!this.selection.selected.includes(item.id)) {
                return false;
            }
        }
        return true;
    }

    isAllNoSelected() {
        for(let item of this.listUserFilter) {
            if(this.selection.selected.includes(item.id)) {
                return false;
            }
        }
        return true;
    }

    masterToggle() {
        if (this.isAllSelected()) {
            for(let item of this.listUserFilter) {
                if(item.roleId == this.adminRole) {
                    this.adminSelectionCount--;
                }
                else if(item.roleId == this.deliverRole) {
                    this.deliverSelectionCount--;
                }
                else if(item.roleId == this.customerRole) {
                    this.customerSelectionCount--;
                }
                else if(item.roleId == this.expertRole) {
                    this.expertSelectionCount--;
                }
                this.selection.deselect(item.id);
            }
        }
        else {
            for(let item of this.listUserFilter) {
                if(!this.selection.selected.includes(item.id)) {
                    if(item.roleId == this.adminRole) {
                        this.adminSelectionCount++;
                    }
                    else if(item.roleId == this.deliverRole) {
                        this.deliverSelectionCount++;
                    }
                    else if(item.roleId == this.customerRole) {
                        this.customerSelectionCount++;
                    }
                    else if(item.roleId == this.expertRole) {
                        this.expertSelectionCount++;
                    }
                }
                this.selection.select(item.id);
            }
        }
    }

    rteCreated(): void {
        this.rte.element.focus();
    }
}
