import { formatDate } from '@angular/common';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { Sort } from '@angular/material/sort';
import { Title } from '@angular/platform-browser';
import { BaseAuth } from 'src/app/shared/auth/base.auth';
import { ConfirmDeleteComponent } from 'src/app/shared/components/confirm-delete/confirm-delete.component';
import { MessageConstants } from 'src/app/shared/constants/message.constants';
import { mType, PopUpService } from 'src/app/shared/services/base/pop-up.service';
import { NotificationManagementService } from 'src/app/shared/services/main/admin/notification-management.service';
import { DialogAddNotificationComponent } from './dialog-add-notification/dialog-add-notification.component';

@Component({
    selector: 'app-notification-management',
    templateUrl: './notification-management.component.html',
    styleUrls: ['./notification-management.component.scss']
})
export class NotificationManagementComponent implements OnInit {
    listThongBao: any;
    loaded: Promise<boolean>;
    totalRecord: any = 0;
    sortActive: any;
    sortDirection: any;
    stt: any = 1;
    displayedColumns = ["stt", "tieuDeThongBao", "noiDungThongBao", "thoiGianHen", "thaoTac"];
    dayNow: any;
    monthNow: any;
    yearNow: any;
    minDate: any;
    params: any = {
        pageIndex: 1,
        pageSize: 10,
        searchString: null,
        order: null,
        orderBy: null,
        thoiGianHenBatDau: null,
        thoiGianHenKetThuc: null,
        gioBatDau: "00:00",
        gioKetThuc: "00:00"
    }
    paramsChange: any = {
        searchString: null,
        thoiGianHenBatDau: null,
        thoiGianHenKetThuc: null,
        gioBatDau: "00:00",
        gioKetThuc: "00:00"
    }
    @ViewChild("paginator") paginator: MatPaginator;

    constructor(private titleService: Title, private base: BaseAuth, private notificationService: NotificationManagementService, private dialog: MatDialog, private popUpService: PopUpService) { }

    ngOnInit(): void {
        this.titleService.setTitle("Quản lý lịch thông báo");
        this.base.checkExpire();
        this.getAllThongBaoCoLichHen();
        this.getDateNow();
    }

    getDateNow() {
        let now = new Date();
        this.dayNow = now.getDate();
        this.monthNow = now.getMonth() + 1;
        this.yearNow = now.getFullYear();
        if(this.dayNow < 10) {
            if(this.monthNow < 10) {
                this.minDate = this.yearNow + "-" + "0" + this.monthNow + "-" + "0" + this.dayNow;
            }
            else {
                this.minDate = this.yearNow + "-" + this.monthNow + "-" + "0" + this.dayNow;
            }
        }
        else {
            if(this.monthNow < 10) {
                this.minDate = this.yearNow + "-" + "0" + this.monthNow + "-" + this.dayNow;
            }
            else {
                this.minDate = this.yearNow + "-" + this.monthNow + "-" + this.dayNow;
            }
        }
    }

    getAllThongBaoCoLichHen(e?: any) {
        if (e && e.pageIndex !== "" && e.pageIndex !== undefined) {
            this.params.pageIndex = e.pageIndex + 1;
        }
        if (e && e.pageSize !== "" && e.pageSize !== undefined) {
            this.params.pageSize = e.pageSize;
        }
        if(this.params.thoiGianHenBatDau) {
            this.params.thoiGianHenBatDau = formatDate(this.params.thoiGianHenBatDau, "yyyy-MM-dd", "en-GB") + "T" + this.params.gioBatDau + ":00";
        }
        if(this.params.thoiGianHenKetThuc) {
            this.params.thoiGianHenKetThuc = formatDate(this.params.thoiGianHenKetThuc, "yyyy-MM-dd", "en-GB") + "T" + this.params.gioKetThuc + ":00";
        }
        this.notificationService.getAllThongBaoCoLichHen(this.params).subscribe({
            next: (res: any) => {
                this.listThongBao = res.data ? res.data : [];
                for(let item of this.listThongBao) {
                    item.stt = this.stt;
                    this.stt++;
                }
                this.stt = 1;
                this.loaded = Promise.resolve(true);
                this.totalRecord = res.totalRecord;
                this.sortActive = this.params.orderBy;
                this.sortDirection = this.params.order;
            }
        });
    }

    sortData(sort: Sort) {
        this.params.order = sort.direction;
        this.params.orderBy = sort.active;
        this.getAllThongBaoCoLichHen();
    }

    openDialogAddThongBao() {
        let listItem: any = [];
        let item: any = {
            tieuDeThongBao: null,
            noiDungThongBao: null,
            coLichHen: false,
            thoiGianHen: null,
            userId: null
        }
        let dialogRef = this.dialog.open(DialogAddNotificationComponent, {
            width: "48rem",
            data: {
                item,
                listItem
            }
        });
        dialogRef.afterClosed().subscribe((res: any) => {
            if(res) {
                this.notificationService.createThongBao(listItem).subscribe({
                    next: (res: any) => {
                        if(res.statusCode == 200) {
                            this.popUpService.open(MessageConstants.CREATE_SUCCESS, mType.success);
                            this.getAllThongBaoCoLichHen();
                        }
                        else {
                            this.popUpService.open(MessageConstants.CREATE_FAILED, mType.error);
                        }
                    },
                    error: (res: any) => {
                        this.popUpService.open(MessageConstants.CREATE_FAILED, mType.error);
                    }
                });
            }
        });
    }

    openDialogUpdateThongBao(item: any) {
        let listItem: any = [];
        let listUpdateItem: any = [];
        let listDeleteItem: any = [];
        let dialogRef = this.dialog.open(DialogAddNotificationComponent, {
            width: "48rem",
            data: {
                item,
                listItem,
                listDeleteItem,
                listUpdateItem
            }
        });
        dialogRef.afterClosed().subscribe((res: any) => {
            if(res) {
                this.notificationService.createThongBao(listItem).subscribe({
                    next: (res: any) => {
                        if(res.statusCode == 200) {
                            this.notificationService.updateThongBao(listUpdateItem).subscribe({
                                next: (ress: any) => {
                                    if(ress.statusCode == 200) {
                                        this.notificationService.deleteThongBao(listDeleteItem).subscribe({
                                            next: (resss: any) => {
                                                if(resss.statusCode == 200) {
                                                    this.popUpService.open(MessageConstants.UPDATE_SUCCESS, mType.success);
                                                    this.getAllThongBaoCoLichHen();
                                                }
                                                else {
                                                    this.popUpService.open(MessageConstants.UPDATE_FAILED, mType.error);
                                                }
                                            },
                                            error: (resss: any) => {
                                                this.popUpService.open(MessageConstants.UPDATE_FAILED, mType.error);
                                            }
                                        });
                                    }
                                    else {
                                        this.popUpService.open(MessageConstants.UPDATE_FAILED, mType.error);
                                    }
                                },
                                error: (ress: any) => {
                                    this.popUpService.open(MessageConstants.UPDATE_FAILED, mType.error);
                                }
                            });
                        }
                        else {
                            this.popUpService.open(MessageConstants.UPDATE_FAILED, mType.error);
                        }
                    },
                    error: (res: any) => {
                        this.popUpService.open(MessageConstants.UPDATE_FAILED, mType.error);
                    }
                });
            }
        });
    }

    deleteThongBao(idThongBao: any) {
        let id: any = [];
        for(let item of idThongBao) {
            id.push(item.id);
        }
        let dialogRef = this.dialog.open(ConfirmDeleteComponent, {
            width: "32rem"
        });
        dialogRef.afterClosed().subscribe((res: any) => {
            if(res) {
                this.notificationService.deleteThongBao(id).subscribe({
                    next: (res: any) => {
                        if(res.statusCode == 200) {
                            this.popUpService.open(MessageConstants.DELETE_SUCCESS, mType.success);
                            this.getAllThongBaoCoLichHen();
                        }
                        else {
                            this.popUpService.open(MessageConstants.DELETE_FAILED, mType.error);
                        }
                    },
                    error: (res: any) => {
                        this.popUpService.open(MessageConstants.DELETE_FAILED, mType.error);
                    }
                });
            }
        });
    }

    toHeadPage() {
        if(this.params.searchString != this.paramsChange.searchString) {
            this.paginator.firstPage();
            this.paramsChange.searchString = this.params.searchString;
        }
        if(this.params.thoiGianHenBatDau != this.paramsChange.thoiGianHenBatDau) {
            this.paginator.firstPage();
            this.paramsChange.thoiGianHenBatDau = this.params.thoiGianHenBatDau;
        }
        if(this.params.thoiGianHenKetThuc != this.paramsChange.thoiGianHenKetThuc) {
            this.paginator.firstPage();
            this.paramsChange.thoiGianHenKetThuc = this.params.thoiGianHenKetThuc;
        }
        if(this.params.gioBatDau != this.paramsChange.gioBatDau) {
            this.paginator.firstPage();
            this.paramsChange.gioBatDau = this.params.gioBatDau;
        }
        if(this.params.gioKetThuc != this.paramsChange.gioKetThuc) {
            this.paginator.firstPage();
            this.paramsChange.gioKetThuc = this.params.gioKetThuc;
        }
    }
}
