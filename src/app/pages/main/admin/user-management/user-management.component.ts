import { SelectionModel } from '@angular/cdk/collections';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { Sort } from '@angular/material/sort';
import { Title } from '@angular/platform-browser';
import { BaseAuth } from 'src/app/shared/auth/base.auth';
import { UserManagementService } from 'src/app/shared/services/main/admin/user-management.service';
import { ResetChat } from 'src/app/shared/utilities/reset-status.utility';
import { DialogUserDetailComponent } from './dialog-user-detail/dialog-user-detail.component';

@Component({
    selector: 'app-user-management',
    templateUrl: './user-management.component.html',
    styleUrls: ['./user-management.component.scss']
})
export class UserManagementComponent implements OnInit {
    loaded: Promise<boolean>;
    listUser: any;
    listRole: any;
    stt: any = 1;
    displayedColumns = ["select", "stt", "userName", "email", "createdAt", "roleName", "hoatDong", "thaoTac"];
    selection = new SelectionModel<any>(true, []);
    sortActive: any;
    sortDirection: any;
    totalRecord: any = 0;
    activateSpinner: any = false;
    banSpinner: any = false;
    loggedUser: any;
    isUser: any = false;
    params: any = {
        pageIndex: 1,
        pageSize: 10,
        searchString: null,
        order: null,
        orderBy: null,
        roleId: "",
        hoatDong: ""
    }
    paramsChange: any = {
        searchString: null,
        roleId: "",
        hoatDong: ""
    }
    @ViewChild("paginator") paginator: MatPaginator;

    constructor(private titleService: Title, private base: BaseAuth, private resetChat: ResetChat, private userManagementService: UserManagementService, private dialog: MatDialog) { }

    ngOnInit(): void {
        this.titleService.setTitle("Quản lý người dùng");
        this.base.checkExpire();
        this.getLoggedUser();
        this.getAllRole();
        this.getAllUser();
    }

    getLoggedUser() {
        let object = localStorage.getItem("loggedUser");
        if(object) {
            this.loggedUser = JSON.parse(object);
        }
    }

    getAllUser(e?: any) {
        if (e && e.pageIndex !== "" && e.pageIndex !== undefined) {
            this.params.pageIndex = e.pageIndex + 1;
        }
        if (e && e.pageSize !== "" && e.pageSize !== undefined) {
            this.params.pageSize = e.pageSize;
        }
        this.userManagementService.getAllUser(this.params).subscribe({
            next: (res: any) => {
                this.isUser = false;
                this.listUser = res.data ? res.data : [];
                for(let user of this.listUser) {
                    user.stt = this.stt;
                    this.stt++;
                    if(user.id == this.loggedUser.userId) {
                        this.isUser = true;
                    }
                }
                this.stt = 1;
                this.totalRecord = res.totalRecord;
                this.loaded = Promise.resolve(true);
                this.selection.clear();
                this.sortActive = this.params.orderBy;
                this.sortDirection = this.params.order;
                this.activateSpinner = false;
                this.banSpinner = false;
            }
        });
    }

    getAllRole() {
        this.userManagementService.getAllRole().subscribe({
            next: (res: any) => {
                this.listRole = res;
            }
        });
    }

    activateUsers(element: any) {
        if(element[0]) {
            this.activateSpinner = true;
            let index = 0;
            for(let obj of element) {
                obj.hoatDong = true;
                this.userManagementService.setStatusAndRoleForUser(obj).subscribe({
                    next: (res: any) => {
                        index++;
                        if(index == element.length) {
                            this.getAllUser();
                        }
                    },
                    error: (res: any) => {
                        index++;
                    }
                });
            }
        }
    }

    banUsers(element: any) {
        if(element[0]) {
            this.banSpinner = true;
            let index = 0;
            for(let obj of element) {
                obj.hoatDong = false;
                this.userManagementService.setStatusAndRoleForUser(obj).subscribe({
                    next: (res: any) => {
                        index++;
                        if(index == element.length) {
                            this.getAllUser();
                        }
                    },
                    error: (res: any) => {
                        index++;
                    }
                });
            }
        }
    }

    changeRole(element: any) {
        this.userManagementService.setStatusAndRoleForUser(element).subscribe({
            next: (res: any) => {
                this.getAllUser();
            }
        });
    }

    sortData(sort: Sort) {
        this.params.order = sort.direction;
        this.params.orderBy = sort.active;
        this.getAllUser();
    }

    isAllSelected() {
        let numSelected = this.isUser ? this.selection.selected.length + 1 : this.selection.selected.length;
        let numRows = this.listUser.length;
        return numSelected === numRows;
    }

    masterToggle() {
        if (this.isAllSelected()) {
            this.selection.clear();
            return;
        }
        let temp = this.listUser.slice();
        if(this.isUser) {
            for(let index in temp) {
                if(temp[index].id == this.loggedUser.userId) {
                    temp.splice(index, 1);
                    break;
                }
            }
        }
        this.selection.select(...temp);
    }

    openChatUser(userId: any) {
        if(this.loggedUser.userId != userId) {
            this.resetChat.setUserUserChatStatus(true);
            this.resetChat.setChatStatus(true);
            this.resetChat.setChatUserId(userId);
        }
    }

    openDialogUserDetail(item: any) {
        this.dialog.open(DialogUserDetailComponent, {
            width: "48rem",
            data: {
                item
            }
        });
    }

    toHeadPage() {
        if(this.params.searchString != this.paramsChange.searchString) {
            this.paginator.firstPage();
            this.paramsChange.searchString = this.params.searchString;
        }
        if(this.params.roleId != this.paramsChange.roleId) {
            this.paginator.firstPage();
            this.paramsChange.roleId = this.params.roleId;
        }
        if(this.params.hoatDong != this.paramsChange.hoatDong) {
            this.paginator.firstPage();
            this.paramsChange.hoatDong = this.params.hoatDong;
        }
    }
}
