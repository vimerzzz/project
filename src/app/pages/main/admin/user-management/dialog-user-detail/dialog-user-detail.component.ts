import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Component({
    selector: 'app-dialog-user-detail',
    templateUrl: './dialog-user-detail.component.html',
    styleUrls: ['./dialog-user-detail.component.scss']
})
export class DialogUserDetailComponent implements OnInit {
    avatarImage: any;
    orient: "landscape" | "portrait";

    constructor(@Inject(MAT_DIALOG_DATA) public data: any) { }

    ngOnInit(): void {
        this.avatarImage = this.data.item.userInfo.anhDaiDien;
        if (this.avatarImage != null) {
            this.avatarImage = environment.apiUrl + "/api/" + this.avatarImage.replaceAll("\\", "/");
            this.getImageSize(this.avatarImage).subscribe(response => {
                let width = parseInt(response.split("x")[0]);
                let heigth = parseInt(response.split("x")[1]);
                if(width >= heigth) {
                    this.orient = "landscape";
                }
                else {
                    this.orient = "portrait";
                }
            });
        }
    }

    getImageSize(url: string): Observable<any> {
        return new Observable(observer => {
            let image = new Image();
            image.src = url;

            image.onload = (e: any) => {
                let width = e.path[0].width;
                let height = e.path[0].height;

                observer.next(width + "x" + height);
                observer.complete();
            };
        });
    }
}
