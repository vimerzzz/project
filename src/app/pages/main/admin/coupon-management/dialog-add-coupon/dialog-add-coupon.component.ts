import { formatDate } from '@angular/common';
import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatCheckboxChange } from '@angular/material/checkbox';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { CategoryManagementService } from 'src/app/shared/services/main/admin/category-management.service';
import { convertViToEn } from 'src/app/shared/utilities/convert-language.utility';

@Component({
    selector: 'app-dialog-add-coupon',
    templateUrl: './dialog-add-coupon.component.html',
    styleUrls: ['./dialog-add-coupon.component.scss']
})
export class DialogAddCouponComponent implements OnInit {
    formControl: FormGroup;
    dayNow: any;
    monthNow: any;
    yearNow: any;
    minDate: any;
    searchString: any = "";
    listCategory: any;
    listCategoryFilter: any;
    params: any = {
        pageIndex: 0,
        pageSize: 0,
        searchString: null,
        order: null,
        orderBy: null
    }

    constructor(private formBuilder: FormBuilder, private dialogRef: MatDialogRef<DialogAddCouponComponent>, private categoryService: CategoryManagementService,
        @Inject(MAT_DIALOG_DATA) public data: any) { }

    ngOnInit(): void {
        this.formControl = this.formBuilder.group({
            tenMaGiamGia: ["", Validators.required],
            moTaMaGiamGia: [""],
            giamGiaTuLuc: ["", Validators.required],
            giamGiaDenLuc: ["", Validators.required],
            phanTramGiamGia: ["", [Validators.required, Validators.pattern("^[0-9]+$"), Validators.max(100)]],
            soLuong: ["", [Validators.required, Validators.pattern("^[0-9]+$")]],
            dieuKienGiamGia: ["", [Validators.required, Validators.pattern("^[0-9]+$")]],
            giamGiaToiDa: ["", [Validators.required, Validators.pattern("^[0-9]+$")]],
            thoiGianBatDau: [""],
            thoiGianKetThuc: [""],
            moiLoaiSanPham: [""],
            timKiem: [""]
        });
        if(this.data.item.id) {
            this.f.thoiGianBatDau.setValue(this.data.item.giamGiaTuLuc.split("T")[1].split(":")[0] + ":" + this.data.item.giamGiaTuLuc.split("T")[1].split(":")[1]);
            this.f.thoiGianKetThuc.setValue(this.data.item.giamGiaDenLuc.split("T")[1].split(":")[0] + ":" + this.data.item.giamGiaDenLuc.split("T")[1].split(":")[1]);
        }
        else {
            this.f.thoiGianBatDau.setValue("00:00");
            this.f.thoiGianKetThuc.setValue("00:00");
        }
        this.getDateNow();
        this.getAllLoaiSanPham();
    }

    getDateNow() {
        let now = new Date();
        this.dayNow = now.getDate();
        this.monthNow = now.getMonth() + 1;
        this.yearNow = now.getFullYear();
        if(this.dayNow < 10) {
            if(this.monthNow < 10) {
                this.minDate = this.yearNow + "-" + "0" + this.monthNow + "-" + "0" + this.dayNow;
            }
            else {
                this.minDate = this.yearNow + "-" + this.monthNow + "-" + "0" + this.dayNow;
            }
        }
        else {
            if(this.monthNow < 10) {
                this.minDate = this.yearNow + "-" + "0" + this.monthNow + "-" + this.dayNow;
            }
            else {
                this.minDate = this.yearNow + "-" + this.monthNow + "-" + this.dayNow;
            }
        }
    }

    getAllLoaiSanPham() {
        this.categoryService.getAllLoaiSanPham(this.params).subscribe({
            next: (res: any) => {
                this.listCategory = res.data ? res.data : [];
                for(let item of this.listCategory) {
                    item.checked = false;
                    if(this.data.item.loaiSanPhamGiamGia.length) {
                        for(let index in this.data.item.loaiSanPhamGiamGia) {
                            if(item.id == this.data.item.loaiSanPhamGiamGia[index].loaiSanPhamId) {
                                item.checked = true;
                                break;
                            }
                        }
                    }
                }
                this.listCategoryFilter = this.listCategory;
            }
        });
    }

    filter() {
        this.listCategoryFilter = this.listCategory.filter((item: any) => convertViToEn(item.tenLoaiSanPham).includes(convertViToEn(this.searchString)));
    }

    clearFilter() {
        this.searchString = "";
        this.listCategoryFilter = this.listCategory;
    }

    selectCategory(event: MatCheckboxChange) {
        if(event.checked) {
            this.data.item.loaiSanPhamGiamGia.push({
                maGiamGiaId: this.data.item.id,
                loaiSanPhamId: event.source.value
            });
        }
        else {
            for(let index = 0; index < this.data.item.loaiSanPhamGiamGia.length; index++) {
                if(this.data.item.loaiSanPhamGiamGia[index].loaiSanPhamId == event.source.value) {
                    this.data.item.loaiSanPhamGiamGia.splice(index, 1);
                    break;
                }
            }
        }
    }

    get f() {
        return this.formControl.controls;
    }

    setTime() {
        this.data.item.giamGiaTuLuc = formatDate(this.data.item.giamGiaTuLuc, "yyyy-MM-dd", "en-GB") + "T" + this.f.thoiGianBatDau.value + ":00";
        this.data.item.giamGiaDenLuc = formatDate(this.data.item.giamGiaDenLuc, "yyyy-MM-dd", "en-GB") + "T" + this.f.thoiGianKetThuc.value + ":00";
        this.save();
    }

    save() {
        this.formControl.markAllAsTouched();
        if(this.formControl.invalid) {
            return;
        }
        else {
            if(!this.data.item.moiLoaiSanPham && !this.data.item.loaiSanPhamGiamGia.length) {
                return;
            }
            this.dialogRef.close(true);
        }
    }
}
