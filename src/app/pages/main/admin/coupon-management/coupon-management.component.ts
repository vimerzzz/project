import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSelect } from '@angular/material/select';
import { Sort } from '@angular/material/sort';
import { Title } from '@angular/platform-browser';
import { BaseAuth } from 'src/app/shared/auth/base.auth';
import { ConfirmDeleteComponent } from 'src/app/shared/components/confirm-delete/confirm-delete.component';
import { MessageConstants } from 'src/app/shared/constants/message.constants';
import { mType, PopUpService } from 'src/app/shared/services/base/pop-up.service';
import { CategoryManagementService } from 'src/app/shared/services/main/admin/category-management.service';
import { CouponManagementService } from 'src/app/shared/services/main/admin/coupon-management.service';
import { DialogAddCouponComponent } from './dialog-add-coupon/dialog-add-coupon.component';

@Component({
    selector: 'app-coupon-management',
    templateUrl: './coupon-management.component.html',
    styleUrls: ['./coupon-management.component.scss']
})
export class CouponManagementComponent implements OnInit {
    loaded: Promise<boolean>;
    listCoupon: any;
    totalRecord: any;
    stt: any = 1;
    displayedColumns = ["stt", "tenMaGiamGia", "giamGiaTuLuc", "giamGiaDenLuc", "phanTramGiamGia", "soLuong", "thaoTac"];
    sortActive: any;
    sortDirection: any;
    listCategory: any;
    params: any = {
        pageIndex: 1,
        pageSize: 10,
        searchString: null,
        order: null,
        orderBy: null,
        trangThai: "",
        loaiSanPhamId: [""]
    }
    paramsChange: any = {
        searchString: null
    }
    @ViewChild("paginator") paginator: MatPaginator;
    @ViewChild("selectLoaiSanPham") selectLoaiSanPham: MatSelect;

    constructor(private titleService: Title, private base: BaseAuth, private popUpService: PopUpService, private dialog: MatDialog, private couponService: CouponManagementService, private categoryService: CategoryManagementService) { }

    ngOnInit(): void {
        this.titleService.setTitle("Quản lý mã giảm giá");
        this.base.checkExpire();
        this.getAllMaGiamGia();
        this.getAllLoaiSanPham();
    }

    getAllMaGiamGia(e?: any) {
        if (e && e.pageIndex !== "" && e.pageIndex !== undefined) {
            this.params.pageIndex = e.pageIndex + 1;
        }
        if (e && e.pageSize !== "" && e.pageSize !== undefined) {
            this.params.pageSize = e.pageSize;
        }
        this.couponService.getAllMaGiamGia(this.params).subscribe({
            next: (res: any) => {
                this.listCoupon = res.data ? res.data : [];
                for(let category of this.listCoupon) {
                    category.stt = this.stt;
                    this.stt++;
                }
                this.stt = 1;
                this.totalRecord = res.totalRecord;
                this.loaded = Promise.resolve(true);
                this.sortActive = this.params.orderBy;
                this.sortDirection = this.params.order;
            }
        });
    }

    getAllLoaiSanPham() {
        let paramsForCategory: any = {
            pageIndex: 0,
            pageSize: 0,
            searchString: null,
            order: null,
            orderBy: null
        }
        this.categoryService.getAllLoaiSanPham(paramsForCategory).subscribe({
            next: (res: any) => {
                this.listCategory = res.data ? res.data : [];
            }
        });
    }

    openDialogAddMaGiamGia() {
        let item = {
            tenMaGiamGia: null,
            moTaMaGiamGia: null,
            batDau: false,
            ketThuc: false,
            dieuKienGiamGia: null,
            giamGiaTuLuc: null,
            giamGiaDenLuc: null,
            giamGiaToiDa: null,
            phanTramGiamGia: null,
            soLuong: null,
            moiLoaiSanPham: true,
            loaiSanPhamGiamGia: []
        }
        let dialogRef = this.dialog.open(DialogAddCouponComponent, {
            width: "48rem",
            data: {
                item
            }
        });
        dialogRef.afterClosed().subscribe((resp: any) => {
            if(resp == true) {
                this.couponService.createMaGiamGia(item).subscribe({
                    next: (res: any) => {
                        if(res.statusCode == 200) {
                            this.popUpService.open(MessageConstants.CREATE_SUCCESS, mType.success);
                            this.getAllMaGiamGia();
                        }
                        else {
                            this.popUpService.open(MessageConstants.CREATE_FAILED, mType.error);
                        }
                    },
                    error: (res: any) => {
                        this.popUpService.open(MessageConstants.CREATE_FAILED, mType.error);
                    }
                });
            }
        });
    }

    openDialogUpdateMaGiamGia(item: any) {
        let dialogRef = this.dialog.open(DialogAddCouponComponent, {
            width: "48rem",
            data: {
                item
            }
        });
        dialogRef.afterClosed().subscribe((resp: any) => {
            if(resp == true) {
                item.batDau = false;
                item.ketThuc = false;
                this.couponService.updateMaGiamGia(item).subscribe({
                    next: (res: any) => {
                        if(res.statusCode == 200) {
                            this.popUpService.open(MessageConstants.UPDATE_SUCCESS, mType.success);
                            this.getAllMaGiamGia();
                        }
                        else {
                            this.popUpService.open(MessageConstants.UPDATE_FAILED, mType.error);
                        }
                    },
                    error: (res: any) => {
                        this.popUpService.open(MessageConstants.UPDATE_FAILED, mType.error);
                    }
                });
            }
        });
    }

    deleteMaGiamGia(id: any) {
        let dialogRef = this.dialog.open(ConfirmDeleteComponent, {
            width: "32rem"
        });
        dialogRef.afterClosed().subscribe((resp: any) => {
            if(resp == true) {
                this.couponService.deleteMaGiamGia(id).subscribe({
                    next: (res: any) => {
                        if(res.statusCode == 200) {
                            this.popUpService.open(MessageConstants.DELETE_SUCCESS, mType.success);
                            this.getAllMaGiamGia();
                        }
                        else {
                            this.popUpService.open(MessageConstants.DELETE_FAILED, mType.error);
                        }
                    },
                    error: (res: any) => {
                        this.popUpService.open(MessageConstants.DELETE_FAILED, mType.error);
                    }
                });
            }
        });
    }

    sortData(sort: Sort) {
        this.params.order = sort.direction;
        this.params.orderBy = sort.active;
        this.getAllMaGiamGia();
    }

    toHeadPage() {
        if(this.params.searchString != this.paramsChange.searchString) {
            this.paginator.firstPage();
            this.paramsChange.searchString = this.params.searchString;
        }
    }

    checkLoaiSanPhamId() {
        if(this.params.loaiSanPhamId.length > 1) {
            if(this.params.loaiSanPhamId[0] == "") {
                this.params.loaiSanPhamId.shift();
                this.selectLoaiSanPham.options.toArray()[0].deselect();
                this.selectLoaiSanPham.options.toArray()[0].disabled = true;
            }
        }
        else if(this.params.loaiSanPhamId.length == 0) {
            this.selectLoaiSanPham.options.toArray()[0].select();
            this.selectLoaiSanPham.options.toArray()[0].disabled = false;
        }
    }
}
