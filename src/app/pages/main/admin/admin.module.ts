import { NgModule } from '@angular/core';
import { MaterialModule } from 'src/app/material.module';
import { AdminRoutingModule } from './admin-routing.module';
import { AdminSidenavComponent } from './admin-sidenav/admin-sidenav.component';
import { CategoryManagementComponent } from './category-management/category-management.component';
import { DialogAddCategoryComponent } from './category-management/dialog-add-category/dialog-add-category.component';
import { CouponManagementComponent } from './coupon-management/coupon-management.component';
import { DialogAddCouponComponent } from './coupon-management/dialog-add-coupon/dialog-add-coupon.component';
import { DialogAddNotificationComponent } from './notification-management/dialog-add-notification/dialog-add-notification.component';
import { NotificationManagementComponent } from './notification-management/notification-management.component';
import { ReportManagementComponent } from './report-management/report-management.component';
import { DialogShopDetailComponent } from './shop-management/dialog-shop-detail/dialog-shop-detail.component';
import { ShopManagementComponent } from './shop-management/shop-management.component';
import { DialogUserDetailComponent } from './user-management/dialog-user-detail/dialog-user-detail.component';
import { UserManagementComponent } from './user-management/user-management.component';

@NgModule({
    declarations: [
        AdminSidenavComponent,
        UserManagementComponent,
        ShopManagementComponent,
        CategoryManagementComponent,
        DialogAddCategoryComponent,
        CouponManagementComponent,
        DialogAddCouponComponent,
        NotificationManagementComponent,
        DialogAddNotificationComponent,
        DialogShopDetailComponent,
        DialogUserDetailComponent,
        ReportManagementComponent
    ],
    imports: [
        AdminRoutingModule,
        MaterialModule
    ]
})
export class AdminModule { }
