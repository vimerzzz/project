import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AdminAuth } from "src/app/shared/auth/admin.auth";
import { CustomerAuth } from "src/app/shared/auth/customer.auth";
import { DeliverAuth } from "src/app/shared/auth/deliver.auth";
import { AdminSidenavComponent } from "./admin-sidenav/admin-sidenav.component";
import { CategoryManagementComponent } from "./category-management/category-management.component";
import { CouponManagementComponent } from "./coupon-management/coupon-management.component";
import { NotificationManagementComponent } from "./notification-management/notification-management.component";
import { ReportManagementComponent } from "./report-management/report-management.component";
import { ShopManagementComponent } from "./shop-management/shop-management.component";
import { UserManagementComponent } from "./user-management/user-management.component";

const routes: Routes = [
    {
        path: "",
        children: [
            {
                path: "",
                redirectTo: "/admin/user-management",
                pathMatch: "full"
            },
            {
                path: "",
                component: AdminSidenavComponent,
                children: [
                    {
                        path: "user-management",
                        component: UserManagementComponent,
                        canActivate: [AdminAuth]
                    },
                    {
                        path: "shop-management",
                        component: ShopManagementComponent,
                        canActivate: [AdminAuth]
                    },
                    {
                        path: "category-management",
                        component: CategoryManagementComponent,
                        canActivate: [AdminAuth]
                    },
                    // {
                    //     path: "coupon-management",
                    //     component: CouponManagementComponent,
                    //     canActivate: [AdminAuth]
                    // },
                    {
                        path: "notification-management",
                        component: NotificationManagementComponent,
                        canActivate: [AdminAuth]
                    },
                    {
                        path: "report-management",
                        component: ReportManagementComponent,
                        canActivate: [AdminAuth]
                    }
                ]
            }
        ]
    }
]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
  })
  export class AdminRoutingModule { }
