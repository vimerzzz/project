import { AfterViewInit, Component, OnDestroy, OnInit } from '@angular/core';
import { fromEvent, Subscription } from 'rxjs';

@Component({
  selector: 'app-deliver-sidenav',
  templateUrl: './deliver-sidenav.component.html',
  styleUrls: ['./deliver-sidenav.component.scss']
})
export class DeliverSidenavComponent implements OnInit, AfterViewInit, OnDestroy {
    roleId: any;
    opened: any = true;
    mode: "side" | "over" | "push";
    toggleButton: any = true;
    resizeSub: Subscription;
    widthBreakpoint: any = 1000;

    constructor() { }

    ngOnInit() {
        let object = localStorage.getItem("loggedUser");
        if(object) {
            this.roleId = JSON.parse(object).roleId;
        }
        this.mode = "side";
        this.sideNavCollapse(window.innerWidth);
    }

    ngAfterViewInit() {
        this.resizeSub = fromEvent(window, "resize").subscribe(() => {
            this.sideNavCollapse(window.innerWidth);
        });
    }

    sideNavCollapse(width: number) {
        if(width < this.widthBreakpoint) {
            this.opened = false;
            this.mode = "push";
            this.toggleButton = false;
        }
        else {
            this.opened = true;
            this.mode = "side";
            this.toggleButton = true;
        }
    }

    ngOnDestroy() {
        this.resizeSub.unsubscribe();
    }
}
