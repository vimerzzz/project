import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { Sort } from '@angular/material/sort';
import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { BaseAuth } from 'src/app/shared/auth/base.auth';
import { GuidConstants } from 'src/app/shared/constants/guid.constants';
import { MessageConstants } from 'src/app/shared/constants/message.constants';
import { mType, PopUpService } from 'src/app/shared/services/base/pop-up.service';
import { OrderManagementService } from 'src/app/shared/services/main/deliver/order-management.service';
import { ViewDetailComponent } from './view-detail/view-detail.component';

@Component({
    selector: 'app-order-management',
    templateUrl: './order-management.component.html',
    styleUrls: ['./order-management.component.scss']
})
export class OrderManagementComponent implements OnInit {
    tabIndex: any = 0;
    loaded: Promise<boolean>;
    loggedUser: any;
    currentTab: any = 0;
    totalRecord: any;
    stt: any = 1;
    displayedColumns = ["stt", "maDonHang", "createdAt", "tinhTrangDonHangId", "thanhTien", "thaoTac"];
    sortActive: any;
    sortDirection: any;
    listDonHang: any;
    acceptedStatus = GuidConstants.TWO;
    deliveringStatus = GuidConstants.THREE;
    deliveredStatus = GuidConstants.FOUR;
    canceledDueDeliveringStatus = GuidConstants.SIX;
    userInfo: any;
    deliver: any;
    init: any = true;
    listTab = [
        { name: "Tất cả" },
        { name: "Đã xác nhận" },
        { name: "Đang giao hàng" },
        { name: "Đã hoàn tất" },
        { name: "Đã hủy" }
    ]
    params: any = {
        pageIndex: 1,
        pageSize: 10,
        order: null,
        orderBy: null,
        donViVanChuyenId: null,
        tinhTrangDonHangId: null,
        maDonHang: null
    }
    paramsChange: any = {
        maDonHang: null
    }
    @ViewChild("paginator") paginator: MatPaginator;

    constructor(private titleService: Title, private base: BaseAuth, private popUpService: PopUpService, private router: Router, private orderManagementService: OrderManagementService, private dialog: MatDialog) { }

    ngOnInit(): void {
        this.titleService.setTitle("Quản lý đơn hàng");
        this.base.checkExpire();
        this.getLoggedUser();
    }

    getLoggedUser() {
        let object = localStorage.getItem("loggedUser");
        if (object) {
            this.loggedUser = JSON.parse(object);
            this.getUserInfoByUserId(this.loggedUser.userId);
        }
    }

    getUserInfoByUserId(id: any) {
        this.orderManagementService.getUserInfoByUserId(id).subscribe({
            next: (res: any) => {
                this.userInfo = res;
                this.checkUserInfo();
            }
        });
    }

    checkUserInfo() {
        if (!this.userInfo.emailConfirmed) {
            this.popUpService.open(MessageConstants.NEED_EMAIL_VALIDATION, mType.info);
            this.router.navigateByUrl("/information/setting");
        }
        else {
            this.getDonViByUserId();
        }
    }

    getDonViByUserId() {
        this.orderManagementService.getDonViVanChuyenByUserId(this.loggedUser.userId).subscribe({
            next: (res: any) => {
                if(res) {
                    this.deliver = res;
                    this.params.donViVanChuyenId = this.deliver.id;
                    this.changeTab();
                }
                else {
                    this.router.navigateByUrl("/deliver/create-deliver");
                }
            }
        });
    }

    changeTab(e?: any) {
        if(this.currentTab == this.tabIndex) {
            if (e && e.pageIndex !== "" && e.pageIndex !== undefined) {
                this.params.pageIndex = e.pageIndex + 1;
            }
            if (e && e.pageSize !== "" && e.pageSize !== undefined) {
                this.params.pageSize = e.pageSize;
            }
        }
        else {
            this.currentTab = this.tabIndex;
            this.params.pageIndex = 1;
            this.params.pageSize = 10;
            this.params.order = null;
            this.params.orderBy = null;
            this.params.maDonHang = null;
            this.paramsChange.maDonHang = null;
            this.init = true;
        }
        if(this.tabIndex == 0) {
            this.params.tinhTrangDonHangId = null;
        }
        else if(this.tabIndex == 1) {
            this.params.tinhTrangDonHangId = this.acceptedStatus;
        }
        else if(this.tabIndex == 2) {
            this.params.tinhTrangDonHangId = this.deliveringStatus;
        }
        else if(this.tabIndex == 3) {
            this.params.tinhTrangDonHangId = this.deliveredStatus;
        }
        else if(this.tabIndex == 4) {
            this.params.tinhTrangDonHangId = this.canceledDueDeliveringStatus;
        }
        this.orderManagementService.getAllDonHang(this.params).subscribe({
            next: (res: any) => {
                this.listDonHang = res.data ? res.data : [];
                for(let item of this.listDonHang) {
                    item.stt = this.stt;
                    this.stt++;
                }
                this.stt = 1;
                this.totalRecord = res.totalRecord;
                if(this.init) {
                    this.loaded = Promise.resolve(true);
                    this.init = false;
                }
                this.sortActive = this.params.orderBy;
                this.sortDirection = this.params.order;
            }
        });
    }

    sortData(sort: Sort) {
        this.params.order = sort.direction;
        this.params.orderBy = sort.active;
        this.changeTab();
    }

    viewDonHang(item: any) {
        let dialogRef = this.dialog.open(ViewDetailComponent, {
            width: "100%",
            data: {
                item
            }
        });
        dialogRef.afterClosed().subscribe((res: any) => {
            if(res == "giaoHang") {
                item.tinhTrangDonHangId = this.deliveringStatus;
                for(let index in item.donHangSanPham) {
                    item.donHangSanPham[index].tinhTrangDonHangId = this.deliveringStatus;
                    if(parseInt(index) == item.donHangSanPham.length - 1) {
                        this.orderManagementService.updateTinhTrangDonHang(this.loggedUser.userId, item).subscribe({
                            next: (ress: any) => {
                                if(ress.statusCode == 200) {
                                    this.popUpService.open(MessageConstants.UPDATE_SUCCESS, mType.success);
                                    this.changeTab();
                                    // this.createThongBao(item, 1);
                                }
                                else {
                                    this.popUpService.open(MessageConstants.UPDATE_FAILED, mType.error);
                                }
                            },
                            error: (res: any) => {
                                this.popUpService.open(MessageConstants.UPDATE_FAILED, mType.error);
                            }
                        });
                    }
                }
            }
            else if(res == "daGiaoHang") {
                item.tinhTrangDonHangId = this.deliveredStatus;
                for(let index in item.donHangSanPham) {
                    item.donHangSanPham[index].tinhTrangDonHangId = this.deliveredStatus;
                    if(parseInt(index) == item.donHangSanPham.length - 1) {
                        this.orderManagementService.updateTinhTrangDonHang(this.loggedUser.userId, item).subscribe({
                            next: (ress: any) => {
                                if(ress.statusCode == 200) {
                                    this.popUpService.open(MessageConstants.UPDATE_SUCCESS, mType.success);
                                    this.changeTab();
                                    // this.createThongBao(item, 2);
                                }
                                else {
                                    this.popUpService.open(MessageConstants.UPDATE_FAILED, mType.error);
                                }
                            },
                            error: (res: any) => {
                                this.popUpService.open(MessageConstants.UPDATE_FAILED, mType.error);
                            }
                        });
                    }
                }
            }
            else if(res && res.lyDoHuy) {
                item.tinhTrangDonHangId = this.canceledDueDeliveringStatus;
                for(let index in item.donHangSanPham) {
                    item.donHangSanPham[index].tinhTrangDonHangId = this.canceledDueDeliveringStatus;
                    if(parseInt(index) == item.donHangSanPham.length - 1) {
                        this.orderManagementService.updateTinhTrangDonHang(this.loggedUser.userId, item).subscribe({
                            next: (ress: any) => {
                                if(ress.statusCode == 200) {
                                    this.popUpService.open(MessageConstants.UPDATE_SUCCESS, mType.success);
                                    this.changeTab();
                                    // this.createThongBao(item, 3, res.lyDoHuy);
                                }
                                else {
                                    this.popUpService.open(MessageConstants.UPDATE_FAILED, mType.error);
                                }
                            },
                            error: (ress: any) => {
                                this.popUpService.open(MessageConstants.UPDATE_FAILED, mType.error);
                            }
                        });
                    }
                }
            }
        });
    }

    // createThongBao(donHang: any, type: any, lyDoHuy?: any) {
    //     let item: any = [];
    //     let tieuDeThongBao = "";
    //     if(type == 1) {
    //         tieuDeThongBao = "Đơn hàng đang được vận chuyển";
    //     }
    //     else if(type == 2) {
    //         tieuDeThongBao = "Đơn hàng giao thành công";
    //     }
    //     else if(type == 3) {
    //         tieuDeThongBao = "Đơn hàng giao không thành công";
    //     }
    //     let noiDungThongBao = "Đơn hàng số " + donHang.maDonHang + " bao gồm các sản phẩm: ";
    //     for(let index in donHang.donHangSanPham) {
    //         noiDungThongBao += "<strong>" + donHang.donHangSanPham[index].soLuong + " x " + donHang.donHangSanPham[index].tenSanPham + "</strong>";
    //         if(parseInt(index) == donHang.donHangSanPham.length - 1) {
    //             if(type == 1) {
    //                 noiDungThongBao += " đang được vận chuyển. Chi tiết đơn hàng xem tại <a name=\"notify-link\" href=\"/information/order/detail?maDonHang=" + donHang.maDonHang + "\">đây</a>";
    //             }
    //             else if(type == 2) {
    //                 noiDungThongBao += " đã được vận chuyển thành công. Chi tiết đơn hàng xem tại <a name=\"notify-link\" href=\"/information/order/detail?maDonHang=" + donHang.maDonHang + "\">đây</a>";
    //             }
    //             else if(type == 3) {
    //                 if(lyDoHuy) {
    //                     noiDungThongBao += " đã bị hủy vì giao không thành công. Lý do giao không thành công: " + lyDoHuy;
    //                 }
    //                 else {
    //                     noiDungThongBao += " đã bị hủy vì giao không thành công";
    //                 }
    //             }
    //         }
    //         else {
    //             noiDungThongBao += "<strong>, </strong>";
    //         }
    //     }
    //     let thongBao: any = {
    //         tieuDeThongBao: tieuDeThongBao,
    //         noiDungThongBao: noiDungThongBao,
    //         userId: donHang.userId
    //     }
    //     item.push(thongBao);
    //     this.orderManagementService.createThongBaoForUser(this.loggedUser.userId, item).subscribe({
    //         next: (res: any) => {

    //         }
    //     });
    // }

    toHeadPage() {
        if(this.params.maDonHang != this.paramsChange.maDonHang) {
            this.paginator.firstPage();
            this.paramsChange.maDonHang = this.params.maDonHang;
        }
    }
}
