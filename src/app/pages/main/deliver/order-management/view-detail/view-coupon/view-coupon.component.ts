import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
    selector: 'app-view-coupon',
    templateUrl: './view-coupon.component.html',
    styleUrls: ['./view-coupon.component.scss']
})
export class ViewCouponComponent implements OnInit {

    constructor(@Inject(MAT_DIALOG_DATA) public data: any) { }

    ngOnInit(): void {
    }

}
