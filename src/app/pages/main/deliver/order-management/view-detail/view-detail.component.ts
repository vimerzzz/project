import { Component, Inject, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { GuidConstants } from 'src/app/shared/constants/guid.constants';
import { environment } from 'src/environments/environment';
import { LyDoHuyComponent } from './ly-do-huy/ly-do-huy.component';
import { ViewCouponComponent } from './view-coupon/view-coupon.component';

@Component({
    selector: 'app-view-detail',
    templateUrl: './view-detail.component.html',
    styleUrls: ['./view-detail.component.scss']
})
export class ViewDetailComponent implements OnInit {
    acceptedStatus = GuidConstants.TWO;
    deliveringStatus = GuidConstants.THREE;
    displayedColumns = ["hinhAnh", "tenSanPham", "donGia", "soLuong", "maGiamGia", "tongTien"];
    tongCong: any = 0;

    constructor(private dialogRef: MatDialogRef<ViewDetailComponent>, private dialog: MatDialog,
        @Inject(MAT_DIALOG_DATA) public data: any) { }

    ngOnInit(): void {
        for(let item of this.data.item.donHangSanPham) {
            item.avatar = item.hinhAnhSanPham;
            if(item.avatar) {
                item.avatar = environment.apiUrl + "/api/" + item.avatar.replaceAll("\\", "/");
            }
            item.linkAnimation = false;
            item.giamGiaAnimation = false;
            item.giaGiam = 0;
            if(item.coMaGiamGia) {
                if(item.giamGia * item.soLuong < item.dieuKienGiamGia) {
                    item.giaGiam = 0;
                }
                else {
                    item.giaGiam = item.giamGia * item.soLuong * item.phanTramGiamGia / 100;
                    item.giaGiam = item.giaGiam < item.giamGiaToiDa ? item.giaGiam : item.giamGiaToiDa;
                    item.giaGiam = item.giaGiam < item.giamGia * item.soLuong ? item.giaGiam : item.giamGia * item.soLuong;
                }
            }
        }
    }

    viewMaGiamGia(item: any) {
        this.dialog.open(ViewCouponComponent, {
            width: "32rem",
            data: {
                item
            }
        });
    }

    giaoHang() {
        this.dialogRef.close("giaoHang");
    }

    daGiaoHang() {
        this.dialogRef.close("daGiaoHang");
    }

    huyDonHang() {
        let item = {
            lyDoHuy: ""
        }
        let ref = this.dialog.open(LyDoHuyComponent, {
            width: "32rem",
            data: {
                item
            }
        });
        ref.afterClosed().subscribe((res: any) => {
            if(res) {
                this.dialogRef.close(item);
            }
        });
    }
}
