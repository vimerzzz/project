import { NgModule } from '@angular/core';
import { MaterialModule } from 'src/app/material.module';
import { CreateDeliverComponent } from './create-deliver/create-deliver.component';
import { DeliverRoutingModule } from './deliver-routing.module';
import { DeliverSidenavComponent } from './deliver-sidenav/deliver-sidenav.component';
import { DeliverComponent } from './deliver/deliver.component';
import { OrderManagementComponent } from './order-management/order-management.component';
import { LyDoHuyComponent } from './order-management/view-detail/ly-do-huy/ly-do-huy.component';
import { ViewCouponComponent } from './order-management/view-detail/view-coupon/view-coupon.component';
import { ViewDetailComponent } from './order-management/view-detail/view-detail.component';

@NgModule({
    declarations: [
        DeliverSidenavComponent,
        CreateDeliverComponent,
        DeliverComponent,
        OrderManagementComponent,
        ViewDetailComponent,
        ViewCouponComponent,
        LyDoHuyComponent
    ],
    imports: [
        DeliverRoutingModule,
        MaterialModule
    ]
})
export class DeliverModule { }
