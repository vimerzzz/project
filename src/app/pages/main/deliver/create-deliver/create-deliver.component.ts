import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { BaseAuth } from 'src/app/shared/auth/base.auth';
import { MessageConstants } from 'src/app/shared/constants/message.constants';
import { mType, PopUpService } from 'src/app/shared/services/base/pop-up.service';
import { CreateDeliverService } from 'src/app/shared/services/main/deliver/create-deliver.service';

@Component({
    selector: 'app-create-deliver',
    templateUrl: './create-deliver.component.html',
    styleUrls: ['./create-deliver.component.scss']
})
export class CreateDeliverComponent implements OnInit {
    formControl: FormGroup;
    loggedUser: any;
    spinner: any = false;
    userInfo: any;
    error: any;

    constructor(private base: BaseAuth, private titleService: Title, private formBuilder: FormBuilder, private popUpService: PopUpService, private createDeliverSerivce: CreateDeliverService, private router: Router) { }

    ngOnInit(): void {
        this.titleService.setTitle("Tạo đơn vị vận chuyển mới");
        this.base.checkExpire();
        this.formControl = this.formBuilder.group({
            tenDonVi: ["", Validators.required],
            moTaDonVi: [""],
            dieuKienMienPhi: ["", [Validators.required, Validators.pattern("^[0-9]+$")]],
            phiVanChuyen: ["", [Validators.required, Validators.pattern("^[0-9]+$")]]
        });
        this.getLoggedUser();
    }

    getLoggedUser() {
        let object = localStorage.getItem("loggedUser");
        if (object) {
            this.loggedUser = JSON.parse(object);
            this.getUserInfoByUserId(this.loggedUser.userId);
        }
    }

    getUserInfoByUserId(id: any) {
        this.createDeliverSerivce.getUserInfoByUserId(id).subscribe({
            next: (res: any) => {
                this.userInfo = res;
                this.checkUserInfo();
            }
        });
    }

    checkUserInfo() {
        if (!this.userInfo.emailConfirmed) {
            this.popUpService.open(MessageConstants.NEED_EMAIL_VALIDATION, mType.info);
            this.router.navigateByUrl("/information/setting");
        }
        else {
            this.createDeliverSerivce.getDonViVanChuyenByUserId(this.loggedUser.userId).subscribe({
                next: (res: any) => {
                    if (res) {
                        this.router.navigateByUrl("/deliver/information");
                    }
                }
            });
        }
    }

    get f() {
        return this.formControl.controls;
    }

    createDonVi() {
        this.formControl.markAllAsTouched();
        if(this.formControl.invalid) {
            return;
        }
        else {
            this.spinner = true;
            this.createDeliverSerivce.getDonViVanChuyenByName(this.f.tenDonVi.value).subscribe({
                next: (res: any) => {
                    if(res) {
                        this.popUpService.open(MessageConstants.UPDATE_FAILED, mType.error);
                        this.spinner = false;
                        this.error = MessageConstants.EXISTED_DELIVER;
                    }
                    else {
                        this.error = null;
                        let item: any = {
                            tenDonVi: this.f.tenDonVi.value,
                            moTaDonVi: this.f.moTaDonVi.value,
                            phiVanChuyen: this.f.phiVanChuyen.value,
                            dieuKienMienPhi: this.f.dieuKienMienPhi.value,
                            userId: this.loggedUser.userId
                        }
                        this.createDeliverSerivce.createDonViVanChuyen(item).subscribe({
                            next: (ress: any) => {
                                if(ress.statusCode == 200) {
                                    this.popUpService.open(MessageConstants.CREATE_SUCCESS, mType.success);
                                    this.spinner = false;
                                    this.router.navigateByUrl("/information/deliver");
                                }
                                else {
                                    this.popUpService.open(MessageConstants.CREATE_FAILED, mType.error);
                                    this.spinner = false;
                                }
                            },
                            error: (ress: any) => {
                                this.popUpService.open(MessageConstants.CREATE_FAILED, mType.error);
                                this.spinner = false;
                            }
                        });
                    }
                },
                error: (res: any) => {
                    this.popUpService.open(MessageConstants.SYSTEM_ERROR, mType.error);
                    this.spinner = false;
                }
            });
        }
    }
}
