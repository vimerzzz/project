import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AdminAuth } from "src/app/shared/auth/admin.auth";
import { CustomerAuth } from "src/app/shared/auth/customer.auth";
import { DeliverAuth } from "src/app/shared/auth/deliver.auth";
import { CreateDeliverComponent } from "./create-deliver/create-deliver.component";
import { DeliverSidenavComponent } from "./deliver-sidenav/deliver-sidenav.component";
import { DeliverComponent } from "./deliver/deliver.component";
import { OrderManagementComponent } from "./order-management/order-management.component";

const routes: Routes = [
    {
        path: "",
        children: [
            {
                path: "",
                redirectTo: "/deliver/information",
                pathMatch: "full"
            },
            {
                path: "",
                component: DeliverSidenavComponent,
                children: [
                    {
                        path: "information",
                        component: DeliverComponent,
                        canActivate: [DeliverAuth]
                    },
                    {
                        path: "create-deliver",
                        component: CreateDeliverComponent,
                        canActivate: [DeliverAuth]
                    },
                    {
                        path: "order-management",
                        component: OrderManagementComponent,
                        canActivate: [DeliverAuth]
                    }
                ]
            }
        ]
    }
]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
  })
  export class DeliverRoutingModule { }
