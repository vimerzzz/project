import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DomSanitizer, Title } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { BaseAuth } from 'src/app/shared/auth/base.auth';
import { MessageConstants } from 'src/app/shared/constants/message.constants';
import { ImageCropService } from 'src/app/shared/services/base/image-crop.service';
import { mType, PopUpService } from 'src/app/shared/services/base/pop-up.service';
import { DeliverService } from 'src/app/shared/services/main/deliver/deliver.service';
import { environment } from 'src/environments/environment';

@Component({
    selector: 'app-deliver',
    templateUrl: './deliver.component.html',
    styleUrls: ['./deliver.component.scss']
})
export class DeliverComponent implements OnInit {
    loggedUser: any;
    spinner: any = false;
    userInfo: any;
    avatarImage: any;
    correctType: any = true;
    error: any;
    initialDeliver: any;
    deliver: any;
    loaded: Promise<boolean>;
    formControl: FormGroup;
    @ViewChild("avatarUpload") avatarUpload: ElementRef;

    constructor(private base: BaseAuth, private formBuilder: FormBuilder, private titleService: Title, private sanitizer: DomSanitizer, private imageCropService: ImageCropService, private popUpService: PopUpService, private deliverSerivce: DeliverService, private router: Router) { }

    ngOnInit(): void {
        this.titleService.setTitle("Đơn vị vận chuyển");
        this.base.checkExpire();
        this.formControl = this.formBuilder.group({
            tenDonVi: ["", Validators.required],
            moTaDonVi: [""],
            dieuKienMienPhi: ["", [Validators.required, Validators.pattern("^[0-9]+$")]],
            phiVanChuyen: ["", [Validators.required, Validators.pattern("^[0-9]+$")]]
        });
        this.getLoggedUser();
    }

    getLoggedUser() {
        let object = localStorage.getItem("loggedUser");
        if (object) {
            this.loggedUser = JSON.parse(object);
            this.getUserInfoByUserId(this.loggedUser.userId);
        }
    }

    getUserInfoByUserId(id: any) {
        this.deliverSerivce.getUserInfoByUserId(id).subscribe({
            next: (res: any) => {
                this.userInfo = res;
                this.checkUserInfo();
            }
        });
    }

    checkUserInfo() {
        if (!this.userInfo.emailConfirmed) {
            this.popUpService.open(MessageConstants.NEED_EMAIL_VALIDATION, mType.info);
            this.router.navigateByUrl("/information/setting");
        }
        else {
            this.getDonViByUserId();
        }
    }

    getDonViByUserId() {
        this.deliverSerivce.getDonViVanChuyenByUserId(this.loggedUser.userId).subscribe({
            next: (res: any) => {
                if(res) {
                    this.deliver = res;
                    this.initialDeliver = this.deliver.tenDonVi;
                    this.avatarImage = this.deliver.anhDaiDien;
                    if (this.avatarImage != null) {
                        this.avatarImage = environment.apiUrl + "/api/" + this.avatarImage.replaceAll("\\", "/");
                    }
                    this.loaded = Promise.resolve(true);
                }
                else {
                    this.router.navigateByUrl("/deliver/create-deliver");
                }
            }
        });
    }

    get f() {
        return this.formControl.controls;
    }

    updateDonVi() {
        this.formControl.markAllAsTouched();
        if(this.formControl.invalid) {
            return;
        }
        else {
            this.spinner = true;
            this.deliverSerivce.getDonViVanChuyenByName(this.deliver.tenDonVi).subscribe({
                next: (res: any) => {
                    if(res) {
                        if(res.tenDonVi != this.initialDeliver) {
                            this.popUpService.open(MessageConstants.UPDATE_FAILED, mType.error);
                            this.spinner = false;
                            this.error = MessageConstants.EXISTED_DELIVER;
                        }
                        else {
                            this.error = null;
                            this.deliverSerivce.updateDonViVanChuyen(this.deliver).subscribe({
                                next: (ress: any) => {
                                    if(ress.statusCode == 200) {
                                        this.uploadAnhDaiDien();
                                    }
                                    else {
                                        this.popUpService.open(MessageConstants.UPDATE_FAILED, mType.error);
                                        this.spinner = false;
                                    }
                                },
                                error: (ress: any) => {
                                    this.popUpService.open(MessageConstants.UPDATE_FAILED, mType.error);
                                    this.spinner = false;
                                }
                            });
                        }
                    }
                    else {
                        this.error = null;
                        this.deliverSerivce.updateDonViVanChuyen(this.deliver).subscribe({
                            next: (ress: any) => {
                                if(ress.statusCode == 200) {
                                    this.uploadAnhDaiDien();
                                }
                                else {
                                    this.popUpService.open(MessageConstants.UPDATE_FAILED, mType.error);
                                    this.spinner = false;
                                }
                            },
                            error: (ress: any) => {
                                this.popUpService.open(MessageConstants.UPDATE_FAILED, mType.error);
                                this.spinner = false;
                            }
                        });
                    }
                },
                error: (res: any) => {
                    this.popUpService.open(MessageConstants.SYSTEM_ERROR, mType.error);
                    this.spinner = false;
                }
            });
        }
    }

    uploadAnhDaiDien() {
        let file: File = this.avatarUpload.nativeElement.files[0];
        if (file && this.correctType) {
            let formData = new FormData();
            formData.append('file', file, file.name);
            this.deliverSerivce.uploadAnhDaiDien(this.loggedUser.userId, formData).subscribe({
                next: (res: any) => {
                    if (res.statusCode == 200) {
                        this.popUpService.open(MessageConstants.UPDATE_SUCCESS, mType.success);
                        this.spinner = false;
                        this.getDonViByUserId();
                    }
                    else {
                        this.popUpService.open(MessageConstants.UPDATE_FAILED, mType.error);
                        this.spinner = false;
                    }
                },
                error: (res: any) => {
                    this.popUpService.open(MessageConstants.UPDATE_FAILED, mType.error);
                    this.spinner = false;
                }
            });
        }
        else {
            this.popUpService.open(MessageConstants.UPDATE_SUCCESS, mType.success);
            this.spinner = false;
            this.getDonViByUserId();
        }
    }

    checkFileType() {
        if(this.avatarUpload.nativeElement.files[0].type != "image/png" &&
            this.avatarUpload.nativeElement.files[0].type != "image/jpeg" &&
            this.avatarUpload.nativeElement.files[0].type != "image/webp") {
                this.popUpService.open(MessageConstants.WRONG_FILE_TYPE, mType.warn);
                this.correctType = false;
            }
        else {
            this.correctType = true;
            this.openImageCrop();
        }
    }

    openImageCrop() {
        let reader = new FileReader();
        let files: string[] = [];
        let names: string[] = [];
        reader.onload = (e: any) => {
            files.push(e.target.result);
            names.push(this.avatarUpload.nativeElement.files[0].name);
            this.imageCropService.open(files, 1, 1, 160, names, 1);
            this.imageCropService.image.subscribe((res: any) => {
                if(res == "") {
                    this.avatarUpload.nativeElement.value = "";
                }
                else {
                    let list = new DataTransfer();
                    for(let item of res) {
                        list.items.add(item);
                    }
                    this.avatarUpload.nativeElement.files = list.files;
                    this.displayImage();
                }
            });
        }
        reader.readAsDataURL(this.avatarUpload.nativeElement.files[0]);
    }

    displayImage() {
        let reader = new FileReader();
        reader.onload = (e: any) => {
            this.avatarImage = this.sanitizer.bypassSecurityTrustUrl(e.target.result);
        }
        reader.readAsDataURL(this.avatarUpload.nativeElement.files[0]);
    }
}
