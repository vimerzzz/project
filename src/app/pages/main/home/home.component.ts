import { Component, OnInit, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { MatCheckboxChange } from '@angular/material/checkbox';
import { Title } from '@angular/platform-browser';
import { HubConnection, HubConnectionBuilder } from '@microsoft/signalr';
import { GrayShadow } from 'src/app/shared/animations/box-shadow.animations';
import { ThanhToanButton } from 'src/app/shared/animations/specific.animations';
import { BaseAuth } from 'src/app/shared/auth/base.auth';
import { GuidConstants } from 'src/app/shared/constants/guid.constants';
import { SocketConstants } from 'src/app/shared/constants/socket.constants';
import { HomeService } from 'src/app/shared/services/main/home/home.service';
import { environment } from 'src/environments/environment';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss'],
    animations: [
        GrayShadow
    ]
})
export class HomeComponent implements OnInit {
    listSanPham: any;
    listLoaiSanPham: any;
    listDauGia: any = [];
    listDauGiaKiemDinh: any = [];
    loaded: Promise<boolean>;
    loggedUser: any;
    EXPERT_ROLE_ID: any = GuidConstants.FOUR;
    PENDING_APPROVE_ID: any = GuidConstants.ONE;
    PENDING_AUCTION_ID: any = GuidConstants.FOUR;
    IN_AUCTION_ID: any = GuidConstants.FIVE;
    lastPage: any;
    lastPageDauGia: any;
    lastPageKiemDinh: any;
    init: any = true;
    init1: any = false;
    init2: any = false;
    init3: any = false;
    hub: HubConnection;
    params: any = {
        pageIndex: 1,
        pageSize: 30,
        searchString: null,
        order: "desc",
        orderBy: "createdAt",
        loaiSanPhamId: [],
        conHang: null
    }
    auctionParams: any = {
        pageIndex: 1,
        pageSize: 10,
        searchString: null,
        order: "asc",
        orderBy: "updatedAt",
        loaiSanPhamId: [],
        tinhTrangDauGiaId: []
    }
    approveParams: any = {
        pageIndex: 1,
        pageSize: 10,
        searchString: null,
        order: "asc",
        orderBy: "createdAt",
        loaiSanPhamId: [],
        tinhTrangDauGiaId: []
    }

    constructor(private titleService: Title, private base: BaseAuth, private homeService: HomeService) {}

    ngOnInit(): void {
        this.titleService.setTitle("Trang chủ");
        this.base.checkExpire();
        this.getAllLoaiSanPham();
        this.getAllSanPhamDauGia(1);
        this.getAllSanPhamDauGiaKiemDinh(1);
        this.getAllSanPham(1);
        this.getLoggedUser();
        this.startSocket();
        this.addDauGiaListener();
    }

    getAllSanPham(index: any) {
        this.params.pageIndex = index;
        this.homeService.getAllSanPham(this.params).subscribe({
            next: (res: any) => {
                this.listSanPham = res.data ? res.data : [];
                for(let item of this.listSanPham) {
                    item.avatar = item.hinhAnhSanPham[0].hinhAnh;
                    if (item.avatar != null) {
                        item.avatar = environment.apiUrl + "/api/" + item.avatar.replaceAll("\\", "/");
                    }
                    item.grayShadowAnimation = false;
                }
                this.lastPage = res.totalRecord % this.params.pageSize == 0 ? Math.floor(res.totalRecord / this.params.pageSize) : Math.floor(res.totalRecord / this.params.pageSize) + 1;
                this.init3 = true;
                if(this.init1 && this.init2 && this.init3 && this.init) {
                    this.init = false;
                    this.loaded = Promise.resolve(true);
                }
            }
        });
    }

    keyCapture(event: any) {
        let e = <KeyboardEvent>event;
        this.auctionParams.searchString = this.params.searchString;
        this.approveParams.searchString = this.params.searchString;
        if(e.keyCode === 13) {
            e.preventDefault();
            this.getAllSanPham(1);
            this.getAllSanPhamDauGia(1);
            this.getAllSanPhamDauGiaKiemDinh(1);
        }
    }

    getAllLoaiSanPham() {
        let param: any = {
            order: "asc",
            orderBy: "tenLoaiSanPham"
        }
        this.homeService.getAllLoaiSanPham(param).subscribe({
            next: (res: any) => {
                this.listLoaiSanPham = res.data ? res.data : [];
                let otherId = GuidConstants.ONE;
                let object = this.listLoaiSanPham.find((s: any) => s.id == otherId);
                if(object) {
                    this.listLoaiSanPham.splice(this.listLoaiSanPham.indexOf(object), 1);
                    this.listLoaiSanPham.push(object);
                }
            }
        });
    }

    getLoggedUser() {
        let object = localStorage.getItem("loggedUser");
        if (object) {
            this.loggedUser = JSON.parse(object);
        }
    }

    getAllSanPhamDauGia(index: any) {
        this.auctionParams.pageIndex = index;
        this.auctionParams.tinhTrangDauGiaId = [this.PENDING_AUCTION_ID];
        this.auctionParams.orderBy = "thoiGianBatDau";
        this.homeService.getAllSanPhamDauGia(this.auctionParams).subscribe({
            next: (res: any) => {
                this.listDauGia = res.data ? res.data : [];
                this.auctionParams.tinhTrangDauGiaId = [this.IN_AUCTION_ID];
                this.auctionParams.orderBy = "thoiGianKetThuc";
                this.homeService.getAllSanPhamDauGia(this.auctionParams).subscribe({
                    next: (res: any) => {
                        this.listDauGia = res.data ? [...this.listDauGia, ...res.data] : [...this.listDauGia];
                        for(let item of this.listDauGia) {
                            item.avatar = item.hinhAnhSanPhamDauGia[0].hinhAnh;
                            if (item.avatar != null) {
                                item.avatar = environment.apiUrl + "/api/" + item.avatar.replaceAll("\\", "/");
                            }
                            if(item.nguoiRaGia) {
                                if((item.nguoiRaGia.userInfo.ho && item.nguoiRaGia.userInfo.ho.trim()) &&
                                    ((item.nguoiRaGia.userInfo.tenDem && item.nguoiRaGia.userInfo.tenDem.trim()) ||
                                    (item.nguoiRaGia.userInfo.ten && item.nguoiRaGia.userInfo.ten.trim()))) {
                                        item.nguoiRaGia.name = "";
                                        if(item.nguoiRaGia.userInfo.ho && item.nguoiRaGia.userInfo.ho.trim()) {
                                            item.nguoiRaGia.name += item.nguoiRaGia.userInfo.ho + " ";
                                        }
                                        if(item.nguoiRaGia.userInfo.tenDem && item.nguoiRaGia.userInfo.tenDem.trim()) {
                                            item.nguoiRaGia.name += item.nguoiRaGia.userInfo.tenDem + " ";
                                        }
                                        if(item.nguoiRaGia.userInfo.ten && item.nguoiRaGia.userInfo.ten.trim()) {
                                            item.nguoiRaGia.name += item.nguoiRaGia.userInfo.ten + " ";
                                        }
                                }
                                else {
                                    item.nguoiRaGia.name = item.nguoiRaGia.userName;
                                }
                            }
                        }
                        this.lastPageDauGia = res.totalRecord % this.auctionParams.pageSize == 0 ? Math.floor(res.totalRecord / this.auctionParams.pageSize) : Math.floor(res.totalRecord / this.auctionParams.pageSize) + 1;
                        this.init1 = true;
                        if(this.init1 && this.init2 && this.init3 && this.init) {
                            this.init = false;
                            this.loaded = Promise.resolve(true);
                        }
                        this.getTimeNow();
                    }
                });
            }
        });
    }

    getAllSanPhamDauGiaKiemDinh(index: any) {
        this.approveParams.pageIndex = index;
        this.approveParams.tinhTrangDauGiaId = [this.PENDING_APPROVE_ID];
        this.homeService.getAllSanPhamDauGia(this.approveParams).subscribe({
            next: (res: any) => {
                this.listDauGiaKiemDinh = res.data ? res.data : [];
                for(let item of this.listDauGiaKiemDinh) {
                    item.avatar = item.hinhAnhSanPhamDauGia[0].hinhAnh;
                    if (item.avatar != null) {
                        item.avatar = environment.apiUrl + "/api/" + item.avatar.replaceAll("\\", "/");
                    }
                }
                this.lastPageKiemDinh = res.totalRecord % this.approveParams.pageSize == 0 ? Math.floor(res.totalRecord / this.approveParams.pageSize) : Math.floor(res.totalRecord / this.approveParams.pageSize) + 1;
                this.init2 = true;
                if(this.init1 && this.init2 && this.init3 && this.init) {
                    this.init = false;
                    this.loaded = Promise.resolve(true);
                }
                this.getTimeNow();
            }
        });
    }

    setAllTinhTrang() {
        if(this.params.conHang != null) {
            this.params.conHang = null;
            this.getAllSanPham(1);
        }
    }

    setAllLoaiSanPham() {
        if(this.params.loaiSanPhamId.length) {
            this.params.loaiSanPhamId = [];
            this.getAllSanPham(1);
        }
        if(this.approveParams.loaiSanPhamId.length) {
            this.approveParams.loaiSanPhamId = [];
            this.getAllSanPhamDauGiaKiemDinh(1);
        }
        if(this.auctionParams.loaiSanPhamId.length) {
            this.auctionParams.loaiSanPhamId = [];
            this.getAllSanPhamDauGia(1);
        }
    }

    changeConHang(status: any) {
        if(this.params.conHang == null) {
            this.params.conHang = status;
        }
        else {
            this.params.conHang = null;
        }
        this.getAllSanPham(1);
    }

    changeLoaiSanPham(loaiSanPhamId: any) {
        let index = this.params.loaiSanPhamId.indexOf(loaiSanPhamId);
        if(index == -1) {
            this.params.loaiSanPhamId.push(loaiSanPhamId);
        }
        else {
            this.params.loaiSanPhamId.splice(index, 1);
        }
        index = this.auctionParams.loaiSanPhamId.indexOf(loaiSanPhamId);
        if(index == -1) {
            this.auctionParams.loaiSanPhamId.push(loaiSanPhamId);
        }
        else {
            this.auctionParams.loaiSanPhamId.splice(index, 1);
        }
        index = this.approveParams.loaiSanPhamId.indexOf(loaiSanPhamId);
        if(index == -1) {
            this.approveParams.loaiSanPhamId.push(loaiSanPhamId);
        }
        else {
            this.approveParams.loaiSanPhamId.splice(index, 1);
        }
        this.getAllSanPham(1);
        this.getAllSanPhamDauGia(1);
        this.getAllSanPhamDauGiaKiemDinh(1);
    }

    getTimeNow() {
        let now = new Date().getTime();
        for(let obj of this.listDauGiaKiemDinh) {
            let tmp = new Date(obj.createdAt);
            tmp.setDate(tmp.getDate() + 3);
            let time = tmp.getTime();
            if(time > now) {
                let seconds = Math.floor((time - now) / 1000);
                let minutes = Math.floor(seconds / 60);
                let hours = Math.floor(minutes / 60);
                let days = Math.floor(hours / 24);
                seconds = seconds % 60;
                minutes = minutes % 60;
                hours = hours % 24;
                if(days == 0) {
                    obj.timeLeft = `${hours.toString().padStart(2, '0')}:${minutes.toString().padStart(2, '0')}:${seconds.toString().padStart(2, '0')}`;
                }
                else {
                    obj.timeLeft = `${days.toString().padStart(2, '0')}:${hours.toString().padStart(2, '0')}:${minutes.toString().padStart(2, '0')}:${seconds.toString().padStart(2, '0')}`;
                }
            }
            else {
                obj.timeLeft = "00:00:00";
            }
        }
        for(let obj of this.listDauGia) {
            let tmp = new Date();
            if(obj.tinhTrangDauGiaId == this.PENDING_AUCTION_ID) {
                tmp = new Date(obj.thoiGianBatDau);
            }
            else if(obj.tinhTrangDauGiaId == this.IN_AUCTION_ID) {
                tmp = new Date(obj.thoiGianKetThuc);
            }
            let time = tmp.getTime();
            if(time > now) {
                let seconds = Math.floor((time - now) / 1000);
                let minutes = Math.floor(seconds / 60);
                let hours = Math.floor(minutes / 60);
                let days = Math.floor(hours / 24);
                seconds = seconds % 60;
                minutes = minutes % 60;
                hours = hours % 24;
                if(days == 0) {
                    obj.timeLeft = `${hours.toString().padStart(2, '0')}:${minutes.toString().padStart(2, '0')}:${seconds.toString().padStart(2, '0')}`;
                }
                else {
                    obj.timeLeft = `${days.toString().padStart(2, '0')}:${hours.toString().padStart(2, '0')}:${minutes.toString().padStart(2, '0')}:${seconds.toString().padStart(2, '0')}`;
                }
            }
            else {
                obj.timeLeft = "00:00:00";
            }
        }
        setTimeout(() => this.getTimeNow(), 1000);
    }

    startSocket() {
        this.hub = new HubConnectionBuilder().withUrl(SocketConstants.dauGiaHub).build();
        this.hub.start();
    }

    addDauGiaListener() {
        this.hub.on("sanPhamDauGia", (res: any) => {
            if(res) {
                let sanPham = this.listDauGiaKiemDinh.find((s: any) => s.id == res.id);
                sanPham = sanPham ? sanPham : this.listDauGia.find((s: any) => s.id == res.id);
                if(sanPham) {
                    sanPham.tinhTrangDauGiaId = res.tinhTrangDauGiaId;
                    sanPham.tinhTrang = res.tinhTrang;
                }
            }
        });
    }
}
