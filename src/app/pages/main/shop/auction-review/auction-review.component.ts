import { Component, ElementRef, OnInit, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { HubConnection, HubConnectionBuilder } from '@microsoft/signalr';
import { PrimaryButton, SuccessButton, WarnButton } from 'src/app/shared/animations/button-fade.animations';
import { NewMessageButton } from 'src/app/shared/animations/specific.animations';
import { SuccessColor } from 'src/app/shared/animations/text-color.animations';
import { BaseAuth } from 'src/app/shared/auth/base.auth';
import { ReportComponent } from 'src/app/shared/components/report/report.component';
import { GuidConstants } from 'src/app/shared/constants/guid.constants';
import { MessageConstants } from 'src/app/shared/constants/message.constants';
import { SocketConstants } from 'src/app/shared/constants/socket.constants';
import { mType, PopUpService } from 'src/app/shared/services/base/pop-up.service';
import { ViewImageService } from 'src/app/shared/services/base/view-image.service';
import { AuctionReviewService } from 'src/app/shared/services/main/shop/auction-review.service';
import { ResetChat, ResetGioHang } from 'src/app/shared/utilities/reset-status.utility';
import { environment } from 'src/environments/environment';

@Component({
    selector: 'app-auction-review',
    templateUrl: './auction-review.component.html',
    styleUrls: ['./auction-review.component.scss'],
    animations: [
        WarnButton,
        PrimaryButton,
        SuccessColor,
        SuccessButton,
        NewMessageButton
    ]
})
export class AuctionReviewComponent implements OnInit {
    shop: any;
    user: any;
    listChat: any;
    loaded: Promise<boolean>;
    chatLoaded: Promise<boolean>;
    avatarImage: any;
    sanPham: any;
    loggedUser: any;
    index: any = 0;
    shopButtonAnimation: any = false;
    chatButtonAnimation: any = false;
    raGiaButtonAnimation: any = false;
    yesButtonAnimation: any = false;
    noButtonAnimation: any = false;
    yesEndAnimation: any = false;
    noEndAnimation: any = false;
    endButtonAnimation: any = false;
    newMessageAnimation: any = false;
    reportButtonAnimation:any = false;
    areYouSure: any = false;
    endSure: any = false;
    linkAnimation: any = false;
    userLinkAnimation: any = false;
    hub: HubConnection;
    expertRoleGuid = GuidConstants.FOUR;
    pendingApproveGuid = GuidConstants.ONE;
    unApproveGuid = GuidConstants.TWO;
    overdueApproveGuid = GuidConstants.THREE;
    pendingAuctionGuid = GuidConstants.FOUR;
    inAuctionGuid = GuidConstants.FIVE;
    successAuctionGuid = GuidConstants.SIX;
    failedAuctionGuid = GuidConstants.SEVEN;
    successPayAuctionGuid = GuidConstants.NINE;
    price: any;
    formControl: FormGroup;
    contentChat: any;
    selection: any = 2;
    blockGetMore: any = false;
    messageCount: any = 0;
    yeuCauKetThuc: any = false;
    params: any = {
        pageIndex: 1,
        pageSize: 30,
        sanPhamDauGiaId: null
    }
    @ViewChild("messageField") messageField: ElementRef;
    @ViewChildren("message") message: QueryList<ElementRef>;

    constructor(private formBuilder: FormBuilder, private titleService: Title, private base: BaseAuth, private dialog: MatDialog, private resetChat: ResetChat, private resetGioHang: ResetGioHang, private popUpService: PopUpService, private viewImageService: ViewImageService, private router: Router, private activeRouter: ActivatedRoute, private auctionReviewService: AuctionReviewService) {}

    ngOnInit(): void {
        this.titleService.setTitle("Đấu giá");
        this.base.checkExpire();
        this.checkShop();
        this.startSocket();
        this.addDauGiaListener();
        this.addDauGiaChatListener();
        this.formControl = this.formBuilder.group({
            price: ["", Validators.required]
        });
    }

    checkShop() {
        this.activeRouter.queryParams.subscribe((params) => {
            if(params.shopName) {
                this.auctionReviewService.getShopByShopName(params.shopName).subscribe({
                    next: (res: any) => {
                        if(res && res.userHoatDong && res.hoatDong) {
                            this.shop = res;
                            this.avatarImage = this.shop.anhDaiDien;
                            if (this.avatarImage != null) {
                                this.avatarImage = environment.apiUrl + "/api/" + this.avatarImage.replaceAll("\\", "/");
                            }
                            this.getLoggedUser();
                        }
                        else {
                            this.shop = null;
                            this.sanPham = null;
                        }
                    }
                });
            }
            else {
                this.router.navigateByUrl("/shop/my-shop/item-management");
            }
        });
    }

    getLoggedUser() {
        let object = localStorage.getItem("loggedUser");
        if (object) {
            this.loggedUser = JSON.parse(object);
            this.getUserById(this.loggedUser.userId);
        }
        this.checkSanPham();
    }

    getUserById(id: any) {
        this.auctionReviewService.getUserById(id).subscribe({
            next: (res: any) => {
                this.user = res;
                this.getUserInfoByUserId(id);
            }
        });
    }

    getUserInfoByUserId(userId: any) {
        this.auctionReviewService.getUserInfoByUserId(userId).subscribe({
            next: (res: any) => {
                this.user.userInfo = res;
                if(this.user.userInfo.anhDaiDien != null) {
                    this.user.userInfo.anhDaiDien = environment.apiUrl + "/api/" + this.user.userInfo.anhDaiDien.replaceAll("\\", "/");
                }
                if((this.user.userInfo.ho && this.user.userInfo.ho.trim()) &&
                    ((this.user.userInfo.tenDem && this.user.userInfo.tenDem.trim()) ||
                    (this.user.userInfo.ten && this.user.userInfo.ten.trim()))) {
                        this.user.name = "";
                        if(this.user.userInfo.ho && this.user.userInfo.ho.trim()) {
                            this.user.name += this.user.userInfo.ho + " ";
                        }
                        if(this.user.userInfo.tenDem && this.user.userInfo.tenDem.trim()) {
                            this.user.name += this.user.userInfo.tenDem + " ";
                        }
                        if(this.user.userInfo.ten && this.user.userInfo.ten.trim()) {
                            this.user.name += this.user.userInfo.ten + " ";
                        }
                }
                else {
                    this.user.name = this.user.userName;
                }
            }
        });
    }

    checkSanPham() {
        this.activeRouter.queryParams.subscribe((params) => {
            if(params.shopName && params.itemName) {
                this.auctionReviewService.getSanPhamDauGiaWithHinhAnhByName(params.shopName, params.itemName).subscribe({
                    next: (res: any) => {
                        if(res && [this.pendingApproveGuid, this.pendingAuctionGuid, this.inAuctionGuid, this.successPayAuctionGuid, this.successAuctionGuid, this.failedAuctionGuid].includes(res.tinhTrangDauGiaId)) {
                            this.sanPham = res;
                            for(let item of this.sanPham.hinhAnhSanPhamDauGia) {
                                if(item.hinhAnh) {
                                    item.hinhAnh = environment.apiUrl + "/api/" + item.hinhAnh.replaceAll("\\", "/");
                                }
                            }
                            if(this.sanPham.nguoiRaGia) {
                                if(this.sanPham.nguoiRaGia.userInfo.anhDaiDien != null) {
                                    this.sanPham.nguoiRaGia.userInfo.anhDaiDien = environment.apiUrl + "/api/" + this.sanPham.nguoiRaGia.userInfo.anhDaiDien.replaceAll("\\", "/");
                                }
                                if((this.sanPham.nguoiRaGia.userInfo.ho && this.sanPham.nguoiRaGia.userInfo.ho.trim()) &&
                                    ((this.sanPham.nguoiRaGia.userInfo.tenDem && this.sanPham.nguoiRaGia.userInfo.tenDem.trim()) ||
                                    (this.sanPham.nguoiRaGia.userInfo.ten && this.sanPham.nguoiRaGia.userInfo.ten.trim()))) {
                                        this.sanPham.nguoiRaGia.name = "";
                                        if(this.sanPham.nguoiRaGia.userInfo.ho && this.sanPham.nguoiRaGia.userInfo.ho.trim()) {
                                            this.sanPham.nguoiRaGia.name += this.sanPham.nguoiRaGia.userInfo.ho + " ";
                                        }
                                        if(this.sanPham.nguoiRaGia.userInfo.tenDem && this.sanPham.nguoiRaGia.userInfo.tenDem.trim()) {
                                            this.sanPham.nguoiRaGia.name += this.sanPham.nguoiRaGia.userInfo.tenDem + " ";
                                        }
                                        if(this.sanPham.nguoiRaGia.userInfo.ten && this.sanPham.nguoiRaGia.userInfo.ten.trim()) {
                                            this.sanPham.nguoiRaGia.name += this.sanPham.nguoiRaGia.userInfo.ten + " ";
                                        }
                                }
                                else {
                                    this.sanPham.nguoiRaGia.name = this.sanPham.nguoiRaGia.userName;
                                }
                            }
                            this.sanPham.minPrice = this.sanPham.giaHienTai >= this.sanPham.giaKhoiDiem ? this.sanPham.giaHienTai : this.sanPham.giaKhoiDiem;
                            this.sanPham.minPrice += this.sanPham.khoangGiaToiThieu;
                            this.f.price.setValidators([Validators.required, Validators.min(this.sanPham.minPrice)]);
                            this.titleService.setTitle("Đấu giá | " + this.sanPham.tenSanPham);
                            this.params.sanPhamDauGiaId = this.sanPham.id;
                            this.getAllTroChuyenDauGia();
                            this.getTimeNow();
                        }
                        else {
                            this.titleService.setTitle("Đấu giá");
                            this.sanPham = null;
                        }
                        this.loaded = Promise.resolve(true);
                    }
                });
            }
        });
    }

    getAllTroChuyenDauGia() {
        this.auctionReviewService.getAllTroChuyenDauGia(this.params).subscribe({
            next: (res: any) => {
                this.listChat = res.data ? res.data.reverse() : [];
                for(let item of this.listChat) {
                    if(!item.isSystem && !item.isShopOwner) {
                        if(item.user.userInfo.anhDaiDien != null) {
                            item.user.userInfo.anhDaiDien = environment.apiUrl + "/api/" + item.user.userInfo.anhDaiDien.replaceAll("\\", "/");
                        }
                        if((item.user.userInfo.ho && item.user.userInfo.ho.trim()) &&
                            ((item.user.userInfo.tenDem && item.user.userInfo.tenDem.trim()) ||
                            (item.user.userInfo.ten && item.user.userInfo.ten.trim()))) {
                                item.user.name = "";
                                if(item.user.userInfo.ho && item.user.userInfo.ho.trim()) {
                                    item.user.name += item.user.userInfo.ho + " ";
                                }
                                if(item.user.userInfo.tenDem && item.user.userInfo.tenDem.trim()) {
                                    item.user.name += item.user.userInfo.tenDem + " ";
                                }
                                if(item.user.userInfo.ten && item.user.userInfo.ten.trim()) {
                                    item.user.name += item.user.userInfo.ten + " ";
                                }
                        }
                        else {
                            item.user.name = item.user.userName;
                        }
                    }
                    item.clickMenu = false;
                }
                this.chatLoaded = Promise.resolve(true);
                setTimeout(() => {
                    let element = this.message.toArray()[this.message.length - 1];
                    element.nativeElement.parentNode.scrollTop = element.nativeElement.offsetTop;
                }, 1);
            }
        });
    }

    getTimeNow() {
        let now = new Date().getTime();
        let tmp = new Date();
        if(this.sanPham.tinhTrangDauGiaId == this.pendingApproveGuid) {
            tmp = new Date(this.sanPham.createdAt);
            tmp.setDate(tmp.getDate() + 3);
        }
        else if(this.sanPham.tinhTrangDauGiaId == this.pendingAuctionGuid) {
            tmp = new Date(this.sanPham.thoiGianBatDau);
        }
        else if(this.sanPham.tinhTrangDauGiaId == this.inAuctionGuid) {
            tmp = new Date(this.sanPham.thoiGianKetThuc);
        }
        let time = tmp.getTime();
        if(time > now) {
            let seconds = Math.floor((time - now) / 1000);
            let minutes = Math.floor(seconds / 60);
            let hours = Math.floor(minutes / 60);
            let days = Math.floor(hours / 24);
            seconds = seconds % 60;
            minutes = minutes % 60;
            hours = hours % 24;
            if(days == 0) {
                this.sanPham.timeLeft = `${hours.toString().padStart(2, '0')}:${minutes.toString().padStart(2, '0')}:${seconds.toString().padStart(2, '0')}`;
            }
            else {
                this.sanPham.timeLeft = `${days.toString().padStart(2, '0')}:${hours.toString().padStart(2, '0')}:${minutes.toString().padStart(2, '0')}:${seconds.toString().padStart(2, '0')}`;
            }
        }
        else {
            this.sanPham.timeLeft = "00:00:00";
        }
        setTimeout(() => this.getTimeNow(), 1000);
    }

    keyCapture(event: any) {
        let e = <KeyboardEvent>event;
        if(e.keyCode === 13) {
            e.preventDefault();
            this.createTroChuyenDauGia();
        }
    }

    createTroChuyenDauGia() {
        if(this.loggedUser) {
            if(this.contentChat && this.contentChat.trim()) {
                let item: any = {
                    sanPhamDauGiaId: this.sanPham.id,
                    userId: this.selection == 1 ? null : this.loggedUser.userId,
                    isShopOwner: this.selection == 1 ? true : false,
                    noiDung: this.contentChat.trim(),
                    halfOpacity: true,
                    errorSending: false,
                    user: this.user,
                    createdAt: new Date()
                }
                this.contentChat = "";
                this.listChat.push(item);
                this.returnToNewest();
                this.auctionReviewService.createTroChuyenDauGia(this.loggedUser.userId, item).subscribe({
                    next: (res: any) => {
                        if(res.statusCode == 200) {
                            item.halfOpacity = false;
                            this.messageCount++;
                        }
                        else {
                            this.popUpService.open(MessageConstants.SYSTEM_ERROR, mType.error);
                            item.halfOpacity = false;
                            item.errorSending = true;
                        }
                    },
                    error: (res: any) => {
                        this.popUpService.open(MessageConstants.SYSTEM_ERROR, mType.error);
                        item.halfOpacity = false;
                        item.errorSending = true;
                    }
                });
            }
        }
        else {
            this.router.navigateByUrl("/error/unauthorized");
        }
    }

    getMoreTroChuyenDauGia() {
        if(!this.blockGetMore) {
            let messageElement = this.message.toArray()[1];
            let elementTop = messageElement.nativeElement.offsetTop;
            let fieldTop = this.messageField.nativeElement.scrollTop;
            if(elementTop >= fieldTop) {
                this.blockGetMore = true;
                this.params.pageIndex += 1 + (this.messageCount - this.messageCount % this.params.pageSize) / this.params.pageSize;
                this.auctionReviewService.getAllTroChuyenDauGia(this.params).subscribe({
                    next: (res: any) => {
                        if(res.data && res.data.length) {
                            for(let index in res.data) {
                                if(!res.data[index].isSystem && !res.data[index].isShopOwner) {
                                    if(res.data[index].user.userInfo.anhDaiDien != null) {
                                        res.data[index].user.userInfo.anhDaiDien = environment.apiUrl + "/api/" + res.data[index].user.userInfo.anhDaiDien.replaceAll("\\", "/");
                                    }
                                    if((res.data[index].user.userInfo.ho && res.data[index].user.userInfo.ho.trim()) &&
                                        ((res.data[index].user.userInfo.tenDem && res.data[index].user.userInfo.tenDem.trim()) ||
                                        (res.data[index].user.userInfo.ten && res.data[index].user.userInfo.ten.trim()))) {
                                            res.data[index].user.name = "";
                                            if(res.data[index].user.userInfo.ho && res.data[index].user.userInfo.ho.trim()) {
                                                res.data[index].user.name += res.data[index].user.userInfo.ho + " ";
                                            }
                                            if(res.data[index].user.userInfo.tenDem && res.data[index].user.userInfo.tenDem.trim()) {
                                                res.data[index].user.name += res.data[index].user.userInfo.tenDem + " ";
                                            }
                                            if(res.data[index].user.userInfo.ten && res.data[index].user.userInfo.ten.trim()) {
                                                res.data[index].user.name += res.data[index].user.userInfo.ten + " ";
                                            }
                                    }
                                    else {
                                        res.data[index].user.name = res.data[index].user.userName;
                                    }
                                    res.data[index].clickMenu = false;
                                }
                                if(!this.listChat.find((s: any) => s.id == res.data[index].id)) {
                                    this.listChat.unshift(res.data[index]);
                                }
                                if(parseInt(index) == res.data.length - 1) {
                                    this.messageCount = 0;
                                    this.blockGetMore = false;
                                }
                            }
                        }
                        else {
                            this.blockGetMore = true;
                        }
                    },
                    error: (res: any) => {
                        this.blockGetMore = false;
                    }
                });
            }
        }
        setTimeout(() => {
            let messageElements = this.message.toArray();
            let length = this.message.length;
            let messageElement = messageElements[length - 2];
            if(length == 1) {
                messageElement = messageElements[length - 1];
            }
            let elementTop = messageElement.nativeElement.offsetTop;
            let elementBottom = elementTop + messageElement.nativeElement.clientHeight;
            let fieldTop = this.messageField.nativeElement.scrollTop;
            let fieldBottom = fieldTop + this.messageField.nativeElement.clientHeight;
            if(elementBottom <= fieldBottom) {
                this.newMessageAnimation = false;
            }
        }, 10);
    }

    openDialogBaoCao(userId: any, shopId: any, name: any) {
        let item: any = {
            nguoiBaoCaoId: this.loggedUser.userId,
            userId: userId,
            shopId: shopId,
            lyDoBaoCao: null,
            noiGuiBaoCao: this.router.url
        }
        let dialogRef = this.dialog.open(ReportComponent, {
            width: "48rem",
            data: {
                name: name,
                item: item
            }
        });
        dialogRef.afterClosed().subscribe((res: any) => {
            if(res) {
                this.auctionReviewService.createBaoCao(this.loggedUser.userId, item).subscribe({
                    next: (ress: any) => {
                        if(ress.statusCode == 200) {

                        }
                        else {
                            this.popUpService.open(MessageConstants.SYSTEM_ERROR, mType.error);
                        }
                    },
                    error: (ress: any) => {
                        this.popUpService.open(MessageConstants.SYSTEM_ERROR, mType.error);
                    }
                });
            }
        });
    }

    openQuestion() {
        if(this.loggedUser) {
            this.formControl.markAllAsTouched();
            if (this.formControl.invalid) {
                return;
            }
            else {
                this.areYouSure = true;
            }
        }
        else {
            this.router.navigateByUrl("/error/unauthorized");
        }
    }

    closeQuestion() {
        this.areYouSure = false;
        this.yesButtonAnimation = false;
        this.noButtonAnimation = false;
    }

    updateTinhHinhDauGia() {
        this.closeQuestion();
        this.sanPham.giaHienTai = this.price;
        this.sanPham.userId = this.loggedUser.userId;
        this.auctionReviewService.updateTinhHinhDauGia(this.sanPham).subscribe({
            next: (res: any) => {
                if(res.statusCode == 200) {

                }
                else {
                    this.popUpService.open(MessageConstants.SYSTEM_ERROR, mType.error);
                }
            },
            error: (res: any) => {
                this.popUpService.open(MessageConstants.SYSTEM_ERROR, mType.error);
            }
        });
    }

    endAuction() {
        if(this.yeuCauKetThuc) {
            this.yeuCauKetThucDauGia();
            this.yeuCauKetThuc = false;
        }
        else {
            if(this.sanPham.nguoiRaGia) {
                this.updateTinhTrangDauGia(this.successAuctionGuid);
            }
            else {
                this.updateTinhTrangDauGia(this.failedAuctionGuid);
            }
        }
    }

    yeuCauKetThucDauGia() {
        this.sanPham.yeuCauKetThuc = true;
        this.auctionReviewService.yeuCauKetThucDauGia(this.sanPham).subscribe({
            next: (res: any) => {
                if(res.statusCode == 200) {

                }
                else {
                    this.popUpService.open(MessageConstants.SYSTEM_ERROR, mType.error);
                }
            },
            error: (res: any) => {
                this.popUpService.open(MessageConstants.SYSTEM_ERROR, mType.error);
            }
        });
    }

    updateTinhTrangDauGia(id: any) {
        let item: any = {
            id: this.sanPham.id,
            shopId: this.sanPham.shopId,
            tinhTrangDauGiaid: id
        }
        this.auctionReviewService.updateTinhTrangDauGia(item).subscribe({
            next: (res: any) => {
                if(res.statusCode == 200) {
                    this.popUpService.open(MessageConstants.UPDATE_SUCCESS, mType.success);
                }
                else {
                    this.popUpService.open(MessageConstants.UPDATE_FAILED, mType.error);
                }
            },
            error: (res: any) => {
                this.popUpService.open(MessageConstants.SYSTEM_ERROR, mType.error);
            }
        });
    }

    toLeft() {
        this.index = this.index == 0 ? 0 : this.index - 1;
        this.smooth();
    }

    toRight() {
        this.index = this.index == this.sanPham.hinhAnhSanPhamDauGia.length - 1 ? this.sanPham.hinhAnhSanPhamDauGia.length - 1 : this.index + 1;
        this.smooth();
    }

    toIndex(index: any) {
        this.index = index;
        this.smooth();
    }

    smooth() {
        setTimeout(() => {
            document.getElementById("thumbnailPanel")!.scrollIntoView({
                behavior: 'smooth',
                block: 'end',
                inline: 'nearest'
            });
        }, 1);
    }

    openImage(imageSource: string) {
        this.viewImageService.open(imageSource);
    }

    openChat() {
        if(this.loggedUser) {
            this.resetChat.setUserShopChatStatus(true);
            this.resetChat.setChatStatus(true);
            this.resetChat.setChatShopId(this.shop.id);
        }
        else {
            this.router.navigateByUrl("/error/unauthorized");
        }
    }

    openChatUser(userId: any) {
        if(this.loggedUser) {
            if(this.loggedUser.userId != userId) {
                this.resetChat.setUserUserChatStatus(true);
                this.resetChat.setChatStatus(true);
                this.resetChat.setChatUserId(userId);
            }
        }
        else {
            this.router.navigateByUrl("/error/unauthorized");
        }
    }

    get f() {
        return this.formControl.controls;
    }

    startSocket() {
        this.hub = new HubConnectionBuilder().withUrl(SocketConstants.dauGiaHub).build();
        this.hub.start();
    }

    addDauGiaListener() {
        this.hub.on("sanPhamDauGia", (res: any) => {
            if(res) {
                if(res.id == this.sanPham.id) {
                    this.sanPham.tinhTrang = res.tinhTrang;
                    this.sanPham.tinhTrangDauGiaId = res.tinhTrangDauGiaId;
                    this.sanPham.giaHienTai = res.giaHienTai;
                    this.sanPham.nguoiRaGia = res.nguoiRaGia;
                    this.sanPham.userId = res.userId;
                    this.sanPham.yeuCauKetThuc = res.yeuCauKetThuc;
                    this.sanPham.minPrice = this.sanPham.giaHienTai >= this.sanPham.giaKhoiDiem ? this.sanPham.giaHienTai : this.sanPham.giaKhoiDiem;
                    this.sanPham.minPrice += this.sanPham.khoangGiaToiThieu;
                    this.f.price.setValidators([Validators.required, Validators.min(this.sanPham.minPrice)]);
                    this.f.price.markAsTouched();
                    this.f.price.updateValueAndValidity();
                    if(this.sanPham.nguoiRaGia) {
                        if(this.sanPham.nguoiRaGia.userInfo.anhDaiDien != null) {
                            this.sanPham.nguoiRaGia.userInfo.anhDaiDien = environment.apiUrl + "/api/" + this.sanPham.nguoiRaGia.userInfo.anhDaiDien.replaceAll("\\", "/");
                        }
                        if((this.sanPham.nguoiRaGia.userInfo.ho && this.sanPham.nguoiRaGia.userInfo.ho.trim()) &&
                            ((this.sanPham.nguoiRaGia.userInfo.tenDem && this.sanPham.nguoiRaGia.userInfo.tenDem.trim()) ||
                            (this.sanPham.nguoiRaGia.userInfo.ten && this.sanPham.nguoiRaGia.userInfo.ten.trim()))) {
                                this.sanPham.nguoiRaGia.name = "";
                                if(this.sanPham.nguoiRaGia.userInfo.ho && this.sanPham.nguoiRaGia.userInfo.ho.trim()) {
                                    this.sanPham.nguoiRaGia.name += this.sanPham.nguoiRaGia.userInfo.ho + " ";
                                }
                                if(this.sanPham.nguoiRaGia.userInfo.tenDem && this.sanPham.nguoiRaGia.userInfo.tenDem.trim()) {
                                    this.sanPham.nguoiRaGia.name += this.sanPham.nguoiRaGia.userInfo.tenDem + " ";
                                }
                                if(this.sanPham.nguoiRaGia.userInfo.ten && this.sanPham.nguoiRaGia.userInfo.ten.trim()) {
                                    this.sanPham.nguoiRaGia.name += this.sanPham.nguoiRaGia.userInfo.ten + " ";
                                }
                        }
                        else {
                            this.sanPham.nguoiRaGia.name = this.sanPham.nguoiRaGia.userName;
                        }
                    }
                    if(this.sanPham.tinhTrangDauGiaId == this.successAuctionGuid && this.sanPham.userId && this.loggedUser && this.sanPham.userId == this.loggedUser.userId) {
                        let count = this.resetGioHang.getGioHangCount();
                        this.resetGioHang.setGioHangCount(count + 1);
                    }
                }
            }
        });
    }

    addDauGiaChatListener() {
        this.hub.on("dauGiaChat", (res: any) => {
            if(res) {
                if(res.sanPhamDauGiaId == this.sanPham.id) {
                    let noIdItem = this.listChat.find((s: any) => !s.id);
                    if(!noIdItem) {
                        if(!res.isSystem && !res.isShopOwner) {
                            if(res.user.userInfo.anhDaiDien != null) {
                                res.user.userInfo.anhDaiDien = environment.apiUrl + "/api/" + res.user.userInfo.anhDaiDien.replaceAll("\\", "/");
                            }
                            if((res.user.userInfo.ho && res.user.userInfo.ho.trim()) &&
                                ((res.user.userInfo.tenDem && res.user.userInfo.tenDem.trim()) ||
                                (res.user.userInfo.ten && res.user.userInfo.ten.trim()))) {
                                    res.user.name = "";
                                    if(res.user.userInfo.ho && res.user.userInfo.ho.trim()) {
                                        res.user.name += res.user.userInfo.ho + " ";
                                    }
                                    if(res.user.userInfo.tenDem && res.user.userInfo.tenDem.trim()) {
                                        res.user.name += res.user.userInfo.tenDem + " ";
                                    }
                                    if(res.user.userInfo.ten && res.user.userInfo.ten.trim()) {
                                        res.user.name += res.user.userInfo.ten + " ";
                                    }
                            }
                            else {
                                res.user.name = res.user.userName;
                            }
                        }
                        this.listChat.push(res);
                        this.messageCount++;
                        setTimeout(() => {
                            let element = this.message.toArray()[this.message.length - 3];
                            let elementTop = element.nativeElement.offsetTop;
                            let elementBottom = elementTop + element.nativeElement.clientHeight;
                            let fieldTop = this.messageField.nativeElement.scrollTop;
                            let fieldBottom = fieldTop + this.messageField.nativeElement.clientHeight;
                            if(elementBottom <= fieldBottom) {
                                this.returnToNewest();
                            }
                            else {
                                this.newMessageAnimation = true;
                            }
                        }, 10);
                    }
                    else {
                        noIdItem.id = res.id;
                    }
                }
            }
        });
    }

    returnToNewest() {
        setTimeout(() => {
            let lastElement = this.message.toArray()[this.message.length - 1];
            lastElement.nativeElement.scrollIntoView({
                behavior: 'smooth',
                block: 'nearest',
                inline: 'start'
            });
            this.newMessageAnimation = false;
        }, 1);
    }
}
