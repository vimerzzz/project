import { AfterViewInit, Component, ElementRef, OnInit, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { PrimaryButton, SuccessButton, SuccessOutlineButton, WarnButton, WarnOutlineButton } from 'src/app/shared/animations/button-fade.animations';
import { SuccessColor } from 'src/app/shared/animations/text-color.animations';
import { BaseAuth } from 'src/app/shared/auth/base.auth';
import { ReportComponent } from 'src/app/shared/components/report/report.component';
import { MessageConstants } from 'src/app/shared/constants/message.constants';
import { mType, PopUpService } from 'src/app/shared/services/base/pop-up.service';
import { ViewImageService } from 'src/app/shared/services/base/view-image.service';
import { ItemReviewService } from 'src/app/shared/services/main/shop/item-review.service';
import { ResetChat, ResetGioHang } from 'src/app/shared/utilities/reset-status.utility';
import { environment } from 'src/environments/environment';

@Component({
    selector: 'app-item-review',
    templateUrl: './item-review.component.html',
    styleUrls: ['./item-review.component.scss'],
    animations: [
        WarnButton,
        PrimaryButton,
        SuccessColor,
        SuccessButton,
        SuccessOutlineButton,
        WarnOutlineButton
    ]
})
export class ItemReviewComponent implements OnInit, AfterViewInit {
    shop: any;
    loaded: Promise<boolean>;
    avatarImage: any;
    sanPham: any;
    loggedUser: any;
    index: any = 0;
    discount: any;
    count: any = 0;
    shopButtonAnimation: any = false;
    addCommentButtonAnimation: any = false;
    chatButtonAnimation: any = false;
    reportButtonAnimation: any = false;
    linkAnimation: any = false;
    listDanhGia: any;
    listDanhGiaHuuIch: any;
    endRateContent: any = false;
    nhanXet: any;
    danhGia: any;
    enableMouse: any = false;
    enableComment: any = false;
    filterSelect: any = "1";
    rateParams: any = {
        pageIndex: 1,
        pageSize: 10,
        sanPhamId: null,
        allStar: true,
        oneStar: false,
        twoStar: false,
        threeStar: false,
        fourStar: false,
        fiveStar: false,
        order: "desc",
        orderBy: "createdAt"
    }
    comm: any = {
        oneStar: false,
        twoStar: false,
        threeStar: false,
        fourStar: false,
        fiveStar: false
    }
    rateInfo: any = {
        oneStar: 0,
        twoStar: 0,
        threeStar: 0,
        fourStar: 0,
        fiveStar: 0
    }
    @ViewChild("countInput") countInput: ElementRef;
    @ViewChildren("comment") comment: QueryList<ElementRef>;

    constructor(private titleService: Title, private base: BaseAuth, private dialog: MatDialog, private resetChat: ResetChat, private resetGioHang: ResetGioHang, private popUpService: PopUpService, private viewImageService: ViewImageService, private router: Router, private activeRouter: ActivatedRoute, private itemReviewService: ItemReviewService) { }

    ngOnInit(): void {
        this.titleService.setTitle("Sản phẩm");
        this.base.checkExpire();
        this.checkShop();
    }

    ngAfterViewInit(): void {
        this.comment.changes.subscribe((res: any) => {
            let commentArray = res.toArray();
            setTimeout(() => {
                for(let index in commentArray) {
                    if(commentArray[index].nativeElement.offsetHeight > 160) {
                        this.listDanhGia[index].moreText = true;
                        commentArray[index].nativeElement.classList.add("comm__text-hidden");
                    }
                }
            });
        });
    }

    checkShop() {
        this.activeRouter.queryParams.subscribe((params) => {
            if(params.shopName) {
                this.itemReviewService.getShopByShopName(params.shopName).subscribe({
                    next: (res: any) => {
                        if(res && res.userHoatDong && res.hoatDong) {
                            this.shop = res;
                            this.avatarImage = this.shop.anhDaiDien;
                            if (this.avatarImage != null) {
                                this.avatarImage = environment.apiUrl + "/api/" + this.avatarImage.replaceAll("\\", "/");
                            }
                            this.getLoggedUser();
                        }
                        else {
                            this.shop = null;
                            this.sanPham = null;
                        }
                    }
                });
            }
            else {
                this.router.navigateByUrl("/shop/my-shop/item-management");
            }
        });
    }

    getLoggedUser() {
        let object = localStorage.getItem("loggedUser");
        if (object) {
            this.loggedUser = JSON.parse(object);
        }
        this.checkSanPham();
    }

    checkSanPham() {
        this.activeRouter.queryParams.subscribe((params) => {
            if(params.shopName && params.itemName) {
                this.itemReviewService.getSanPhamWithHinhAnhByName(params.shopName, params.itemName).subscribe({
                    next: (res: any) => {
                        if(res) {
                            this.sanPham = res;
                            this.discount = 100 - 100 * (this.sanPham.giamGia / this.sanPham.donGia);
                            for(let item of this.sanPham.hinhAnhSanPham) {
                                if(item.hinhAnh) {
                                    item.hinhAnh = environment.apiUrl + "/api/" + item.hinhAnh.replaceAll("\\", "/");
                                }
                            }
                            this.titleService.setTitle("Sản phẩm | " + this.sanPham.tenSanPham);
                            this.rateParams.sanPhamId = this.sanPham.id;
                            if(this.sanPham.soNguoiDanhGia) {
                                this.rateInfo.oneStar = this.sanPham.motSao / this.sanPham.soNguoiDanhGia * 100;
                                this.rateInfo.twoStar = this.sanPham.haiSao / this.sanPham.soNguoiDanhGia * 100;
                                this.rateInfo.threeStar = this.sanPham.baSao / this.sanPham.soNguoiDanhGia * 100;
                                this.rateInfo.fourStar = this.sanPham.bonSao / this.sanPham.soNguoiDanhGia * 100;
                                this.rateInfo.fiveStar = this.sanPham.namSao / this.sanPham.soNguoiDanhGia * 100;
                            }
                            this.getAllDanhGia();
                        }
                        else {
                            this.titleService.setTitle("Sản phẩm");
                            this.sanPham = null;
                        }
                        this.loaded = Promise.resolve(true);
                    }
                });
            }
        });
    }

    toLeft() {
        this.index = this.index == 0 ? 0 : this.index - 1;
        this.smooth();
    }

    toRight() {
        this.index = this.index == this.sanPham.hinhAnhSanPham.length - 1 ? this.sanPham.hinhAnhSanPham.length - 1 : this.index + 1;
        this.smooth();
    }

    toIndex(index: any) {
        this.index = index;
        this.smooth();
    }

    smooth() {
        setTimeout(() => {
            document.getElementById("thumbnailPanel")!.scrollIntoView({
                behavior: 'smooth',
                block: 'end',
                inline: 'nearest'
            });
        }, 1);
    }

    openImage(imageSource: string) {
        this.viewImageService.open(imageSource);
    }

    checkCount() {
        if(parseInt(this.countInput.nativeElement.value) > this.sanPham.soLuong) {
            this.countInput.nativeElement.value = this.sanPham.soLuong;
            this.count = this.sanPham.soLuong;
        }
        if(this.countInput.nativeElement.value == "") {
            this.countInput.nativeElement.value = 0;
            this.count = 0;
        }
    }

    addGioHang(type: any) {
        if(this.loggedUser) {
            let item = {
                userId: this.loggedUser.userId,
                sanPhamId: this.sanPham.id,
                soLuong: this.count,
                donGia: this.sanPham.donGia,
                giamGia: this.sanPham.giamGia
            }
            this.itemReviewService.createGioHang(item).subscribe({
                next: (res: any) => {
                    if(res.statusCode == 200) {
                        this.popUpService.open(MessageConstants.CREATE_SUCCESS, mType.success);
                        this.getAllGioHang(type);
                    }
                    else {
                        this.popUpService.open(MessageConstants.CREATE_FAILED, mType.error);
                    }
                },
                error: (res: any) => {
                    this.popUpService.open(MessageConstants.CREATE_FAILED, mType.error);
                }
            });
        }
        else {
            this.router.navigateByUrl("/error/unauthorized");
        }
    }

    getAllGioHang(type: any) {
        this.itemReviewService.getAllGioHang(this.loggedUser.userId).subscribe({
            next: (res: any) => {
                let soGioHang = 0;
                if(res) {
                    for(let item of res) {
                        soGioHang += item.listSanPhamInGioHang && item.listSanPhamInGioHang.length ? item.listSanPhamInGioHang.length : 0;
                    }
                }
                this.itemReviewService.getAllGioHangDauGia(this.loggedUser.userId).subscribe({
                    next: (res: any) => {
                        if(res) {
                            for(let item of res) {
                                soGioHang += item.listSanPhamInGioHang && item.listSanPhamInGioHang.length ? item.listSanPhamInGioHang.length : 0;
                            }
                        }
                        this.resetGioHang.setGioHangCount(soGioHang);
                        if(type == 2) {
                            this.router.navigateByUrl("/cart");
                        }
                    }
                });
            }
        });
    }

    getAllDanhGia() {
        this.rateParams.pageSize = 10;
        this.endRateContent = false;
        this.itemReviewService.getAllDanhGia(this.rateParams).subscribe({
            next: (res: any) => {
                this.listDanhGia = res.data ? res.data : [];
                for(let item of this.listDanhGia) {
                    if(item.user.userInfo.anhDaiDien != null) {
                        item.user.userInfo.anhDaiDien = environment.apiUrl + "/api/" + item.user.userInfo.anhDaiDien.replaceAll("\\", "/");
                    }
                    if((item.user.userInfo.ho && item.user.userInfo.ho.trim()) &&
                        ((item.user.userInfo.tenDem && item.user.userInfo.tenDem.trim()) ||
                        (item.user.userInfo.ten && item.user.userInfo.ten.trim()))) {
                            item.user.name = "";
                            if(item.user.userInfo.ho && item.user.userInfo.ho.trim()) {
                                item.user.name += item.user.userInfo.ho + " ";
                            }
                            if(item.user.userInfo.tenDem && item.user.userInfo.tenDem.trim()) {
                                item.user.name += item.user.userInfo.tenDem + " ";
                            }
                            if(item.user.userInfo.ten && item.user.userInfo.ten.trim()) {
                                item.user.name += item.user.userInfo.ten + " ";
                            }
                    }
                    else {
                        item.user.name = item.user.userName
                    }
                    item.addHuuIchButtonAnimation = false;
                    item.huuIchCheck = false;
                    item.huuIchSpinner = false;
                    item.moreText = false;
                    item.linkAnimation = false;
                    item.reportButtonAnimation = false;
                }
                if(res.totalRecord <= this.rateParams.pageSize) {
                    this.endRateContent = true;
                }
                this.getAllDanhGiaHuuIchByuserId();
            }
        });
    }

    getAllDanhGiaHuuIchByuserId(element?: any) {
        if(this.loggedUser && this.loggedUser.userId) {
            this.itemReviewService.getDanhGiaHuuIchByUserId(this.loggedUser.userId).subscribe({
                next: (res: any) => {
                    this.listDanhGiaHuuIch = res ? res : [];
                    for(let item of this.listDanhGia) {
                        if(this.listDanhGiaHuuIch.filter((s: any) => s.danhGiaSanPhamId == item.id).length > 0) {
                            item.huuIchCheck = true;
                        }
                        else {
                            if(item.huuIchCheck) {
                                item.huuIchCheck = false;
                            }
                        }
                    }
                    if(element) {
                        element.huuIchSpinner = false;
                    }
                },
                error: (res: any) => {
                    if(element) {
                        element.huuIchSpinner = false;
                    }
                }
            });
        }
        else {
            this.listDanhGiaHuuIch = [];
        }
    }

    getMoreDanhGia() {
        this.rateParams.pageSize += 10;
        this.itemReviewService.getAllDanhGia(this.rateParams).subscribe({
            next: (res: any) => {
                this.listDanhGia = res.data ? res.data : [];
                for(let item of this.listDanhGia) {
                    if(item.user.userInfo.anhDaiDien != null) {
                        item.user.userInfo.anhDaiDien = environment.apiUrl + "/api/" + item.user.userInfo.anhDaiDien.replaceAll("\\", "/");
                    }
                    if((item.user.userInfo.ho && item.user.userInfo.ho.trim()) &&
                        ((item.user.userInfo.tenDem && item.user.userInfo.tenDem.trim()) ||
                        (item.user.userInfo.ten && item.user.userInfo.ten.trim()))) {
                            item.user.name = "";
                            if(item.user.userInfo.ho && item.user.userInfo.ho.trim()) {
                                item.user.name += item.user.userInfo.ho + " ";
                            }
                            if(item.user.userInfo.tenDem && item.user.userInfo.tenDem.trim()) {
                                item.user.name += item.user.userInfo.tenDem + " ";
                            }
                            if(item.user.userInfo.ten && item.user.userInfo.ten.trim()) {
                                item.user.name += item.user.userInfo.ten + " ";
                            }
                    }
                    else {
                        item.user.name = item.user.userName
                    }
                    item.addHuuIchButtonAnimation = false;
                    item.huuIchCheck = false;
                    item.huuIchSpinner = false;
                    item.moreText = false;
                    item.linkAnimation = false;
                    item.reportButtonAnimation = false;
                }
                if(res.totalRecord <= this.rateParams.pageSize) {
                    this.endRateContent = true;
                }
                this.getAllDanhGiaHuuIchByuserId();
            }
        });
    }

    createDanhGia() {
        if(this.loggedUser) {
            if(!this.danhGia || this.danhGia == 0) {
                this.danhGia = 0;
                return;
            }
            else {
                let item = {
                    userId: this.loggedUser.userId,
                    sanPhamId: this.sanPham.id,
                    danhGia: this.danhGia,
                    nhanXet: this.nhanXet
                }
                this.itemReviewService.createDanhGia(item).subscribe({
                    next: (res: any) => {
                        if(res.statusCode == 200) {
                            this.popUpService.open(MessageConstants.CREATE_SUCCESS, mType.success);
                            this.getAllDanhGia();
                            this.danhGia = null;
                            this.nhanXet = null;
                        }
                        else {
                            this.popUpService.open(MessageConstants.CREATE_FAILED, mType.error);
                        }
                    },
                    error: (res: any) => {
                        this.popUpService.open(MessageConstants.CREATE_FAILED, mType.error);
                    }
                });
            }
        }
        else {
            this.router.navigateByUrl("/error/unauthorized");
        }
    }

    toggleAllStar() {
        this.rateParams.allStar = true;
        this.rateParams.oneStar = false;
        this.rateParams.twoStar = false;
        this.rateParams.threeStar = false;
        this.rateParams.fourStar = false;
        this.rateParams.fiveStar = false;
    }

    toggleOnOneStar() {
        this.rateParams.allStar = false;
        this.rateParams.oneStar = true;
    }

    toggleOnTwoStar() {
        this.rateParams.allStar = false;
        this.rateParams.twoStar = true;
    }

    toggleOnThreeStar() {
        this.rateParams.allStar = false;
        this.rateParams.threeStar = true;
    }

    toggleOnFourStar() {
        this.rateParams.allStar = false;
        this.rateParams.fourStar = true;
    }

    toggleOnFiveStar() {
        this.rateParams.allStar = false;
        this.rateParams.fiveStar = true;
    }

    toggleOffOneStar() {
        this.rateParams.oneStar = false;
        if(!this.rateParams.oneStar &&
            !this.rateParams.twoStar &&
            !this.rateParams.threeStar &&
            !this.rateParams.fourStar &&
            !this.rateParams.fiveStar){
                this.rateParams.allStar = true;
        }
        else {
            this.rateParams.allStar = false;
        }
    }

    toggleOffTwoStar() {
        this.rateParams.twoStar = false;
        if(!this.rateParams.oneStar &&
            !this.rateParams.twoStar &&
            !this.rateParams.threeStar &&
            !this.rateParams.fourStar &&
            !this.rateParams.fiveStar){
                this.rateParams.allStar = true;
        }
        else {
            this.rateParams.allStar = false;
        }
    }

    toggleOffThreeStar() {
        this.rateParams.threeStar = false;
        if(!this.rateParams.oneStar &&
            !this.rateParams.twoStar &&
            !this.rateParams.threeStar &&
            !this.rateParams.fourStar &&
            !this.rateParams.fiveStar){
                this.rateParams.allStar = true;
        }
        else {
            this.rateParams.allStar = false;
        }
    }

    toggleOffFourStar() {
        this.rateParams.fourStar = false;
        if(!this.rateParams.oneStar &&
            !this.rateParams.twoStar &&
            !this.rateParams.threeStar &&
            !this.rateParams.fourStar &&
            !this.rateParams.fiveStar){
                this.rateParams.allStar = true;
        }
        else {
            this.rateParams.allStar = false;
        }
    }

    toggleOffFiveStar() {
        this.rateParams.fiveStar = false;
        if(!this.rateParams.oneStar &&
            !this.rateParams.twoStar &&
            !this.rateParams.threeStar &&
            !this.rateParams.fourStar &&
            !this.rateParams.fiveStar){
                this.rateParams.allStar = true;
        }
        else {
            this.rateParams.allStar = false;
        }
    }

    onCommOneStar() {
        this.comm.oneStar = true;
        this.comm.twoStar = false;
        this.comm.threeStar = false;
        this.comm.fourStar = false;
        this.comm.fiveStar = false;
    }

    onCommTwoStar() {
        this.comm.oneStar = true;
        this.comm.twoStar = true;
        this.comm.threeStar = false;
        this.comm.fourStar = false;
        this.comm.fiveStar = false;
    }

    onCommThreeStar() {
        this.comm.oneStar = true;
        this.comm.twoStar = true;
        this.comm.threeStar = true;
        this.comm.fourStar = false;
        this.comm.fiveStar = false;
    }

    onCommFourStar() {
        this.comm.oneStar = true;
        this.comm.twoStar = true;
        this.comm.threeStar = true;
        this.comm.fourStar = true;
        this.comm.fiveStar = false;
    }

    onCommFiveStar() {
        this.comm.oneStar = true;
        this.comm.twoStar = true;
        this.comm.threeStar = true;
        this.comm.fourStar = true;
        this.comm.fiveStar = true;
    }

    offCommOneStar() {
        this.comm.oneStar = false;
        this.comm.twoStar = false;
        this.comm.threeStar = false;
        this.comm.fourStar = false;
        this.comm.fiveStar = false;
    }

    offCommTwoStar() {
        this.comm.oneStar = true;
        this.comm.twoStar = false;
        this.comm.threeStar = false;
        this.comm.fourStar = false;
        this.comm.fiveStar = false;
    }

    offCommThreeStar() {
        this.comm.oneStar = true;
        this.comm.twoStar = true;
        this.comm.threeStar = false;
        this.comm.fourStar = false;
        this.comm.fiveStar = false;
    }

    offCommFourStar() {
        this.comm.oneStar = true;
        this.comm.twoStar = true;
        this.comm.threeStar = true;
        this.comm.fourStar = false;
        this.comm.fiveStar = false;
    }

    offCommFiveStar() {
        this.comm.oneStar = true;
        this.comm.twoStar = true;
        this.comm.threeStar = true;
        this.comm.fourStar = true;
        this.comm.fiveStar = false;
    }

    openChat() {
        if(this.loggedUser) {
            this.resetChat.setUserShopChatStatus(true);
            this.resetChat.setChatStatus(true);
            this.resetChat.setChatShopId(this.shop.id);
        }
        else {
            this.router.navigateByUrl("/error/unauthorized");
        }
    }

    openChatUser(userId: any) {
        if(this.loggedUser) {
            if(this.loggedUser.userId != userId) {
                this.resetChat.setUserUserChatStatus(true);
                this.resetChat.setChatStatus(true);
                this.resetChat.setChatUserId(userId);
            }
        }
        else {

        }
    }

    filterChange() {
        if(this.filterSelect == 1) {
            this.rateParams.order = "desc";
            this.rateParams.orderBy = "createdAt";
        }
        else if(this.filterSelect == 2) {
            this.rateParams.order = "asc";
            this.rateParams.orderBy = "createdAt";
        }
        else if(this.filterSelect == 3) {
            this.rateParams.order = "desc";
            this.rateParams.orderBy = "huuIch";
        }
    }

    addHuuIch(element: any) {
        if(this.loggedUser) {
            element.huuIchSpinner = true;
            let item: any = {
                userId: this.loggedUser.userId,
                danhGiaSanPhamId: element.id
            }
            this.itemReviewService.createDanhGiaHuuIch(item).subscribe({
                next: (res: any) => {
                    if(res.statusCode == 200) {
                        this.getAllDanhGiaHuuIchByuserId(element);
                    }
                    else {
                        element.huuIchSpinner = false;
                    }
                },
                error: (res: any) => {
                    element.huuIchSpinner = false;
                }
            });
        }
        else {
            this.router.navigateByUrl("/error/unauthorized");
        }
    }

    deleteHuuIch(element: any) {
        element.huuIchSpinner = true;
        let item: any = {
            userId: this.loggedUser.userId
        }
        item.id = this.listDanhGiaHuuIch.filter((s: any) => s.danhGiaSanPhamId == element.id)[0].id;
        this.itemReviewService.deleteDanhGiaHuuIch(item).subscribe({
            next: (res: any) => {
                if(res.statusCode == 200) {
                    this.getAllDanhGiaHuuIchByuserId(element);
                }
                else {
                    element.huuIchSpinner = false;
                }
            },
            error: (res: any) => {
                element.huuIchSpinner = false;
            }
        });
    }

    moreComment(index: any) {
        let commentArray = this.comment.toArray();
        commentArray[index].nativeElement.classList.add("comm__text-show");
        setTimeout(() => {
            commentArray[index].nativeElement.classList.add("overflow-auto");
            this.listDanhGia[index].moreText = false;
        }, 1000);
    }

    openDialogBaoCao(userId: any, shopId: any, name: any) {
        let item: any = {
            nguoiBaoCaoId: this.loggedUser.userId,
            userId: userId,
            shopId: shopId,
            lyDoBaoCao: null,
            noiGuiBaoCao: this.router.url
        }
        let dialogRef = this.dialog.open(ReportComponent, {
            width: "48rem",
            data: {
                name: name,
                item: item
            }
        });
        dialogRef.afterClosed().subscribe((res: any) => {
            if(res) {
                this.itemReviewService.createBaoCao(this.loggedUser.userId, item).subscribe({
                    next: (ress: any) => {
                        if(ress.statusCode == 200) {

                        }
                        else {
                            this.popUpService.open(MessageConstants.SYSTEM_ERROR, mType.error);
                        }
                    },
                    error: (ress: any) => {
                        this.popUpService.open(MessageConstants.SYSTEM_ERROR, mType.error);
                    }
                });
            }
        });
    }
}
