import { NgModule } from '@angular/core';
import { MaterialModule } from 'src/app/material.module';
import { ShopRoutingModule } from './shop-routing.module';
import { ShopReviewComponent } from './shop-review/shop-review.component';
import { ItemReviewComponent } from './item-review/item-review.component';
import { DirectivesModule } from 'src/app/shared/directives/directives.module';
import { AuctionReviewComponent } from './auction-review/auction-review.component';

@NgModule({
    declarations: [
        ShopReviewComponent,
        ItemReviewComponent,
        AuctionReviewComponent
    ],
    imports: [
        DirectivesModule,
        ShopRoutingModule,
        MaterialModule
    ]
})
export class ShopModule { }
