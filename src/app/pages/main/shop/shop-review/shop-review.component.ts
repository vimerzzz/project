import { AfterViewInit, Component, ElementRef, OnInit, QueryList, ViewChildren } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { GrayShadow } from 'src/app/shared/animations/box-shadow.animations';
import { PrimaryButton, SuccessButton, SuccessOutlineButton, WarnButton, WarnOutlineButton } from 'src/app/shared/animations/button-fade.animations';
import { SuccessColor } from 'src/app/shared/animations/text-color.animations';
import { BaseAuth } from 'src/app/shared/auth/base.auth';
import { ReportComponent } from 'src/app/shared/components/report/report.component';
import { MessageConstants } from 'src/app/shared/constants/message.constants';
import { mType, PopUpService } from 'src/app/shared/services/base/pop-up.service';
import { ShopReviewService } from 'src/app/shared/services/main/shop/shop-review.service';
import { ResetChat } from 'src/app/shared/utilities/reset-status.utility';
import { environment } from 'src/environments/environment';

@Component({
    selector: 'app-shop',
    templateUrl: './shop-review.component.html',
    styleUrls: ['./shop-review.component.scss'],
    animations: [
        PrimaryButton,
        WarnButton,
        SuccessButton,
        SuccessOutlineButton,
        GrayShadow,
        SuccessColor,
        WarnOutlineButton
    ]
})
export class ShopReviewComponent implements OnInit, AfterViewInit {
    shop: any;
    loaded: Promise<boolean>;
    avatarImage: any;
    loggedUser: any;
    followButtonAnimation: any = false;
    unfollowButtonAnimation: any = false;
    addCommentButtonAnimation: any = false;
    chatButtonAnimation: any = false;
    reportButtonAnimation: any = false;
    grayShadowAnimation: any;
    followRes: any;
    followed: any = false;
    listSanPham: any;
    listDanhGia: any;
    listDanhGiaHuuIch: any;
    endContent: any = false;
    endRateContent: any = false;
    nhanXet: any;
    danhGia: any;
    enableMouse: any = false;
    enableComment: any = false;
    filterSelect: any = "1";
    params: any = {
        pageIndex: 1,
        pageSize: 30,
        searchString: null,
        shopId: null
    }
    rateParams: any = {
        pageIndex: 1,
        pageSize: 10,
        shopId: null,
        allStar: true,
        oneStar: false,
        twoStar: false,
        threeStar: false,
        fourStar: false,
        fiveStar: false,
        order: "desc",
        orderBy: "createdAt"
    }
    comm: any = {
        oneStar: false,
        twoStar: false,
        threeStar: false,
        fourStar: false,
        fiveStar: false
    }
    rateInfo: any = {
        oneStar: 0,
        twoStar: 0,
        threeStar: 0,
        fourStar: 0,
        fiveStar: 0
    }
    @ViewChildren("comment") comment: QueryList<ElementRef>;

    constructor(private titleService: Title, private base: BaseAuth, private dialog: MatDialog, private resetChat: ResetChat, private popUpService: PopUpService, private router: Router, private activeRouter: ActivatedRoute, private shopReviewService: ShopReviewService) { }

    ngOnInit(): void {
        this.titleService.setTitle("Shop");
        this.base.checkExpire();
        this.checkShop();
    }

    ngAfterViewInit(): void {
        this.comment.changes.subscribe((res: any) => {
            let commentArray = res.toArray();
            setTimeout(() => {
                for(let index in commentArray) {
                    if(commentArray[index].nativeElement.offsetHeight > 160) {
                        this.listDanhGia[index].moreText = true;
                        commentArray[index].nativeElement.classList.add("comm__text-hidden");
                    }
                }
            });
        });
    }

    checkShop() {
        this.activeRouter.queryParams.subscribe((params) => {
            if(params.name) {
                this.shopReviewService.getShopByShopName(params.name).subscribe({
                    next: (res: any) => {
                        if(res && res.userHoatDong && res.hoatDong) {
                            this.shop = res;
                            this.titleService.setTitle("Shop | " + params.name);
                            this.avatarImage = this.shop.anhDaiDien;
                            if (this.avatarImage != null) {
                                this.avatarImage = environment.apiUrl + "/api/" + this.avatarImage.replaceAll("\\", "/");
                            }
                            this.params.shopId = this.shop.id;
                            this.rateParams.shopId = this.shop.id;
                            if(this.shop.soNguoiDanhGia) {
                                this.rateInfo.oneStar = this.shop.motSao / this.shop.soNguoiDanhGia * 100;
                                this.rateInfo.twoStar = this.shop.haiSao / this.shop.soNguoiDanhGia * 100;
                                this.rateInfo.threeStar = this.shop.baSao / this.shop.soNguoiDanhGia * 100;
                                this.rateInfo.fourStar = this.shop.bonSao / this.shop.soNguoiDanhGia * 100;
                                this.rateInfo.fiveStar = this.shop.namSao / this.shop.soNguoiDanhGia * 100;
                            }
                            this.getAllSanPham();
                            this.getLoggedUser();
                        }
                        else {
                            this.shop = null;
                            this.titleService.setTitle("Shop");
                        }
                        this.loaded = Promise.resolve(true);
                    }
                });
            }
            else {
                this.router.navigateByUrl("/shop/my-shop/dashboard");
            }
        });
    }

    getAllSanPham() {
        this.params.pageSize = 30;
        this.endContent = false;
        this.shopReviewService.getNewestAllSanPhamWithHinhAnhByShopId(this.params).subscribe({
            next: (res: any) => {
                this.listSanPham = res.data ? res.data : [];
                this.grayShadowAnimation = [];
                for(let item of this.listSanPham) {
                    item.avatar = item.hinhAnhSanPham[0].hinhAnh;
                    if (item.avatar != null) {
                        item.avatar = environment.apiUrl + "/api/" + item.avatar.replaceAll("\\", "/");
                    }
                    this.grayShadowAnimation.push(false);
                }
                if(res.totalRecord <= this.params.pageSize) {
                    this.endContent = true;
                }
            }
        });
    }

    getLoggedUser() {
        let object = localStorage.getItem("loggedUser");
        if (object) {
            this.loggedUser = JSON.parse(object);
            this.getAllSoTheoDoi();
        }
        this.getAllDanhGia();
    }

    getAllSoTheoDoi() {
        let item = {
            userId: this.loggedUser.userId,
            shopId: this.shop.id
        }
        this.shopReviewService.getAllSoTheoDoi(item).subscribe({
            next: (res: any) => {
                this.followRes = res.data ? res.data[0] : [];
                this.followed = res.totalRecord ? true : false;
            }
        });
    }

    addSoTheoDoi() {
        if(this.loggedUser) {
            let item = {
                userId: this.loggedUser.userId,
                shopId: this.shop.id
            }
            this.shopReviewService.createSoTheoDoi(item).subscribe({
                next: (res: any) => {
                    if(res.statusCode == 200) {
                        this.popUpService.open(MessageConstants.FOLLOWED, mType.success);
                        this.followed = !this.followed;
                        this.followButtonAnimation = false;
                        this.unfollowButtonAnimation = false;
                        this.getAllSoTheoDoi();
                    }
                    else {
                        this.popUpService.open(MessageConstants.SYSTEM_ERROR, mType.error);
                    }
                },
                error: (res: any) => {
                    this.popUpService.open(MessageConstants.SYSTEM_ERROR, mType.error);
                }
            });
        }
        else {
            this.router.navigateByUrl("/error/unauthorized");
        }
    }

    deleteSoTheoDoi() {
        this.shopReviewService.deleteSoTheoDoi(this.followRes).subscribe({
            next: (res: any) => {
                if(res.statusCode == 200) {
                    this.popUpService.open(MessageConstants.UNFOLLOWED, mType.success);
                    this.followed = !this.followed;
                    this.followButtonAnimation = false;
                    this.unfollowButtonAnimation = false;
                }
                else {
                    this.popUpService.open(MessageConstants.SYSTEM_ERROR, mType.error);
                }
            },
            error: (res: any) => {
                this.popUpService.open(MessageConstants.SYSTEM_ERROR, mType.error);
            }
        });
    }

    addSanPham() {
        this.params.pageSize += 30;
        this.shopReviewService.getNewestAllSanPhamWithHinhAnhByShopId(this.params).subscribe({
            next: (res: any) => {
                this.listSanPham = res.data ? res.data : [];
                this.grayShadowAnimation = [];
                for(let item of this.listSanPham) {
                    item.avatar = item.hinhAnhSanPham[0].hinhAnh;
                    if (item.avatar != null) {
                        item.avatar = environment.apiUrl + "/api/" + item.avatar.replaceAll("\\", "/");
                    }
                    this.grayShadowAnimation.push(false);
                }
                if(res.totalRecord <= this.params.pageSize) {
                    this.endContent = true;
                }
            }
        });
    }

    getAllDanhGia() {
        this.rateParams.pageSize = 10;
        this.endRateContent = false;
        this.shopReviewService.getAllDanhGia(this.rateParams).subscribe({
            next: (res: any) => {
                this.listDanhGia = res.data ? res.data : [];
                for(let item of this.listDanhGia) {
                    if(item.user.userInfo.anhDaiDien != null) {
                        item.user.userInfo.anhDaiDien = environment.apiUrl + "/api/" + item.user.userInfo.anhDaiDien.replaceAll("\\", "/");
                    }
                    if((item.user.userInfo.ho && item.user.userInfo.ho.trim()) &&
                        ((item.user.userInfo.tenDem && item.user.userInfo.tenDem.trim()) ||
                        (item.user.userInfo.ten && item.user.userInfo.ten.trim()))) {
                            item.user.name = "";
                            if(item.user.userInfo.ho && item.user.userInfo.ho.trim()) {
                                item.user.name += item.user.userInfo.ho + " ";
                            }
                            if(item.user.userInfo.tenDem && item.user.userInfo.tenDem.trim()) {
                                item.user.name += item.user.userInfo.tenDem + " ";
                            }
                            if(item.user.userInfo.ten && item.user.userInfo.ten.trim()) {
                                item.user.name += item.user.userInfo.ten + " ";
                            }
                    }
                    else {
                        item.user.name = item.user.userName
                    }
                    item.addHuuIchButtonAnimation = false;
                    item.huuIchCheck = false;
                    item.huuIchSpinner = false;
                    item.linkAnimation = false;
                    item.moreText = false;
                    item.reportButtonAnimation = false;
                }
                if(res.totalRecord <= this.rateParams.pageSize) {
                    this.endRateContent = true;
                }
                this.getAllDanhGiaHuuIchByuserId();
            }
        });
    }

    getAllDanhGiaHuuIchByuserId(element?: any) {
        if(this.loggedUser && this.loggedUser.userId) {
            this.shopReviewService.getDanhGiaHuuIchByUserId(this.loggedUser.userId).subscribe({
                next: (res: any) => {
                    this.listDanhGiaHuuIch = res ? res : [];
                    for(let item of this.listDanhGia) {
                        if(this.listDanhGiaHuuIch.filter((s: any) => s.danhGiaShopId == item.id).length > 0) {
                            item.huuIchCheck = true;
                        }
                        else {
                            if(item.huuIchCheck) {
                                item.huuIchCheck = false;
                            }
                        }
                    }
                    if(element) {
                        element.huuIchSpinner = false;
                    }
                },
                error: (res: any) => {
                    if(element) {
                        element.huuIchSpinner = false;
                    }
                }
            });
        }
        else {
            this.listDanhGiaHuuIch = [];
        }
    }

    getMoreDanhGia() {
        this.rateParams.pageSize += 10;
        this.shopReviewService.getAllDanhGia(this.rateParams).subscribe({
            next: (res: any) => {
                this.listDanhGia = res.data ? res.data : [];
                for(let item of this.listDanhGia) {
                    if(item.user.userInfo.anhDaiDien != null) {
                        item.user.userInfo.anhDaiDien = environment.apiUrl + "/api/" + item.user.userInfo.anhDaiDien.replaceAll("\\", "/");
                    }
                    if((item.user.userInfo.ho && item.user.userInfo.ho.trim()) &&
                        ((item.user.userInfo.tenDem && item.user.userInfo.tenDem.trim()) ||
                        (item.user.userInfo.ten && item.user.userInfo.ten.trim()))) {
                            item.user.name = "";
                            if(item.user.userInfo.ho && item.user.userInfo.ho.trim()) {
                                item.user.name += item.user.userInfo.ho + " ";
                            }
                            if(item.user.userInfo.tenDem && item.user.userInfo.tenDem.trim()) {
                                item.user.name += item.user.userInfo.tenDem + " ";
                            }
                            if(item.user.userInfo.ten && item.user.userInfo.ten.trim()) {
                                item.user.name += item.user.userInfo.ten + " ";
                            }
                    }
                    else {
                        item.user.name = item.user.userName
                    }
                    item.addHuuIchButtonAnimation = false;
                    item.huuIchCheck = false;
                    item.huuIchSpinner = false;
                    item.linkAnimation = false;
                    item.moreText = false;
                    item.reportButtonAnimation = false;
                }
                if(res.totalRecord <= this.rateParams.pageSize) {
                    this.endRateContent = true;
                }
                this.getAllDanhGiaHuuIchByuserId();
            }
        });
    }

    createDanhGia() {
        if(this.loggedUser) {
            if(!this.danhGia || this.danhGia == 0) {
                this.danhGia = 0;
                return;
            }
            else {
                let item = {
                    userId: this.loggedUser.userId,
                    shopId: this.shop.id,
                    danhGia: this.danhGia,
                    nhanXet: this.nhanXet
                }
                this.shopReviewService.createDanhGia(item).subscribe({
                    next: (res: any) => {
                        if(res.statusCode == 200) {
                            this.popUpService.open(MessageConstants.CREATE_SUCCESS, mType.success);
                            this.getAllDanhGia();
                            this.danhGia = null;
                            this.nhanXet = null;
                        }
                        else {
                            this.popUpService.open(MessageConstants.CREATE_FAILED, mType.error);
                        }
                    },
                    error: (res: any) => {
                        this.popUpService.open(MessageConstants.CREATE_FAILED, mType.error);
                    }
                });
            }
        }
        else {
            this.router.navigateByUrl("/error/unauthorized");
        }
    }

    toggleAllStar() {
        this.rateParams.allStar = true;
        this.rateParams.oneStar = false;
        this.rateParams.twoStar = false;
        this.rateParams.threeStar = false;
        this.rateParams.fourStar = false;
        this.rateParams.fiveStar = false;
    }

    toggleOnOneStar() {
        this.rateParams.allStar = false;
        this.rateParams.oneStar = true;
    }

    toggleOnTwoStar() {
        this.rateParams.allStar = false;
        this.rateParams.twoStar = true;
    }

    toggleOnThreeStar() {
        this.rateParams.allStar = false;
        this.rateParams.threeStar = true;
    }

    toggleOnFourStar() {
        this.rateParams.allStar = false;
        this.rateParams.fourStar = true;
    }

    toggleOnFiveStar() {
        this.rateParams.allStar = false;
        this.rateParams.fiveStar = true;
    }

    toggleOffOneStar() {
        this.rateParams.oneStar = false;
        if(!this.rateParams.oneStar &&
            !this.rateParams.twoStar &&
            !this.rateParams.threeStar &&
            !this.rateParams.fourStar &&
            !this.rateParams.fiveStar){
                this.rateParams.allStar = true;
        }
        else {
            this.rateParams.allStar = false;
        }
    }

    toggleOffTwoStar() {
        this.rateParams.twoStar = false;
        if(!this.rateParams.oneStar &&
            !this.rateParams.twoStar &&
            !this.rateParams.threeStar &&
            !this.rateParams.fourStar &&
            !this.rateParams.fiveStar){
                this.rateParams.allStar = true;
        }
        else {
            this.rateParams.allStar = false;
        }
    }

    toggleOffThreeStar() {
        this.rateParams.threeStar = false;
        if(!this.rateParams.oneStar &&
            !this.rateParams.twoStar &&
            !this.rateParams.threeStar &&
            !this.rateParams.fourStar &&
            !this.rateParams.fiveStar){
                this.rateParams.allStar = true;
        }
        else {
            this.rateParams.allStar = false;
        }
    }

    toggleOffFourStar() {
        this.rateParams.fourStar = false;
        if(!this.rateParams.oneStar &&
            !this.rateParams.twoStar &&
            !this.rateParams.threeStar &&
            !this.rateParams.fourStar &&
            !this.rateParams.fiveStar){
                this.rateParams.allStar = true;
        }
        else {
            this.rateParams.allStar = false;
        }
    }

    toggleOffFiveStar() {
        this.rateParams.fiveStar = false;
        if(!this.rateParams.oneStar &&
            !this.rateParams.twoStar &&
            !this.rateParams.threeStar &&
            !this.rateParams.fourStar &&
            !this.rateParams.fiveStar){
                this.rateParams.allStar = true;
        }
        else {
            this.rateParams.allStar = false;
        }
    }

    onCommOneStar() {
        this.comm.oneStar = true;
        this.comm.twoStar = false;
        this.comm.threeStar = false;
        this.comm.fourStar = false;
        this.comm.fiveStar = false;
    }

    onCommTwoStar() {
        this.comm.oneStar = true;
        this.comm.twoStar = true;
        this.comm.threeStar = false;
        this.comm.fourStar = false;
        this.comm.fiveStar = false;
    }

    onCommThreeStar() {
        this.comm.oneStar = true;
        this.comm.twoStar = true;
        this.comm.threeStar = true;
        this.comm.fourStar = false;
        this.comm.fiveStar = false;
    }

    onCommFourStar() {
        this.comm.oneStar = true;
        this.comm.twoStar = true;
        this.comm.threeStar = true;
        this.comm.fourStar = true;
        this.comm.fiveStar = false;
    }

    onCommFiveStar() {
        this.comm.oneStar = true;
        this.comm.twoStar = true;
        this.comm.threeStar = true;
        this.comm.fourStar = true;
        this.comm.fiveStar = true;
    }

    offCommOneStar() {
        this.comm.oneStar = false;
        this.comm.twoStar = false;
        this.comm.threeStar = false;
        this.comm.fourStar = false;
        this.comm.fiveStar = false;
    }

    offCommTwoStar() {
        this.comm.oneStar = true;
        this.comm.twoStar = false;
        this.comm.threeStar = false;
        this.comm.fourStar = false;
        this.comm.fiveStar = false;
    }

    offCommThreeStar() {
        this.comm.oneStar = true;
        this.comm.twoStar = true;
        this.comm.threeStar = false;
        this.comm.fourStar = false;
        this.comm.fiveStar = false;
    }

    offCommFourStar() {
        this.comm.oneStar = true;
        this.comm.twoStar = true;
        this.comm.threeStar = true;
        this.comm.fourStar = false;
        this.comm.fiveStar = false;
    }

    offCommFiveStar() {
        this.comm.oneStar = true;
        this.comm.twoStar = true;
        this.comm.threeStar = true;
        this.comm.fourStar = true;
        this.comm.fiveStar = false;
    }

    openChat() {
        if(this.loggedUser) {
            this.resetChat.setUserShopChatStatus(true);
            this.resetChat.setChatStatus(true);
            this.resetChat.setChatShopId(this.shop.id);
        }
        else {
            this.router.navigateByUrl("/error/unauthorized");
        }
    }

    openChatUser(userId: any) {
        if(this.loggedUser) {
            if(this.loggedUser.userId != userId) {
                this.resetChat.setUserUserChatStatus(true);
                this.resetChat.setChatStatus(true);
                this.resetChat.setChatUserId(userId);
            }
        }
        else {

        }
    }

    filterChange() {
        if(this.filterSelect == 1) {
            this.rateParams.order = "desc";
            this.rateParams.orderBy = "createdAt";
        }
        else if(this.filterSelect == 2) {
            this.rateParams.order = "asc";
            this.rateParams.orderBy = "createdAt";
        }
        else if(this.filterSelect == 3) {
            this.rateParams.order = "desc";
            this.rateParams.orderBy = "huuIch";
        }
    }

    addHuuIch(element: any) {
        if(this.loggedUser) {
            element.huuIchSpinner = true;
            let item: any = {
                userId: this.loggedUser.userId,
                danhGiaShopId: element.id
            }
            this.shopReviewService.createDanhGiaHuuIch(item).subscribe({
                next: (res: any) => {
                    if(res.statusCode == 200) {
                        this.getAllDanhGiaHuuIchByuserId(element);
                    }
                    else {
                        element.huuIchSpinner = false;
                    }
                },
                error: (res: any) => {
                    element.huuIchSpinner = false;
                }
            });
        }
        else {
            this.router.navigateByUrl("/error/unauthorized");
        }
    }

    deleteHuuIch(element: any) {
        element.huuIchSpinner = true;
        let item: any = {
            userId: this.loggedUser.userId
        }
        item.id = this.listDanhGiaHuuIch.filter((s: any) => s.danhGiaShopId == element.id)[0].id;
        this.shopReviewService.deleteDanhGiaHuuIch(item).subscribe({
            next: (res: any) => {
                if(res.statusCode == 200) {
                    this.getAllDanhGiaHuuIchByuserId(element);
                }
                else {
                    element.huuIchSpinner = false;
                }
            },
            error: (res: any) => {
                element.huuIchSpinner = false;
            }
        });
    }

    moreComment(index: any) {
        let commentArray = this.comment.toArray();
        commentArray[index].nativeElement.classList.add("comm__text-show");
        setTimeout(() => {
            commentArray[index].nativeElement.classList.add("overflow-auto");
            this.listDanhGia[index].moreText = false;
        }, 1000);
    }

    openDialogBaoCao(userId: any, shopId: any, name: any) {
        let item: any = {
            nguoiBaoCaoId: this.loggedUser.userId,
            userId: userId,
            shopId: shopId,
            lyDoBaoCao: null,
            noiGuiBaoCao: this.router.url
        }
        let dialogRef = this.dialog.open(ReportComponent, {
            width: "48rem",
            data: {
                name: name,
                item: item
            }
        });
        dialogRef.afterClosed().subscribe((res: any) => {
            if(res) {
                this.shopReviewService.createBaoCao(this.loggedUser.userId, item).subscribe({
                    next: (ress: any) => {
                        if(ress.statusCode == 200) {

                        }
                        else {
                            this.popUpService.open(MessageConstants.SYSTEM_ERROR, mType.error);
                        }
                    },
                    error: (ress: any) => {
                        this.popUpService.open(MessageConstants.SYSTEM_ERROR, mType.error);
                    }
                });
            }
        });
    }
}
