import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AdminAuth } from "src/app/shared/auth/admin.auth";
import { CustomerAuth } from "src/app/shared/auth/customer.auth";
import { DeliverAuth } from "src/app/shared/auth/deliver.auth";
import { AuctionReviewComponent } from "./auction-review/auction-review.component";
import { ItemReviewComponent } from "./item-review/item-review.component";
import { ShopReviewComponent } from "./shop-review/shop-review.component";

const routes: Routes = [
    {
        path: "",
        children: [
            {
                path: "",
                component: ShopReviewComponent
            },
            {
                path: "my-shop",
                loadChildren: () => import('./my-shop/my-shop.module').then(m => m.MyShopModule)
            },
            {
                path: "item",
                component: ItemReviewComponent
            },
            {
                path: "auction",
                component: AuctionReviewComponent
            }
        ]
    }
]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
  })
  export class ShopRoutingModule { }
