import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { Sort } from '@angular/material/sort';
import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { BaseAuth } from 'src/app/shared/auth/base.auth';
import { ConfirmDeleteComponent } from 'src/app/shared/components/confirm-delete/confirm-delete.component';
import { GuidConstants } from 'src/app/shared/constants/guid.constants';
import { MessageConstants } from 'src/app/shared/constants/message.constants';
import { mType, PopUpService } from 'src/app/shared/services/base/pop-up.service';
import { ItemManagementService } from 'src/app/shared/services/main/shop/my-shop/item-management.service';
import { DialogAddItemComponent } from './dialog-add-item/dialog-add-item.component';

@Component({
    selector: 'app-item-management',
    templateUrl: './item-management.component.html',
    styleUrls: ['./item-management.component.scss']
})
export class ItemManagementComponent implements OnInit {
    loaded: Promise<boolean>;
    loggedUser: any;
    userInfo: any;
    shop: any;
    listSanPham: any;
    listLoaiSanPham: any;
    totalRecord: any;
    stt: any = 1;
    sortActive: any;
    sortDirection: any;
    init: any = true;
    displayedColumns = ["stt", "tenSanPham", "danhGiaChung", "soLuong", "soLuongDaBan", "createdAt", "thaoTac"];
    params: any = {
        pageIndex: 1,
        pageSize: 10,
        searchString: null,
        order: null,
        orderBy: null,
        shopId: null,
        loaiSanPhamId: ""
    }
    paramsChange: any = {
        searchString: null,
        loaiSanPhamId: null
    }
    @ViewChild("paginator") paginator: MatPaginator;

    constructor(private titleService: Title, private base: BaseAuth, private router: Router, private itemManagementService: ItemManagementService, private popUpService: PopUpService, private dialog: MatDialog) { }

    ngOnInit(): void {
        this.titleService.setTitle("Quản lý sản phẩm");
        this.base.checkExpire();
        this.getLoggedUser();
    }

    getLoggedUser() {
        let object = localStorage.getItem("loggedUser");
        if (object) {
            this.loggedUser = JSON.parse(object);
            this.getUserInfoByUserId(this.loggedUser.userId);
        }
    }

    getUserInfoByUserId(id: any) {
        this.itemManagementService.getUserInfoByUserId(id).subscribe({
            next: (res: any) => {
                this.userInfo = res;
                this.checkUserInfo();
            }
        });
    }

    checkUserInfo() {
        if (!this.userInfo.emailConfirmed) {
            this.popUpService.open(MessageConstants.NEED_EMAIL_VALIDATION, mType.info);
            this.router.navigateByUrl("/information/setting");
        }
        else {
            if (!this.userInfo.coShop) {
                this.router.navigateByUrl("/shop/my-shop/create-shop");
            }
            else {
                this.getShopByUserId();
                this.getAllLoaiSanPham();
            }
        }
    }

    getShopByUserId() {
        this.itemManagementService.getShopByUserId(this.loggedUser.userId).subscribe({
            next: (res: any) => {
                if(res) {
                    this.shop = res;
                    this.params.shopId = this.shop.id;
                    this.getAllSanPhamByShopId();
                }
            }
        });
    }

    getAllSanPhamByShopId(e?: any) {
        if (e && e.pageIndex !== "" && e.pageIndex !== undefined) {
            this.params.pageIndex = e.pageIndex + 1;
        }
        if (e && e.pageSize !== "" && e.pageSize !== undefined) {
            this.params.pageSize = e.pageSize;
        }
        this.itemManagementService.getAllSanPhamByShopId(this.params).subscribe({
            next: (res: any) => {
                this.listSanPham = res.data ? res.data : [];
                for(let diaChi of this.listSanPham) {
                    diaChi.stt = this.stt;
                    this.stt++;
                }
                this.stt = 1;
                this.totalRecord = res.totalRecord;
                if(this.init) {
                    this.loaded = Promise.resolve(true);
                    this.init = false;
                }
                this.sortActive = this.params.orderBy;
                this.sortDirection = this.params.order;
            }
        });
    }

    getAllLoaiSanPham() {
        let param: any = {
            order: "asc",
            orderBy: "tenLoaiSanPham"
        }
        this.itemManagementService.getAllLoaiSanPham(param).subscribe({
            next: (res: any) => {
                this.listLoaiSanPham = res.data ? res.data : [];
                let otherId = GuidConstants.ONE;
                let object = this.listLoaiSanPham.find((s: any) => s.id == otherId);
                if(object) {
                    this.listLoaiSanPham.splice(this.listLoaiSanPham.indexOf(object), 1);
                    this.listLoaiSanPham.push(object);
                }
            }
        });
    }

    openDialogCreateSanPham() {
        let item: any = {
            userId: this.shop.userId,
            shopId: this.shop.id,
            tenSanPham: null,
            donGia: null,
            soLuong: null,
            thongTinKhac: null,
            giamGia: null,
            loaiSanPhamId: "",
            nongDo: null,
            xuatXu: null,
            nam: 0,
            mauSac: null,
            dungTich: null
        }
        let image: any = [];
        let tenShop = this.shop.tenShop;
        let dialogRef = this.dialog.open(DialogAddItemComponent, {
            width: "48rem",
            data: {
                item,
                image,
                tenShop
            }
        });
        dialogRef.afterClosed().subscribe((res: any) => {
            if(res) {
                this.itemManagementService.createSanPham(item).subscribe({
                    next: (res: any) => {
                        if(res.statusCode == 200) {
                            this.createHinhAnhSanPham(image, res.id, 1);
                        }
                        else {
                            this.popUpService.open(MessageConstants.CREATE_FAILED, mType.error);
                        }
                    },
                    error: (res: any) => {
                        this.popUpService.open(MessageConstants.CREATE_FAILED, mType.error);
                    }
                });
            }
        });
    }

    openDialogUpdateSanPham(item: any) {
        let image: any = [];
        let deleteImage: any = [];
        let updateImage: any = [];
        let tenShop = this.shop.tenShop;
        let oldName = item.tenSanPham;
        let dialogRef = this.dialog.open(DialogAddItemComponent, {
            width: "48rem",
            data: {
                item,
                image,
                deleteImage,
                updateImage,
                tenShop,
                oldName
            }
        });
        dialogRef.afterClosed().subscribe((res: any) => {
            if(res) {
                this.itemManagementService.updateSanPham(item).subscribe({
                    next: (res: any) => {
                        if(res.statusCode == 200) {
                            this.deleteHinhAnhSanPham(deleteImage, updateImage, image, item.id, 2);
                        }
                        else {
                            this.popUpService.open(MessageConstants.UPDATE_FAILED, mType.error);
                        }
                    },
                    error: (res: any) => {
                        this.popUpService.open(MessageConstants.UPDATE_FAILED, mType.error);
                    }
                });
            }
        });
    }

    deleteSanPham(item: any) {
        let dialogRef = this.dialog.open(ConfirmDeleteComponent, {
            width: "32rem"
        });
        dialogRef.afterClosed().subscribe((res: any) => {
            if(res) {
                this.itemManagementService.deleteSanPham(item).subscribe({
                    next: (res: any) => {
                        if(res.statusCode == 200) {
                            this.popUpService.open(MessageConstants.DELETE_SUCCESS, mType.success);
                            this.getAllSanPhamByShopId();
                        }
                        else {
                            this.popUpService.open(MessageConstants.DELETE_FAILED, mType.error);
                        }
                    },
                    error: (res: any) => {
                        this.popUpService.open(MessageConstants.DELETE_FAILED, mType.error);
                    }
                });
            }
        });
    }

    createHinhAnhSanPham(image: any, sanPhamId: any, type: any) {
        let formData = new FormData();
        for(let index in image) {
            if(image[index]) {
                formData.append("file[]", image[index], image[index].name);
            }
            if(parseInt(index) == image.length - 1) {
                this.itemManagementService.createHinhAnhSanPham(this.shop.userId, sanPhamId, formData).subscribe({
                    next: (res: any) => {
                        if(res.statusCode == 200) {
                            if(type == 1) {
                                this.popUpService.open(MessageConstants.CREATE_SUCCESS, mType.success);
                            }
                            else {
                                this.popUpService.open(MessageConstants.UPDATE_SUCCESS, mType.success);
                            }
                            this.getAllSanPhamByShopId();
                        }
                        else {
                            if(type == 1) {
                                this.popUpService.open(MessageConstants.CREATE_FAILED, mType.error);
                            }
                            else {
                                this.popUpService.open(MessageConstants.UPDATE_FAILED, mType.error);
                            }
                        }
                    },
                    error: (res: any) => {
                        if(type == 1) {
                            this.popUpService.open(MessageConstants.CREATE_FAILED, mType.error);
                        }
                        else {
                            this.popUpService.open(MessageConstants.UPDATE_FAILED, mType.error);
                        }
                    }
                });
            }
        }
    }

    updateHinhAnhSanPham(updateImage: any, image: any, sanPhamId: any, type: any) {
        if(updateImage.length) {
            this.itemManagementService.updateHinhAnhSanPham(updateImage).subscribe({
                next: (res: any) => {
                    if(res.statusCode == 200) {
                        if(image.length) {
                            this.createHinhAnhSanPham(image, sanPhamId, type);
                        }
                        else {
                            this.popUpService.open(MessageConstants.UPDATE_SUCCESS, mType.success);
                            this.getAllSanPhamByShopId();
                        }
                    }
                    else {
                        this.popUpService.open(MessageConstants.UPDATE_FAILED, mType.error);
                    }
                },
                error: (res: any) => {
                    this.popUpService.open(MessageConstants.UPDATE_FAILED, mType.error);
                }
            });
        }
        else {
            if(image.length) {
                this.createHinhAnhSanPham(image, sanPhamId, type);
            }
            else {
                this.popUpService.open(MessageConstants.UPDATE_SUCCESS, mType.success);
                this.getAllSanPhamByShopId();
            }
        }
    }

    deleteHinhAnhSanPham(deleteImage: any, updateImage: any, image: any, sanPhamId: any, type: any) {
        if(deleteImage.length) {
            this.itemManagementService.deleteHinhAnhSanPham(this.shop.userId, deleteImage).subscribe({
                next: (res: any) => {
                    if(res.statusCode == 200) {
                        this.updateHinhAnhSanPham(updateImage, image, sanPhamId, type);
                    }
                    else {
                        this.popUpService.open(MessageConstants.UPDATE_FAILED, mType.error);
                    }
                },
                error: (res: any) => {
                    this.popUpService.open(MessageConstants.UPDATE_FAILED, mType.error);
                }
            });
        }
        else {
            this.updateHinhAnhSanPham(updateImage, image, sanPhamId, type);
        }
    }

    sortData(sort: Sort) {
        this.params.order = sort.direction;
        this.params.orderBy = sort.active;
        this.getAllSanPhamByShopId();
    }

    toHeadPage() {
        if(this.params.searchString != this.paramsChange.searchString) {
            this.paginator.firstPage();
            this.paramsChange.searchString = this.params.searchString;
        }
        if(this.params.loaiSanPhamId != this.paramsChange.loaiSanPhamId) {
            this.paginator.firstPage();
            this.paramsChange.loaiSanPhamId = this.params.loaiSanPhamId;
        }
    }
}
