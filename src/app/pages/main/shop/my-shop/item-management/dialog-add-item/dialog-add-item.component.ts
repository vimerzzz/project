import { Component, ElementRef, Inject, OnInit, ViewChild } from '@angular/core';
import { AbstractControl, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DomSanitizer } from '@angular/platform-browser';
import { HtmlEditorService, ImageService, LinkService, RichTextEditorComponent, ToolbarService } from '@syncfusion/ej2-angular-richtexteditor';
import { GuidConstants } from 'src/app/shared/constants/guid.constants';
import { MessageConstants } from 'src/app/shared/constants/message.constants';
import { ImageCropService } from 'src/app/shared/services/base/image-crop.service';
import { mType, PopUpService } from 'src/app/shared/services/base/pop-up.service';
import { ViewImageService } from 'src/app/shared/services/base/view-image.service';
import { ItemManagementService } from 'src/app/shared/services/main/shop/my-shop/item-management.service';
import { convertViToEn } from 'src/app/shared/utilities/convert-language.utility';
import { environment } from 'src/environments/environment';

@Component({
    selector: 'app-dialog-add-item',
    templateUrl: './dialog-add-item.component.html',
    styleUrls: ['./dialog-add-item.component.scss'],
    providers: [ToolbarService, LinkService, ImageService, HtmlEditorService]
})
export class DialogAddItemComponent implements OnInit {
    formControl: FormGroup;
    listLoaiSanPham: any;
    listLoaiSanPhamFilter: any;
    listFileToUpload: any = [];
    listFileToUpdate: any = [];
    listFileToDelete: any = [];
    listThuocTinh: any;
    searchString: FormControl;
    correctType: any = true;
    itemImage: any = [];
    index: any = 0;
    peek: any = [];
    noImage: any = false;
    type: any;
    khongGiamGia: any = true;
    loggedUser: any;
    rteImageUploading: any;
    rteImageUploadSuccess: any;
    tools: object = {
        items: ['Undo', 'Redo', '|',
            'Bold', 'Italic', 'Underline', 'StrikeThrough', '|','Image', '|',
            'FontName', 'FontSize', 'FontColor', '|',
            'Alignments', '|', 'OrderedList', 'UnorderedList', '|',
            'CreateLink', 'RemoveLink', '|', 'ClearFormat']
    };
    insertImageSettings: any = {
        saveUrl: `${environment.apiUrl}/api/sanPham/uploadImageGioiThieuSanPham?userId=`,
        path: `${environment.apiUrl}/api/Statics/Images/Items/`,
        resize: false,
    }
    @ViewChild("formRTE") rte: RichTextEditorComponent;
    @ViewChild("imageUpload") imageUpload: ElementRef;
    @ViewChild("imagePanel") imagePanel: ElementRef;

    constructor(private formBuilder: FormBuilder, private dialogRef: MatDialogRef<DialogAddItemComponent>, private viewImageService: ViewImageService, private sanitizer: DomSanitizer, private imageCropService: ImageCropService, private popUpService: PopUpService, private itemManagementService: ItemManagementService,
        @Inject(MAT_DIALOG_DATA) public data: any) { }

    ngOnInit(): void {
        this.formControl = this.formBuilder.group({
            tenSanPham: ["", Validators.required],
            donGia: ["", Validators.required],
            soLuong: ["", Validators.required],
            thongTinKhac: [""],
            loaiSanPham: ["", Validators.required],
            nongDo: ["", [Validators.required, Validators.max(100)]],
            xuatXu: ["", Validators.required],
            nam: [""],
            mauSac: ["", Validators.required],
            dungTich: ["", Validators.required],
            giamGia: ["", Validators.required],
            khongGiamGia: [""]
        });
        this.getLoggedUser();
        this.getAllLoaiSanPham();
        if(this.data.item.id) {
            if(this.data.item.donGia != this.data.item.giamGia) {
                this.khongGiamGia = false;
            }
            else {
                this.khongGiamGia = true;
            }
            this.getAllHinhAnhSanPham();
        }
        this.searchString = new FormControl("");
        this.searchString.valueChanges.subscribe((value: any) => {
            this.listLoaiSanPhamFilter = this.listLoaiSanPham.filter((item: any) => convertViToEn(item.tenLoaiSanPham).includes(convertViToEn(value)));
        });
    }

    getLoggedUser() {
        let object = localStorage.getItem("loggedUser");
        if (object) {
            this.loggedUser = JSON.parse(object);
            if(this.data.item.id) {
                this.insertImageSettings.saveUrl += `${this.loggedUser.userId}&sanPhamId=${this.data.item.id}`;
                this.insertImageSettings.path += `${this.data.item.shopId}/${this.data.item.id}/`;
            }
        }
    }

    overLimit(control: AbstractControl): { [key: string]: any } {
        return { "overLimit": true };
    }

    checkGioiHan() {
        if (parseInt(this.f.giamGia.value) > parseInt(this.f.donGia.value)) {
            this.f.giamGia.setValidators([Validators.required, this.overLimit]);
            this.f.giamGia.updateValueAndValidity();
        }
        else {
            this.f.giamGia.setValidators(Validators.required);
            this.f.giamGia.updateValueAndValidity();
        }
    }

    getAllLoaiSanPham() {
        let param: any = {
            order: "asc",
            orderBy: "tenLoaiSanPham"
        }
        this.itemManagementService.getAllLoaiSanPham(param).subscribe({
            next: (res: any) => {
                this.listLoaiSanPham = res.data ? res.data : [];
                let otherId = GuidConstants.ONE;
                let object = this.listLoaiSanPham.find((s: any) => s.id == otherId);
                if(object) {
                    this.listLoaiSanPham.splice(this.listLoaiSanPham.indexOf(object), 1);
                    this.listLoaiSanPham.push(object);
                }
                this.listLoaiSanPhamFilter = this.listLoaiSanPham;
            }
        });
    }

    getAllHinhAnhSanPham() {
        this.itemManagementService.getAllHinhAnhSanPhamBySanPhamId(this.data.item.id).subscribe({
            next: (res: any) => {
                this.listFileToUpdate = res ? res : [];
                for(let item of this.listFileToUpdate) {
                    if(item.hinhAnh != null) {
                        this.itemImage.push(environment.apiUrl + "/api/" + item.hinhAnh.replaceAll("\\", "/"));
                    }
                }
            }
        });
    }

    checkFileType() {
        for (let index = 0; index < this.imageUpload.nativeElement.files.length; index++) {
            if (this.imageUpload.nativeElement.files[index].type != "image/png" &&
                this.imageUpload.nativeElement.files[index].type != "image/jpeg" &&
                this.imageUpload.nativeElement.files[index].type != "image/webp") {
                this.correctType = false;
            }
            else {
                this.correctType = true;
            }
            if (!this.correctType) {
                this.popUpService.open(MessageConstants.WRONG_FILE_TYPE, mType.warn);
                break;
            }
            if (index == this.imageUpload.nativeElement.files.length - 1) {
                this.correctType = true;
                this.openImageCrop();
            }
        }
    }

    openImageCrop() {
        let files: string[] = [];
        let names: string[] = [];
        this.readFile(0, files, names);
    }

    readFile(index: any, files: string[], names: string[]) {
        if (index >= this.imageUpload.nativeElement.files.length) return;
        let reader = new FileReader();
        reader.onload = (e: any) => {
            names.push(this.imageUpload.nativeElement.files[index].name);
            files.push(e.target.result);
            if (index == this.imageUpload.nativeElement.files.length - 1) {
                this.imageCropService.open(files, 3, 4, 900, names, index + 1);
                this.imageCropService.image.subscribe((res: any) => {
                    if (res == "") {
                        this.imageUpload.nativeElement.value = "";
                    }
                    else {
                        let list = new DataTransfer();
                        for (let item of res) {
                            list.items.add(item);
                        }
                        if(this.type == 0) {
                            this.listFileToUpload = [];
                            this.itemImage = [];
                            this.peek = [];
                            for(let item of this.listFileToUpdate) {
                                this.listFileToDelete.push(item);
                            }
                            this.listFileToUpdate = [];
                        }
                        this.index = this.listFileToUpdate.length + this.listFileToUpload.length;
                        let indexToPush = this.listFileToUpload.length;
                        this.noImage = false;
                        for(let i = 0; i < list.files.length; i++) {
                            this.listFileToUpload.push(list.files[i]);
                        }
                        this.displayImage(this.index, indexToPush);
                    }
                });
            }
            this.readFile(index + 1, files, names);
        }
        reader.readAsDataURL(this.imageUpload.nativeElement.files[index]);
    }

    displayImage(index: any, indexToPush: any) {
        if (index >= this.listFileToUpdate.length + this.listFileToUpload.length) {
            setTimeout(() => {
                this.imagePanel.nativeElement.scrollIntoView({
                    behavior: 'smooth',
                    block: 'start',
                    inline: 'nearest'
                });
                document.getElementById("thumbnailPanel")!.scrollIntoView({
                    behavior: 'smooth',
                    block: 'start',
                    inline: 'nearest'
                });
            }, 1);
            return;
        }
        let reader = new FileReader();
        reader.onload = (e: any) => {
            this.itemImage[index] = this.sanitizer.bypassSecurityTrustUrl(e.target.result);
            this.peek[index] = false;
            this.displayImage(index + 1, indexToPush + 1);
        }
        reader.readAsDataURL(this.listFileToUpload[indexToPush]);
    }

    getIndexOfListUpdate(index: any) {
        let countUpdate = -1;
        for(let i = 0; i <= index; i++) {
            if(typeof(this.itemImage[i]) == "string") {
                countUpdate++;
            }
        }
        return countUpdate;
    }

    getIndexOfListUpload(index: any) {
        let countUpload = -1;
        for(let i = 0; i <= index; i++) {
            if(typeof(this.itemImage[i]) == "object") {
                countUpload++;
            }
        }
        return countUpload;
    }

    removeImage(index: any) {
        if(this.data.item.id) {
            let countUpdate = this.getIndexOfListUpdate(index);
            let countUpload = this.getIndexOfListUpload(index);
            if(typeof(this.itemImage[index]) == "string") {
                this.listFileToDelete.push(this.listFileToUpdate.splice(countUpdate, 1)[0]);
                for(let i = countUpdate; i < this.listFileToUpdate.length; i++) {
                    this.listFileToUpdate[i].thuTu--;
                }
            }
            else {
                this.listFileToUpload.splice(countUpload, 1);
                for(let i = countUpdate + 1; i < this.listFileToUpdate.length; i++) {
                    this.listFileToUpdate[i].thuTu--;
                }
            }
        }
        else {
            this.listFileToUpload.splice(index, 1);
        }
        this.itemImage.splice(index, 1);
        if(index < this.index) {
            this.index--;
        }
        else {
            if(this.index == this.itemImage.length) {
                this.index--;
            }
        }
    }

    toLeft() {
        this.index = this.index == 0 ? 0 : this.index - 1;
        this.smooth();
    }

    toRight() {
        this.index = this.index == this.itemImage.length - 1 ? this.itemImage.length - 1 : this.index + 1;
        this.smooth();
    }

    toIndex(index: any) {
        this.index = index;
        this.smooth();
    }

    smooth() {
        setTimeout(() => {
            document.getElementById("thumbnailPanel")!.scrollIntoView({
                behavior: 'smooth',
                block: 'start',
                inline: 'nearest'
            });
        }, 1);
    }

    prevSwitch(index: any) {
        if(this.data.item.id) {
            let countUpdate = this.getIndexOfListUpdate(index);
            let countUpload = this.getIndexOfListUpload(index);
            if(typeof(this.itemImage[index]) == "string") {
                if(typeof(this.itemImage[index - 1]) == "string") {
                    this.listFileToUpdate[countUpdate].thuTu--;
                    this.listFileToUpdate[countUpdate - 1].thuTu++;
                    let temp2 = this.listFileToUpdate[countUpdate];
                    this.listFileToUpdate[countUpdate] = this.listFileToUpdate[countUpdate - 1];
                    this.listFileToUpdate[countUpdate - 1] = temp2;
                }
                else {
                    this.listFileToUpdate[countUpdate].thuTu--;
                }
            }
            else {
                if(typeof(this.itemImage[index - 1]) == "string") {
                    this.listFileToUpdate[countUpdate].thuTu++;
                }
                else {
                    let temp2 = this.listFileToUpload[countUpload];
                    this.listFileToUpload[countUpload] = this.listFileToUpload[countUpload - 1];
                    this.listFileToUpload[countUpload - 1] = temp2;
                }
            }
        }
        else {
            let temp2 = this.listFileToUpload[index];
            this.listFileToUpload[index] = this.listFileToUpload[index - 1];
            this.listFileToUpload[index - 1] = temp2;
        }
        let temp1 = this.itemImage[index];
        this.itemImage[index] = this.itemImage[index - 1];
        this.itemImage[index - 1] = temp1;
    }

    nextSwitch(index: any) {
        if(this.data.item.id) {
            let countUpdate = this.getIndexOfListUpdate(index);
            let countUpload = this.getIndexOfListUpload(index);
            if(typeof(this.itemImage[index]) == "string") {
                if(typeof(this.itemImage[index + 1]) == "string") {
                    this.listFileToUpdate[countUpdate].thuTu++;
                    this.listFileToUpdate[countUpdate + 1].thuTu--;
                    let temp2 = this.listFileToUpdate[countUpdate];
                    this.listFileToUpdate[countUpdate] = this.listFileToUpdate[countUpdate + 1];
                    this.listFileToUpdate[countUpdate + 1] = temp2;
                }
                else {
                    this.listFileToUpdate[countUpdate].thuTu++;
                }
            }
            else {
                if(typeof(this.itemImage[index + 1]) == "string") {
                    this.listFileToUpdate[countUpdate + 1].thuTu--;
                }
                else {
                    let temp2 = this.listFileToUpload[countUpload];
                    this.listFileToUpload[countUpload] = this.listFileToUpload[countUpload + 1];
                    this.listFileToUpload[countUpload + 1] = temp2;
                }
            }
        }
        else {
            let temp2 = this.listFileToUpload[index];
            this.listFileToUpload[index] = this.listFileToUpload[index + 1];
            this.listFileToUpload[index + 1] = temp2;
        }
        let temp1 = this.itemImage[index];
        this.itemImage[index] = this.itemImage[index + 1];
        this.itemImage[index + 1] = temp1;
    }

    openImage(imageSource: string) {
        this.viewImageService.open(imageSource);
    }

    // changeLoaiSanPham() {
    //     if(this.data.item.loaiSanPhamId && this.f.loaiSanPham.dirty) {
    //         this.itemManagementService.getAllThuocTinh(this.data.item.loaiSanPhamId).subscribe({
    //             next: (res: any) => {
    //                 this.listThuocTinh = res ? res : [];
    //                 for(let item of this.listLoaiSanPham) {
    //                     if(item.id == this.data.item.loaiSanPhamId) {
    //                         this.data.item.thongTinKhac = "Loại sản phẩm: " + item.tenLoaiSanPham;
    //                         if(item.moTaLoaiSanPham) {
    //                             this.data.item.thongTinKhac += "\nMô tả loại sản phẩm: " + item.moTaLoaiSanPham;
    //                         }
    //                         break;
    //                     }
    //                 }
    //                 if(this.listThuocTinh.length) {
    //                     this.data.item.thongTinKhac += "\n";
    //                 }
    //                 for(let item of this.listThuocTinh) {
    //                     this.data.item.thongTinKhac += "\n" + item.tenThuocTinh + ": ";
    //                     if(item.thuocTinhMacDinh) {
    //                         this.data.item.thongTinKhac += item.thuocTinhMacDinh;
    //                     }
    //                 }
    //             }
    //         });
    //     }
    // }

    get f() {
        return this.formControl.controls;
    }

    checkExistedItem() {
        if(this.khongGiamGia) {
            this.data.item.giamGia = this.data.item.donGia;
            this.f.giamGia.setValue(this.f.donGia.value);
        }
        this.checkGioiHan();
        if (this.itemImage.length == 0) {
            this.noImage = true;
        }
        this.formControl.markAllAsTouched();
        if (this.formControl.invalid || this.noImage) {
            return;
        }
        else {
            this.itemManagementService.getSanPhamWithHinhAnhByName(this.data.tenShop, this.data.item.tenSanPham).subscribe({
                next: (res: any) => {
                    if(res) {
                        if(!this.data.oldName) {
                            this.save();
                        }
                        else {
                            if(this.data.oldName == res.tenSanPham) {
                                this.save();
                            }
                            else {
                                this.popUpService.open(MessageConstants.EXISTED_ITEM, mType.warn);
                            }
                        }
                    }
                    else {
                        this.save();
                    }
                },
                error: (res: any) => {
                    this.popUpService.open(MessageConstants.SYSTEM_ERROR, mType.error);
                }
            });
        }
    }

    save() {
        for(let item of this.listFileToUpdate) {
            this.data.updateImage.push(item);
        }
        for(let item of this.listFileToDelete) {
            this.data.deleteImage.push(item.id);
        }
        for(let index in this.listFileToUpload) {
            this.data.image.push(this.listFileToUpload[index]);
        }
        this.dialogRef.close(true);
    }

    imageSubscribe() {
        this.rte.imageModule.uploadObj.selected = (e: any) => {
            this.rteImageUploadSuccess = this.rte.imageUploadSuccess.subscribe((res: any) => {
                let obj = JSON.parse(res.e.currentTarget.response);
                if(obj && obj.noiDung) {
                    e.filesData[0].name = obj.noiDung;
                }
                if(this.rteImageUploadSuccess) {
                    this.rteImageUploadSuccess.unsubscribe();
                }
            });
        }
        this.rteImageUploading = this.rte.imageUploading.subscribe((res: any) => {
            res.currentRequest.setRequestHeader('Authorization', `Bearer ${this.loggedUser.token}`);
        });
    }

    imageDestroy() {
        if(this.rteImageUploading) {
            this.rteImageUploading.unsubscribe();
        }
        if(this.rteImageUploadSuccess) {
            this.rteImageUploadSuccess.unsubscribe();
        }
    }
}
