import { AfterViewInit, Component, OnDestroy, OnInit } from '@angular/core';
import { fromEvent, Subscription } from 'rxjs';
import { MyShopSidenavService } from 'src/app/shared/services/main/shop/my-shop/my-shop-sidenav.service';
import { ResetUserInfo } from 'src/app/shared/utilities/reset-status.utility';

@Component({
    selector: 'app-my-shop-sidenav',
    templateUrl: './my-shop-sidenav.component.html',
    styleUrls: ['./my-shop-sidenav.component.scss']
})
export class MyShopSidenavComponent implements OnInit, AfterViewInit, OnDestroy {
    loggedUser: any;
    opened: any = true;
    mode: "side" | "over" | "push";
    toggleButton: any = true;
    resizeSub: Subscription;
    widthBreakpoint: any = 1000;
    userInfo: any;
    shop: any;
    loaded: Promise<boolean>;

    constructor(private myShopService: MyShopSidenavService, private resetUserInfo: ResetUserInfo) { }

    ngOnInit() {
        let object = localStorage.getItem("loggedUser");
        if(object) {
            this.loggedUser = JSON.parse(object);
            this.getUserInfoByUserId(this.loggedUser.userId);
        }
        this.mode = "side";
        this.sideNavCollapse(window.innerWidth);
    }

    ngAfterViewInit() {
        this.resizeSub = fromEvent(window, "resize").subscribe(() => {
            this.sideNavCollapse(window.innerWidth);
        });
        this.resetUserInfo.userInfo.subscribe((res: any) => {
            if(res) {
                this.getUserInfoByUserId(this.loggedUser.userId);
            }
        });
    }

    getUserInfoByUserId(id: any) {
        this.myShopService.getUserInfoByUserId(id).subscribe({
            next: (res: any) => {
                this.userInfo = res;
                if (!this.userInfo.coShop) {
                    this.loaded = Promise.resolve(true);
                }
                else {
                    this.getShopByUserId(id);
                }
            }
        });
    }

    getShopByUserId(id: any) {
        this.myShopService.getShopByUserId(id).subscribe({
            next: (res: any) => {
                if(res) {
                    this.shop = res;
                    this.loaded = Promise.resolve(true);
                }
            }
        });
    }

    sideNavCollapse(width: number) {
        if(width < this.widthBreakpoint) {
            this.opened = false;
            this.mode = "push";
            this.toggleButton = false;
        }
        else {
            this.opened = true;
            this.mode = "side";
            this.toggleButton = true;
        }
    }

    ngOnDestroy() {
        this.resizeSub.unsubscribe();
    }
}
