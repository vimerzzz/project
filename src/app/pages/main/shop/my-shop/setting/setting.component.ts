import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { DomSanitizer, Title } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { HtmlEditorService, ImageService, LinkService, RichTextEditorComponent, ToolbarService } from '@syncfusion/ej2-angular-richtexteditor';
import { Observable } from 'rxjs';
import { BaseAuth } from 'src/app/shared/auth/base.auth';
import { PasswordConfirmComponent } from 'src/app/shared/components/password-confirm/password-confirm.component';
import { MessageConstants } from 'src/app/shared/constants/message.constants';
import { ImageCropService } from 'src/app/shared/services/base/image-crop.service';
import { mType, PopUpService } from 'src/app/shared/services/base/pop-up.service';
import { SettingService } from 'src/app/shared/services/main/shop/my-shop/setting.service';
import { ResetUserInfo } from 'src/app/shared/utilities/reset-status.utility';
import { environment } from 'src/environments/environment';

@Component({
    selector: 'app-setting',
    templateUrl: './setting.component.html',
    styleUrls: ['./setting.component.scss'],
    providers: [ToolbarService, LinkService, ImageService, HtmlEditorService]
})
export class SettingComponent implements OnInit {
    loaded: Promise<boolean>;
    formControl: FormGroup;
    spinner:any = false;
    userInfo: any;
    loggedUser: any;
    shop: any;
    avatarImage: any;
    correctType: any = true;
    error: any;
    initialShop: any;
    user: any;
    rteImageUploading: any;
    rteImageUploadSuccess: any;
    tools: object = {
        items: ['Undo', 'Redo', '|',
            'Bold', 'Italic', 'Underline', 'StrikeThrough', '|','Image', '|',
            'FontName', 'FontSize', 'FontColor', '|',
            'Alignments', '|', 'OrderedList', 'UnorderedList', '|',
            'CreateLink', 'RemoveLink', '|', 'ClearFormat']
    };
    insertImageSettings: any = {
        saveUrl: `${environment.apiUrl}/api/shop/uploadImageGioiThieuShop?userId=`,
        path: `${environment.apiUrl}/api/Statics/Images/Shops/`,
        resize: false,
    }
    @ViewChild("formRTE") rte: RichTextEditorComponent;
    @ViewChild("avatarUpload") avatarUpload: ElementRef;

    constructor(private  titleService: Title, private base: BaseAuth, private formBuilder: FormBuilder, private sanitizer: DomSanitizer, private imageCropService: ImageCropService, private settingService: SettingService, private router: Router, private popUpService: PopUpService, private resetUserInfo: ResetUserInfo, private dialog: MatDialog) { }

    ngOnInit(): void {
        this.titleService.setTitle("Cài đặt shop");
        this.base.checkExpire();
        this.formControl = this.formBuilder.group({
            tenShop: ["", Validators.required],
            moTaShop: [""]
        });
        this.getLoggedUser();
    }

    getLoggedUser() {
        let object = localStorage.getItem("loggedUser");
        if (object) {
            this.loggedUser = JSON.parse(object);
            this.insertImageSettings.saveUrl += this.loggedUser.userId;
            this.getUserInfoByUserId(this.loggedUser.userId);
        }
    }

    getUserInfoByUserId(id: any) {
        this.settingService.getUserInfoByUserId(id).subscribe({
            next: (res: any) => {
                this.userInfo = res;
                this.checkUserInfo();
            }
        });
    }

    checkUserInfo() {
        if (!this.userInfo.emailConfirmed) {
            this.popUpService.open(MessageConstants.NEED_EMAIL_VALIDATION, mType.info);
            this.router.navigateByUrl("/information/setting");
        }
        else {
            if (!this.userInfo.coShop) {
                this.router.navigateByUrl("/shop/my-shop/create-shop");
            }
            else {
                this.getUserById();
                this.getShopByUserId();
            }
        }
    }

    getUserById() {
        this.settingService.getUserById(this.loggedUser.userId).subscribe({
            next: (res: any) => {
                if(res) {
                    this.user = res;
                }
            }
        });
    }

    getShopByUserId() {
        this.settingService.getShopByUserId(this.loggedUser.userId).subscribe({
            next: (res: any) => {
                if(res) {
                    this.shop = res;
                    this.insertImageSettings.path += this.shop.id + "/";
                    this.initialShop = this.shop.tenShop;
                    this.avatarImage = this.shop.anhDaiDien;
                    if (this.avatarImage != null) {
                        this.avatarImage = environment.apiUrl + "/api/" + this.avatarImage.replaceAll("\\", "/");
                    }
                    this.loaded = Promise.resolve(true);
                }
            }
        });
    }

    updateShop() {
        this.formControl.markAllAsTouched();
        if(this.formControl.invalid) {
            return;
        }
        else {
            this.spinner = true;
            this.settingService.getShopByShopName(this.shop.tenShop).subscribe({
                next: (res: any) => {
                    if(res) {
                        if(res.tenShop != this.initialShop) {
                            this.popUpService.open(MessageConstants.UPDATE_FAILED, mType.error);
                            this.spinner = false;
                            this.error = MessageConstants.EXISTED_SHOP;
                        }
                        else {
                            this.error = null;
                            this.settingService.updateShop(this.shop).subscribe({
                                next: (ress: any) => {
                                    if(ress.statusCode == 200) {
                                        this.uploadAnhDaiDien();
                                    }
                                    else {
                                        this.popUpService.open(MessageConstants.UPDATE_FAILED, mType.error);
                                        this.spinner = false;
                                    }
                                },
                                error: (ress: any) => {
                                    this.popUpService.open(MessageConstants.UPDATE_FAILED, mType.error);
                                    this.spinner = false;
                                }
                            });
                        }
                    }
                    else {
                        this.error = null;
                        this.settingService.updateShop(this.shop).subscribe({
                            next: (ress: any) => {
                                if(ress.statusCode == 200) {
                                    this.uploadAnhDaiDien();
                                }
                                else {
                                    this.popUpService.open(MessageConstants.UPDATE_FAILED, mType.error);
                                    this.spinner = false;
                                }
                            },
                            error: (ress: any) => {
                                this.popUpService.open(MessageConstants.UPDATE_FAILED, mType.error);
                                this.spinner = false;
                            }
                        });
                    }
                },
                error: (res: any) => {
                    this.popUpService.open(MessageConstants.SYSTEM_ERROR, mType.error);
                    this.spinner = false;
                }
            });
        }
    }

    uploadAnhDaiDien() {
        let file: File = this.avatarUpload.nativeElement.files[0];
        if (file && this.correctType) {
            let formData = new FormData();
            formData.append('file', file, file.name);
            this.settingService.uploadAnhDaiDien(this.shop.userId, formData).subscribe({
                next: (res: any) => {
                    if (res.statusCode == 200) {
                        this.popUpService.open(MessageConstants.UPDATE_SUCCESS, mType.success);
                        this.spinner = false;
                        this.getShopByUserId();
                    }
                    else {
                        this.popUpService.open(MessageConstants.UPDATE_FAILED, mType.error);
                        this.spinner = false;
                    }
                },
                error: (res: any) => {
                    this.popUpService.open(MessageConstants.UPDATE_FAILED, mType.error);
                    this.spinner = false;
                }
            });
        }
        else {
            this.popUpService.open(MessageConstants.UPDATE_SUCCESS, mType.success);
            this.spinner = false;
            this.getShopByUserId();
        }
    }

    checkFileType() {
        if(this.avatarUpload.nativeElement.files[0].type != "image/png" &&
            this.avatarUpload.nativeElement.files[0].type != "image/jpeg" &&
            this.avatarUpload.nativeElement.files[0].type != "image/webp") {
                this.popUpService.open(MessageConstants.WRONG_FILE_TYPE, mType.warn);
                this.correctType = false;
            }
        else {
            this.correctType = true;
            this.openImageCrop();
        }
    }

    openImageCrop() {
        let reader = new FileReader();
        let files: string[] = [];
        let names: string[] = [];
        reader.onload = (e: any) => {
            files.push(e.target.result);
            names.push(this.avatarUpload.nativeElement.files[0].name);
            this.imageCropService.open(files, 1, 1, 160, names, 1);
            this.imageCropService.image.subscribe((res: any) => {
                if(res == "") {
                    this.avatarUpload.nativeElement.value = "";
                }
                else {
                    let list = new DataTransfer();
                    for(let item of res) {
                        list.items.add(item);
                    }
                    this.avatarUpload.nativeElement.files = list.files;
                    this.displayImage();
                }
            });
        }
        reader.readAsDataURL(this.avatarUpload.nativeElement.files[0]);
    }

    displayImage() {
        let reader = new FileReader();
        reader.onload = (e: any) => {
            this.avatarImage = this.sanitizer.bypassSecurityTrustUrl(e.target.result);
        }
        reader.readAsDataURL(this.avatarUpload.nativeElement.files[0]);
    }

    get f() {
        return this.formControl.controls;
    }

    imageSubscribe() {
        this.rte.imageModule.uploadObj.selected = (e: any) => {
            this.rteImageUploadSuccess = this.rte.imageUploadSuccess.subscribe((res: any) => {
                let obj = JSON.parse(res.e.currentTarget.response);
                if(obj && obj.noiDung) {
                    e.filesData[0].name = obj.noiDung;
                }
                if(this.rteImageUploadSuccess) {
                    this.rteImageUploadSuccess.unsubscribe();
                }
            });
        }
        this.rteImageUploading = this.rte.imageUploading.subscribe((res: any) => {
            res.currentRequest.setRequestHeader('Authorization', `Bearer ${this.loggedUser.token}`);
        });
    }

    imageDestroy() {
        if(this.rteImageUploading) {
            this.rteImageUploading.unsubscribe();
        }
        if(this.rteImageUploadSuccess) {
            this.rteImageUploadSuccess.unsubscribe();
        }
    }
}
