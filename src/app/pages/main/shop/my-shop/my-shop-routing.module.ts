import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AdminAuth } from "src/app/shared/auth/admin.auth";
import { CustomerAuth } from "src/app/shared/auth/customer.auth";
import { DeliverAuth } from "src/app/shared/auth/deliver.auth";
import { AuctionItemManagementComponent } from "./auction-item-management/auction-item-management.component";
import { CreateShopComponent } from "./create-shop/create-shop.component";
import { DashboardComponent } from "./dashboard/dashboard.component";
import { ItemManagementComponent } from "./item-management/item-management.component";
import { MyShopSidenavComponent } from "./my-shop-sidenav/my-shop-sidenav.component";
import { OrderManagementComponent } from "./order-management/order-management.component";
import { SettingComponent } from "./setting/setting.component";

const routes: Routes = [
    {
        path: "",
        children: [
            {
                path: "",
                redirectTo: "/shop/my-shop/item-management",
                pathMatch: "full"
            },
            {
                path: "",
                component: MyShopSidenavComponent,
                children: [
                    {
                        path: "dashboard",
                        component: DashboardComponent,
                        canActivate: [CustomerAuth]
                    },
                    {
                        path: "create-shop",
                        component: CreateShopComponent,
                        canActivate: [CustomerAuth]
                    },
                    {
                        path: "setting",
                        component: SettingComponent,
                        canActivate: [CustomerAuth]
                    },
                    {
                        path: "item-management",
                        component: ItemManagementComponent,
                        canActivate: [CustomerAuth]
                    },
                    {
                        path: "auction-item-management",
                        component: AuctionItemManagementComponent,
                        canActivate: [CustomerAuth]
                    },
                    {
                        path: "order-management",
                        component: OrderManagementComponent,
                        canActivate: [CustomerAuth]
                    }
                ]
            }
        ]
    }
]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
  })
  export class MyShopRoutingModule { }
