import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { BaseAuth } from 'src/app/shared/auth/base.auth';
import { MessageConstants } from 'src/app/shared/constants/message.constants';
import { mType, PopUpService } from 'src/app/shared/services/base/pop-up.service';
import { DashboardService } from 'src/app/shared/services/main/shop/my-shop/dashboard.service';

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
    loggedUser: any;
    userInfo: any;
    shop: any;
    loaded: Promise<boolean>;

    constructor(private titleService: Title, private base: BaseAuth, private dashboardService: DashboardService, private router: Router, private popUpService: PopUpService) { }

    ngOnInit(): void {
        this.titleService.setTitle("Dashboard");
        this.base.checkExpire();
        this.getLoggedUser();
    }

    getLoggedUser() {
        let object = localStorage.getItem("loggedUser");
        if (object) {
            this.loggedUser = JSON.parse(object);
            this.getUserInfoByUserId(this.loggedUser.userId);
        }
    }

    getUserInfoByUserId(id: any) {
        this.dashboardService.getUserInfoByUserId(id).subscribe({
            next: (res: any) => {
                this.userInfo = res;
                this.checkUserInfo();
            }
        });
    }

    checkUserInfo() {
        if (!this.userInfo.emailConfirmed) {
            this.popUpService.open(MessageConstants.NEED_EMAIL_VALIDATION, mType.info);
            this.router.navigateByUrl("/information/setting");
        }
        else {
            if (!this.userInfo.coShop) {
                this.router.navigateByUrl("/shop/my-shop/create-shop");
            }
            else {
                this.getShopByUserId();
            }
        }
    }

    getShopByUserId() {
        this.dashboardService.getShopByUserId(this.loggedUser.userId).subscribe({
            next: (res: any) => {
                if(res) {
                    this.shop = res;
                    this.loaded = Promise.resolve(true);
                }
            }
        });
    }
}
