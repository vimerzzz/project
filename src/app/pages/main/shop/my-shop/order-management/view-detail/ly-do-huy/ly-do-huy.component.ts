import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
    selector: 'app-ly-do-huy',
    templateUrl: './ly-do-huy.component.html',
    styleUrls: ['./ly-do-huy.component.scss']
})
export class LyDoHuyComponent implements OnInit {
    formControl: FormGroup;
    title: any = "Lý do hủy";

    constructor(private formBuilder: FormBuilder, private dialogRef: MatDialogRef<LyDoHuyComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any) { }

    ngOnInit(): void {
        if(this.data && this.data.item && this.data.item.title) {
            this.title = this.data.item.title;
        }
        this.formControl = this.formBuilder.group({
            lyDoHuy: ["", Validators.required]
        });
    }

    get f() {
        return this.formControl.controls;
    }

    save() {
        this.formControl.markAllAsTouched();
        if (this.formControl.invalid) {
            return;
        }
        else {
            this.dialogRef.close(true);
        }
    }
}
