import { Component, Inject, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { SuccessColor } from 'src/app/shared/animations/text-color.animations';
import { GuidConstants } from 'src/app/shared/constants/guid.constants';
import { ResetChat } from 'src/app/shared/utilities/reset-status.utility';
import { environment } from 'src/environments/environment';
import { LyDoHuyComponent } from './ly-do-huy/ly-do-huy.component';
import { ViewCouponComponent } from './view-coupon/view-coupon.component';

@Component({
    selector: 'app-view-detail',
    templateUrl: './view-detail.component.html',
    styleUrls: ['./view-detail.component.scss'],
    animations: [
        SuccessColor
    ]
})
export class ViewDetailComponent implements OnInit {
    acceptingStatus = GuidConstants.ONE;
    acceptedStatus = GuidConstants.TWO;
    deliveringStatus = GuidConstants.THREE;
    deliveredStatus = GuidConstants.FOUR;
    canceledDueDeliveringStatus = GuidConstants.SIX;
    // displayedColumns = ["hinhAnh", "tenSanPham", "donGia", "soLuong", "maGiamGia", "tongTien"];
    displayedColumns = ["hinhAnh", "tenSanPham", "donGia", "soLuong", "tongTien"];
    tongCong: any = 0;
    chatAnimation: any = false;

    constructor(private dialogRef: MatDialogRef<ViewDetailComponent>, private dialog: MatDialog, private resetChat: ResetChat,
        @Inject(MAT_DIALOG_DATA) public data: any) { }

    ngOnInit(): void {
        for(let item of this.data.item.donHangSanPham) {
            item.avatar = item.hinhAnhSanPham;
            if(item.avatar) {
                item.avatar = environment.apiUrl + "/api/" + item.avatar.replaceAll("\\", "/");
            }
            item.linkAnimation = false;
            item.giamGiaAnimation = false;
            item.giaGiam = 0;
            if(item.coMaGiamGia) {
                if(item.giamGia * item.soLuong < item.dieuKienGiamGia) {
                    item.giaGiam = 0;
                }
                else {
                    item.giaGiam = item.giamGia * item.soLuong * item.phanTramGiamGia / 100;
                    item.giaGiam = item.giaGiam < item.giamGiaToiDa ? item.giaGiam : item.giamGiaToiDa;
                    item.giaGiam = item.giaGiam < item.giamGia * item.soLuong ? item.giaGiam : item.giamGia * item.soLuong;
                }
            }
            item.tongCong = item.giamGia * item.soLuong - item.giaGiam;
            this.tongCong += item.tongCong;
        }
    }

    viewMaGiamGia(item: any) {
        this.dialog.open(ViewCouponComponent, {
            width: "32rem",
            data: {
                item
            }
        });
    }

    xacNhan() {
        this.dialogRef.close("xacNhan");
    }

    huyDonHang() {
        let item = {
            lyDoHuy: "",
            type: "huyDonHang"
        }
        let ref = this.dialog.open(LyDoHuyComponent, {
            width: "32rem",
            data: {
                item
            }
        });
        ref.afterClosed().subscribe((res: any) => {
            if(res) {
                this.dialogRef.close(item);
            }
        });
    }

    dangGiaoHang() {
        this.dialogRef.close("dangGiaoHang");
    }

    giaoHangThanhCong() {
        this.dialogRef.close("giaoHangThanhCong");
    }

    giaoHangThatBai() {
        let item = {
            lyDoHuy: "",
            title: "Lý do giao hàng thất bại",
            type: "giaoHangThatBai"
        }
        let ref = this.dialog.open(LyDoHuyComponent, {
            width: "32rem",
            data: {
                item
            }
        });
        ref.afterClosed().subscribe((res: any) => {
            if(res) {
                this.dialogRef.close(item);
            }
        });
    }

    openChat() {
        this.resetChat.setShopChatStatus(true);
        this.resetChat.setChatStatus(true);
        this.resetChat.setChatUserIdForShop(this.data.item.userId);
    }
}
