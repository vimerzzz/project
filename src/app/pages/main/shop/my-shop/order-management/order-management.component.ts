import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { Sort } from '@angular/material/sort';
import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { BaseAuth } from 'src/app/shared/auth/base.auth';
import { GuidConstants } from 'src/app/shared/constants/guid.constants';
import { MessageConstants } from 'src/app/shared/constants/message.constants';
import { mType, PopUpService } from 'src/app/shared/services/base/pop-up.service';
import { OrderManagementService } from 'src/app/shared/services/main/shop/my-shop/order-management.service';
import { ViewDetailComponent } from './view-detail/view-detail.component';

@Component({
    selector: 'app-order-management',
    templateUrl: './order-management.component.html',
    styleUrls: ['./order-management.component.scss']
})
export class OrderManagementComponent implements OnInit {
    loaded: Promise<boolean>;
    shopLoaded: Promise<boolean>;
    loggedUser: any;
    userInfo: any;
    shop: any;
    tabIndex: any = 0;
    currentTab: any = 0;
    totalRecord: any;
    stt: any = 1;
    displayedColumns = ["stt", "maDonHang", "soSanPham", "createdAt", "tinhTrangDonHangId", "thaoTac"];
    sortActive: any;
    sortDirection: any;
    listDonHang: any;
    acceptingStatus = GuidConstants.ONE;
    acceptedStatus = GuidConstants.TWO;
    deliveringStatus = GuidConstants.THREE;
    deliveredStatus = GuidConstants.FOUR;
    canceledStatus = GuidConstants.FIVE;
    canceledDueDeliveringStatus = GuidConstants.SIX;
    init: any = true;
    listTab = [
        { name: "Tất cả" },
        { name: "Đang xác nhận" },
        { name: "Đã xác nhận" },
        { name: "Đang giao hàng" },
        { name: "Đã hoàn tất" },
        { name: "Đã hủy" }
    ]
    params: any = {
        pageIndex: 1,
        pageSize: 10,
        order: null,
        orderBy: null,
        shopId: null,
        tinhTrangDonHangId: null,
        maDonHang: null
    }
    paramsChange: any = {
        maDonHang: null
    }
    @ViewChild("paginator") paginator: MatPaginator;

    constructor(private titleService: Title, private base: BaseAuth, private dialog: MatDialog, private popUpService: PopUpService, private orderManagementService: OrderManagementService, private router: Router) { }

    ngOnInit(): void {
        this.titleService.setTitle("Quản lý đơn hàng");
        this.base.checkExpire();
        this.getLoggedUser();
    }

    getLoggedUser() {
        let object = localStorage.getItem("loggedUser");
        if (object) {
            this.loggedUser = JSON.parse(object);
            this.getUserInfoByUserId(this.loggedUser.userId);
        }
    }

    getUserInfoByUserId(id: any) {
        this.orderManagementService.getUserInfoByUserId(id).subscribe({
            next: (res: any) => {
                this.userInfo = res;
                this.checkUserInfo();
            }
        });
    }

    checkUserInfo() {
        if (!this.userInfo.emailConfirmed) {
            this.popUpService.open(MessageConstants.NEED_EMAIL_VALIDATION, mType.info);
            this.router.navigateByUrl("/information/setting");
        }
        else {
            if (!this.userInfo.coShop) {
                this.router.navigateByUrl("/shop/my-shop/create-shop");
            }
            else {
                this.getShopByUserId()
            }
        }
    }

    getShopByUserId() {
        this.orderManagementService.getShopByUserId(this.loggedUser.userId).subscribe({
            next: (res: any) => {
                if(res) {
                    this.shop = res;
                    this.params.shopId = this.shop.id;
                    this.shopLoaded = Promise.resolve(true);
                    this.changeTab();
                }
            }
        });
    }

    changeTab(e?: any) {
        if(this.currentTab == this.tabIndex) {
            if (e && e.pageIndex !== "" && e.pageIndex !== undefined) {
                this.params.pageIndex = e.pageIndex + 1;
            }
            if (e && e.pageSize !== "" && e.pageSize !== undefined) {
                this.params.pageSize = e.pageSize;
            }
        }
        else {
            this.currentTab = this.tabIndex;
            this.params.pageIndex = 1;
            this.params.pageSize = 10;
            this.params.order = null;
            this.params.orderBy = null;
            this.params.maDonHang = null;
            this.paramsChange.maDonHang = null;
            this.init = true;
        }
        if(this.tabIndex == 0) {
            this.params.tinhTrangDonHangId = null;
        }
        else if(this.tabIndex == 1) {
            this.params.tinhTrangDonHangId = this.acceptingStatus;
        }
        else if(this.tabIndex == 2) {
            this.params.tinhTrangDonHangId = this.acceptedStatus;
        }
        else if(this.tabIndex == 3) {
            this.params.tinhTrangDonHangId = this.deliveringStatus;
        }
        else if(this.tabIndex == 4) {
            this.params.tinhTrangDonHangId = this.deliveredStatus;
        }
        else if(this.tabIndex == 5) {
            this.params.tinhTrangDonHangId = this.canceledDueDeliveringStatus;
        }
        this.orderManagementService.getAllDonHang(this.params).subscribe({
            next: (res: any) => {
                this.listDonHang = res.data ? res.data : [];
                for(let item of this.listDonHang) {
                    item.stt = this.stt;
                    this.stt++;
                }
                this.stt = 1;
                this.totalRecord = res.totalRecord;
                if(this.init) {
                    this.loaded = Promise.resolve(true);
                    this.init = false;
                }
                this.sortActive = this.params.orderBy;
                this.sortDirection = this.params.order;
            }
        });
    }

    sortData(sort: Sort) {
        this.params.order = sort.direction;
        this.params.orderBy = sort.active;
        this.changeTab();
    }

    viewDonHang(item: any) {
        let dialogRef = this.dialog.open(ViewDetailComponent, {
            width: "100%",
            data: {
                item
            }
        });
        dialogRef.afterClosed().subscribe((res: any) => {
            if(res == "xacNhan") {
                item.tinhTrangDonHangId = this.acceptedStatus;
                this.updateTinhTrangDonHang(item);
            }
            else if(res == "dangGiaoHang") {
                item.tinhTrangDonHangId = this.deliveringStatus;
                this.updateTinhTrangDonHang(item);
            }
            else if(res == "giaoHangThanhCong") {
                item.tinhTrangDonHangId = this.deliveredStatus;
                this.updateTinhTrangDonHang(item);
            }
            else if(res && res.type == "huyDonHang") {
                item.tinhTrangDonHangId = this.canceledStatus;
                item.lyDoHuy = res.lyDoHuy;
                this.updateTinhTrangDonHang(item);
            }
            else if(res && res.type == "giaoHangThatBai") {
                item.tinhTrangDonHangId = this.canceledDueDeliveringStatus;
                item.lyDoHuy = res.lyDoHuy;
                this.updateTinhTrangDonHang(item);
            }
        });
    }

    updateTinhTrangDonHang(item: any) {
        this.orderManagementService.updateTinhTrangDonHang(this.loggedUser.userId, item).subscribe({
            next: (res: any) => {
                if(res.statusCode == 200) {
                    this.popUpService.open(MessageConstants.UPDATE_SUCCESS, mType.success);
                    this.changeTab();
                }
                else {
                    this.popUpService.open(MessageConstants.UPDATE_FAILED, mType.error);
                }
            },
            error: (res: any) => {
                this.popUpService.open(MessageConstants.UPDATE_FAILED, mType.error);
            }
        });
    }

    toHeadPage() {
        if(this.params.maDonHang != this.paramsChange.maDonHang) {
            this.paginator.firstPage();
            this.paramsChange.maDonHang = this.params.maDonHang;
        }
    }
}
