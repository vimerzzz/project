import { NgModule } from '@angular/core';
import { MaterialModule } from 'src/app/material.module';
import { DirectivesModule } from 'src/app/shared/directives/directives.module';
import { AuctionItemManagementComponent } from './auction-item-management/auction-item-management.component';
import { DialogAddAuctionItemComponent } from './auction-item-management/dialog-add-auction-item/dialog-add-auction-item.component';
import { CreateShopComponent } from './create-shop/create-shop.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { DialogAddItemComponent } from './item-management/dialog-add-item/dialog-add-item.component';
import { ItemManagementComponent } from './item-management/item-management.component';
import { MyShopRoutingModule } from './my-shop-routing.module';
import { MyShopSidenavComponent } from './my-shop-sidenav/my-shop-sidenav.component';
import { OrderManagementComponent } from './order-management/order-management.component';
import { LyDoHuyComponent } from './order-management/view-detail/ly-do-huy/ly-do-huy.component';
import { ViewCouponComponent } from './order-management/view-detail/view-coupon/view-coupon.component';
import { ViewDetailComponent } from './order-management/view-detail/view-detail.component';
import { SettingComponent } from './setting/setting.component';

@NgModule({
    declarations: [
        DashboardComponent,
        MyShopSidenavComponent,
        CreateShopComponent,
        SettingComponent,
        ItemManagementComponent,
        DialogAddItemComponent,
        OrderManagementComponent,
        ViewDetailComponent,
        ViewCouponComponent,
        LyDoHuyComponent,
        AuctionItemManagementComponent,
        DialogAddAuctionItemComponent
    ],
    imports: [
        MyShopRoutingModule,
        MaterialModule,
        DirectivesModule
    ]
})
export class MyShopModule { }
