import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { BaseAuth } from 'src/app/shared/auth/base.auth';
import { mType, PopUpService } from 'src/app/shared/services/base/pop-up.service';
import { CreateShopService } from 'src/app/shared/services/main/shop/my-shop/create-shop.service';
import { MessageConstants } from 'src/app/shared/constants/message.constants';
import { ResetUserInfo } from 'src/app/shared/utilities/reset-status.utility';

@Component({
    selector: 'app-create-shop',
    templateUrl: './create-shop.component.html',
    styleUrls: ['./create-shop.component.scss']
})
export class CreateShopComponent implements OnInit {
    formControl: FormGroup;
    loggedUser: any;
    spinner: any = false;
    userInfo: any;
    error: any;

    constructor(private titleService: Title, private base: BaseAuth, private router: Router, private createShopService: CreateShopService, private formBuilder: FormBuilder, private popUpService: PopUpService, private resetUserInfo: ResetUserInfo) { }

    ngOnInit(): void {
        this.titleService.setTitle("Tạo shop mới");
        this.base.checkExpire();
        this.formControl = this.formBuilder.group({
            tenShop: ["", Validators.required],
            moTaShop: [""]
        });
        this.getLoggedUser();
    }

    getLoggedUser() {
        let object = localStorage.getItem("loggedUser");
        if(object) {
            this.loggedUser = JSON.parse(object);
            this.getUsernameByUserId(this.loggedUser.userId);
            this.getUserInfoByUserId(this.loggedUser.userId);
        }
    }

    getUsernameByUserId(id: any) {
        this.createShopService.getUserById(id).subscribe({
            next: (res: any) => {
                this.f.tenShop.setValue("Shop của " + res.userName);
            }
        });
    }

    getUserInfoByUserId(id: any) {
        this.createShopService.getUserInfoByUserId(id).subscribe({
            next: (res: any) => {
                this.userInfo = res;
                this.checkUserInfo();
            }
        });
    }

    checkUserInfo() {
        if(this.userInfo.coShop) {
            this.router.navigateByUrl("/shop/my-shop/item-management");
        }
        else {

        }
    }

    get f() {
        return this.formControl.controls;
    }

    createShop() {
        this.formControl.markAllAsTouched();
        if(this.formControl.invalid) {
            return;
        }
        else {
            this.spinner = true;
            this.createShopService.getShopByShopName(this.f.tenShop.value).subscribe({
                next: (res: any) => {
                    if(res) {
                        this.popUpService.open(MessageConstants.UPDATE_FAILED, mType.error);
                        this.spinner = false;
                        this.error = MessageConstants.EXISTED_SHOP;
                    }
                    else {
                        this.error = null;
                        let item: any = {
                            tenShop: this.f.tenShop.value,
                            moTaShop: this.f.moTaShop.value,
                            userId: this.loggedUser.userId
                        }
                        this.createShopService.createShop(item).subscribe({
                            next: (ress: any) => {
                                if(ress.statusCode == 200) {
                                    this.popUpService.open(MessageConstants.CREATE_SUCCESS, mType.success);
                                    this.spinner = false;
                                    this.router.navigateByUrl("/shop/my-shop/item-management");
                                    this.resetUserInfo.setUserInfoStatus(true);
                                }
                                else {
                                    this.popUpService.open(MessageConstants.CREATE_FAILED, mType.error);
                                    this.spinner = false;
                                }
                            },
                            error: (ress: any) => {
                                this.popUpService.open(MessageConstants.CREATE_FAILED, mType.error);
                                this.spinner = false;
                            }
                        });
                    }
                },
                error: (res: any) => {
                    this.popUpService.open(MessageConstants.SYSTEM_ERROR, mType.error);
                    this.spinner = false;
                }
            });
        }
    }
}
