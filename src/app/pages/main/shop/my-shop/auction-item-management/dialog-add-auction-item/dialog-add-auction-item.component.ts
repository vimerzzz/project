import { formatDate } from '@angular/common';
import { Component, ElementRef, Inject, OnInit, ViewChild } from '@angular/core';
import { AbstractControl, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DomSanitizer } from '@angular/platform-browser';
import { HtmlEditorService, ImageService, LinkService, RichTextEditorComponent, ToolbarService } from '@syncfusion/ej2-angular-richtexteditor';
import { GuidConstants } from 'src/app/shared/constants/guid.constants';
import { MessageConstants } from 'src/app/shared/constants/message.constants';
import { ImageCropService } from 'src/app/shared/services/base/image-crop.service';
import { mType, PopUpService } from 'src/app/shared/services/base/pop-up.service';
import { ViewImageService } from 'src/app/shared/services/base/view-image.service';
import { AuctionItemManagementService } from 'src/app/shared/services/main/shop/my-shop/auction-item-management.service';
import { convertViToEn } from 'src/app/shared/utilities/convert-language.utility';
import { environment } from 'src/environments/environment';

@Component({
    selector: 'app-dialog-add-auction-item',
    templateUrl: './dialog-add-auction-item.component.html',
    styleUrls: ['./dialog-add-auction-item.component.scss'],
    providers: [ToolbarService, LinkService, ImageService, HtmlEditorService]
})
export class DialogAddAuctionItemComponent implements OnInit {
    formControl: FormGroup;
    listLoaiSanPham: any;
    listLoaiSanPhamFilter: any;
    listFileToUpload: any = [];
    listFileToUpdate: any = [];
    listFileToDelete: any = [];
    listThuocTinh: any;
    searchString: FormControl;
    correctType: any = true;
    itemImage: any = [];
    index: any = 0;
    peek: any = [];
    noImage: any = false;
    type: any;
    khongGiamGia: any = true;
    dayNow: any;
    monthNow: any;
    yearNow: any;
    minDate: any;
    minDate2: any;
    maxDate: any;
    maxDate2: any;
    loggedUser: any;
    rteImageUploading: any;
    rteImageUploadSuccess: any;
    tools: object = {
        items: ['Undo', 'Redo', '|',
            'Bold', 'Italic', 'Underline', 'StrikeThrough', '|','Image', '|',
            'FontName', 'FontSize', 'FontColor', '|',
            'Alignments', '|', 'OrderedList', 'UnorderedList', '|',
            'CreateLink', 'RemoveLink', '|', 'ClearFormat']
    };
    insertImageSettings: any = {
        saveUrl: `${environment.apiUrl}/api/sanPhamDauGia/uploadImageGioiThieuSanPham?userId=`,
        path: `${environment.apiUrl}/api/Statics/Images/Auctions/`,
        resize: false,
    }
    @ViewChild("formRTE") rte: RichTextEditorComponent;
    @ViewChild("imageUpload") imageUpload: ElementRef;
    @ViewChild("imagePanel") imagePanel: ElementRef;

    constructor(private formBuilder: FormBuilder, private dialogRef: MatDialogRef<DialogAddAuctionItemComponent>, private viewImageService: ViewImageService, private sanitizer: DomSanitizer, private imageCropService: ImageCropService, private popUpService: PopUpService, private auctionItemManagementService: AuctionItemManagementService,
        @Inject(MAT_DIALOG_DATA) public data: any) {}

    ngOnInit(): void {
        this.formControl = this.formBuilder.group({
            tenSanPham: ["", Validators.required],
            giaKhoiDiem: ["", Validators.required],
            khoangGiaToiThieu: ["", Validators.required],
            thoiGianBatDau: ["", Validators.required],
            thoiGianKetThuc: ["", Validators.required],
            timeBatDau: [""],
            timeKetThuc: [""],
            thongTinKhac: [""],
            loaiSanPham: ["", Validators.required],
            nongDo: ["", [Validators.required, Validators.max(100)]],
            xuatXu: ["", Validators.required],
            nam: [""],
            mauSac: ["", Validators.required],
            dungTich: ["", Validators.required]
        });
        this.getLoggedUser();
        this.getAllLoaiSanPham();
        this.getMinDate();
        this.getMaxDate();
        if(this.data.item.id) {
            this.f.timeBatDau.setValue(this.data.item.thoiGianBatDau.split("T")[1].split(":")[0] + ":" + this.data.item.thoiGianBatDau.split("T")[1].split(":")[1]);
            this.f.timeKetThuc.setValue(this.data.item.thoiGianKetThuc.split("T")[1].split(":")[0] + ":" + this.data.item.thoiGianKetThuc.split("T")[1].split(":")[1]);
            this.getAllHinhAnhSanPham();
        }
        else {
            this.f.timeBatDau.setValue("00:00");
            this.f.timeKetThuc.setValue("00:00");
        }
        this.searchString = new FormControl("");
        this.searchString.valueChanges.subscribe((value: any) => {
            this.listLoaiSanPhamFilter = this.listLoaiSanPham.filter((item: any) => convertViToEn(item.tenLoaiSanPham).includes(convertViToEn(value)));
        });
    }

    getLoggedUser() {
        let object = localStorage.getItem("loggedUser");
        if (object) {
            this.loggedUser = JSON.parse(object);
            if(this.data.item.id) {
                this.insertImageSettings.saveUrl += `${this.loggedUser.userId}&sanPhamDauGiaId=${this.data.item.id}`;
                this.insertImageSettings.path += `${this.data.item.shopId}/${this.data.item.id}/`;
            }
        }
    }

    getAllLoaiSanPham() {
        let param: any = {
            order: "asc",
            orderBy: "tenLoaiSanPham"
        }
        this.auctionItemManagementService.getAllLoaiSanPham(param).subscribe({
            next: (res: any) => {
                this.listLoaiSanPham = res.data ? res.data : [];
                let otherId = GuidConstants.ONE;
                let object = this.listLoaiSanPham.find((s: any) => s.id == otherId);
                if(object) {
                    this.listLoaiSanPham.splice(this.listLoaiSanPham.indexOf(object), 1);
                    this.listLoaiSanPham.push(object);
                }
                this.listLoaiSanPhamFilter = this.listLoaiSanPham;
            }
        });
    }

    getAllHinhAnhSanPham() {
        this.auctionItemManagementService.getAllHinhAnhSanPhamDauGiaBySanPhamId(this.data.item.id).subscribe({
            next: (res: any) => {
                this.listFileToUpdate = res ? res : [];
                for(let item of this.listFileToUpdate) {
                    if(item.hinhAnh != null) {
                        this.itemImage.push(environment.apiUrl + "/api/" + item.hinhAnh.replaceAll("\\", "/"));
                    }
                }
            }
        });
    }

    checkFileType() {
        for (let index = 0; index < this.imageUpload.nativeElement.files.length; index++) {
            if (this.imageUpload.nativeElement.files[index].type != "image/png" &&
                this.imageUpload.nativeElement.files[index].type != "image/jpeg" &&
                this.imageUpload.nativeElement.files[index].type != "image/webp") {
                this.correctType = false;
            }
            else {
                this.correctType = true;
            }
            if (!this.correctType) {
                this.popUpService.open(MessageConstants.WRONG_FILE_TYPE, mType.warn);
                break;
            }
            if (index == this.imageUpload.nativeElement.files.length - 1) {
                this.correctType = true;
                this.openImageCrop();
            }
        }
    }

    openImageCrop() {
        let files: string[] = [];
        let names: string[] = [];
        this.readFile(0, files, names);
    }

    readFile(index: any, files: string[], names: string[]) {
        if (index >= this.imageUpload.nativeElement.files.length) return;
        let reader = new FileReader();
        reader.onload = (e: any) => {
            names.push(this.imageUpload.nativeElement.files[index].name);
            files.push(e.target.result);
            if (index == this.imageUpload.nativeElement.files.length - 1) {
                this.imageCropService.open(files, 3, 4, 900, names, index + 1);
                this.imageCropService.image.subscribe((res: any) => {
                    if (res == "") {
                        this.imageUpload.nativeElement.value = "";
                    }
                    else {
                        let list = new DataTransfer();
                        for (let item of res) {
                            list.items.add(item);
                        }
                        if(this.type == 0) {
                            this.listFileToUpload = [];
                            this.itemImage = [];
                            this.peek = [];
                            for(let item of this.listFileToUpdate) {
                                this.listFileToDelete.push(item);
                            }
                            this.listFileToUpdate = [];
                        }
                        this.index = this.listFileToUpdate.length + this.listFileToUpload.length;
                        let indexToPush = this.listFileToUpload.length;
                        this.noImage = false;
                        for(let i = 0; i < list.files.length; i++) {
                            this.listFileToUpload.push(list.files[i]);
                        }
                        this.displayImage(this.index, indexToPush);
                    }
                });
            }
            this.readFile(index + 1, files, names);
        }
        reader.readAsDataURL(this.imageUpload.nativeElement.files[index]);
    }

    displayImage(index: any, indexToPush: any) {
        if (index >= this.listFileToUpdate.length + this.listFileToUpload.length) {
            setTimeout(() => {
                this.imagePanel.nativeElement.scrollIntoView({
                    behavior: 'smooth',
                    block: 'start',
                    inline: 'nearest'
                });
                document.getElementById("thumbnailPanel")!.scrollIntoView({
                    behavior: 'smooth',
                    block: 'start',
                    inline: 'nearest'
                });
            }, 1);
            return;
        }
        let reader = new FileReader();
        reader.onload = (e: any) => {
            this.itemImage[index] = this.sanitizer.bypassSecurityTrustUrl(e.target.result);
            this.peek[index] = false;
            this.displayImage(index + 1, indexToPush + 1);
        }
        reader.readAsDataURL(this.listFileToUpload[indexToPush]);
    }

    getIndexOfListUpdate(index: any) {
        let countUpdate = -1;
        for(let i = 0; i <= index; i++) {
            if(typeof(this.itemImage[i]) == "string") {
                countUpdate++;
            }
        }
        return countUpdate;
    }

    getIndexOfListUpload(index: any) {
        let countUpload = -1;
        for(let i = 0; i <= index; i++) {
            if(typeof(this.itemImage[i]) == "object") {
                countUpload++;
            }
        }
        return countUpload;
    }

    removeImage(index: any) {
        if(this.data.item.id) {
            let countUpdate = this.getIndexOfListUpdate(index);
            let countUpload = this.getIndexOfListUpload(index);
            if(typeof(this.itemImage[index]) == "string") {
                this.listFileToDelete.push(this.listFileToUpdate.splice(countUpdate, 1)[0]);
                for(let i = countUpdate; i < this.listFileToUpdate.length; i++) {
                    this.listFileToUpdate[i].thuTu--;
                }
            }
            else {
                this.listFileToUpload.splice(countUpload, 1);
                for(let i = countUpdate + 1; i < this.listFileToUpdate.length; i++) {
                    this.listFileToUpdate[i].thuTu--;
                }
            }
        }
        else {
            this.listFileToUpload.splice(index, 1);
        }
        this.itemImage.splice(index, 1);
        if(index < this.index) {
            this.index--;
        }
        else {
            if(this.index == this.itemImage.length) {
                this.index--;
            }
        }
    }

    toLeft() {
        this.index = this.index == 0 ? 0 : this.index - 1;
        this.smooth();
    }

    toRight() {
        this.index = this.index == this.itemImage.length - 1 ? this.itemImage.length - 1 : this.index + 1;
        this.smooth();
    }

    toIndex(index: any) {
        this.index = index;
        this.smooth();
    }

    smooth() {
        setTimeout(() => {
            document.getElementById("thumbnailPanel")!.scrollIntoView({
                behavior: 'smooth',
                block: 'start',
                inline: 'nearest'
            });
        }, 1);
    }

    prevSwitch(index: any) {
        if(this.data.item.id) {
            let countUpdate = this.getIndexOfListUpdate(index);
            let countUpload = this.getIndexOfListUpload(index);
            if(typeof(this.itemImage[index]) == "string") {
                if(typeof(this.itemImage[index - 1]) == "string") {
                    this.listFileToUpdate[countUpdate].thuTu--;
                    this.listFileToUpdate[countUpdate - 1].thuTu++;
                    let temp2 = this.listFileToUpdate[countUpdate];
                    this.listFileToUpdate[countUpdate] = this.listFileToUpdate[countUpdate - 1];
                    this.listFileToUpdate[countUpdate - 1] = temp2;
                }
                else {
                    this.listFileToUpdate[countUpdate].thuTu--;
                }
            }
            else {
                if(typeof(this.itemImage[index - 1]) == "string") {
                    this.listFileToUpdate[countUpdate].thuTu++;
                }
                else {
                    let temp2 = this.listFileToUpload[countUpload];
                    this.listFileToUpload[countUpload] = this.listFileToUpload[countUpload - 1];
                    this.listFileToUpload[countUpload - 1] = temp2;
                }
            }
        }
        else {
            let temp2 = this.listFileToUpload[index];
            this.listFileToUpload[index] = this.listFileToUpload[index - 1];
            this.listFileToUpload[index - 1] = temp2;
        }
        let temp1 = this.itemImage[index];
        this.itemImage[index] = this.itemImage[index - 1];
        this.itemImage[index - 1] = temp1;
    }

    nextSwitch(index: any) {
        if(this.data.item.id) {
            let countUpdate = this.getIndexOfListUpdate(index);
            let countUpload = this.getIndexOfListUpload(index);
            if(typeof(this.itemImage[index]) == "string") {
                if(typeof(this.itemImage[index + 1]) == "string") {
                    this.listFileToUpdate[countUpdate].thuTu++;
                    this.listFileToUpdate[countUpdate + 1].thuTu--;
                    let temp2 = this.listFileToUpdate[countUpdate];
                    this.listFileToUpdate[countUpdate] = this.listFileToUpdate[countUpdate + 1];
                    this.listFileToUpdate[countUpdate + 1] = temp2;
                }
                else {
                    this.listFileToUpdate[countUpdate].thuTu++;
                }
            }
            else {
                if(typeof(this.itemImage[index + 1]) == "string") {
                    this.listFileToUpdate[countUpdate + 1].thuTu--;
                }
                else {
                    let temp2 = this.listFileToUpload[countUpload];
                    this.listFileToUpload[countUpload] = this.listFileToUpload[countUpload + 1];
                    this.listFileToUpload[countUpload + 1] = temp2;
                }
            }
        }
        else {
            let temp2 = this.listFileToUpload[index];
            this.listFileToUpload[index] = this.listFileToUpload[index + 1];
            this.listFileToUpload[index + 1] = temp2;
        }
        let temp1 = this.itemImage[index];
        this.itemImage[index] = this.itemImage[index + 1];
        this.itemImage[index + 1] = temp1;
    }

    openImage(imageSource: string) {
        this.viewImageService.open(imageSource);
    }

    // changeLoaiSanPham() {
    //     if(this.data.item.loaiSanPhamId && this.f.loaiSanPham.dirty) {
    //         this.auctionItemManagementService.getAllThuocTinh(this.data.item.loaiSanPhamId).subscribe({
    //             next: (res: any) => {
    //                 this.listThuocTinh = res ? res : [];
    //                 for(let item of this.listLoaiSanPham) {
    //                     if(item.id == this.data.item.loaiSanPhamId) {
    //                         this.data.item.thongTinKhac = "Loại sản phẩm: " + item.tenLoaiSanPham;
    //                         if(item.moTaLoaiSanPham) {
    //                             this.data.item.thongTinKhac += "\nMô tả loại sản phẩm: " + item.moTaLoaiSanPham;
    //                         }
    //                         break;
    //                     }
    //                 }
    //                 if(this.listThuocTinh.length) {
    //                     this.data.item.thongTinKhac += "\n";
    //                 }
    //                 for(let item of this.listThuocTinh) {
    //                     this.data.item.thongTinKhac += "\n" + item.tenThuocTinh + ": ";
    //                     if(item.thuocTinhMacDinh) {
    //                         this.data.item.thongTinKhac += item.thuocTinhMacDinh;
    //                     }
    //                 }
    //             }
    //         });
    //     }
    // }

    getMinDate() {
        let now = new Date();
        now.setDate(now.getDate() + 5);
        this.dayNow = now.getDate();
        this.monthNow = now.getMonth() + 1;
        this.yearNow = now.getFullYear();
        if(this.dayNow < 10) {
            if(this.monthNow < 10) {
                this.minDate = this.yearNow + "-" + "0" + this.monthNow + "-" + "0" + this.dayNow;
            }
            else {
                this.minDate = this.yearNow + "-" + this.monthNow + "-" + "0" + this.dayNow;
            }
        }
        else {
            if(this.monthNow < 10) {
                this.minDate = this.yearNow + "-" + "0" + this.monthNow + "-" + this.dayNow;
            }
            else {
                this.minDate = this.yearNow + "-" + this.monthNow + "-" + this.dayNow;
            }
        }
        now.setDate(now.getDate() + 4);
        this.dayNow = now.getDate();
        this.monthNow = now.getMonth() + 1;
        this.yearNow = now.getFullYear();
        if(this.dayNow < 10) {
            if(this.monthNow < 10) {
                this.minDate2 = this.yearNow + "-" + "0" + this.monthNow + "-" + "0" + this.dayNow;
            }
            else {
                this.minDate2 = this.yearNow + "-" + this.monthNow + "-" + "0" + this.dayNow;
            }
        }
        else {
            if(this.monthNow < 10) {
                this.minDate2 = this.yearNow + "-" + "0" + this.monthNow + "-" + this.dayNow;
            }
            else {
                this.minDate2 = this.yearNow + "-" + this.monthNow + "-" + this.dayNow;
            }
        }
    }

    getMaxDate() {
        if(this.data.item.thoiGianBatDau) {
            let date = new Date(this.data.item.thoiGianBatDau);
            date.setDate(date.getDate() + 7);
            this.dayNow = date.getDate();
            this.monthNow = date.getMonth() + 1;
            this.yearNow = date.getFullYear();
            if(this.dayNow < 10) {
                if(this.monthNow < 10) {
                    this.maxDate = this.yearNow + "-" + "0" + this.monthNow + "-" + "0" + this.dayNow;
                }
                else {
                    this.maxDate = this.yearNow + "-" + this.monthNow + "-" + "0" + this.dayNow;
                }
            }
            else {
                if(this.monthNow < 10) {
                    this.maxDate = this.yearNow + "-" + "0" + this.monthNow + "-" + this.dayNow;
                }
                else {
                    this.maxDate = this.yearNow + "-" + this.monthNow + "-" + this.dayNow;
                }
            }
            date.setDate(date.getDate() + 8);
            this.dayNow = date.getDate();
            this.monthNow = date.getMonth() + 1;
            this.yearNow = date.getFullYear();
            if(this.dayNow < 10) {
                if(this.monthNow < 10) {
                    this.maxDate2 = this.yearNow + "-" + "0" + this.monthNow + "-" + "0" + this.dayNow;
                }
                else {
                    this.maxDate2 = this.yearNow + "-" + this.monthNow + "-" + "0" + this.dayNow;
                }
            }
            else {
                if(this.monthNow < 10) {
                    this.maxDate2 = this.yearNow + "-" + "0" + this.monthNow + "-" + this.dayNow;
                }
                else {
                    this.maxDate2 = this.yearNow + "-" + this.monthNow + "-" + this.dayNow;
                }
            }
        }
        else {
            this.maxDate = null;
        }
    }

    get f() {
        return this.formControl.controls;
    }

    wrongTimeRange(control: AbstractControl): {[key: string]: any} {
        return {"wrongTimeRange": true};
    }

    setTime() {
        this.data.item.thoiGianBatDau = formatDate(this.data.item.thoiGianBatDau, "yyyy-MM-dd", "en-GB") + "T" + this.f.timeBatDau.value + ":00";
        this.data.item.thoiGianKetThuc = formatDate(this.data.item.thoiGianKetThuc, "yyyy-MM-dd", "en-GB") + "T" + this.f.timeKetThuc.value + ":00";
        if(this.data.item.thoiGianBatDau >= this.data.item.thoiGianKetThuc || this.data.item.thoiGianBatDau < new Date(this.minDate2) || this.data.item.thoiGianKetThuc > new Date(this.maxDate2)) {
            this.f.thoiGianBatDau.setValidators([this.wrongTimeRange, Validators.required]);
            this.f.thoiGianBatDau.markAsTouched();
            this.f.thoiGianBatDau.updateValueAndValidity();
        }
        else {
            this.f.thoiGianBatDau.setValidators([Validators.required]);
            this.f.thoiGianBatDau.markAsTouched();
            this.f.thoiGianBatDau.updateValueAndValidity();
            this.save();
        }
    }

    save() {
        if (this.itemImage.length == 0) {
            this.noImage = true;
        }
        this.formControl.markAllAsTouched();
        if (this.formControl.invalid || this.noImage) {
            return;
        }
        else {
            for(let item of this.listFileToUpdate) {
                this.data.updateImage.push(item);
            }
            for(let item of this.listFileToDelete) {
                this.data.deleteImage.push(item.id);
            }
            for(let index in this.listFileToUpload) {
                this.data.image.push(this.listFileToUpload[index]);
            }
            this.dialogRef.close(true);
        }
    }

    imageSubscribe() {
        this.rte.imageModule.uploadObj.selected = (e: any) => {
            this.rteImageUploadSuccess = this.rte.imageUploadSuccess.subscribe((res: any) => {
                let obj = JSON.parse(res.e.currentTarget.response);
                if(obj && obj.noiDung) {
                    e.filesData[0].name = obj.noiDung;
                }
                if(this.rteImageUploadSuccess) {
                    this.rteImageUploadSuccess.unsubscribe();
                }
            });
        }
        this.rteImageUploading = this.rte.imageUploading.subscribe((res: any) => {
            res.currentRequest.setRequestHeader('Authorization', `Bearer ${this.loggedUser.token}`);
        });
    }

    imageDestroy() {
        if(this.rteImageUploading) {
            this.rteImageUploading.unsubscribe();
        }
        if(this.rteImageUploadSuccess) {
            this.rteImageUploadSuccess.unsubscribe();
        }
    }
}
