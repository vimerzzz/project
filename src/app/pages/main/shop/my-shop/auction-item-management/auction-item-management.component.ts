import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { Sort } from '@angular/material/sort';
import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { HubConnection, HubConnectionBuilder } from '@microsoft/signalr';
import { BaseAuth } from 'src/app/shared/auth/base.auth';
import { ConfirmDeleteComponent } from 'src/app/shared/components/confirm-delete/confirm-delete.component';
import { GuidConstants } from 'src/app/shared/constants/guid.constants';
import { MessageConstants } from 'src/app/shared/constants/message.constants';
import { SocketConstants } from 'src/app/shared/constants/socket.constants';
import { mType, PopUpService } from 'src/app/shared/services/base/pop-up.service';
import { AuctionItemManagementService } from 'src/app/shared/services/main/shop/my-shop/auction-item-management.service';
import { DialogAddAuctionItemComponent } from './dialog-add-auction-item/dialog-add-auction-item.component';

@Component({
    selector: 'app-auction-item-management',
    templateUrl: './auction-item-management.component.html',
    styleUrls: ['./auction-item-management.component.scss'],
})
export class AuctionItemManagementComponent implements OnInit {
    loaded: Promise<boolean>;
    loggedUser: any;
    userInfo: any;
    shop: any;
    listSanPham: any;
    listLoaiSanPham: any;
    listTinhTrang: any;
    totalRecord: any;
    stt: any = 1;
    sortActive: any;
    sortDirection: any;
    init: any = true;
    displayedColumns = ["stt", "tenSanPham", "thoiGianBatDau", "thoiGianKetThuc", "tinhTrang", "createdAt", "thaoTac"];
    pendingApproveGuid = GuidConstants.ONE;
    unApproveGuid = GuidConstants.TWO;
    overdueApproveGuid = GuidConstants.THREE;
    pendingAuctionGuid = GuidConstants.FOUR;
    inAuctionGuid = GuidConstants.FIVE;
    successAuctionGuid = GuidConstants.SIX;
    failedAuctionGuid = GuidConstants.SEVEN;
    canceledAuctionGuid = GuidConstants.EIGHT;
    successPayAuctionGuid = GuidConstants.NINE;
    hub: HubConnection;
    params: any = {
        pageIndex: 1,
        pageSize: 10,
        searchString: null,
        order: null,
        orderBy: null,
        shopId: null,
        loaiSanPhamId: "",
        tinhTrangDauGiaId: ""
    }
    paramsChange: any = {
        searchString: null,
        loaiSanPhamId: null,
        tinhTrangDauGiaId: null
    }
    @ViewChild("paginator") paginator: MatPaginator;

    constructor(private titleService: Title, private base: BaseAuth, private router: Router, private auctionItemManagementService: AuctionItemManagementService, private popUpService: PopUpService, private dialog: MatDialog) {}

    ngOnInit(): void {
        this.titleService.setTitle("Quản lý sản phẩm đấu giá");
        this.base.checkExpire();
        this.getLoggedUser();
        this.startSocket();
        this.addDauGiaListener();
    }

    getLoggedUser() {
        let object = localStorage.getItem("loggedUser");
        if (object) {
            this.loggedUser = JSON.parse(object);
            this.getUserInfoByUserId(this.loggedUser.userId);
        }
    }

    getUserInfoByUserId(id: any) {
        this.auctionItemManagementService.getUserInfoByUserId(id).subscribe({
            next: (res: any) => {
                this.userInfo = res;
                this.checkUserInfo();
            }
        });
    }

    checkUserInfo() {
        if (!this.userInfo.emailConfirmed) {
            this.popUpService.open(MessageConstants.NEED_EMAIL_VALIDATION, mType.info);
            this.router.navigateByUrl("/information/setting");
        }
        else {
            if (!this.userInfo.coShop) {
                this.router.navigateByUrl("/shop/my-shop/create-shop");
            }
            else {
                this.getShopByUserId();
                this.getAllLoaiSanPham();
                this.getAllTinhTrangDauGia();
            }
        }
    }

    getShopByUserId() {
        this.auctionItemManagementService.getShopByUserId(this.loggedUser.userId).subscribe({
            next: (res: any) => {
                if(res) {
                    this.shop = res;
                    this.params.shopId = this.shop.id;
                    this.getAllSanPhamDauGiaByShopId();
                }
            }
        });
    }

    getAllSanPhamDauGiaByShopId(e?: any) {
        if (e && e.pageIndex !== "" && e.pageIndex !== undefined) {
            this.params.pageIndex = e.pageIndex + 1;
        }
        if (e && e.pageSize !== "" && e.pageSize !== undefined) {
            this.params.pageSize = e.pageSize;
        }
        this.auctionItemManagementService.getAllSanPhamDauGiaByShopId(this.params).subscribe({
            next: (res: any) => {
                this.listSanPham = res.data ? res.data : [];
                for(let diaChi of this.listSanPham) {
                    diaChi.stt = this.stt;
                    this.stt++;
                }
                this.stt = 1;
                this.totalRecord = res.totalRecord;
                if(this.init) {
                    this.loaded = Promise.resolve(true);
                    this.init = false;
                }
                this.sortActive = this.params.orderBy;
                this.sortDirection = this.params.order;
            }
        });
    }

    getAllLoaiSanPham() {
        let param: any = {
            order: "asc",
            orderBy: "tenLoaiSanPham"
        }
        this.auctionItemManagementService.getAllLoaiSanPham(param).subscribe({
            next: (res: any) => {
                this.listLoaiSanPham = res.data ? res.data : [];
                let otherId = GuidConstants.ONE;
                let object = this.listLoaiSanPham.find((s: any) => s.id == otherId);
                if(object) {
                    this.listLoaiSanPham.splice(this.listLoaiSanPham.indexOf(object), 1);
                    this.listLoaiSanPham.push(object);
                }
            }
        });
    }

    getAllTinhTrangDauGia() {
        this.auctionItemManagementService.getAllTinhTrangDauGia().subscribe({
            next: (res: any) => {
                this.listTinhTrang = res ? res : [];
            }
        });
    }

    openDialogCreateSanPhamDauGia() {
        let item: any = {
            userId: this.shop.userId,
            shopId: this.shop.id,
            tenSanPham: null,
            giaKhoiDiem: null,
            khoangGiaToiThieu: null,
            giaHienTai: 0,
            thongTinKhac: null,
            thoiGianBatDau: null,
            thoiGianKetThuc: null,
            loaiSanPhamId: "",
            nongDo: null,
            xuatXu: null,
            nam: 0,
            mauSac: null,
            dungTich: null
        }
        let image: any = [];
        let dialogRef = this.dialog.open(DialogAddAuctionItemComponent, {
            width: "48rem",
            data: {
                item,
                image
            }
        });
        dialogRef.afterClosed().subscribe((res: any) => {
            if(res) {
                this.auctionItemManagementService.createSanPhamDauGia(item).subscribe({
                    next: (res: any) => {
                        if(res.statusCode == 200) {
                            this.createHinhAnhSanPhamDauGia(image, res.id, 1);
                        }
                        else {
                            this.popUpService.open(MessageConstants.CREATE_FAILED, mType.error);
                        }
                    },
                    error: (res: any) => {
                        this.popUpService.open(MessageConstants.CREATE_FAILED, mType.error);
                    }
                });
            }
        });
    }

    openDialogUpdateSanPhamDauGia(item: any) {
        let image: any = [];
        let deleteImage: any = [];
        let updateImage: any = [];
        let dialogRef = this.dialog.open(DialogAddAuctionItemComponent, {
            width: "48rem",
            data: {
                item,
                image,
                deleteImage,
                updateImage
            }
        });
        dialogRef.afterClosed().subscribe((res: any) => {
            if(res) {
                this.auctionItemManagementService.updateSanPhamDauGia(item).subscribe({
                    next: (res: any) => {
                        if(res.statusCode == 200) {
                            this.deleteHinhAnhSanPhamDauGia(deleteImage, updateImage, image, item.id, 2);
                        }
                        else {
                            this.popUpService.open(MessageConstants.UPDATE_FAILED, mType.error);
                        }
                    },
                    error: (res: any) => {
                        this.popUpService.open(MessageConstants.UPDATE_FAILED, mType.error);
                    }
                });
            }
        });
    }

    cancelSanPhamDauGia(item: any) {
        let dialogRef = this.dialog.open(ConfirmDeleteComponent, {
            width: "32rem",
            data: {
                title: "Bạn có chắc muốn hủy đấu giá?"
            }

        });
        dialogRef.afterClosed().subscribe((res: any) => {
            if(res) {
                this.auctionItemManagementService.cancelSanPhamDauGia(item).subscribe({
                    next: (res: any) => {
                        if(res.statusCode == 200) {
                            this.popUpService.open(MessageConstants.CANCEL_SUCCESS, mType.success);
                            this.getAllSanPhamDauGiaByShopId();
                        }
                        else {
                            this.popUpService.open(MessageConstants.CANCEL_FAILED, mType.error);
                        }
                    },
                    error: (res: any) => {
                        this.popUpService.open(MessageConstants.CANCEL_FAILED, mType.error);
                    }
                });
            }
        });
    }

    deleteSanPhamDauGia(item: any) {
        let dialogRef = this.dialog.open(ConfirmDeleteComponent, {
            width: "32rem"
        });
        dialogRef.afterClosed().subscribe((res: any) => {
            if(res) {
                this.auctionItemManagementService.deleteSanPhamDauGia(item).subscribe({
                    next: (res: any) => {
                        if(res.statusCode == 200) {
                            this.popUpService.open(MessageConstants.DELETE_SUCCESS, mType.success);
                            this.getAllSanPhamDauGiaByShopId();
                        }
                        else {
                            this.popUpService.open(MessageConstants.DELETE_FAILED, mType.error);
                        }
                    },
                    error: (res: any) => {
                        this.popUpService.open(MessageConstants.DELETE_FAILED, mType.error);
                    }
                });
            }
        });
    }

    createHinhAnhSanPhamDauGia(image: any, sanPhamDauGiaId: any, type: any) {
        let formData = new FormData();
        for(let index in image) {
            if(image[index]) {
                formData.append("file[]", image[index], image[index].name);
            }
            if(parseInt(index) == image.length - 1) {
                this.auctionItemManagementService.createHinhAnhSanPhamDauGia(this.shop.userId, sanPhamDauGiaId, formData).subscribe({
                    next: (res: any) => {
                        if(res.statusCode == 200) {
                            if(type == 1) {
                                this.popUpService.open(MessageConstants.CREATE_SUCCESS, mType.success);
                            }
                            else {
                                this.popUpService.open(MessageConstants.UPDATE_SUCCESS, mType.success);
                            }
                            this.getAllSanPhamDauGiaByShopId();
                        }
                        else {
                            if(type == 1) {
                                this.popUpService.open(MessageConstants.CREATE_FAILED, mType.error);
                            }
                            else {
                                this.popUpService.open(MessageConstants.UPDATE_FAILED, mType.error);
                            }
                        }
                    },
                    error: (res: any) => {
                        if(type == 1) {
                            this.popUpService.open(MessageConstants.CREATE_FAILED, mType.error);
                        }
                        else {
                            this.popUpService.open(MessageConstants.UPDATE_FAILED, mType.error);
                        }
                    }
                });
            }
        }
    }

    updateHinhAnhSanPhamDauGia(updateImage: any, image: any, sanPhamId: any, type: any) {
        if(updateImage.length) {
            this.auctionItemManagementService.updateHinhAnhSanPhamDauGia(updateImage).subscribe({
                next: (res: any) => {
                    if(res.statusCode == 200) {
                        if(image.length) {
                            this.createHinhAnhSanPhamDauGia(image, sanPhamId, type);
                        }
                        else {
                            this.popUpService.open(MessageConstants.UPDATE_SUCCESS, mType.success);
                            this.getAllSanPhamDauGiaByShopId();
                        }
                    }
                    else {
                        this.popUpService.open(MessageConstants.UPDATE_FAILED, mType.error);
                    }
                },
                error: (res: any) => {
                    this.popUpService.open(MessageConstants.UPDATE_FAILED, mType.error);
                }
            });
        }
        else {
            if(image.length) {
                this.createHinhAnhSanPhamDauGia(image, sanPhamId, type);
            }
            else {
                this.popUpService.open(MessageConstants.UPDATE_SUCCESS, mType.success);
                this.getAllSanPhamDauGiaByShopId();
            }
        }
    }

    deleteHinhAnhSanPhamDauGia(deleteImage: any, updateImage: any, image: any, sanPhamId: any, type: any) {
        if(deleteImage.length) {
            this.auctionItemManagementService.deleteHinhAnhSanPhamDauGia(this.shop.userId, deleteImage).subscribe({
                next: (res: any) => {
                    if(res.statusCode == 200) {
                        this.updateHinhAnhSanPhamDauGia(updateImage, image, sanPhamId, type);
                    }
                    else {
                        this.popUpService.open(MessageConstants.UPDATE_FAILED, mType.error);
                    }
                },
                error: (res: any) => {
                    this.popUpService.open(MessageConstants.UPDATE_FAILED, mType.error);
                }
            });
        }
        else {
            this.updateHinhAnhSanPhamDauGia(updateImage, image, sanPhamId, type);
        }
    }

    sortData(sort: Sort) {
        this.params.order = sort.direction;
        this.params.orderBy = sort.active;
        this.getAllSanPhamDauGiaByShopId();
    }

    toHeadPage() {
        if(this.params.searchString != this.paramsChange.searchString) {
            this.paginator.firstPage();
            this.paramsChange.searchString = this.params.searchString;
        }
        if(this.params.loaiSanPhamId != this.paramsChange.loaiSanPhamId) {
            this.paginator.firstPage();
            this.paramsChange.loaiSanPhamId = this.params.loaiSanPhamId;
        }
        if(this.params.tinhTrangDauGiaId != this.paramsChange.tinhTrangDauGiaId) {
            this.paginator.firstPage();
            this.paramsChange.tinhTrangDauGiaId = this.params.tinhTrangDauGiaId;
        }
    }

    startSocket() {
        this.hub = new HubConnectionBuilder().withUrl(SocketConstants.dauGiaHub).build();
        this.hub.start();
    }

    addDauGiaListener() {
        this.hub.on("sanPhamDauGia", (res: any) => {
            if(res) {
                let sanPham = this.listSanPham.find((s: any) => s.id == res.id);
                if(sanPham) {
                    sanPham.tinhTrangDauGiaId = res.tinhTrangDauGiaId;
                    sanPham.tinhTrang = res.tinhTrang;
                }
            }
        });
    }
}
