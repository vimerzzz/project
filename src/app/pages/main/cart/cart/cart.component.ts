import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { ThanhToanButton } from 'src/app/shared/animations/specific.animations';
import { SuccessColor } from 'src/app/shared/animations/text-color.animations';
import { BaseAuth } from 'src/app/shared/auth/base.auth';
import { GuidConstants } from 'src/app/shared/constants/guid.constants';
import { MessageConstants } from 'src/app/shared/constants/message.constants';
import { mType, PopUpService } from 'src/app/shared/services/base/pop-up.service';
import { CartService } from 'src/app/shared/services/main/cart/cart.service';
import { convertViToEn } from 'src/app/shared/utilities/convert-language.utility';
import { ResetGioHang, ResetThongBao } from 'src/app/shared/utilities/reset-status.utility';
import { environment } from 'src/environments/environment';
import { ViewCouponComponent } from './view-coupon/view-coupon.component';

@Component({
    selector: 'app-cart',
    templateUrl: './cart.component.html',
    styleUrls: ['./cart.component.scss'],
    animations: [
        SuccessColor,
        ThanhToanButton
    ]
})
export class CartComponent implements OnInit {
    loggedUser: any;
    listGioHang: any;
    listGioHangDauGia: any;
    loaded: Promise<boolean>;
    userLoaded: Promise<boolean>;
    linkAnimation: any = false;
    payButtonAnimation: any = false;
    thanhTien: any = 0;
    // setGiaGiam: any = 0;
    // phiShip: any = 0;
    // phiShipTruocDieuKien: any = 0;
    // maGiamGia: any;
    blockButton: any = false;
    userInfo: any;
    listDiaChi: any;
    listDiaChiFilter: any;
    formControl: FormGroup;
    datHangHo: any = false;
    diaChiKhac: any;
    themDiaChi: any = true;
    chonDiaChiKhac: any = false;
    diaChi: any = "";
    diaChiSearch: any;
    xuatHoaDon: any = false;
    // listDonVi: any;
    // listDonViFilter: any;
    // donViSearch: any;
    // donVi: any = "";
    // dieuKienMienPhi: any;
    // phuongThucType: any = 0;
    // listMaGiamGiaDaDung: any;
    init: any = {
        ho: null,
        tenDem: null,
        ten: null,
        sdt: null,
        diaChi: "",
        id: null,
        userId: null,
        email: null,
        gioiTinh: null,
        ngaySinh: null,
        anhDaiDien: null,
        coAnhDaiDien: null,
        coShop: null
    }
    @ViewChild("countInput") countInput: ElementRef;

    constructor(private titleService: Title, private base: BaseAuth, private resetThongBao: ResetThongBao, private router: Router, private resetCart: ResetGioHang, private formBuilder: FormBuilder, private dialog: MatDialog, private resetGioHang: ResetGioHang, private popUpService: PopUpService, private cartService: CartService) { }

    ngOnInit(): void {
        this.titleService.setTitle("Giỏ hàng");
        this.base.checkExpire();
        this.formControl = this.formBuilder.group({
            hoDat: [""],
            tenDemDat: [""],
            tenDat: [""],
            sdtDat: [""],
            ho: [""],
            tenDem: [""],
            ten: ["", Validators.required],
            diaChi: ["", Validators.required],
            sdt: ["", Validators.required],
            diaChiKhac: [""],
            // donVi: ["", Validators.required],
            // phuongThucType: [""],
            ghiChu: [""]
        });
        this.getLoggedUser();
        this.search();
    }

    search() {
        this.diaChiSearch = new FormControl("");
        this.diaChiSearch.valueChanges.subscribe((value: any) => {
            this.listDiaChiFilter = this.listDiaChi.filter((item: any) => convertViToEn(item.diaChi).includes(convertViToEn(value)));
        });
        // this.donViSearch = new FormControl("");
        // this.donViSearch.valueChanges.subscribe((value: any) => {
        //     this.listDonViFilter = this.listDonVi.filter((item: any) => convertViToEn(item.tenDonVi).includes(convertViToEn(value)));
        // });
    }

    get f() {
        return this.formControl.controls;
    }

    getLoggedUser() {
        let object = localStorage.getItem("loggedUser");
        if(object) {
            this.loggedUser = JSON.parse(object);
            this.getUserInfoByUserId();
            this.getAllGioHang();
            this.getAllDiaChi();
            // this.getAllDonViVanChuyen();
        }
    }

    getUserInfoByUserId() {
        this.cartService.getUserInfoByUserId(this.loggedUser.userId).subscribe({
            next: (res: any) => {
                this.userInfo = res;
                this.init.ho = this.userInfo.ho;
                this.init.tenDem = this.userInfo.tenDem;
                this.init.ten = this.userInfo.ten;
                this.init.sdt = this.userInfo.sdt;
                this.init.id = this.userInfo.id;
                this.init.userId = this.userInfo.userId;
                this.init.email = this.userInfo.email;
                this.init.gioiTinh = this.userInfo.gioiTinh;
                this.init.ngaySinh = this.userInfo.ngaySinh;
                this.init.anhDaiDien = this.userInfo.anhDaiDien;
                this.init.coAnhDaiDien = this.userInfo.coAnhDaiDien;
                this.init.coShop = this.userInfo.coShop;
                this.userLoaded = Promise.resolve(true);
            }
        });
    }

    getAllDiaChi() {
        this.cartService.getAllDiaChi(this.loggedUser.userId).subscribe({
            next: (res: any) => {
                this.listDiaChi = res.data ? res.data : [];
                this.listDiaChiFilter = this.listDiaChi;
            }
        });
    }

    // getAllDonViVanChuyen() {
    //     this.cartService.getAllDonViVanChuyen().subscribe({
    //         next: (res: any) => {
    //             this.listDonVi = res ? res : [];
    //             this.listDonViFilter = this.listDonVi;
    //         }
    //     });
    // }

    getAllGioHang() {
        this.cartService.getAllGioHang(this.loggedUser.userId).subscribe({
            next: (res: any) => {
                this.listGioHang = res ? res : [];
                this.thanhTien = 0;
                // this.setGiaGiam = 0;
                // this.maGiamGia = null;
                for(let item of this.listGioHang) {
                    if(item.anhDaiDienShop != null) {
                        item.anhDaiDienShop = environment.apiUrl + "/api/" + item.anhDaiDienShop.replaceAll("\\", "/");
                    }
                    for(let item2 of item.listSanPhamInGioHang) {
                        item2.linkAnimation = false;
                        item2.editable = false;
                        // item2.maGiamGia = null;
                        item2.giaGiam = 0;
                        // item2.listMaGiamGia = [];
                        if(item2.soLuong > item2.sanPham.soLuong) {
                            item2.soLuong = item2.sanPham.soLuong
                        }
                        if(item2.soLuong <= 0) {
                            this.blockButton = true;
                        }
                        item2.initSoLuong = item2.soLuong;
                        this.thanhTien += item2.soLuong * item2.giamGia;
                        item2.sanPham.avatar = item2.sanPham.hinhAnhSanPham[0].hinhAnh;
                        if(item2.sanPham.avatar != null) {
                            item2.sanPham.avatar = environment.apiUrl + "/api/" + item2.sanPham.avatar.replaceAll("\\", "/");
                        }
                        // this.getAllMaGiamGiaDaDung(item2);
                    }
                }
                this.getAllGioHangDauGia();
            }
        });
    }

    getAllGioHangDauGia() {
        this.cartService.getAllGioHangDauGia(this.loggedUser.userId).subscribe({
            next: (res: any) => {
                this.listGioHangDauGia = res ? res : [];
                for(let item of this.listGioHangDauGia) {
                    if(item.anhDaiDienShop != null) {
                        item.anhDaiDienShop = environment.apiUrl + "/api/" + item.anhDaiDienShop.replaceAll("\\", "/");
                    }
                    for(let item2 of item.listSanPhamInGioHang) {
                        item2.linkAnimation = false;
                        item2.editable = false;
                        this.thanhTien += item2.soLuong * item2.giamGia;
                        item2.sanPhamDauGia.avatar = item2.sanPhamDauGia.hinhAnhSanPhamDauGia[0].hinhAnh;
                        if(item2.sanPhamDauGia.avatar != null) {
                            item2.sanPhamDauGia.avatar = environment.apiUrl + "/api/" + item2.sanPhamDauGia.avatar.replaceAll("\\", "/");
                        }
                    }
                }
                this.getTimeNow();
                this.loaded = Promise.resolve(true);
            }
        });
    }

    // getAllMaGiamGiaDaDung(item: any) {
    //     this.cartService.getAllMaGiamGiaDaDung(this.loggedUser.userId).subscribe({
    //         next: (res: any) => {
    //             this.listMaGiamGiaDaDung = res ? res : [];
    //             this.getAllMaGiamGia(item);
    //         }
    //     });
    // }

    // getAllMaGiamGia(item: any)  {
    //     let param = {
    //         batDau: true,
    //         ketThuc: false,
    //         loaiSanPhamId: item.sanPham.loaiSanPhamId
    //     }
    //     this.cartService.getAllMaGiamGia(param).subscribe({
    //         next: (res: any) => {
    //             if(res.data && res.data.length) {
    //                 for(let obj of res.data) {
    //                     let check = false;
    //                     for(let obj2 of this.listMaGiamGiaDaDung) {
    //                         if(obj2.maGiamGiaId == obj.id) {
    //                             check = true;
    //                             break;
    //                         }
    //                     }
    //                     if(!check) {
    //                         item.listMaGiamGia.push(obj);
    //                     }
    //                 }
    //             }
    //         }
    //     });
    // }

    getTimeNow() {
        let now = new Date().getTime();
        for(let item of this.listGioHangDauGia) {
            for(let obj of item.listSanPhamInGioHang) {
                let tmp = new Date(obj.sanPhamDauGia.updatedAt);
                tmp.setDate(tmp.getDate() + 3);
                let time = tmp.getTime();
                if(time > now) {
                    let seconds = Math.floor((time - now) / 1000);
                    let minutes = Math.floor(seconds / 60);
                    let hours = Math.floor(minutes / 60);
                    let days = Math.floor(hours / 24);
                    seconds = seconds % 60;
                    minutes = minutes % 60;
                    hours = hours % 24;
                    if(days == 0) {
                        obj.timeLeft = `${hours} giờ ${minutes} phút ${seconds} giây`;
                    }
                    else {
                        obj.timeLeft = `${days} ngày ${hours} giờ ${minutes} phút ${seconds} giây`;
                    }
                }
                else {
                    obj.timeLeft = "0 giờ 0 phút 0 giây";
                }
            }
        }
        setTimeout(() => this.getTimeNow(), 1000);
    }

    updateGioHang(item: any) {
        this.cartService.updateGioHang(item).subscribe({
            next: (res: any) => {
                if(res.statusCode == 200) {
                    this.popUpService.open(MessageConstants.UPDATE_SUCCESS, mType.success);
                    this.getAllGioHang();
                }
                else {
                    this.popUpService.open(MessageConstants.UPDATE_FAILED, mType.error);
                    this.getAllGioHang();
                }
            },
            error: (res: any) => {
                this.popUpService.open(MessageConstants.UPDATE_FAILED, mType.error);
                this.getAllGioHang();
            }
        });
    }

    deleteGioHang(item: any) {
        this.cartService.deleteGioHang(item).subscribe({
            next: (res: any) => {
                if(res.statusCode == 200) {
                    this.popUpService.open(MessageConstants.DELETE_SUCCESS, mType.success);
                    let count = this.resetGioHang.getGioHangCount();
                    this.resetGioHang.setGioHangCount(count - 1);
                    this.getAllGioHang();
                }
                else {
                    this.popUpService.open(MessageConstants.DELETE_FAILED, mType.error);
                }
            },
            error: (res: any) => {
                this.popUpService.open(MessageConstants.DELETE_FAILED, mType.error);
            }
        });
    }

    checkCount(item: any) {
        if(parseInt(this.countInput.nativeElement.value) > item.sanPham.soLuong) {
            this.countInput.nativeElement.value = item.sanPham.soLuong;
            item.soLuong = item.sanPham.soLuong;
        }
        if(this.countInput.nativeElement.value == "" || parseInt(this.countInput.nativeElement.value) == 0) {
            this.countInput.nativeElement.value = 1;
            item.soLuong = 1;
        }
    }

    changeDatHangHo() {
        if(this.datHangHo) {
            this.datHangHo = false;
            this.f.ho.setValue(this.init.ho);
            this.f.tenDem.setValue(this.init.tenDem);
            this.f.ten.setValue(this.init.ten);
            this.f.sdt.setValue(this.init.sdt);
            this.f.diaChi.setValue(this.init.diaChi);
            this.f.diaChiKhac.clearValidators();
            this.f.diaChiKhac.reset();
            this.f.diaChi.setValidators(Validators.required);
            this.f.tenDat.clearValidators();
            this.f.sdtDat.clearValidators();
            this.f.diaChi.updateValueAndValidity();
            this.f.tenDat.updateValueAndValidity();
            this.f.sdtDat.updateValueAndValidity();
        }
        else {
            this.datHangHo = true;
            this.f.ho.reset();
            this.f.tenDem.reset();
            this.f.ten.reset();
            this.f.sdt.reset();
            this.f.diaChi.clearValidators();
            this.f.diaChi.updateValueAndValidity();
            this.f.tenDat.setValidators(Validators.required);
            this.f.sdtDat.setValidators(Validators.required);
            this.f.diaChiKhac.setValidators(Validators.required);
        }
    }

    changeDiaChiKhac() {
        if(this.chonDiaChiKhac) {
            this.chonDiaChiKhac = false;
            this.f.diaChi.setValue(this.init.diaChi);
            this.f.diaChiKhac.clearValidators();
            this.f.diaChiKhac.reset();
            this.f.diaChi.setValidators(Validators.required);
        }
        else {
            this.chonDiaChiKhac = true;
            this.f.diaChi.clearValidators();
            this.f.diaChi.updateValueAndValidity();
            this.f.diaChiKhac.setValidators(Validators.required);
        }
    }

    setInitValue() {
        if(!this.datHangHo) {
            this.init.ho = this.userInfo.ho;
            this.init.tenDem = this.userInfo.tenDem;
            this.init.ten = this.userInfo.ten;
            this.init.sdt = this.userInfo.sdt;
            if(!this.chonDiaChiKhac) {
                this.init.diaChi = this.diaChi;
            }
        }
    }

    // changeDonVi(item: any) {
    //     if(item) {
    //         this.phiShipTruocDieuKien = item.phiVanChuyen;
    //         this.dieuKienMienPhi = item.dieuKienMienPhi;
    //         this.phiShip = this.thanhTien + this.setGiaGiam >= this.dieuKienMienPhi ? 0 : this.phiShipTruocDieuKien;
    //         this.thanhTien += this.phiShip;
    //     }
    //     else {
    //         this.phiShipTruocDieuKien = 0;
    //         this.thanhTien -= this.phiShip;
    //         this.phiShip = 0;
    //         this.dieuKienMienPhi = 0;
    //     }
    // }

    // viewMaGiamGia(item: any) {
    //     let maGiamGiaId = this.maGiamGia;
    //     if(!this.maGiamGia || item.maGiamGia) {
    //         let dialogRef = this.dialog.open(ViewCouponComponent, {
    //             width: "32rem",
    //             data: {
    //                 item,
    //                 maGiamGiaId
    //             }
    //         });
    //         dialogRef.afterClosed().subscribe((res: any) => {
    //             if(res && res != "remove") {
    //                 this.maGiamGia = res.id;
    //                 item.maGiamGia = res.tenMaGiamGia;
    //                 item.giaGiam = item.soLuong * item.giamGia * res.phanTramGiamGia / 100;
    //                 item.giaGiam = item.giaGiam < res.giamGiaToiDa ? item.giaGiam : res.giamGiaToiDa;
    //                 item.giaGiam = item.giaGiam < item.giamGia * item.soLuong ? item.giaGiam : item.giamGia * item.soLuong;
    //                 this.thanhTien -= item.giaGiam;
    //                 this.setGiaGiam = item.giaGiam;
    //             }
    //             else if(res == "remove") {
    //                 this.maGiamGia = null;
    //                 item.maGiamGia = null;
    //                 item.giaGiam = 0;
    //                 this.thanhTien += this.setGiaGiam;
    //             }
    //         });
    //     }
    //     else {
    //         this.popUpService.open(MessageConstants.CHOOSE_ONLY_ONE_COUPON, mType.info);
    //     }
    // }

    async pay() {
        this.formControl.markAllAsTouched();
        if(this.formControl.invalid) {
            return;
        }
        else {
            let finalInfo = {
                ho: this.userInfo.ho,
                tenDem: this.userInfo.tenDem,
                ten: this.userInfo.ten,
                sdt: this.userInfo.sdt,
                diaChi: null
            }
            this.cartService.updateUserInfo(this.init).subscribe({
                next: (res: any) => {

                }
            });
            if(!this.datHangHo) {
                if(this.chonDiaChiKhac) {
                    if(this.themDiaChi) {
                        let item = {
                            diaChi: this.diaChiKhac,
                            userId: this.loggedUser.userId
                        }
                        this.cartService.createDiaChi(item).subscribe({
                            next: (res: any) => {

                            }
                        });
                    }
                    finalInfo.diaChi = this.diaChiKhac;
                }
                else {
                    finalInfo.diaChi = this.diaChi;
                }
            }
            else {
                finalInfo.diaChi = this.diaChiKhac;
            }
            // if(this.maGiamGia) {
            //     let item = {
            //         userId: this.loggedUser.userId,
            //         maGiamGiaId: this.maGiamGia
            //     }
            //     this.cartService.createMaGiamGiaDaDung(item).subscribe({
            //         next: (res: any) => {

            //         }
            //     });
            // }
            // if(this.phuongThucType == 0) {
                let tenDayDu = "";
                if(finalInfo.ho && finalInfo.ho.trim()) {
                    tenDayDu += finalInfo.ho + " ";
                }
                if(finalInfo.tenDem && finalInfo.tenDem.trim()) {
                    tenDayDu += finalInfo.tenDem + " ";
                }
                if(finalInfo.ten && finalInfo.ten.trim()) {
                    tenDayDu += finalInfo.ten;
                }
                let tenDayDuNguoiDat = "";
                if(this.init.ho && this.init.ho.trim()) {
                    tenDayDuNguoiDat += this.init.ho + " ";
                }
                if(this.init.tenDem && this.init.tenDem.trim()) {
                    tenDayDuNguoiDat += this.init.tenDem + " ";
                }
                if(this.init.ten && this.init.ten.trim()) {
                    tenDayDuNguoiDat += this.init.ten;
                }
                let item: any = {
                    userId: this.loggedUser.userId,
                    tenDayDu: tenDayDu,
                    tenDayDuNguoiDat: tenDayDuNguoiDat,
                    diaChi: finalInfo.diaChi,
                    soDienThoai: finalInfo.sdt,
                    soDienThoaiNguoiDat: this.init.sdt,
                    ghiChu: this.f.ghiChu.value,
                    xuatHoaDon: this.xuatHoaDon,
                    datHo: this.datHangHo,
                    // donViVanChuyenId: this.donVi.id,
                    // maGiamGiaId: this.maGiamGia ? this.maGiamGia : "1",
                    donHangSanPham: [],
                    soSanPham: 0,
                    thanhTien: 0
                }
                let listTmp = [...this.listGioHang, ...this.listGioHangDauGia];
                for(let obj of listTmp) {
                    let gioHang: any = [];
                    item.donHangSanPham = [];
                    item.shopId = obj.shopId;
                    item.soSanPham = 0;
                    item.thanhTien = 0;
                    for(let obj2 of obj.listSanPhamInGioHang) {
                        let sanPham: any;
                        if(obj2.sanPhamId && !obj2.sanPhamDauGiaId) {
                            sanPham = {
                                tenSanPham: obj2.sanPham.tenSanPham,
                                hinhAnhSanPham: obj2.sanPham.hinhAnhSanPham[0].hinhAnh,
                                donGia: obj2.donGia,
                                soLuong: obj2.soLuong,
                                giamGia: obj2.giamGia,
                                // coMaGiamGia: obj2.maGiamGia ? true : false,
                                sanPhamGocId: obj2.sanPhamId,
                                sanPhamDauGia: false
                            }
                        }
                        else if(!obj2.sanPhamId && obj2.sanPhamDauGiaId) {
                            sanPham = {
                                tenSanPham: obj2.sanPhamDauGia.tenSanPham,
                                hinhAnhSanPham: obj2.sanPhamDauGia.hinhAnhSanPhamDauGia[0].hinhAnh,
                                donGia: obj2.donGia,
                                soLuong: obj2.soLuong,
                                giamGia: obj2.giamGia,
                                // coMaGiamGia: obj2.maGiamGia ? true : false,
                                sanPhamGocId: obj2.sanPhamDauGiaId,
                                sanPhamDauGia: true
                            }
                        }
                        let tmp = {
                            id: obj2.id,
                            userId: this.loggedUser.userId
                        }
                        gioHang.push(tmp);
                        item.donHangSanPham.push(sanPham);
                        item.soSanPham++;
                        item.thanhTien += obj2.soLuong * obj2.giamGia;
                    }
                    await this.cartService.createDonHang(item).toPromise().then(async (res: any) => {
                        if(res.statusCode == 200) {
                            let check = false;
                            for(let index in gioHang) {
                                await this.cartService.deleteGioHang(gioHang[index]).toPromise().then((ress: any) => {
                                    if(ress.statusCode == 200) {
                                        if(parseInt(index) == gioHang.length - 1) {
                                            if(check) {
                                                this.popUpService.open(MessageConstants.SYSTEM_ERROR, mType.error);
                                            }
                                            else {
                                                if(listTmp.indexOf(obj) == listTmp.length - 1) {
                                                    this.router.navigate(["/information/order"]);
                                                    this.popUpService.open(MessageConstants.PAYMENT_SUCCESS, mType.success);
                                                }
                                                let cnt = this.resetCart.getGioHangCount();
                                                this.resetCart.setGioHangCount(cnt - item.soSanPham > 0 ? cnt - item.soSanPham : 0);
                                            }
                                        }
                                    }
                                    else {
                                        check = true;
                                        if(parseInt(index) == gioHang.length - 1) {
                                            this.popUpService.open(MessageConstants.SYSTEM_ERROR, mType.error);
                                        }
                                    }
                                }).catch((ress: any) => {
                                    check = true;
                                    if(parseInt(index) == gioHang.length - 1) {
                                        this.popUpService.open(MessageConstants.SYSTEM_ERROR, mType.error);
                                    }
                                });
                            }
                        }
                        else if(res.statusCode == 406) {
                            this.popUpService.open(MessageConstants.INSUFFICIENT, mType.error);
                            this.getAllGioHang();
                        }
                        else {
                            this.popUpService.open(MessageConstants.PAYMENT_FAILED, mType.error);
                        }
                    }).catch((res: any) => {
                        this.popUpService.open(MessageConstants.PAYMENT_FAILED, mType.error);
                    });
                }
            // }
        }
    }
}
