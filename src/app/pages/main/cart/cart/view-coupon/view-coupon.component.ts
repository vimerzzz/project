import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
    selector: 'app-view-coupon',
    templateUrl: './view-coupon.component.html',
    styleUrls: ['./view-coupon.component.scss']
})
export class ViewCouponComponent implements OnInit {
    maGiamGia: any;
    error: any;

    constructor(private dialogRef: MatDialogRef<ViewCouponComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any) { }

    ngOnInit(): void {
        if(this.data.maGiamGiaId) {
            for(let item of this.data.item.listMaGiamGia) {
                if(item.id == this.data.maGiamGiaId) {
                    this.maGiamGia = item;
                }
            }
        }
    }

    save() {
        if(!this.maGiamGia) {
            this.error = "Chưa chọn mã giảm giá!";
            return;
        }
        else {
            this.error = null;
            this.dialogRef.close(this.maGiamGia);
        }
    }

    remove() {
        this.maGiamGia = null;
        this.dialogRef.close("remove");
    }

    chooseMaGiamGia(item: any) {
        this.maGiamGia = item;
    }
}
