import { NgModule } from '@angular/core';
import { MaterialModule } from 'src/app/material.module';
import { DirectivesModule } from 'src/app/shared/directives/directives.module';
import { CartRoutingModule } from './cart-routing.module';
import { CartComponent } from './cart/cart.component';
import { ViewCouponComponent } from './cart/view-coupon/view-coupon.component';

@NgModule({
    declarations: [
        CartComponent,
        ViewCouponComponent
    ],
    imports: [
        CartRoutingModule,
        MaterialModule,
        DirectivesModule
    ]
})
export class CartModule { }
