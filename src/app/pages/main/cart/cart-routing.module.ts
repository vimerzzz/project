import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AdminAuth } from "src/app/shared/auth/admin.auth";
import { CustomerAuth } from "src/app/shared/auth/customer.auth";
import { DeliverAuth } from "src/app/shared/auth/deliver.auth";
import { CartComponent } from "./cart/cart.component";

const routes: Routes = [
    {
        path: "",
        children: [
            {
                path: "",
                component: CartComponent,
                canActivate: [CustomerAuth]
            }
        ]
    }
]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
  })
  export class CartRoutingModule { }
