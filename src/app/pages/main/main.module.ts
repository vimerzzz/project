import { NgModule } from '@angular/core';
import { MaterialModule } from 'src/app/material.module';
import { HomeComponent } from './home/home.component';
import { MainRoutingModule } from './main-routing.module';

@NgModule({
    declarations: [
        HomeComponent
    ],
    imports: [
        MainRoutingModule,
        MaterialModule
    ]
})
export class MainModule { }
