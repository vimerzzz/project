import { Directive, ElementRef, EventEmitter, HostListener, Input, OnInit, Output } from '@angular/core';

@Directive({
    selector: '[directiveResizable]'
})
export class ResizeDirective implements OnInit {
    @Input() directiveResizable: "all" | "onlyWidth" | "onlyHeight";
    @Input() directiveResizableBorderWidth: "left" | "right";
    @Input() directiveResizableBorderHeight: "top" | "bottom";
    @Input() directiveResizableGrabWidth = 0;
    @Input() directiveResizableGrabHeight = 0;
    @Input() directiveResizableMinWidth = 50;
    @Input() directiveResizableMinHeight = 50;
    @Input() directiveResizableMaxWidth = 10000;
    @Input() directiveResizableMaxHeight = 10000;
    @Output() directiveResizableNewWidth: EventEmitter<string> = new EventEmitter<string>();
    @Output() directiveResizableNewHeight: EventEmitter<string> = new EventEmitter<string>();

    dragging = false;

    constructor(private el: ElementRef) {

        const self = this;

        const EventListenerMode = { capture: true };

        function preventGlobalMouseEvents(evt: any) {
            evt.preventDefault();
            document.body.style.pointerEvents = 'none';
        }

        function restoreGlobalMouseEvents() {
            document.body.style.pointerEvents = 'auto';
        }


        const newWidth = (wid: any) => {
            const maxWidth = Math.max(this.directiveResizableMinWidth, wid);
            const minWidth = Math.min(this.directiveResizableMaxWidth, maxWidth);
            el.nativeElement.style.width = minWidth + "px";
            this.directiveResizableNewWidth.emit(minWidth + "px");
        }

        const newHeight = (hei: any) => {
            const maxHeight = Math.max(this.directiveResizableMinHeight, hei);
            const minHeight = Math.min(this.directiveResizableMaxHeight, maxHeight);
            el.nativeElement.style.height = minHeight + "px";
            this.directiveResizableNewHeight.emit(minHeight + "px");
        }

        const mouseMoveG = (evt: any) => {
            if (!this.dragging) {
                return;
            }
            if(this.directiveResizable == "onlyWidth") {
                if(this.directiveResizableBorderWidth == "right") {
                    newWidth(evt.clientX - el.nativeElement.offsetLeft);
                }
                else if(this.directiveResizableBorderWidth == "left") {
                    newWidth(document.body.offsetWidth - evt.clientX);
                }
            }
            else if(this.directiveResizable == "onlyHeight") {
                if(this.directiveResizableBorderHeight == "bottom") {
                    newHeight(evt.clientY - el.nativeElement.offsetTop);
                }
                else if(this.directiveResizableBorderHeight == "top") {
                    newHeight(document.body.offsetHeight - evt.clientY);
                }
            }
            else if(this.directiveResizable == "all") {
                if(this.directiveResizableBorderWidth == "right" && this.directiveResizableBorderHeight == "bottom") {
                    newWidth(evt.clientX - el.nativeElement.offsetLeft);
                    newHeight(evt.clientY - el.nativeElement.offsetTop);
                }
                else if(this.directiveResizableBorderWidth == "left" && this.directiveResizableBorderHeight == "bottom") {
                    newWidth(document.body.offsetWidth - evt.clientX);
                    newHeight(evt.clientY - el.nativeElement.offsetTop);
                }
                else if(this.directiveResizableBorderWidth == "right" && this.directiveResizableBorderHeight == "top") {
                    newWidth(evt.clientX - el.nativeElement.offsetLeft);
                    newHeight(document.body.offsetHeight - evt.clientY);
                }
                else if(this.directiveResizableBorderWidth == "left" && this.directiveResizableBorderHeight == "top") {
                    newWidth(document.body.offsetWidth - evt.clientX);
                    newHeight(document.body.offsetHeight - evt.clientY);
                }
            }
            evt.stopPropagation();
        };

        const mouseUpG = (evt: any) => {
            if (!this.dragging) {
                return;
            }
            restoreGlobalMouseEvents();
            this.dragging = false;
            evt.stopPropagation();
        };

        const mouseDown = (evt: any) => {
            if (this.inDragRegionWidth(evt) || this.inDragRegionHeight(evt)) {
                this.dragging = true;
                preventGlobalMouseEvents(evt);
                evt.stopPropagation();
            }
        };


        const mouseMove = (evt: any) => {
            if(this.directiveResizable == "onlyWidth") {
                if (this.inDragRegionWidth(evt) || this.dragging) {
                    el.nativeElement.style.cursor = "ew-resize";
                }
                else {
                    el.nativeElement.style.cursor = "default";
                }
            }
            else if(this.directiveResizable == "onlyHeight") {
                if (this.inDragRegionHeight(evt) || this.dragging) {
                    el.nativeElement.style.cursor = "ns-resize";
                }
                else {
                    el.nativeElement.style.cursor = "default";
                }
            }
            else if(this.directiveResizable == "all") {
                if (this.inDragRegionHeight(evt) || this.inDragRegionWidth(evt) || this.dragging) {
                    el.nativeElement.style.cursor = "move";
                }
                else {
                    el.nativeElement.style.cursor = "default";
                }
            }
        }

        document.addEventListener('mousemove', mouseMoveG, true);
        document.addEventListener('mouseup', mouseUpG, true);
        el.nativeElement.addEventListener('mousedown', mouseDown, true);
        el.nativeElement.addEventListener('mousemove', mouseMove, true);
    }

    ngOnInit(): void {
        if(this.directiveResizable == "onlyWidth") {
            if(!this.directiveResizableBorderWidth) {
                this.directiveResizableBorderWidth = "right";
            }
        }
        else if(this.directiveResizable == "onlyHeight") {
            if(!this.directiveResizableBorderHeight) {
                this.directiveResizableBorderHeight = "bottom";
            }
        }
        else if(this.directiveResizable == "all") {
            if(!this.directiveResizableBorderWidth) {
                this.directiveResizableBorderWidth = "right";
            }
            if(!this.directiveResizableBorderHeight) {
                this.directiveResizableBorderHeight = "bottom";
            }
        }
    }

    inDragRegionWidth(evt: any) {
        if(this.directiveResizableBorderWidth == "right") {
            return this.el.nativeElement.clientWidth - evt.clientX + this.el.nativeElement.offsetLeft < this.directiveResizableGrabWidth;
        }
        else if(this.directiveResizableBorderWidth == "left") {
            return this.el.nativeElement.clientWidth - document.body.offsetWidth + evt.clientX < this.directiveResizableGrabWidth;
        }
        return false;
    }

    inDragRegionHeight(evt: any) {
        if(this.directiveResizableBorderHeight == "bottom") {
            return this.el.nativeElement.clientHeight - evt.clientY + this.el.nativeElement.offsetTop < this.directiveResizableGrabHeight;
        }
        else if(this.directiveResizableBorderHeight == "top") {
            return this.el.nativeElement.clientHeight - document.body.offsetHeight + evt.clientY < this.directiveResizableGrabHeight;
        }
        return false;
    }
}
