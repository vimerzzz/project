import { Directive, ElementRef, Host, HostListener, Input, OnInit } from '@angular/core';

@Directive({
    selector: '[directiveCurrencySpace]'
})
export class CurrencySpaceDirective implements OnInit {

    constructor(private el: ElementRef) { }

    @Input() directiveCurrencySpace: string;

    @HostListener("focus", ["event"]) onFocus(event: any) {
        this.el.nativeElement.value = this.el.nativeElement.value.replaceAll(" " + this.directiveCurrencySpace, "");
        this.el.nativeElement.value = this.el.nativeElement.value.replaceAll(",", "");
    }

    @HostListener("blur", ["event"]) onFocusOut(event: any) {
        let value = this.el.nativeElement.value;
        let tmp = "";
        let res = "";
        let decimal = value.split(".")[1];
        if(value) {
            value = parseInt(value).toString();
            while(parseInt(value) > 0) {
                tmp = (parseInt(value) / 1000).toString();
                if(!res) {
                    if(parseInt(value) >= 1000) {
                        if((parseInt(value) - parseInt(tmp) * 1000) >= 100) {
                            res = (parseInt(value) - parseInt(tmp) * 1000).toString();
                        }
                        else if((parseInt(value) - parseInt(tmp) * 1000) < 100 && (parseInt(value) - parseInt(tmp) * 1000) >= 10) {
                            res = "0" + (parseInt(value) - parseInt(tmp) * 1000).toString();
                        }
                        else if((parseInt(value) - parseInt(tmp) * 1000) < 10 && (parseInt(value) - parseInt(tmp) * 1000) >= 0) {
                            res = "00" + (parseInt(value) - parseInt(tmp) * 1000).toString();
                        }
                    }
                    else {
                        res = parseInt(value).toString();
                    }
                }
                else {
                    if(parseInt(value) >= 1000) {
                        if((parseInt(value) - parseInt(tmp) * 1000) >= 100) {
                            res = (parseInt(value) - parseInt(tmp) * 1000).toString() + "," + res;
                        }
                        else if((parseInt(value) - parseInt(tmp) * 1000) < 100 && (parseInt(value) - parseInt(tmp) * 1000) >= 10) {
                            res = "0" + (parseInt(value) - parseInt(tmp) * 1000).toString() + "," + res;
                        }
                        else if((parseInt(value) - parseInt(tmp) * 1000) < 10 && (parseInt(value) - parseInt(tmp) * 1000) >= 0) {
                            res = "00" + (parseInt(value) - parseInt(tmp) * 1000).toString() + "," + res;
                        }
                    }
                    else {
                        res = parseInt(value).toString() + "," + res;
                    }
                }
                value = tmp;
            }
            if(decimal) {
                this.el.nativeElement.value = res + "." + decimal + " " + this.directiveCurrencySpace;
            }
            else {
                this.el.nativeElement.value = res + " " + this.directiveCurrencySpace;
            }
        }
    }

    ngOnInit(): void {
        this.onFocusOut(null);
    }
}