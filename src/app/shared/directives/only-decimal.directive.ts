import { Directive, ElementRef, HostListener, Input } from '@angular/core';

@Directive({
    selector: '[directiveOnlyDecimal]'
})
export class OnlyDecimalDirective {

    constructor(private el: ElementRef) { }

    @Input() directiveOnlyDecimal: boolean;
    @Input() directiveOnlyDecimalDigit = 2;

    @HostListener('keydown', ['$event']) onKeyDown(event: any) {
        let e = <KeyboardEvent>event;
        if (this.directiveOnlyDecimal) {
            if ([46, 8, 9, 27, 13].indexOf(e.keyCode) !== -1 ||
                // Allow: Ctrl+A
                (e.keyCode === 65 && (e.ctrlKey || e.metaKey)) ||
                // Allow: Ctrl+C
                (e.keyCode === 67 && (e.ctrlKey || e.metaKey)) ||
                // Allow: Ctrl+V
                (e.keyCode === 86 && (e.ctrlKey || e.metaKey)) ||
                // Allow: Ctrl+X
                (e.keyCode === 88 && (e.ctrlKey || e.metaKey)) ||
                // Allow: home, end, left, right
                (e.keyCode >= 35 && e.keyCode <= 39)) {
                // let it happen, don't do anything
                return;
            }
            // Ensure that it is a Decimal and stop the keypress
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                if(e.keyCode === 110 || e.keyCode === 190) {
                    if(!this.el.nativeElement.value) {
                        e.preventDefault();
                    }
                    else {
                        if(this.el.nativeElement.value.indexOf(".") !== -1) {
                            e.preventDefault();
                        }
                        else {
                            return;
                        }
                    }
                }
                else {
                    e.preventDefault();
                }
            }
            if(this.el.nativeElement.selectionEnd === this.el.nativeElement.selectionStart) {
                if(this.el.nativeElement.value.split(".")[1] && (this.el.nativeElement.value.split(".")[1].length >= this.directiveOnlyDecimalDigit)) {
                    if(this.el.nativeElement.selectionStart >= this.el.nativeElement.value.length - this.directiveOnlyDecimalDigit) {
                        e.preventDefault();
                    }
                }
            }
        }
    }
}
