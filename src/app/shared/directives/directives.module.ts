import { NgModule } from "@angular/core";
import { CurrencySpaceDirective } from "./currency-space.directive";
import { OnlyDecimalDirective } from "./only-decimal.directive";
import { OnlyNumberDirective } from "./only-number.directive";
import { ResizeDirective } from "./resize.directive";
import { ThousandsSpaceDirective } from "./thousands-space.directive";

@NgModule({
    declarations: [
        OnlyNumberDirective,
        ResizeDirective,
        CurrencySpaceDirective,
        ThousandsSpaceDirective,
        OnlyDecimalDirective
    ],
    exports: [
        OnlyNumberDirective,
        ResizeDirective,
        CurrencySpaceDirective,
        ThousandsSpaceDirective,
        OnlyDecimalDirective
    ]
})
export class DirectivesModule { }
