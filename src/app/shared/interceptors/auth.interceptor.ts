import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { Observable, of, throwError } from "rxjs";
import { catchError } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class AuthInterceptor implements HttpInterceptor {
    constructor(private router: Router) {}

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        let object = localStorage.getItem("loggedUser");
        if(object) {
            let loggedUser = JSON.parse(object);
            let clone = req.clone({
                headers: req.headers.set("Authorization", "Bearer " + loggedUser.token)
            });
            return next.handle(clone);
        }
        else {
            return next.handle(req);
        }
    }
}
