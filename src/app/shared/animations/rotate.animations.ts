import { animate, state, style, transition, trigger } from "@angular/animations";

export const Rotate180Degree = trigger("rotate180Degree", [
    state("enter", style({
        transform: "rotate(180deg)"
    })),
    state("leave", style({
        transform: "rotate(0deg)"
    })),
    transition("enter => leave", [
        animate("0.25s ease-in-out")
    ]),
    transition("leave => enter", [
        animate("0.25s ease-in-out")
    ])
]);
