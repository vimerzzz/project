import { animate, state, style, transition, trigger } from "@angular/animations";

export const HeightSlide = trigger("heightSlide", [
    state("enter", style({
        height: "{{height}}"
    }), {
        params: {height: "0px"}
    }),
    state("leave", style({
        height: "{{height}}"
    }), {
        params: {height: "0px"}
    }),
    transition("enter => leave", [
        animate("0.25s ease-in-out")
    ]),
    transition("leave => enter", [
        animate("0.25s ease-in-out")
    ])
]);

export const WidthSlide = trigger("widthSlide", [
    state("enter", style({
        width: "{{width}}"
    }), {
        params: {width: "0px"}
    }),
    state("leave", style({
        width: "{{width}}"
    }), {
        params: {width: "0px"}
    }),
    transition("enter => leave", [
        animate("0.25s ease-in-out")
    ]),
    transition("leave => enter", [
        animate("0.25s ease-in-out")
    ])
]);
