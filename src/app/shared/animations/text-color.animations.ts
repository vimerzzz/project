import { animate, state, style, transition, trigger } from "@angular/animations";

export const SuccessColor = trigger("successColor", [
    state("enter", style({
        color: "rgb(33, 161, 97)"
    })),
    state("leave", style({
        color: "#000000"
    })),
    transition("enter => leave", [
        animate("0.25s ease-in-out")
    ]),
    transition("leave => enter", [
        animate("0.25s ease-in-out")
    ])
]);
