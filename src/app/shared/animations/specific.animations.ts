import { group, query, animate, animateChild, state, style, transition, trigger } from "@angular/animations";

export const ThanhToanButton = [
    trigger("slideButton", [
        state("enter", style({
            position: "relative",
            paddingRight: "2rem"
        })),
        state("leave", style({
            position: "relative"
        })),
        transition("enter => leave", [
            group([
                query("@slideNestedIcon", animateChild()),
                animate("0.25s ease-in-out")
            ])
        ]),
        transition("leave => enter", [
            group([
                query("@slideNestedIcon", animateChild()),
                animate("0.25s ease-in-out")
            ])
        ])
    ]),
    trigger("slideNestedIcon", [
        state("enter", style({
            position: "absolute",
            top: "50%",
            left: "calc(50% + 2.5rem)",
            transform: "translate(-50%, -50%)",
            opacity: "1"
        })),
        state("leave", style({
            position: "absolute",
            top: "50%",
            left: "70%",
            transform: "translate(-50%, -50%)",
            opacity: "0"
        })),
        transition("enter => leave", [
            animate("0.25s ease-in-out")
        ]),
        transition("leave => enter", [
            animate("0.25s ease-in-out")
        ])
    ])
];

export const NewMessageButton = trigger("newMessageButton", [
    state("enter", style({
        marginBottom: "0rem",
        visibility: "visible",
        opacity: "1"
    })),
    state("leave", style({
        marginBottom: "0.75rem",
        visibility: "hidden",
        opacity: "0"
    })),
    transition("enter => leave", [
        animate("0.25s ease-in-out")
    ]),
    transition("leave => enter", [
        animate("0.25s ease-in-out")
    ])
]);
