import { animate, state, style, transition, trigger } from "@angular/animations";

export const GrayShadow = trigger("grayShadow", [
    state("enter", style({
        boxShadow: "0.125rem 0.125rem 0.5rem rgba(0, 0, 0, 0.2), 0.125rem 0.125rem 0.5rem rgba(0, 0, 0, 0.2)"
    })),
    state("leave", style({
        boxShadow: "none"
    })),
    transition("enter => leave", [
        animate("0.25s ease-in-out")
    ]),
    transition("leave => enter", [
        animate("0.25s ease-in-out")
    ])
]);
