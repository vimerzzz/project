import { group, query, animate, animateChild, state, style, transition, trigger } from "@angular/animations";

export const PrimaryButton = trigger("primaryButton", [
    state("enter", style({
        backgroundColor: "#5167e0",
        color: "#ffffff"
    })),
    state("leave", style({
        backgroundColor: "#ffffff",
        color: "#3f51b5"
    })),
    transition("enter => leave", [
        animate("0.25s ease-in-out")
    ]),
    transition("leave => enter", [
        animate("0.25s ease-in-out")
    ])
]);

export const WarnButton = trigger("warnButton", [
    state("enter", style({
        backgroundColor: "#f44336",
        color: "#ffffff"
    })),
    state("leave", style({
        backgroundColor: "#ffffff",
        color: "#f44336"
    })),
    transition("enter => leave", [
        animate("0.25s ease-in-out")
    ]),
    transition("leave => enter", [
        animate("0.25s ease-in-out")
    ])
]);

export const SuccessButton = trigger("successButton", [
    state("enter", style({
        backgroundColor: "#198754",
        color: "#ffffff"
    })),
    state("leave", style({
        backgroundColor: "#ffffff",
        color: "#198754"
    })),
    transition("enter => leave", [
        animate("0.25s ease-in-out")
    ]),
    transition("leave => enter", [
        animate("0.25s ease-in-out")
    ])
]);

export const SuccessOutlineButton = trigger("successOutlineButton", [
    state("enter", style({
        backgroundColor: "#ffffff",
        color: "#198754",
        borderColor: "#198754"
    })),
    state("leave", style({
        backgroundColor: "#ffffff",
        color: "#000000",
        borderColor: "rgba(0,0,0,.12)"
    })),
    transition("enter => leave", [
        animate("0.25s ease-in-out")
    ]),
    transition("leave => enter", [
        animate("0.25s ease-in-out")
    ])
]);

export const WarnOutlineButton = trigger("warnOutlineButton", [
    state("enter", style({
        backgroundColor: "#ffffff",
        color: "#f44336",
        borderColor: "#f44336"
    })),
    state("leave", style({
        backgroundColor: "#ffffff",
        color: "#000000",
        borderColor: "rgba(0,0,0,.12)"
    })),
    transition("enter => leave", [
        animate("0.25s ease-in-out")
    ]),
    transition("leave => enter", [
        animate("0.25s ease-in-out")
    ])
]);
