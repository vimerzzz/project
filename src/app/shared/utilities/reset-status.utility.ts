import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";

@Injectable({
    providedIn: 'root'
})
export class ResetAvatar {
    constructor() {}

    avatarUrl = new BehaviorSubject<string>("");

    setAvatarUrl(url: string)  {
        this.avatarUrl.next(url);
    }
}

@Injectable({
    providedIn: 'root'
})
export class ResetThongBao {
    constructor() {}

    thongBaoCount = new BehaviorSubject<number>(0);

    setThongBaoCount(count: number) {
        this.thongBaoCount.next(count);
    }

    getThongBaoCount(): number {
        return this.thongBaoCount.value;
    }
}

@Injectable({
    providedIn: 'root'
})
export class ResetUserInfo {
    constructor() {}

    userInfo = new BehaviorSubject<boolean>(false);

    setUserInfoStatus(status: boolean) {
        this.userInfo.next(status);
    }

    getUserInfoStatus(): boolean {
        return this.userInfo.value;
    }
}

@Injectable({
    providedIn: 'root'
})
export class ResetGioHang {
    constructor() {}

    gioHangCount = new BehaviorSubject<number>(0);

    setGioHangCount(count: number) {
        this.gioHangCount.next(count);
    }

    getGioHangCount(): number {
        return this.gioHangCount.value;
    }
}

@Injectable({
    providedIn: 'root'
})
export class ResetChat {
    constructor() {}

    chatStatus = new BehaviorSubject<boolean>(false);
    userShopChatStatus = new BehaviorSubject<boolean>(false);
    userUserChatStatus = new BehaviorSubject<boolean>(false);
    shopChatStatus = new BehaviorSubject<boolean>(false);
    chatShopId = new BehaviorSubject<string>("");
    chatUserId = new BehaviorSubject<string>("");
    chatUserIdForShop = new BehaviorSubject<string>("");

    setChatStatus(status: boolean) {
        this.chatStatus.next(status);
    }

    setShopChatStatus(status: boolean) {
        this.shopChatStatus.next(status);
    }

    setUserShopChatStatus(status: boolean) {
        this.userShopChatStatus.next(status);
    }

    setUserUserChatStatus(status: boolean) {
        this.userUserChatStatus.next(status);
    }

    getShopChatStatus(): boolean {
        return this.shopChatStatus.value;
    }

    getUserShopChatStatus(): boolean {
        return this.userShopChatStatus.value;
    }

    getUserUserChatStatus(): boolean {
        return this.userUserChatStatus.value;
    }

    setChatShopId(shopId: string) {
        this.chatShopId.next(shopId);
    }

    setChatUserId(userId: string) {
        this.chatUserId.next(userId);
    }

    setChatUserIdForShop(userId: string) {
        this.chatUserIdForShop.next(userId);
    }
}
