import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";
import { MessageConstants } from "../constants/message.constants";
import { NavigationService } from "../services/base/navigation.service";
import { mType, PopUpService } from "../services/base/pop-up.service";
import { HeaderService } from "../services/main/header.service";

@Injectable({
    providedIn: 'root'
})
export class BaseAuth {
    constructor(private navigationService: NavigationService, private popUpService: PopUpService, private headerService: HeaderService) {}

    loggedUser = new BehaviorSubject<boolean>(false);

    checkExpire() {
        let object = localStorage.getItem("loggedUser");
        if(object) {
            let user = JSON.parse(object);
            let now = new Date().getTime();
            if(now > user.timeExpire) {
                localStorage.removeItem("loggedUser");
                if(this.loggedUser.getValue() == true) {
                    this.setLoggedUser(false);
                    this.navigationService.reload();
                    let item: any = {
                        id: user.userId
                    }
                    this.popUpService.open(MessageConstants.LOGIN_AGAIN, mType.info);
                }
            }
            else {
                if(this.loggedUser.getValue() == false) {
                    this.setLoggedUser(true);
                }
            }
        }
        else {
            if(this.loggedUser.getValue() == true) {
                this.setLoggedUser(false);
                this.navigationService.reload();
            }
        }
    }

    setLoggedUser(loggedUser: boolean)  {
        this.loggedUser.next(loggedUser);
    }
}
