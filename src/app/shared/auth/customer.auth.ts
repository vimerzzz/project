import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from "@angular/router";
import { Observable } from "rxjs";
import { GuidConstants } from "../constants/guid.constants";

@Injectable({
    providedIn: 'root'
})
export class CustomerAuth implements CanActivate {
    constructor(private router: Router) {}

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
        let object = localStorage.getItem("loggedUser");
        if(object) {
            let loggedUser = JSON.parse(object);
            if(loggedUser.roleId == GuidConstants.ONE || loggedUser.roleId == GuidConstants.TWO || loggedUser.roleId == GuidConstants.THREE || loggedUser.roleId == GuidConstants.FOUR) {
                return true;
            }
            else {
                return this.redirect(403);
            }
        }
        else {
            return this.redirect(401);
        }
    }

    redirect(errorCode: number): Promise<boolean> {
        if(errorCode == 401) {
            return this.router.navigateByUrl("/error/unauthorized");
        }
        else if(errorCode == 403) {
            return this.router.navigateByUrl("/error/forbidden");
        }
        else {
            return this.router.navigateByUrl("/error/not-found");
        }
    }
}
