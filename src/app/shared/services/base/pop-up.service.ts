import { Injectable } from '@angular/core';
import { MatSnackBar, MatSnackBarConfig, MatSnackBarHorizontalPosition, MatSnackBarRef, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { PopUpComponent } from '../../components/pop-up/pop-up.component';

@Injectable({
    providedIn: 'root'
})
export class PopUpService {

    constructor(private snackBar: MatSnackBar) { }
    snackBarConfig: MatSnackBarConfig;
    snackBarRef: MatSnackBarRef<any>;
    horizontalPosition: MatSnackBarHorizontalPosition = 'right';
    verticalPosition: MatSnackBarVerticalPosition = 'bottom';
    snackBarAutoHide = '3000';
    open(message: string, type: mType) {
        const notifyClass = 'snackbar-' + type;
        this.snackBarRef = this.snackBar.openFromComponent(PopUpComponent, {
            data: {
                message,
                type
            },
            panelClass: [notifyClass],
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
            duration: parseInt(this.snackBarAutoHide, 0),
        });
    }
}
export enum mType {
    success = 'done',
    info = 'info',
    warn = 'warning',
    error = 'error',
}
