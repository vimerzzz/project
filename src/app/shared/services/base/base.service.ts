import { environment } from 'src/environments/environment';

export abstract class BaseService {
    constructor() {}

    protected get apiUrl(): string {
        return `${environment.apiUrl}`;
    }
    protected get apiDefault(): string {
        return this.apiUrl + '/api';
    }
    protected get apiLog(): string {
        return this.apiDefault + "/log";
    }
    protected get apiVerify(): string {
        return this.apiDefault + "/verify";
    }
    protected get apiUser(): string {
        return this.apiDefault + "/user";
    }
    protected get apiRole(): string {
        return this.apiDefault + "/role";
    }
    protected get apiAddress(): string {
        return this.apiDefault + "/soDiaChi";
    }
    protected get apiUserInfo(): string {
        return this.apiDefault + "/userInfo";
    }
    protected get apiCategory(): string {
        return this.apiDefault + "/loaiSanPham";
    }
    protected get apiAttribute(): string {
        return this.apiDefault + "/thuocTinh";
    }
    protected get apiCoupon(): string {
        return this.apiDefault + "/maGiamGia";
    }
    protected get apiNotification(): string {
        return this.apiDefault + "/thongBao";
    }
    protected get apiShop(): string {
        return this.apiDefault + "/shop";
    }
    protected get apiItem(): string {
        return this.apiDefault + "/sanPham";
    }
    protected get apiItemImage(): string {
        return this.apiDefault + "/hinhAnhSanPham";
    }
    protected get apiFollow(): string {
        return this.apiDefault + "/soTheoDoi";
    }
    protected get apiShopRate(): string {
        return this.apiDefault + "/danhGiaShop";
    }
    protected get apiCart(): string {
        return this.apiDefault + "/gioHang";
    }
    protected get apiItemRate(): string {
        return this.apiDefault + "/danhGiaSanPham";
    }
    protected get apiUsedCoupon(): string {
        return this.apiDefault + "/maGiamGiaDaDung";
    }
    protected get apiShopChat(): string {
        return this.apiDefault + "/nhanTinShop";
    }
    protected get apiUserChat(): string {
        return this.apiDefault + "/nhanTinUser";
    }
    protected get apiDeliver(): string {
        return this.apiDefault + "/donViVanChuyen";
    }
    protected get apiOrder(): string {
        return this.apiDefault + "/donHang";
    }
    protected get apiAuction(): string {
        return this.apiDefault + "/sanPhamDauGia";
    }
    protected get apiAuctionImage(): string {
        return this.apiDefault + "/hinhAnhSanPhamDauGia";
    }
    protected get apiAuctionChat(): string {
        return this.apiDefault + "/troChuyenDauGia";
    }
    protected get apiReport(): string {
        return this.apiDefault + "/baoCaoViPham";
    }
}
