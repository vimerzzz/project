import { Injectable } from '@angular/core';
import { NavigationCancel, NavigationEnd, NavigationError, NavigationStart, Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import { filter } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class ProgressBarService {
    visible = new BehaviorSubject<boolean>(false);

    constructor(private router: Router) {
        this.init();
    }

    init() {
        this.router.events.subscribe((event) => {
            if (event instanceof NavigationStart) {
                this.setVisible(true);
            }
            else if(event instanceof NavigationCancel || event instanceof NavigationEnd || event instanceof NavigationError) {
                this.setVisible(false);
            }
        });
    }

    setVisible(visible: boolean) {
        this.visible.next(visible);
    }
}
