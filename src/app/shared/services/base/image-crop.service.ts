import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { BehaviorSubject } from 'rxjs';
import { ImageCropComponent } from '../../components/image-crop/image-crop.component';

@Injectable({
    providedIn: 'root'
})
export class ImageCropService {
    image = new BehaviorSubject<any>("");

    constructor(private dialog: MatDialog) { }

    open(imageBase64String: string[], ratioWidth: number, ratioHeight: number, resizeWidth: number, imageName: string[], imageCount: number) {
        if(!this.image.isStopped) {
            this.image.next("");
        }
        else {
            this.image = new BehaviorSubject<any>("");
        }

        let dialogRef = this.dialog.open(ImageCropComponent, {
            width: "50rem",
            data: {
                imageBase64String,
                ratioWidth,
                ratioHeight,
                imageName,
                resizeWidth,
                imageCount
            }
        });
        dialogRef.afterClosed().subscribe((res: any) => {
            if(res) {
                this.image.next(res);
                this.image.complete();
            }
            else {
                this.image.next("");
                this.image.complete();
            }
        });
    }
}
