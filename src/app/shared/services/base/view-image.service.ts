import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ViewImageComponent } from '../../components/view-image/view-image.component';

@Injectable({
    providedIn: 'root',

})
export class ViewImageService {

    constructor(private dialog: MatDialog) { }

    open(imageSource: string) {
        this.dialog.open(ViewImageComponent, {
            data: {
                imageSource
            },
            panelClass: "no-padding"
        });
    }
}
