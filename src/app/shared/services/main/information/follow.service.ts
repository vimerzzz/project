import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BaseService } from '../../base/base.service';

@Injectable({
    providedIn: 'root'
})
export class FollowService extends BaseService {
    private sharedHeaders = new HttpHeaders();
    constructor(private http: HttpClient) {
        super();
        this.sharedHeaders = this.sharedHeaders.set(
            "Content-Type",
            "application/json"
        );
    }

    getAllSoTheoDoi(item: any) {
        let params = new HttpParams();

        if(item) {
            if(item.userId) {
                params = params.append("userId", item.userId);
            }
            if(item.pageIndex) {
                params = params.append("pageIndex", item.pageIndex);
            }
            if(item.pageSize) {
                params = params.append("pageSize", item.pageSize);
            }
            if(item.searchString) {
                params = params.append("searchString", item.searchString);
            }
        }

        return this.http
            .get<any>(`${this.apiFollow}/getAllSoTheoDoi`, {
                headers: this.sharedHeaders,
                params: params
            });
    }

    createSoTheoDoi(item: any) {
        return this.http
            .post<any>(`${this.apiFollow}/createSoTheoDoi`, item, {
                headers: this.sharedHeaders
            });
    }

    deleteSoTheoDoi(item: any) {
        let params = new HttpParams();

        if(item) {
            if(item.userId) {
                params = params.append("userId", item.userId);
            }
            if(item.id) {
                params = params.append("id", item.id);
            }
            if(item.shopId) {
                params = params.append("shopId", item.shopId);
            }
        }

        return this.http
            .delete<any>(`${this.apiFollow}/deleteSoTheoDoi`, {
                headers: this.sharedHeaders,
                params: params
            });
    }

    getNewestAllSanPhamWithHinhAnhByShopId(item: any) {
        let params = new HttpParams();

        if(item) {
            if(item.shopId) {
                params = params.append("shopId", item.shopId);
            }
            if(item.pageIndex) {
                params = params.append("pageIndex", item.pageIndex);
            }
            if(item.pageSize) {
                params = params.append("pageSize", item.pageSize);
            }
        }

        return this.http
            .get<any>(`${this.apiItem}/getNewestAllSanPhamWithHinhAnhByShopId`, {
                headers: this.sharedHeaders,
                params: params
            });
    }
}
