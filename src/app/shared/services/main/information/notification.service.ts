import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BaseService } from '../../base/base.service';

@Injectable({
    providedIn: 'root'
})
export class NotificationService extends BaseService {
    private sharedHeaders = new HttpHeaders();
    constructor(private http: HttpClient) {
        super();
        this.sharedHeaders = this.sharedHeaders.set(
            "Content-Type",
            "application/json"
        );
    }

    getAllThongBao(item: any) {
        let params = new HttpParams();

        if(item) {
            if(item.pageIndex) {
                params = params.append("pageIndex", item.pageIndex);
            }
            if(item.pageSize) {
                params = params.append("pageSize", item.pageSize);
            }
            if(item.userId) {
                params = params.append("userId", item.userId);
            }
            params = params.append("daDoc", item.daDoc);
        }

        return this.http
            .get<any>(`${this.apiNotification}/getAllThongBao`, {
                headers: this.sharedHeaders,
                params: params
            });
    }

    updateThongBao(item: any) {
        return this.http
            .put<any>(`${this.apiNotification}/updateThongBao`, item, {
                headers: this.sharedHeaders
            });
    }

    deleteThongBao(userId: any, id: any) {
        let params = new HttpParams();

        if(userId) {
            params = params.append("userId", userId);
        }
        if(id.length) {
            for(let item of id) {
                params = params.append("id", item);
            }
        }
        return this.http
            .delete<any>(`${this.apiNotification}/deleteThongBao`, {
                headers: this.sharedHeaders,
                params: params
            });
    }
}
