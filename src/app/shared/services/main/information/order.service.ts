import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BaseService } from '../../base/base.service';

@Injectable({
    providedIn: 'root'
})
export class OrderService extends BaseService {
    private sharedHeaders = new HttpHeaders();
    constructor(private http: HttpClient) {
        super();
        this.sharedHeaders = this.sharedHeaders.set(
            "Content-Type",
            "application/json"
        );
    }

    getAllDonHang(item: any) {
        let params = new HttpParams();

        if(item) {
            if(item.pageIndex) {
                params = params.append("pageIndex", item.pageIndex);
            }
            if(item.pageSize) {
                params = params.append("pageSize", item.pageSize);
            }
            if(item.searchString) {
                params = params.append("searchString", item.searchString);
            }
            if(item.order) {
                params = params.append("order", item.order);
            }
            if(item.orderBy) {
                params = params.append("orderBy", item.orderBy);
            }
            if(item.userId) {
                params = params.append("userId", item.userId);
            }
            if(item.tinhTrangDonHangId) {
                params = params.append("tinhTrangDonHangId", item.tinhTrangDonHangId);
            }
            if(item.maDonHang) {
                params = params.append("maDonHang", item.maDonHang);
            }
        }

        return this.http
            .get<any>(`${this.apiOrder}/getAllDonHang`, {
                headers: this.sharedHeaders,
                params: params
            });
    }

    updateTinhTrangDonHang(userId: any, item: any) {
        let params = new HttpParams();

        if(userId) {
            params = params.append("userId", userId);
        }

        return this.http
            .put<any>(`${this.apiOrder}/updateTinhTrangDonHang`, item, {
                headers: this.sharedHeaders,
                params: params
            });
    }

    updateDonHang(item: any) {
        return this.http
            .put<any>(`${this.apiOrder}/updateDonHang`, item, {
                headers: this.sharedHeaders
            });
    }

    getDonHangByMaDonHang(userId: any, maDonHang: any) {
        let params = new HttpParams();

        if(maDonHang) {
            params = params.append("maDonHang", maDonHang);
        }
        if(userId) {
            params = params.append("userId", userId);
        }

        return this.http
            .get<any>(`${this.apiOrder}/getDonHangByMaDonHang`, {
                headers: this.sharedHeaders,
                params: params
            });
    }

    getAllGioHang(userId: any) {
        let params = new HttpParams();

        if(userId) {
            params = params.append("userId", userId);
        }

        return this.http
            .get<any>(`${this.apiCart}/getAllGioHang`, {
                headers: this.sharedHeaders,
                params: params
            });
    }

    createGioHang(item: any) {
        return this.http
            .post<any>(`${this.apiCart}/createGioHang`, item, {
                headers: this.sharedHeaders
            });
    }

    getSanPhamById(id: any) {
        let params = new HttpParams();

        if(id) {
            params = params.append("id", id);
        }

        return this.http
            .get<any>(`${this.apiItem}/getSanPhamById`, {
                headers: this.sharedHeaders,
                params: params
            });
    }
}
