import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BaseService } from '../../base/base.service';

@Injectable({
    providedIn: 'root'
})
export class SettingService extends BaseService {
    private sharedHeaders = new HttpHeaders();
    constructor(private http: HttpClient) {
        super();
        this.sharedHeaders = this.sharedHeaders.set(
            "Content-Type",
            "application/json"
        );
    }

    getUserById(id: any) {
        let params = new HttpParams();

        if(id) {
            params = params.append("id", id);
        }

        return this.http
            .get<any>(`${this.apiUser}/getUserById`, {
                headers: this.sharedHeaders,
                params: params
            });
    }

    updateUser(item: any) {
        return this.http
            .put<any>(`${this.apiUser}/updateUser`, item, {
                headers: this.sharedHeaders
            });
    }

    checkExistedUserName(item: any) {
        return this.http
            .post<any>(`${this.apiUser}/checkExistedUserName`, item, {
                headers: this.sharedHeaders
            });
    }

    checkExistedEmail(item: any) {
        return this.http
            .post<any>(`${this.apiUser}/checkExistedEmail`, item, {
                headers: this.sharedHeaders
            });
    }

    checkPassword(item: any) {
        return this.http
            .post<any>(`${this.apiUser}/checkPassword`, item, {
                headers: this.sharedHeaders
            });
    }

    getEmailVerification(item: any) {
        let params = new HttpParams();

        if(item) {
            if(item.id) {
                params = params.append("id", item.id);
            }
            if(item.urlClient) {
                params = params.append("urlClient", item.urlClient);
            }
        }

        return this.http
            .get<any>(`${this.apiVerify}/getEmailVerification`, {
                headers: this.sharedHeaders,
                params: params
            });
    }
}
