import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BaseService } from '../../base/base.service';

@Injectable({
    providedIn: 'root'
})
export class MyInformationService extends BaseService {
    private sharedHeaders = new HttpHeaders();
    constructor(private http: HttpClient) {
        super();
        this.sharedHeaders = this.sharedHeaders.set(
            "Content-Type",
            "application/json"
        );
    }

    getUserInfoByUserId(userId: any) {
        let params = new HttpParams();

        if(userId) {
            params = params.append("userId", userId);
        }

        return this.http
            .get<any>(`${this.apiUserInfo}/getUserInfoByUserId`, {
                headers: this.sharedHeaders,
                params: params
            });
    }

    updateUserInfo(item: any) {
        return this.http
            .put<any>(`${this.apiUserInfo}/updateUserInfo`, item, {
                headers: this.sharedHeaders
            });
    }

    uploadAnhDaiDien(userId: any, item: any) {
        let params = new HttpParams();

        if(userId) {
            params = params.append("userId", userId);
        }

        return this.http
            .post<any>(`${this.apiUserInfo}/uploadAnhDaiDien`, item, {
                params: params
            });
    }
}
