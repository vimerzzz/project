import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BaseService } from '../../base/base.service';

@Injectable({
    providedIn: 'root'
})
export class AddressService extends BaseService {
    private sharedHeaders = new HttpHeaders();
    constructor(private http: HttpClient) {
        super();
        this.sharedHeaders = this.sharedHeaders.set(
            "Content-Type",
            "application/json"
        );
    }

    getAllDiaChi(item: any) {
        let params = new HttpParams();

        if(item) {
            if(item.pageIndex) {
                params = params.append("pageIndex", item.pageIndex);
            }
            if(item.pageSize) {
                params = params.append("pageSize", item.pageSize);
            }
            if(item.searchString) {
                params = params.append("searchString", item.searchString);
            }
            if(item.order) {
                params = params.append("order", item.order);
            }
            if(item.orderBy) {
                params = params.append("orderBy", item.orderBy);
            }
            if(item.userId) {
                params = params.append("userId", item.userId);
            }
        }

        return this.http
            .get<any>(`${this.apiAddress}/getAllDiaChi`, {
                headers: this.sharedHeaders,
                params: params
            });
    }

    createDiaChi(item: any) {
        return this.http
            .post<any>(`${this.apiAddress}/createDiaChi`, item, {
                headers: this.sharedHeaders
            });
    }

    updateDiaChi(item: any) {
        return this.http
            .put<any>(`${this.apiAddress}/updateDiaChi`, item, {
                headers: this.sharedHeaders
            });
    }

    deleteDiaChi(item: any) {
        let params = new HttpParams();

        if(item) {
            if(item.id) {
                params = params.append("id", item.id);
            }
            if(item.userId) {
                params = params.append("userId", item.userId);
            }
        }
        return this.http
            .delete<any>(`${this.apiAddress}/deleteDiaChi`, {
                headers: this.sharedHeaders,
                params: params
            });
    }
}
