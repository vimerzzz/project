import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BaseService } from '../../base/base.service';

@Injectable({
    providedIn: 'root'
})
export class ChatUserService extends BaseService {
    private sharedHeaders = new HttpHeaders();
    constructor(private http: HttpClient) {
        super();
        this.sharedHeaders = this.sharedHeaders.set(
            "Content-Type",
            "application/json"
        );
    }

    getAllNhanTinUser(item: any) {
        let params = new HttpParams();

        if(item) {
            if(item.userGuiId) {
                params = params.append("userGuiId", item.userGuiId);
            }
            if(item.userNhanId) {
                params = params.append("userNhanId", item.userNhanId);
            }
            if(item.pageIndex) {
                params = params.append("pageIndex", item.pageIndex);
            }
            if(item.pageSize) {
                params = params.append("pageSize", item.pageSize);
            }
            if(item.daXem != null) {
                params = params.append("daXem", item.daXem);
            }
        }

        return this.http
            .get<any>(`${this.apiUserChat}/getAllNhanTinUser`, {
                headers: this.sharedHeaders,
                params: params
            });
    }

    createNhanTinUser(userId: any, item: any) {
        let params = new HttpParams();

        if(userId) {
            params = params.append("userId", userId);
        }

        return this.http
            .post<any>(`${this.apiUserChat}/createNhanTinUser`, item, {
                headers: this.sharedHeaders,
                params: params
            });
    }

    updateNhanTinUser(userId: any, item: any) {
        let params = new HttpParams();

        if(userId) {
            params = params.append("userId", userId);
        }

        return this.http
            .put<any>(`${this.apiUserChat}/updateNhanTinUser`, item, {
                headers: this.sharedHeaders,
                params: params
            });
    }

    getUserInfoByUserId(userId: any) {
        let params = new HttpParams();

        if(userId) {
            params = params.append("userId", userId);
        }

        return this.http
            .get<any>(`${this.apiUserInfo}/getUserInfoByUserId`, {
                headers: this.sharedHeaders,
                params: params
            });
    }

    getAllUserNhanId(item: any) {
        let params = new HttpParams();

        if(item) {
            if(item.userGuiId) {
                params = params.append("userGuiId", item.userGuiId);
            }
            if(item.pageIndex) {
                params = params.append("pageIndex", item.pageIndex);
            }
            if(item.pageSize) {
                params = params.append("pageSize", item.pageSize);
            }
        }

        return this.http
            .get<any>(`${this.apiUserChat}/getAllUserNhanId`, {
                headers: this.sharedHeaders,
                params: params
            });
    }

    getUserById(id: any) {
        let params = new HttpParams();

        if(id) {
            params = params.append("id", id);
        }

        return this.http
            .get<any>(`${this.apiUser}/getUserById`, {
                headers: this.sharedHeaders,
                params: params
            });
    }

    uploadFile(item: any, file: any) {
        let params = new HttpParams();

        if(item) {
            if(item.userLogId) {
                params = params.append("userLogId", item.userLogId);
            }
            if(item.isImage) {
                params = params.append("isImage", item.isImage);
            }
            if(item.isFile) {
                params = params.append("isFile", item.isFile);
            }
            if(item.userGuiId) {
                params = params.append("userGuiId", item.userGuiId);
            }
            if(item.userNhanId) {
                params = params.append("userNhanId", item.userNhanId);
            }
        }

        return this.http
            .post<any>(`${this.apiUserChat}/uploadFile`, file, {
                params: params,
                reportProgress: true,
                observe: "events"
            });
    }
}
