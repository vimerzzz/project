import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BaseService } from '../../base/base.service';

@Injectable({
    providedIn: 'root'
})
export class ChatShopService extends BaseService {
    private sharedHeaders = new HttpHeaders();
    constructor(private http: HttpClient) {
        super();
        this.sharedHeaders = this.sharedHeaders.set(
            "Content-Type",
            "application/json"
        );
    }

    getAllNhanTinShop(item: any) {
        let params = new HttpParams();

        if(item) {
            if(item.userId) {
                params = params.append("userId", item.userId);
            }
            if(item.shopId) {
                params = params.append("shopId", item.shopId);
            }
            if(item.pageIndex) {
                params = params.append("pageIndex", item.pageIndex);
            }
            if(item.pageSize) {
                params = params.append("pageSize", item.pageSize);
            }
            if(item.daXem != null) {
                params = params.append("daXem", item.daXem);
            }
        }

        return this.http
            .get<any>(`${this.apiShopChat}/getAllNhanTinShop`, {
                headers: this.sharedHeaders,
                params: params
            });
    }

    getAllShopId(item: any) {
        let params = new HttpParams();

        if(item) {
            if(item.userId) {
                params = params.append("userId", item.userId);
            }
            if(item.pageIndex) {
                params = params.append("pageIndex", item.pageIndex);
            }
            if(item.pageSize) {
                params = params.append("pageSize", item.pageSize);
            }
        }

        return this.http
            .get<any>(`${this.apiShopChat}/getAllShopId`, {
                headers: this.sharedHeaders,
                params: params
            });
    }

    createNhanTinShop(userId: any, item: any) {
        let params = new HttpParams();

        if(userId) {
            params = params.append("userId", userId);
        }

        return this.http
            .post<any>(`${this.apiShopChat}/createNhanTinShop`, item, {
                headers: this.sharedHeaders,
                params: params
            });
    }

    updateNhanTinShop(userId: any, item: any) {
        let params = new HttpParams();

        if(userId) {
            params = params.append("userId", userId);
        }

        return this.http
            .put<any>(`${this.apiShopChat}/updateNhanTinShop`, item, {
                headers: this.sharedHeaders,
                params: params
            });
    }

    getShopByShopId(shopId: any) {
        let params = new HttpParams();

        if(shopId) {
            params = params.append("shopId", shopId);
        }

        return this.http
            .get<any>(`${this.apiShop}/getShopByShopId`, {
                headers: this.sharedHeaders,
                params: params
            });
    }

    getUserInfoByUserId(userId: any) {
        let params = new HttpParams();

        if(userId) {
            params = params.append("userId", userId);
        }

        return this.http
            .get<any>(`${this.apiUserInfo}/getUserInfoByUserId`, {
                headers: this.sharedHeaders,
                params: params
            });
    }

    getAllUserId(item: any) {
        let params = new HttpParams();

        if(item) {
            if(item.shopId) {
                params = params.append("shopId", item.shopId);
            }
            if(item.pageIndex) {
                params = params.append("pageIndex", item.pageIndex);
            }
            if(item.pageSize) {
                params = params.append("pageSize", item.pageSize);
            }
        }

        return this.http
            .get<any>(`${this.apiShopChat}/getAllUserId`, {
                headers: this.sharedHeaders,
                params: params
            });
    }

    getShopByUserId(userId: any) {
        let params = new HttpParams();

        if(userId) {
            params = params.append("userId", userId);
        }

        return this.http
            .get<any>(`${this.apiShop}/getShopByUserId`, {
                headers: this.sharedHeaders,
                params: params
            });
    }

    getUserById(id: any) {
        let params = new HttpParams();

        if(id) {
            params = params.append("id", id);
        }

        return this.http
            .get<any>(`${this.apiUser}/getUserById`, {
                headers: this.sharedHeaders,
                params: params
            });
    }

    uploadFile(item: any, file: any) {
        let params = new HttpParams();

        if(item) {
            if(item.userLogId) {
                params = params.append("userLogId", item.userLogId);
            }
            if(item.isImage) {
                params = params.append("isImage", item.isImage);
            }
            if(item.isFile) {
                params = params.append("isFile", item.isFile);
            }
            if(item.userId) {
                params = params.append("userId", item.userId);
            }
            if(item.shopId) {
                params = params.append("shopId", item.shopId);
            }
            if(item.nguoiGuiId) {
                params = params.append("nguoiGuiId", item.nguoiGuiId);
            }
        }

        return this.http
            .post<any>(`${this.apiShopChat}/uploadFile`, file, {
                params: params,
                reportProgress: true,
                observe: "events"
            });
    }

    createBaoCao(userId: any, item: any) {
        let params = new HttpParams();

        if(userId) {
            params = params.append("userId", userId);
        }

        return this.http
            .post<any>(`${this.apiReport}/createBaoCao`, item, {
                headers: this.sharedHeaders,
                params: params
            });
    }
}
