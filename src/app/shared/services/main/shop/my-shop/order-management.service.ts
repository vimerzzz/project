import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BaseService } from '../../../base/base.service';

@Injectable({
    providedIn: 'root'
})
export class OrderManagementService extends BaseService {
    private sharedHeaders = new HttpHeaders();
    constructor(private http: HttpClient) {
        super();
        this.sharedHeaders = this.sharedHeaders.set(
            "Content-Type",
            "application/json"
        );
    }

    getUserInfoByUserId(userId: any) {
        let params = new HttpParams();

        if(userId) {
            params = params.append("userId", userId);
        }

        return this.http
            .get<any>(`${this.apiUserInfo}/getUserInfoByUserId`, {
                headers: this.sharedHeaders,
                params: params
            });
    }

    getShopByUserId(userId: any) {
        let params = new HttpParams();

        if(userId) {
            params = params.append("userId", userId);
        }

        return this.http
            .get<any>(`${this.apiShop}/getShopByUserId`, {
                headers: this.sharedHeaders,
                params: params
            });
    }

    getAllDonHang(item: any) {
        let params = new HttpParams();

        if(item) {
            if(item.pageIndex) {
                params = params.append("pageIndex", item.pageIndex);
            }
            if(item.pageSize) {
                params = params.append("pageSize", item.pageSize);
            }
            if(item.searchString) {
                params = params.append("searchString", item.searchString);
            }
            if(item.order) {
                params = params.append("order", item.order);
            }
            if(item.orderBy) {
                params = params.append("orderBy", item.orderBy);
            }
            if(item.shopId) {
                params = params.append("shopId", item.shopId);
            }
            if(item.tinhTrangDonHangId) {
                params = params.append("tinhTrangDonHangId", item.tinhTrangDonHangId);
            }
            if(item.maDonHang) {
                params = params.append("maDonHang", item.maDonHang);
            }
        }

        return this.http
            .get<any>(`${this.apiOrder}/getAllDonHang`, {
                headers: this.sharedHeaders,
                params: params
            });
    }

    updateTinhTrangDonHang(userId: any, item: any) {
        let params = new HttpParams();

        if(userId) {
            params = params.append("userId", userId);
        }

        return this.http
            .put<any>(`${this.apiOrder}/updateTinhTrangDonHang`, item, {
                headers: this.sharedHeaders,
                params: params
            });
    }
}
