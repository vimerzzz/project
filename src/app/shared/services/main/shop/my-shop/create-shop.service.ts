import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BaseService } from '../../../base/base.service';

@Injectable({
    providedIn: 'root'
})
export class CreateShopService extends BaseService {
    private sharedHeaders = new HttpHeaders();
    constructor(private http: HttpClient) {
        super();
        this.sharedHeaders = this.sharedHeaders.set(
            "Content-Type",
            "application/json"
        );
    }

    getShopByShopName(shopName: any) {
        let params = new HttpParams();

        if(shopName) {
            params = params.append("shopName", shopName);
        }

        return this.http
            .get<any>(`${this.apiShop}/getShopByShopName`, {
                headers: this.sharedHeaders,
                params: params
            });
    }

    createShop(item: any) {
        return this.http
            .post<any>(`${this.apiShop}/createShop`, item, {
                headers: this.sharedHeaders
            });
    }

    getUserInfoByUserId(userId: any) {
        let params = new HttpParams();

        if(userId) {
            params = params.append("userId", userId);
        }

        return this.http
            .get<any>(`${this.apiUserInfo}/getUserInfoByUserId`, {
                headers: this.sharedHeaders,
                params: params
            });
    }

    getUserById(id: any) {
        let params = new HttpParams();

        if(id) {
            params = params.append("id", id);
        }

        return this.http
            .get<any>(`${this.apiUser}/getUserById`, {
                headers: this.sharedHeaders,
                params: params
            });
    }
}
