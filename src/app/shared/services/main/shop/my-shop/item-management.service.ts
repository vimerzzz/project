import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BaseService } from '../../../base/base.service';

@Injectable({
    providedIn: 'root'
})
export class ItemManagementService extends BaseService {
    private sharedHeaders = new HttpHeaders();
    constructor(private http: HttpClient) {
        super();
        this.sharedHeaders = this.sharedHeaders.set(
            "Content-Type",
            "application/json"
        );
    }

    getUserInfoByUserId(userId: any) {
        let params = new HttpParams();

        if(userId) {
            params = params.append("userId", userId);
        }

        return this.http
            .get<any>(`${this.apiUserInfo}/getUserInfoByUserId`, {
                headers: this.sharedHeaders,
                params: params
            });
    }

    getShopByUserId(userId: any) {
        let params = new HttpParams();

        if(userId) {
            params = params.append("userId", userId);
        }

        return this.http
            .get<any>(`${this.apiShop}/getShopByUserId`, {
                headers: this.sharedHeaders,
                params: params
            });
    }

    getAllSanPhamByShopId(item: any) {
        let params = new HttpParams();

        if(item) {
            if(item.shopId) {
                params = params.append("shopId", item.shopId);
            }
            if(item.pageIndex) {
                params = params.append("pageIndex", item.pageIndex);
            }
            if(item.pageSize) {
                params = params.append("pageSize", item.pageSize);
            }
            if(item.searchString) {
                params = params.append("searchString", item.searchString);
            }
            if(item.order) {
                params = params.append("order", item.order);
            }
            if(item.orderBy) {
                params = params.append("orderBy", item.orderBy);
            }
            if(item.loaiSanPhamId) {
                params = params.append("loaiSanPhamId", item.loaiSanPhamId);
            }
        }

        return this.http
            .get<any>(`${this.apiItem}/getAllSanPhamByShopId`, {
                headers: this.sharedHeaders,
                params: params
            });
    }

    getSanPhamWithHinhAnhByName(tenShop: any, tenSanPham: any) {
        let params = new HttpParams();

        if(tenShop) {
            params = params.append("tenShop", tenShop);
        }

        if(tenSanPham) {
            params = params.append("tenSanPham", tenSanPham);
        }

        return this.http
            .get<any>(`${this.apiItem}/getSanPhamWithHinhAnhByName`, {
                headers: this.sharedHeaders,
                params: params
            });
    }

    createSanPham(item: any) {
        return this.http
            .post<any>(`${this.apiItem}/createSanPham`, item, {
                headers: this.sharedHeaders
            });
    }

    updateSanPham(item: any) {
        return this.http
            .put<any>(`${this.apiItem}/updateSanPham`, item, {
                headers: this.sharedHeaders
            });
    }

    deleteSanPham(item: any) {
        let params = new HttpParams();

        if(item) {
            if(item.id) {
                params = params.append("id", item.id);
            }
            if(item.userId) {
                params = params.append("userId", item.userId);
            }
            if(item.shopId) {
                params = params.append("shopId", item.shopId);
            }
        }

        return this.http
            .delete<any>(`${this.apiItem}/deleteSanPham`, {
                headers: this.sharedHeaders,
                params: params
            });
    }

    getAllLoaiSanPham(item: any) {
        let params = new HttpParams();

        if(item) {
            if(item.pageIndex) {
                params = params.append("pageIndex", item.pageIndex);
            }
            if(item.pageSize) {
                params = params.append("pageSize", item.pageSize);
            }
            if(item.searchString) {
                params = params.append("searchString", item.searchString);
            }
            if(item.order) {
                params = params.append("order", item.order);
            }
            if(item.orderBy) {
                params = params.append("orderBy", item.orderBy);
            }
        }

        return this.http
            .get<any>(`${this.apiCategory}/getAllLoaiSanPham`, {
                headers: this.sharedHeaders,
                params: params
            });
    }

    getAllHinhAnhSanPhamBySanPhamId(sanPhamId: any) {
        let params = new HttpParams();

        if(sanPhamId) {
            params = params.append("sanPhamId", sanPhamId);
        }

        return this.http
            .get<any>(`${this.apiItemImage}/getAllHinhAnhSanPhamBySanPhamId`, {
                headers: this.sharedHeaders,
                params: params
            });
    }

    createHinhAnhSanPham(userId: any, sanPhamId: any, item: any) {
        let params = new HttpParams();

        if(userId) {
            params = params.append("userId", userId);
        }
        if(sanPhamId) {
            params = params.append("sanPhamId", sanPhamId);
        }

        return this.http
            .post<any>(`${this.apiItemImage}/createHinhAnhSanPham`, item, {
                params: params
            });
    }

    deleteHinhAnhSanPham(userId: any, item: any) {
        let params = new HttpParams();

        if(userId) {
            params = params.append("userId", userId);
        }
        if(item) {
            for(let obj of item) {
                params = params.append("hinhAnhSanPhamId", obj);
            }
        }

        return this.http
            .delete<any>(`${this.apiItemImage}/deleteHinhAnhSanPham`, {
                headers: this.sharedHeaders,
                params: params
            });
    }

    updateHinhAnhSanPham(item: any) {
        return this.http
            .put<any>(`${this.apiItemImage}/updateHinhAnhSanPham`, item, {
                headers: this.sharedHeaders,
            });
    }

    // getAllThuocTinh(item: any) {
    //     let params = new HttpParams();

    //     if(item) {
    //         params = params.append("loaiSanPhamId", item);
    //     }

    //     return this.http
    //         .get<any>(`${this.apiAttribute}/getAllThuocTinh`, {
    //             headers: this.sharedHeaders,
    //             params: params
    //         });
    // }
}
