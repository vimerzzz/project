import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BaseService } from '../../../base/base.service';

@Injectable({
    providedIn: 'root',
})
export class AuctionItemManagementService  extends BaseService {
    private sharedHeaders = new HttpHeaders();
    constructor(private http: HttpClient) {
        super();
        this.sharedHeaders = this.sharedHeaders.set(
            "Content-Type",
            "application/json"
        );
    }

    getUserInfoByUserId(userId: any) {
        let params = new HttpParams();

        if(userId) {
            params = params.append("userId", userId);
        }

        return this.http
            .get<any>(`${this.apiUserInfo}/getUserInfoByUserId`, {
                headers: this.sharedHeaders,
                params: params
            });
    }

    getShopByUserId(userId: any) {
        let params = new HttpParams();

        if(userId) {
            params = params.append("userId", userId);
        }

        return this.http
            .get<any>(`${this.apiShop}/getShopByUserId`, {
                headers: this.sharedHeaders,
                params: params
            });
    }

    getAllSanPhamDauGiaByShopId(item: any) {
        let params = new HttpParams();

        if(item) {
            if(item.shopId) {
                params = params.append("shopId", item.shopId);
            }
            if(item.pageIndex) {
                params = params.append("pageIndex", item.pageIndex);
            }
            if(item.pageSize) {
                params = params.append("pageSize", item.pageSize);
            }
            if(item.searchString) {
                params = params.append("searchString", item.searchString);
            }
            if(item.order) {
                params = params.append("order", item.order);
            }
            if(item.orderBy) {
                params = params.append("orderBy", item.orderBy);
            }
            if(item.loaiSanPhamId) {
                params = params.append("loaiSanPhamId", item.loaiSanPhamId);
            }
            if(item.tinhTrangDauGiaId) {
                params = params.append("tinhTrangDauGiaId", item.tinhTrangDauGiaId);
            }
        }

        return this.http
            .get<any>(`${this.apiAuction}/getAllSanPhamDauGiaByShopId`, {
                headers: this.sharedHeaders,
                params: params
            });
    }

    getSanPhamDauGiaWithHinhAnhByName(tenShop: any, tenSanPham: any) {
        let params = new HttpParams();

        if(tenShop) {
            params = params.append("tenShop", tenShop);
        }

        if(tenSanPham) {
            params = params.append("tenSanPham", tenSanPham);
        }

        return this.http
            .get<any>(`${this.apiAuction}/getSanPhamDauGiaWithHinhAnhByName`, {
                headers: this.sharedHeaders,
                params: params
            });
    }

    createSanPhamDauGia(item: any) {
        return this.http
            .post<any>(`${this.apiAuction}/createSanPhamDauGia`, item, {
                headers: this.sharedHeaders
            });
    }

    updateSanPhamDauGia(item: any) {
        return this.http
            .put<any>(`${this.apiAuction}/updateSanPhamDauGia`, item, {
                headers: this.sharedHeaders
            });
    }

    cancelSanPhamDauGia(item: any) {
        return this.http
            .put<any>(`${this.apiAuction}/cancelSanPhamDauGia`, item, {
                headers: this.sharedHeaders
            });
    }

    deleteSanPhamDauGia(item: any) {
        let params = new HttpParams();

        if(item) {
            if(item.id) {
                params = params.append("id", item.id);
            }
            if(item.userId) {
                params = params.append("userId", item.userId);
            }
            if(item.shopId) {
                params = params.append("shopId", item.shopId);
            }
        }

        return this.http
            .delete<any>(`${this.apiAuction}/deleteSanPhamDauGia`, {
                headers: this.sharedHeaders,
                params: params
            });
    }

    getAllLoaiSanPham(item: any) {
        let params = new HttpParams();

        if(item) {
            if(item.pageIndex) {
                params = params.append("pageIndex", item.pageIndex);
            }
            if(item.pageSize) {
                params = params.append("pageSize", item.pageSize);
            }
            if(item.searchString) {
                params = params.append("searchString", item.searchString);
            }
            if(item.order) {
                params = params.append("order", item.order);
            }
            if(item.orderBy) {
                params = params.append("orderBy", item.orderBy);
            }
        }

        return this.http
            .get<any>(`${this.apiCategory}/getAllLoaiSanPham`, {
                headers: this.sharedHeaders,
                params: params
            });
    }

    getAllTinhTrangDauGia() {
        return this.http
            .get<any>(`${this.apiAuction}/getAllTinhTrangDauGia`, {
                headers: this.sharedHeaders
            });
    }

    getAllHinhAnhSanPhamDauGiaBySanPhamId(sanPhamDauGiaId: any) {
        let params = new HttpParams();

        if(sanPhamDauGiaId) {
            params = params.append("sanPhamDauGiaId", sanPhamDauGiaId);
        }

        return this.http
            .get<any>(`${this.apiAuctionImage}/getAllHinhAnhSanPhamDauGiaBySanPhamId`, {
                headers: this.sharedHeaders,
                params: params
            });
    }

    createHinhAnhSanPhamDauGia(userId: any, sanPhamDauGiaId: any, item: any) {
        let params = new HttpParams();

        if(userId) {
            params = params.append("userId", userId);
        }
        if(sanPhamDauGiaId) {
            params = params.append("sanPhamDauGiaId", sanPhamDauGiaId);
        }

        return this.http
            .post<any>(`${this.apiAuctionImage}/createHinhAnhSanPhamDauGia`, item, {
                params: params
            });
    }

    deleteHinhAnhSanPhamDauGia(userId: any, item: any) {
        let params = new HttpParams();

        if(userId) {
            params = params.append("userId", userId);
        }
        if(item) {
            for(let obj of item) {
                params = params.append("hinhAnhSanPhamDauGiaId", obj);
            }
        }

        return this.http
            .delete<any>(`${this.apiAuctionImage}/deleteHinhAnhSanPhamDauGia`, {
                headers: this.sharedHeaders,
                params: params
            });
    }

    updateHinhAnhSanPhamDauGia(item: any) {
        return this.http
            .put<any>(`${this.apiAuctionImage}/updateHinhAnhSanPhamDauGia`, item, {
                headers: this.sharedHeaders,
            });
    }
}
