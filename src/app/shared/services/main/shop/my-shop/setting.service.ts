import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BaseService } from '../../../base/base.service';

@Injectable({
    providedIn: 'root'
})
export class SettingService extends BaseService {
    private sharedHeaders = new HttpHeaders();
    constructor(private http: HttpClient) {
        super();
        this.sharedHeaders = this.sharedHeaders.set(
            "Content-Type",
            "application/json"
        );
    }

    getUserInfoByUserId(userId: any) {
        let params = new HttpParams();

        if(userId) {
            params = params.append("userId", userId);
        }

        return this.http
            .get<any>(`${this.apiUserInfo}/getUserInfoByUserId`, {
                headers: this.sharedHeaders,
                params: params
            });
    }

    getShopByUserId(userId: any) {
        let params = new HttpParams();

        if(userId) {
            params = params.append("userId", userId);
        }

        return this.http
            .get<any>(`${this.apiShop}/getShopByUserId`, {
                headers: this.sharedHeaders,
                params: params
            });
    }

    getShopByShopName(shopName: any) {
        let params = new HttpParams();

        if(shopName) {
            params = params.append("shopName", shopName);
        }

        return this.http
            .get<any>(`${this.apiShop}/getShopByShopName`, {
                headers: this.sharedHeaders,
                params: params
            });
    }

    updateShop(item: any) {
        return this.http
            .put<any>(`${this.apiShop}/updateShop`, item, {
                headers: this.sharedHeaders
            });
    }

    uploadAnhDaiDien(userId: any, item: any) {
        let params = new HttpParams();

        if(userId) {
            params = params.append("userId", userId);
        }

        return this.http
            .post<any>(`${this.apiShop}/uploadAnhDaiDien`, item, {
                params: params
            });
    }

    getUserById(id: any) {
        let params = new HttpParams();

        if(id) {
            params = params.append("id", id);
        }

        return this.http
            .get<any>(`${this.apiUser}/getUserById`, {
                headers: this.sharedHeaders,
                params: params
            });
    }
}
