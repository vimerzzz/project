import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BaseService } from '../../base/base.service';

@Injectable({
    providedIn: 'root'
})
export class ShopReviewService extends BaseService {
    private sharedHeaders = new HttpHeaders();
    constructor(private http: HttpClient) {
        super();
        this.sharedHeaders = this.sharedHeaders.set(
            "Content-Type",
            "application/json"
        );
    }

    getShopByShopName(shopName: any) {
        let params = new HttpParams();

        if(shopName) {
            params = params.append("shopName", shopName);
        }

        return this.http
            .get<any>(`${this.apiShop}/getShopByShopName`, {
                headers: this.sharedHeaders,
                params: params
            });
    }

    getAllSoTheoDoi(item: any) {
        let params = new HttpParams();

        if(item) {
            if(item.userId) {
                params = params.append("userId", item.userId);
            }
            if(item.shopId) {
                params = params.append("shopId", item.shopId);
            }
        }

        return this.http
            .get<any>(`${this.apiFollow}/getAllSoTheoDoi`, {
                headers: this.sharedHeaders,
                params: params
            });
    }

    createSoTheoDoi(item: any) {
        return this.http
            .post<any>(`${this.apiFollow}/createSoTheoDoi`, item, {
                headers: this.sharedHeaders
            });
    }

    deleteSoTheoDoi(item: any) {
        let params = new HttpParams();

        if(item) {
            if(item.userId) {
                params = params.append("userId", item.userId);
            }
            if(item.id) {
                params = params.append("id", item.id);
            }
            if(item.shopId) {
                params = params.append("shopId", item.shopId);
            }
        }

        return this.http
            .delete<any>(`${this.apiFollow}/deleteSoTheoDoi`, {
                headers: this.sharedHeaders,
                params: params
            });
    }

    getNewestAllSanPhamWithHinhAnhByShopId(item: any) {
        let params = new HttpParams();

        if(item) {
            if(item.shopId) {
                params = params.append("shopId", item.shopId);
            }
            if(item.pageIndex) {
                params = params.append("pageIndex", item.pageIndex);
            }
            if(item.pageSize) {
                params = params.append("pageSize", item.pageSize);
            }
            if(item.searchString) {
                params = params.append("searchString", item.searchString);
            }
        }

        return this.http
            .get<any>(`${this.apiItem}/getNewestAllSanPhamWithHinhAnhByShopId`, {
                headers: this.sharedHeaders,
                params: params
            });
    }

    getAllDanhGia(item: any) {
        let params = new HttpParams();

        if(item) {
            if(item.shopId) {
                params = params.append("shopId", item.shopId);
            }
            if(item.pageIndex) {
                params = params.append("pageIndex", item.pageIndex);
            }
            if(item.pageSize) {
                params = params.append("pageSize", item.pageSize);
            }
            if(item.oneStar) {
                params = params.append("danhGia", 1);
            }
            if(item.twoStar) {
                params = params.append("danhGia", 2);
            }
            if(item.threeStar) {
                params = params.append("danhGia", 3);
            }
            if(item.fourStar) {
                params = params.append("danhGia", 4);
            }
            if(item.fiveStar) {
                params = params.append("danhGia", 5);
            }
            if(item.order) {
                params = params.append("order", item.order);
            }
            if(item.orderBy) {
                params = params.append("orderBy", item.orderBy);
            }
        }

        return this.http
            .get<any>(`${this.apiShopRate}/getAllDanhGia`, {
                headers: this.sharedHeaders,
                params: params
            });
    }

    createDanhGia(item: any) {
        return this.http
            .post<any>(`${this.apiShopRate}/createDanhGia`, item, {
                headers: this.sharedHeaders
            });
    }

    getDanhGiaHuuIchByUserId(userId: any) {
        let params = new HttpParams();

        if(userId) {
            params = params.append("userId", userId);
        }

        return this.http
            .get<any>(`${this.apiShopRate}/getDanhGiaHuuIchByUserId`, {
                headers: this.sharedHeaders,
                params: params
            });
    }

    createDanhGiaHuuIch(item: any) {
        return this.http
            .post<any>(`${this.apiShopRate}/createDanhGiaHuuIch`, item, {
                headers: this.sharedHeaders
            });
    }

    deleteDanhGiaHuuIch(item: any) {
        let params = new HttpParams();

        if(item) {
            if(item.id) {
                params = params.append("id", item.id);
            }
            if(item.userId) {
                params = params.append("userId", item.userId);
            }
        }

        return this.http
            .delete<any>(`${this.apiShopRate}/deleteDanhGiaHuuIch`, {
                headers: this.sharedHeaders,
                params: params
            });
    }

    createBaoCao(userId: any, item: any) {
        let params = new HttpParams();

        if(userId) {
            params = params.append("userId", userId);
        }

        return this.http
            .post<any>(`${this.apiReport}/createBaoCao`, item, {
                headers: this.sharedHeaders,
                params: params
            });
    }
}
