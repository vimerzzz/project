import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BaseService } from '../../base/base.service';

@Injectable({
    providedIn: 'root',
})
export class AuctionReviewService extends BaseService {
    private sharedHeaders = new HttpHeaders();
    constructor(private http: HttpClient) {
        super();
        this.sharedHeaders = this.sharedHeaders.set(
            "Content-Type",
            "application/json"
        );
    }

    getShopByShopName(shopName: any) {
        let params = new HttpParams();

        if(shopName) {
            params = params.append("shopName", shopName);
        }

        return this.http
            .get<any>(`${this.apiShop}/getShopByShopName`, {
                headers: this.sharedHeaders,
                params: params
            });
    }

    getSanPhamDauGiaWithHinhAnhByName(tenShop: any, tenSanPham: any) {
        let params = new HttpParams();

        if(tenShop) {
            params = params.append("tenShop", tenShop);
        }

        if(tenSanPham) {
            params = params.append("tenSanPham", tenSanPham);
        }

        return this.http
            .get<any>(`${this.apiAuction}/getSanPhamDauGiaWithHinhAnhByName`, {
                headers: this.sharedHeaders,
                params: params
            });
    }

    updateTinhHinhDauGia(item: any) {
        return this.http
            .put<any>(`${this.apiAuction}/updateTinhHinhDauGia`, item, {
                headers: this.sharedHeaders
            });
    }

    yeuCauKetThucDauGia(item: any) {
        return this.http
            .put<any>(`${this.apiAuction}/yeuCauKetThucDauGia`, item, {
                headers: this.sharedHeaders
            });
    }

    updateTinhTrangDauGia(item: any) {
        return this.http
            .put<any>(`${this.apiAuction}/updateTinhTrangDauGia`, item, {
                headers: this.sharedHeaders
            });
    }

    getAllTroChuyenDauGia(item: any) {
        let params = new HttpParams();

        if(item) {
            if(item.userId) {
                params = params.append("userId", item.userId);
            }
            if(item.sanPhamDauGiaId) {
                params = params.append("sanPhamDauGiaId", item.sanPhamDauGiaId);
            }
            if(item.pageIndex) {
                params = params.append("pageIndex", item.pageIndex);
            }
            if(item.pageSize) {
                params = params.append("pageSize", item.pageSize);
            }
        }

        return this.http
            .get<any>(`${this.apiAuctionChat}/getAllTroChuyenDauGia`, {
                headers: this.sharedHeaders,
                params: params
            });
    }

    createTroChuyenDauGia(userId: any, item: any) {
        let params = new HttpParams();

        if(userId) {
            params = params.append("userId", userId);
        }

        return this.http
            .post<any>(`${this.apiAuctionChat}/createTroChuyenDauGia`, item, {
                headers: this.sharedHeaders,
                params: params
            });
    }

    getUserById(id: any) {
        let params = new HttpParams();

        if(id) {
            params = params.append("id", id);
        }

        return this.http
            .get<any>(`${this.apiUser}/getUserById`, {
                headers: this.sharedHeaders,
                params: params
            });
    }

    getUserInfoByUserId(userId: any) {
        let params = new HttpParams();

        if(userId) {
            params = params.append("userId", userId);
        }

        return this.http
            .get<any>(`${this.apiUserInfo}/getUserInfoByUserId`, {
                headers: this.sharedHeaders,
                params: params
            });
    }

    createBaoCao(userId: any, item: any) {
        let params = new HttpParams();

        if(userId) {
            params = params.append("userId", userId);
        }

        return this.http
            .post<any>(`${this.apiReport}/createBaoCao`, item, {
                headers: this.sharedHeaders,
                params: params
            });
    }
}
