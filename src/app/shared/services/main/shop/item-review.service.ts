import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BaseService } from '../../base/base.service';

@Injectable({
    providedIn: 'root'
})
export class ItemReviewService extends BaseService {
    private sharedHeaders = new HttpHeaders();
    constructor(private http: HttpClient) {
        super();
        this.sharedHeaders = this.sharedHeaders.set(
            "Content-Type",
            "application/json"
        );
    }

    getShopByShopName(shopName: any) {
        let params = new HttpParams();

        if(shopName) {
            params = params.append("shopName", shopName);
        }

        return this.http
            .get<any>(`${this.apiShop}/getShopByShopName`, {
                headers: this.sharedHeaders,
                params: params
            });
    }

    getSanPhamWithHinhAnhByName(tenShop: any, tenSanPham: any) {
        let params = new HttpParams();

        if(tenShop) {
            params = params.append("tenShop", tenShop);
        }

        if(tenSanPham) {
            params = params.append("tenSanPham", tenSanPham);
        }

        return this.http
            .get<any>(`${this.apiItem}/getSanPhamWithHinhAnhByName`, {
                headers: this.sharedHeaders,
                params: params
            });
    }

    getAllGioHangDauGia(userId: any) {
        let params = new HttpParams();

        if(userId) {
            params = params.append("userId", userId);
        }

        return this.http
            .get<any>(`${this.apiCart}/getAllGioHangDauGia`, {
                headers: this.sharedHeaders,
                params: params
            });
    }

    getAllGioHang(userId: any) {
        let params = new HttpParams();

        if(userId) {
            params = params.append("userId", userId);
        }

        return this.http
            .get<any>(`${this.apiCart}/getAllGioHang`, {
                headers: this.sharedHeaders,
                params: params
            });
    }

    createGioHang(item: any) {
        return this.http
            .post<any>(`${this.apiCart}/createGioHang`, item, {
                headers: this.sharedHeaders
            });
    }

    getAllDanhGia(item: any) {
        let params = new HttpParams();

        if(item) {
            if(item.sanPhamId) {
                params = params.append("sanPhamId", item.sanPhamId);
            }
            if(item.pageIndex) {
                params = params.append("pageIndex", item.pageIndex);
            }
            if(item.pageSize) {
                params = params.append("pageSize", item.pageSize);
            }
            if(item.oneStar) {
                params = params.append("danhGia", 1);
            }
            if(item.twoStar) {
                params = params.append("danhGia", 2);
            }
            if(item.threeStar) {
                params = params.append("danhGia", 3);
            }
            if(item.fourStar) {
                params = params.append("danhGia", 4);
            }
            if(item.fiveStar) {
                params = params.append("danhGia", 5);
            }
            if(item.order) {
                params = params.append("order", item.order);
            }
            if(item.orderBy) {
                params = params.append("orderBy", item.orderBy);
            }
        }

        return this.http
            .get<any>(`${this.apiItemRate}/getAllDanhGia`, {
                headers: this.sharedHeaders,
                params: params
            });
    }

    createDanhGia(item: any) {
        return this.http
            .post<any>(`${this.apiItemRate}/createDanhGia`, item, {
                headers: this.sharedHeaders
            });
    }

    getDanhGiaHuuIchByUserId(userId: any) {
        let params = new HttpParams();

        if(userId) {
            params = params.append("userId", userId);
        }

        return this.http
            .get<any>(`${this.apiItemRate}/getDanhGiaHuuIchByUserId`, {
                headers: this.sharedHeaders,
                params: params
            });
    }

    createDanhGiaHuuIch(item: any) {
        return this.http
            .post<any>(`${this.apiItemRate}/createDanhGiaHuuIch`, item, {
                headers: this.sharedHeaders
            });
    }

    deleteDanhGiaHuuIch(item: any) {
        let params = new HttpParams();

        if(item) {
            if(item.id) {
                params = params.append("id", item.id);
            }
            if(item.userId) {
                params = params.append("userId", item.userId);
            }
        }

        return this.http
            .delete<any>(`${this.apiItemRate}/deleteDanhGiaHuuIch`, {
                headers: this.sharedHeaders,
                params: params
            });
    }

    createBaoCao(userId: any, item: any) {
        let params = new HttpParams();

        if(userId) {
            params = params.append("userId", userId);
        }

        return this.http
            .post<any>(`${this.apiReport}/createBaoCao`, item, {
                headers: this.sharedHeaders,
                params: params
            });
    }
}
