import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BaseService } from '../../base/base.service';

@Injectable({
    providedIn: 'root'
})
export class HomeService extends BaseService {
    private sharedHeaders = new HttpHeaders();
    constructor(private http: HttpClient) {
        super();
        this.sharedHeaders = this.sharedHeaders.set(
            "Content-Type",
            "application/json"
        );
    }

    getAllSanPham(item: any) {
        let params = new HttpParams();

        if(item) {
            if(item.pageIndex) {
                params = params.append("pageIndex", item.pageIndex);
            }
            if(item.pageSize) {
                params = params.append("pageSize", item.pageSize);
            }
            if(item.searchString) {
                params = params.append("searchString", item.searchString);
            }
            if(item.order) {
                params = params.append("order", item.order);
            }
            if(item.orderBy) {
                params = params.append("orderBy", item.orderBy);
            }
            if(item.loaiSanPhamId.length) {
                if(item.loaiSanPhamId[0]) {
                    for(let id of item.loaiSanPhamId) {
                        params = params.append("loaiSanPhamId", id);
                    }
                }
            }
            if(item.conHang != null) {
                params = params.append("conHang", item.conHang);
            }
        }

        return this.http
            .get<any>(`${this.apiItem}/getAllSanPham`, {
                headers: this.sharedHeaders,
                params: params
            });
    }

    getAllLoaiSanPham(item: any) {
        let params = new HttpParams();

        if(item) {
            if(item.pageIndex) {
                params = params.append("pageIndex", item.pageIndex);
            }
            if(item.pageSize) {
                params = params.append("pageSize", item.pageSize);
            }
            if(item.searchString) {
                params = params.append("searchString", item.searchString);
            }
            if(item.order) {
                params = params.append("order", item.order);
            }
            if(item.orderBy) {
                params = params.append("orderBy", item.orderBy);
            }
        }

        return this.http
            .get<any>(`${this.apiCategory}/getAllLoaiSanPham`, {
                headers: this.sharedHeaders,
                params: params
            });
    }

    getAllSanPhamDauGia(item: any) {
        let params = new HttpParams();

        if(item) {
            if(item.pageIndex) {
                params = params.append("pageIndex", item.pageIndex);
            }
            if(item.pageSize) {
                params = params.append("pageSize", item.pageSize);
            }
            if(item.searchString) {
                params = params.append("searchString", item.searchString);
            }
            if(item.order) {
                params = params.append("order", item.order);
            }
            if(item.orderBy) {
                params = params.append("orderBy", item.orderBy);
            }
            if(item.loaiSanPhamId.length) {
                if(item.loaiSanPhamId[0]) {
                    for(let id of item.loaiSanPhamId) {
                        params = params.append("loaiSanPhamId", id);
                    }
                }
            }
            if(item.tinhTrangDauGiaId.length) {
                if(item.tinhTrangDauGiaId[0]) {
                    for(let id of item.tinhTrangDauGiaId) {
                        params = params.append("tinhTrangDauGiaId", id);
                    }
                }
            }
        }

        return this.http
            .get<any>(`${this.apiAuction}/getAllSanPhamDauGia`, {
                headers: this.sharedHeaders,
                params: params
            });
    }
}
