import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BaseService } from '../../base/base.service';

@Injectable({
    providedIn: 'root'
})
export class CreateDeliverService extends BaseService {
    private sharedHeaders = new HttpHeaders();
    constructor(private http: HttpClient) {
        super();
        this.sharedHeaders = this.sharedHeaders.set(
            "Content-Type",
            "application/json"
        );
    }

    getDonViVanChuyenByUserId(userId: any) {
        let params = new HttpParams();

        if(userId) {
            params = params.append("userId", userId);
        }

        return this.http
            .get<any>(`${this.apiDeliver}/getDonViVanChuyenByUserId`, {
                headers: this.sharedHeaders,
                params: params
            });
    }

    getDonViVanChuyenByName(tenDonVi: any) {
        let params = new HttpParams();

        if(tenDonVi) {
            params = params.append("tenDonVi", tenDonVi);
        }

        return this.http
            .get<any>(`${this.apiDeliver}/getDonViVanChuyenByName`, {
                headers: this.sharedHeaders,
                params: params
            });
    }

    createDonViVanChuyen(item: any) {
        return this.http
            .post<any>(`${this.apiDeliver}/createDonViVanChuyen`, item, {
                headers: this.sharedHeaders
            });
    }

    getUserInfoByUserId(userId: any) {
        let params = new HttpParams();

        if(userId) {
            params = params.append("userId", userId);
        }

        return this.http
            .get<any>(`${this.apiUserInfo}/getUserInfoByUserId`, {
                headers: this.sharedHeaders,
                params: params
            });
    }
}
