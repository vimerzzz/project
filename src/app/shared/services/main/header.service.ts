import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BaseService } from '../base/base.service';

@Injectable({
    providedIn: 'root'
})
export class HeaderService extends BaseService {
    private sharedHeaders = new HttpHeaders();
    constructor(private http: HttpClient) {
        super();
        this.sharedHeaders = this.sharedHeaders.set(
            "Content-Type",
            "application/json"
        );
    }

    getUserById(id: any) {
        let params = new HttpParams();

        if(id) {
            params = params.append("id", id);
        }

        return this.http
            .get<any>(`${this.apiUser}/getUserById`, {
                headers: this.sharedHeaders,
                params: params
            });
    }

    login(item: any) {
        return this.http
            .post<any>(`${this.apiLog}/login`, item, {
                headers: this.sharedHeaders
            });
    }

    createUser(item: any) {
        return this.http
            .post<any>(`${this.apiUser}/createUser`, item, {
                headers: this.sharedHeaders
            });
    }

    getUserInfoByUserId(userId: any) {
        let params = new HttpParams();

        if(userId) {
            params = params.append("userId", userId);
        }

        return this.http
            .get<any>(`${this.apiUserInfo}/getUserInfoByUserId`, {
                headers: this.sharedHeaders,
                params: params
            });
    }

    getAllThongBao(item: any) {
        let params = new HttpParams();

        if(item) {
            params = params.append("pageSize", item.pageSize);
            params = params.append("pageIndex", item.pageIndex);
            params = params.append("daXem", item.daXem);
            if(item.userId) {
                params = params.append("userId", item.userId);
            }
        }

        return this.http
            .get<any>(`${this.apiNotification}/getAllThongBao`, {
                headers: this.sharedHeaders,
                params: params
            });
    }

    getAllGioHang(userId: any) {
        let params = new HttpParams();

        if(userId) {
            params = params.append("userId", userId);
        }

        return this.http
            .get<any>(`${this.apiCart}/getAllGioHang`, {
                headers: this.sharedHeaders,
                params: params
            });
    }

    getAllGioHangDauGia(userId: any) {
        let params = new HttpParams();

        if(userId) {
            params = params.append("userId", userId);
        }

        return this.http
            .get<any>(`${this.apiCart}/getAllGioHangDauGia`, {
                headers: this.sharedHeaders,
                params: params
            });
    }

    getAllBaoCao(item: any) {
        let params = new HttpParams();

        if(item) {
            params = params.append("pageSize", item.pageSize);
            params = params.append("pageIndex", item.pageIndex);
            params = params.append("daXem", item.daXem);
        }

        return this.http
            .get<any>(`${this.apiReport}/getAllBaoCao`, {
                headers: this.sharedHeaders,
                params: params
            });
    }
}
