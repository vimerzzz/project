import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BaseService } from '../../base/base.service';

@Injectable({
    providedIn: 'root'
})
export class CartService extends BaseService {
    private sharedHeaders = new HttpHeaders();
    constructor(private http: HttpClient) {
        super();
        this.sharedHeaders = this.sharedHeaders.set(
            "Content-Type",
            "application/json"
        );
    }

    getAllGioHang(userId: any) {
        let params = new HttpParams();

        if(userId) {
            params = params.append("userId", userId);
        }

        return this.http
            .get<any>(`${this.apiCart}/getAllGioHang`, {
                headers: this.sharedHeaders,
                params: params
            });
    }

    getAllGioHangDauGia(userId: any) {
        let params = new HttpParams();

        if(userId) {
            params = params.append("userId", userId);
        }

        return this.http
            .get<any>(`${this.apiCart}/getAllGioHangDauGia`, {
                headers: this.sharedHeaders,
                params: params
            });
    }

    updateGioHang(item: any) {
        return this.http
            .put<any>(`${this.apiCart}/updateGioHang`, item, {
                headers: this.sharedHeaders
            });
    }

    deleteGioHang(item: any) {
        let params = new HttpParams();

        if(item) {
            if(item.userId) {
                params = params.append("userId", item.userId);
            }
            if(item.id) {
                params = params.append("id", item.id);
            }
        }

        return this.http
            .delete<any>(`${this.apiCart}/deleteGioHang`, {
                headers: this.sharedHeaders,
                params: params
            });
    }

    // getAllMaGiamGia(item: any) {
    //     let params = new HttpParams();

    //     if(item) {
    //         if(item.loaiSanPhamId) {
    //             params = params.append("loaiSanPhamId", item.loaiSanPhamId);
    //         }
    //         params = params.append("batDau", item.batDau);
    //         params = params.append("ketThuc", item.ketThuc);
    //     }

    //     return this.http
    //         .get<any>(`${this.apiCoupon}/getAllMaGiamGia`, {
    //             headers: this.sharedHeaders,
    //             params: params
    //         });
    // }

    // getAllMaGiamGiaDaDung(userId: any) {
    //     let params = new HttpParams();

    //     if(userId) {
    //         params = params.append("userId", userId);
    //     }

    //     return this.http
    //         .get<any>(`${this.apiUsedCoupon}/getAllMaGiamGiaDaDung`, {
    //             headers: this.sharedHeaders,
    //             params: params
    //         });
    // }

    getUserInfoByUserId(userId: any) {
        let params = new HttpParams();

        if(userId) {
            params = params.append("userId", userId);
        }

        return this.http
            .get<any>(`${this.apiUserInfo}/getUserInfoByUserId`, {
                headers: this.sharedHeaders,
                params: params
            });
    }

    getAllDiaChi(userId: any) {
        let params = new HttpParams();

        if(userId) {
            params = params.append("userId", userId);
        }

        return this.http
            .get<any>(`${this.apiAddress}/getAllDiaChi`, {
                headers: this.sharedHeaders,
                params: params
            });
    }

    // getAllDonViVanChuyen() {
    //     return this.http
    //         .get<any>(`${this.apiDeliver}/getAllDonViVanChuyen`, {
    //             headers: this.sharedHeaders
    //         });
    // }

    createDiaChi(item: any) {
        return this.http
            .post<any>(`${this.apiAddress}/createDiaChi`, item, {
                headers: this.sharedHeaders
            });
    }

    updateUserInfo(item: any) {
        return this.http
            .put<any>(`${this.apiUserInfo}/updateUserInfo`, item, {
                headers: this.sharedHeaders
            });
    }

    // createMaGiamGiaDaDung(item: any) {
    //     return this.http
    //         .post<any>(`${this.apiUsedCoupon}/createMaGiamGiaDaDung`, item, {
    //             headers: this.sharedHeaders
    //         });
    // }

    createDonHang(item: any) {
        return this.http
            .post<any>(`${this.apiOrder}/createDonHang`, item, {
                headers: this.sharedHeaders
            });
    }
}
