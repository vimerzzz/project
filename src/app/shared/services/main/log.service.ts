import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BaseService } from '../base/base.service';

@Injectable({
    providedIn: 'root'
})
export class LogService extends BaseService {
    private sharedHeaders = new HttpHeaders();
    constructor(private http: HttpClient) {
        super();
        this.sharedHeaders = this.sharedHeaders.set(
            "Content-Type",
            "application/json"
        );
    }

    checkExistedUserName(item: any) {
        return this.http
            .post<any>(`${this.apiUser}/checkExistedUserName`, item, {
                headers: this.sharedHeaders
            });
    }

    checkExistedEmail(item: any) {
        return this.http
            .post<any>(`${this.apiUser}/checkExistedEmail`, item, {
                headers: this.sharedHeaders
            });
    }

    checkPassword(item: any) {
        return this.http
            .post<any>(`${this.apiUser}/checkPassword`, item, {
                headers: this.sharedHeaders
            });
    }

    getRecoverPassword(item: any) {
        let params = new HttpParams();

        if(item) {
            if(item.email) {
                params = params.append("email", item.email);
            }
            if(item.urlClient) {
                params = params.append("urlClient", item.urlClient);
            }
        }

        return this.http
            .get<any>(`${this.apiVerify}/getRecoverPassword`, {
                headers: this.sharedHeaders,
                params: params
            });
    }
}
