import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BaseService } from '../../base/base.service';

@Injectable({
    providedIn: 'root'
})
export class CategoryManagementService extends BaseService {
    private sharedHeaders = new HttpHeaders();
    constructor(private http: HttpClient) {
        super();
        this.sharedHeaders = this.sharedHeaders.set(
            "Content-Type",
            "application/json"
        );
    }

    getAllLoaiSanPham(item: any) {
        let params = new HttpParams();

        if(item) {
            if(item.pageIndex) {
                params = params.append("pageIndex", item.pageIndex);
            }
            if(item.pageSize) {
                params = params.append("pageSize", item.pageSize);
            }
            if(item.searchString) {
                params = params.append("searchString", item.searchString);
            }
            if(item.order) {
                params = params.append("order", item.order);
            }
            if(item.orderBy) {
                params = params.append("orderBy", item.orderBy);
            }
        }

        return this.http
            .get<any>(`${this.apiCategory}/getAllLoaiSanPham`, {
                headers: this.sharedHeaders,
                params: params
            });
    }

    createLoaiSanPham(item: any) {
        return this.http
            .post<any>(`${this.apiCategory}/createLoaiSanPham`, item, {
                headers: this.sharedHeaders
            });
    }

    updateLoaiSanPham(item: any) {
        return this.http
            .put<any>(`${this.apiCategory}/updateLoaiSanPham`, item, {
                headers: this.sharedHeaders
            });
    }

    deleteLoaiSanPham(id: any) {
        let params = new HttpParams();

        if(id) {
            params = params.append("id", id);
        }

        return this.http
            .delete<any>(`${this.apiCategory}/deleteLoaiSanPham`, {
                headers: this.sharedHeaders,
                params: params
            });
    }
}
