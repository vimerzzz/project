import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BaseService } from '../../base/base.service';

@Injectable({
    providedIn: 'root'
})
export class AttributeCategoryService extends BaseService {
    private sharedHeaders = new HttpHeaders();
    constructor(private http: HttpClient) {
        super();
        this.sharedHeaders = this.sharedHeaders.set(
            "Content-Type",
            "application/json"
        );
    }

    getAllThuocTinh(item: any) {
        let params = new HttpParams();

        if(item) {
            params = params.append("loaiSanPhamId", item);
        }

        return this.http
            .get<any>(`${this.apiAttribute}/getAllThuocTinh`, {
                headers: this.sharedHeaders,
                params: params
            });
    }

    createThuocTinh(item: any) {
        return this.http
            .post<any>(`${this.apiAttribute}/createThuocTinh`, item, {
                headers: this.sharedHeaders
            });
    }

    updateThuocTinh(item: any) {
        return this.http
            .put<any>(`${this.apiAttribute}/updateThuocTinh`, item, {
                headers: this.sharedHeaders
            });
    }

    deleteThuocTinh(id: any) {
        let params = new HttpParams();

        if(id.length) {
            for(let item of id) {
                params = params.append("id", item);
            }
        }
        return this.http
            .delete<any>(`${this.apiAttribute}/deleteThuocTinh`, {
                headers: this.sharedHeaders,
                params: params
            });
    }
}
