import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BaseService } from '../../base/base.service';

@Injectable({
    providedIn: 'root'
})
export class NotificationManagementService extends BaseService {
    private sharedHeaders = new HttpHeaders();
    constructor(private http: HttpClient) {
        super();
        this.sharedHeaders = this.sharedHeaders.set(
            "Content-Type",
            "application/json"
        );
    }

    getAllThongBaoCoLichHen(item: any) {
        let params = new HttpParams();

        if(item) {
            if(item.pageIndex) {
                params = params.append("pageIndex", item.pageIndex);
            }
            if(item.pageSize) {
                params = params.append("pageSize", item.pageSize);
            }
            if(item.searchString) {
                params = params.append("searchString", item.searchString);
            }
            if(item.order) {
                params = params.append("order", item.order);
            }
            if(item.orderBy) {
                params = params.append("orderBy", item.orderBy);
            }
            if(item.thoiGianHenBatDau) {
                params = params.append("thoiGianHenBatDau", item.thoiGianHenBatDau);
            }
            if(item.thoiGianHenKetThuc) {
                params = params.append("thoiGianHenKetThuc", item.thoiGianHenKetThuc);
            }
        }

        return this.http
            .get<any>(`${this.apiNotification}/getAllThongBaoCoLichHen`, {
                headers: this.sharedHeaders,
                params: params
            });
    }

    createThongBao(item: any) {
        return this.http
            .post<any>(`${this.apiNotification}/createThongBao`, item, {
                headers: this.sharedHeaders
            });
    }

    updateThongBao(item: any) {
        return this.http
            .put<any>(`${this.apiNotification}/updateThongBao`, item, {
                headers: this.sharedHeaders
            });
    }

    deleteThongBao(id: any) {
        let params = new HttpParams();

        if(id.length) {
            for(let item of id) {
                params = params.append("id", item);
            }
        }
        return this.http
            .delete<any>(`${this.apiNotification}/deleteThongBao`, {
                headers: this.sharedHeaders,
                params: params
            });
    }

    getAllUser(item: any) {
        let params = new HttpParams();

        if(item) {
            if(item.order) {
                params = params.append("order", item.order);
            }
            if(item.orderBy) {
                params = params.append("orderBy", item.orderBy);
            }
            params = params.append("hoatDong", item.hoatDong);
        }

        return this.http
            .get<any>(`${this.apiUser}/getAllUser`, {
                headers: this.sharedHeaders,
                params: params
            });
    }
}
