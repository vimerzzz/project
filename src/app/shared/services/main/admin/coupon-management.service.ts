import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BaseService } from '../../base/base.service';

@Injectable({
    providedIn: 'root'
})
export class CouponManagementService extends BaseService {
    private sharedHeaders = new HttpHeaders();
    constructor(private http: HttpClient) {
        super();
        this.sharedHeaders = this.sharedHeaders.set(
            "Content-Type",
            "application/json"
        );
    }

    getAllMaGiamGia(item: any) {
        let params = new HttpParams();

        if(item) {
            if(item.pageIndex) {
                params = params.append("pageIndex", item.pageIndex);
            }
            if(item.pageSize) {
                params = params.append("pageSize", item.pageSize);
            }
            if(item.searchString) {
                params = params.append("searchString", item.searchString);
            }
            if(item.order) {
                params = params.append("order", item.order);
            }
            if(item.orderBy) {
                params = params.append("orderBy", item.orderBy);
            }
            if(item.trangThai == 1) {
                params = params.append("batDau", false);
                params = params.append("ketThuc", false);
            }
            else if(item.trangThai == 2) {
                params = params.append("batDau", true);
                params = params.append("ketThuc", false);
            }
            else if(item.trangThai == 3) {
                params = params.append("batDau", true);
                params = params.append("ketThuc", true);
            }
            if(item.loaiSanPhamId.length) {
                if(item.loaiSanPhamId[0]) {
                    for(let id of item.loaiSanPhamId) {
                        params = params.append("loaiSanPhamId", id);
                    }
                }
            }
        }

        return this.http
            .get<any>(`${this.apiCoupon}/getAllMaGiamGia`, {
                headers: this.sharedHeaders,
                params: params
            });
    }

    createMaGiamGia(item: any) {
        return this.http
            .post<any>(`${this.apiCoupon}/createMaGiamGia`, item, {
                headers: this.sharedHeaders
            });
    }

    updateMaGiamGia(item: any) {
        return this.http
            .put<any>(`${this.apiCoupon}/updateMaGiamGia`, item, {
                headers: this.sharedHeaders
            });
    }

    deleteMaGiamGia(id: any) {
        let params = new HttpParams();

        if(id) {
            params = params.append("id", id);
        }

        return this.http
            .delete<any>(`${this.apiCoupon}/deleteMaGiamGia`, {
                headers: this.sharedHeaders,
                params: params
            });
    }
}
