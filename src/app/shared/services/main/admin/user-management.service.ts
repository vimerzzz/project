import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BaseService } from '../../base/base.service';

@Injectable({
    providedIn: 'root'
})
export class UserManagementService extends BaseService {
    private sharedHeaders = new HttpHeaders();
    constructor(private http: HttpClient) {
        super();
        this.sharedHeaders = this.sharedHeaders.set(
            "Content-Type",
            "application/json"
        );
    }

    getAllUser(item: any) {
        let params = new HttpParams();

        if(item) {
            if(item.pageIndex) {
                params = params.append("pageIndex", item.pageIndex);
            }
            if(item.pageSize) {
                params = params.append("pageSize", item.pageSize);
            }
            if(item.searchString) {
                params = params.append("searchString", item.searchString);
            }
            if(item.order) {
                params = params.append("order", item.order);
            }
            if(item.orderBy) {
                params = params.append("orderBy", item.orderBy);
            }
            if(item.roleId) {
                params = params.append("roleId", item.roleId);
            }
            if(item.hoatDong) {
                params = params.append("hoatDong", item.hoatDong);
            }
        }
        return this.http
            .get<any>(`${this.apiUser}/getAllUser`, {
                headers: this.sharedHeaders,
                params: params
            });
    }

    getAllRole() {
        return this.http
            .get<any>(`${this.apiRole}/getAllRole`, {
                headers: this.sharedHeaders
            });
    }

    setStatusAndRoleForUser(item: any) {
        return this.http
            .put<any>(`${this.apiUser}/setStatusAndRoleForUser`, item, {
                headers: this.sharedHeaders
            });
    }
}
