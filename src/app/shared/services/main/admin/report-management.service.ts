import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BaseService } from '../../base/base.service';

@Injectable({
    providedIn: 'root',
})
export class ReportManagementService extends BaseService {
    private sharedHeaders = new HttpHeaders();
    constructor(private http: HttpClient) {
        super();
        this.sharedHeaders = this.sharedHeaders.set(
            "Content-Type",
            "application/json"
        );
    }

    getAllBaoCao(item: any) {
        let params = new HttpParams();

        if(item) {
            if(item.pageIndex) {
                params = params.append("pageIndex", item.pageIndex);
            }
            if(item.pageSize) {
                params = params.append("pageSize", item.pageSize);
            }
            if(item.searchString) {
                params = params.append("searchString", item.searchString);
            }
            if(item.order) {
                params = params.append("order", item.order);
            }
            if(item.orderBy) {
                params = params.append("orderBy", item.orderBy);
            }
            if(item.daXem != null) {
                params = params.append("daXem", item.daXem);
            }
            if(item.userId) {
                params = params.append("userId", item.userId);
            }
            if(item.shopId) {
                params = params.append("shopId", item.shopId);
            }
            if(item.nguoiBaoCaoId) {
                params = params.append("nguoiBaoCaoId", item.nguoiBaoCaoId);
            }
        }
        return this.http
            .get<any>(`${this.apiReport}/getAllBaoCao`, {
                headers: this.sharedHeaders,
                params: params
            });
    }

    getAllNguoiBaoCao() {
        return this.http
            .get<any>(`${this.apiReport}/getAllNguoiBaoCao`, {
                headers: this.sharedHeaders
            });
    }

    getAllNguoiBiBaoCao() {
        return this.http
            .get<any>(`${this.apiReport}/getAllNguoiBiBaoCao`, {
                headers: this.sharedHeaders
            });
    }

    getAllShopBiBaoCao() {
        return this.http
            .get<any>(`${this.apiReport}/getAllShopBiBaoCao`, {
                headers: this.sharedHeaders
            });
    }

    updateBaoCao(item: any) {
        return this.http
            .put<any>(`${this.apiReport}/updateBaoCao`, item, {
                headers: this.sharedHeaders
            });
    }

    deleteBaoCao(id: any) {
        let params = new HttpParams();

        if(id) {
            params = params.append("id", id);
        }
        return this.http
            .delete<any>(`${this.apiReport}/deleteBaoCao`, {
                headers: this.sharedHeaders,
                params: params
            });
    }
}
