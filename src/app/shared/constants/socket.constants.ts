import { environment } from "src/environments/environment";

export class SocketConstants {
    public static thongBaoHub = environment.apiUrl + "/hub/thongBao";
    public static chatHub = environment.apiUrl + "/hub/chat";
    public static dauGiaHub = environment.apiUrl + "/hub/sanPhamDauGia";
    public static baoCaoHub = environment.apiUrl + "/hub/baoCao";
}
