export class SendMailConstants {
    public static urlVerifyEmail = "http://localhost:4201/verification/verify-email";
    public static urlForgotPassword = "http://localhost:4201/verification/forgot-password";
}
