import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
    selector: 'app-report',
    templateUrl: './report.component.html',
    styleUrls: ['./report.component.scss'],
})
export class ReportComponent implements OnInit {
    title: any = "Báo cáo ";
    formControl: FormGroup;

    constructor(private formBuilder: FormBuilder, private dialogRef: MatDialogRef<ReportComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any) { }

    ngOnInit(): void {
        this.formControl = this.formBuilder.group({
            lyDoBaoCao: ["", [Validators.required]]
        });
        if(this.data && this.data.name) {
            this.title += this.data.name;
        }
    }

    get f() {
        return this.formControl.controls;
    }

    save() {
        this.formControl.markAllAsTouched();
        if (this.formControl.invalid) {
            return;
        }
        else {
            this.dialogRef.close(true);
        }
    }
}
