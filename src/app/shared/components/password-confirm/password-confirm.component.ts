import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MessageConstants } from '../../constants/message.constants';
import { LogService } from '../../services/main/log.service';

@Component({
    selector: 'app-password-confirm',
    templateUrl: './password-confirm.component.html',
    styleUrls: ['./password-confirm.component.scss']
})
export class PasswordConfirmComponent implements OnInit {
    formControl: FormGroup;
    error: any;
    spinner: any = false;
    passwordType: "password" | "text";

    constructor(private formBuilder: FormBuilder, private dialogRef: MatDialogRef<PasswordConfirmComponent>, private logService: LogService,
        @Inject(MAT_DIALOG_DATA) public data: any) { }

    ngOnInit(): void {
        this.formControl = this.formBuilder.group({
            password: ["", [Validators.required, Validators.pattern("^[a-zA-Z0-9-_]+$"), Validators.minLength(5)]]
        });
        this.passwordType = "password";
    }

    get f() {
        return this.formControl.controls;
    }

    checkPassword() {
        this.formControl.markAllAsTouched();
        if(this.formControl.invalid) {
            return;
        }
        else {
            this.spinner = true;
            this.logService.checkPassword(this.data.item).subscribe({
                next: (res: any) => {
                    if(res.statusCode == 200) {
                        this.close();
                    }
                    else if(res.statusCode == 401) {
                        this.error = MessageConstants.WRONG_PASSWORD;
                        this.spinner = false;
                    }
                    else {
                        this.error = MessageConstants.SYSTEM_ERROR;
                        this.spinner = false;
                    }
                },
                error: (res: any) => {
                    this.error = MessageConstants.SYSTEM_ERROR;
                    this.spinner = false;
                }
            });
        }
    }

    close() {
        this.error = null;
        this.spinner = false;
        this.dialogRef.close(true);
    }

    passwordToggle() {
        if(this.passwordType == "password") {
            this.passwordType = "text";
        }
        else if(this.passwordType == "text") {
            this.passwordType = "password";
        }
    }
}
