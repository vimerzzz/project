import { NgModule } from '@angular/core';
import { MaterialModule } from 'src/app/material.module';
import { ConfirmDeleteComponent } from './confirm-delete/confirm-delete.component';
import { ImageCropComponent } from './image-crop/image-crop.component';
import { PasswordConfirmComponent } from './password-confirm/password-confirm.component';
import { PopUpComponent } from './pop-up/pop-up.component';
import { ReportComponent } from './report/report.component';
import { ViewImageComponent } from './view-image/view-image.component';

@NgModule({
    declarations: [
        PopUpComponent,
        ConfirmDeleteComponent,
        ImageCropComponent,
        PasswordConfirmComponent,
        ViewImageComponent,
        ReportComponent
    ],
    imports: [
        MaterialModule
    ]
})
export class SharedComponentsModule { }
