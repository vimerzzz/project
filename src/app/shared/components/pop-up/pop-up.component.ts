import { Component, Inject, OnInit } from '@angular/core';
import { MatSnackBar, MAT_SNACK_BAR_DATA } from '@angular/material/snack-bar';

@Component({
    selector: 'app-pop-up',
    templateUrl: './pop-up.component.html',
    styleUrls: ['./pop-up.component.scss']
})
export class PopUpComponent implements OnInit {

    constructor(
        public snackBar: MatSnackBar,
        @Inject(MAT_SNACK_BAR_DATA) public data: Message) {
    }
    ngOnInit(): void {
        if (this.data.type === 'error') { this.data.title = 'Có lỗi!'; }
        if (this.data.type === 'warning') { this.data.title = 'Cảnh báo!'; }
        if (this.data.type === 'info') { this.data.title = 'Thông báo!'; }
        if (this.data.type === 'done') { this.data.title = 'Thành công!'; }
    }
}
export interface Message {
    title?: string;
    message: string;
    type: string;
}
