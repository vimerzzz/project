import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { ProgressBarService } from '../../services/base/progress-bar.service';

@Component({
    selector: 'app-progress-bar',
    templateUrl: './progress-bar.component.html',
    styleUrls: ['./progress-bar.component.scss']
})
export class ProgressBarComponent implements OnInit {
    visible: boolean = false;

    constructor(private progressBarService: ProgressBarService) { }

    ngOnInit(): void {
        this.setVisible();
    }

    setVisible() {
        this.progressBarService.visible
            .subscribe((visible) => {
                this.visible = visible;
            });
    }
}
