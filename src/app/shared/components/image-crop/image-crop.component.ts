import { Component, ElementRef, Inject, OnInit, ViewChild, } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';
import { base64ToFile, CropperPosition, ImageCroppedEvent, ImageTransform, } from 'ngx-image-cropper';
import { MessageConstants } from '../../constants/message.constants';
import { mType, PopUpService } from '../../services/base/pop-up.service';

@Component({
    selector: 'app-image-crop',
    templateUrl: './image-crop.component.html',
    styleUrls: ['./image-crop.component.scss'],
})
export class ImageCropComponent implements OnInit {
    croppedImage: any = [];
    rotation: any = [];
    scale: any = [];
    flipH: any = [];
    flipV: any = [];
    translateH: any = [];
    translateV: any = [];
    showCropper = false;
    transform: ImageTransform = {};
    index: any = 0;
    check: any = [];
    x1: any = [];
    x2: any = [];
    y1: any = [];
    y2: any = [];
    cropper: CropperPosition = {
        x1: 0,
        x2: 0,
        y1: 0,
        y2: 0,
    };
    init: any = false;
    disabled: any = true;
    hideResizeSquares: any = [];
    disabledCropper: any = [];
    alter: any = [];
    pos1: any = 0;
    pos2: any = 0;
    pos3: any = 0;
    pos4: any = 0;
    @ViewChild('input') input: ElementRef;
    @ViewChild('toggle') toggle: MatSlideToggleChange;

    constructor(
        private dialogRef: MatDialogRef<ImageCropComponent>,
        private popUpService: PopUpService,
        @Inject(MAT_DIALOG_DATA) public data: any
    ) {}

    ngOnInit(): void {
        for (let i = 0; i < this.data.imageCount; i++) {
            this.check.push(0);
            this.x1.push(0);
            this.x2.push(0);
            this.y1.push(0);
            this.y2.push(0);
            this.rotation.push(0);
            this.scale.push(1);
            this.flipH.push(false);
            this.flipV.push(false);
            this.translateH.push(0);
            this.translateV.push(0);
            this.hideResizeSquares.push(false);
            this.disabledCropper.push(false);
            this.alter.push(false);
        }
        this.check[0] = 1;
        this.transform = {
            flipH: this.flipH[0],
            flipV: this.flipV[0],
            rotate: this.rotation[0],
            scale: this.scale[0],
            translateH: this.translateH[0],
            translateV: this.translateV[0]
        };
        if (this.data.imageCount == 1) {
            this.disabled = false;
        }
    }

    imageCropped(event: ImageCroppedEvent) {
        this.croppedImage[this.index] = event.base64;
        if (this.init) {
            this.x1[this.index] = event.cropperPosition.x1;
            this.x2[this.index] = event.cropperPosition.x2;
            this.y1[this.index] = event.cropperPosition.y1;
            this.y2[this.index] = event.cropperPosition.y2;
        }
    }

    imageLoaded() {
        this.showCropper = true;
    }

    loadImageFailed() {
        this.popUpService.open(MessageConstants.IMAGE_LOAD_FAILED, mType.error);
    }

    cropperReady() {
        if (
            this.x1[this.index] != 0 ||
            this.x2[this.index] != 0 ||
            this.y1[this.index] != 0 ||
            this.y2[this.index] != 0
        ) {
            this.cropper = {
                x1: this.x1[this.index],
                x2: this.x2[this.index],
                y1: this.y1[this.index],
                y2: this.y2[this.index],
            };
        }
        if (!this.init) {
            this.x1[this.index] = this.cropper.x1;
            this.x2[this.index] = this.cropper.x2;
            this.y1[this.index] = this.cropper.y1;
            this.y2[this.index] = this.cropper.y2;
        }
        this.init = true;
    }

    rotateLeft() {
        this.rotation[this.index] =
            this.rotation[this.index] >= 90
                ? this.rotation[this.index] - 90
                : this.rotation[this.index] - 90 + 360;
        this.transform = {
            ...this.transform,
            rotate: this.rotation[this.index],
        };
        this.flipAfterRotate();
    }

    rotateRight() {
        this.rotation[this.index] =
            this.rotation[this.index] < 270
                ? this.rotation[this.index] + 90
                : this.rotation[this.index] + 90 - 360;
        this.transform = {
            ...this.transform,
            rotate: this.rotation[this.index],
        };
        this.flipAfterRotate();
    }

    flipAfterRotate() {
        let temp = this.flipV[this.index];
        this.flipV[this.index] = this.flipH[this.index];
        this.flipH[this.index] = temp;
        this.transform = {
            ...this.transform,
            flipH: this.flipH[this.index],
            flipV: this.flipV[this.index],
        };
        this.alter[this.index] = !this.alter[this.index];
    }

    flipHorizontal() {
        this.flipH[this.index] = !this.flipH[this.index];
        this.transform = {
            ...this.transform,
            flipH: this.flipH[this.index],
        };
    }

    flipVertical() {
        this.flipV[this.index] = !this.flipV[this.index];
        this.transform = {
            ...this.transform,
            flipV: this.flipV[this.index],
        };
    }

    resetImage() {
        this.scale[this.index] = 1;
        this.rotation[this.index] = 0;
        this.flipH[this.index] = false;
        this.flipV[this.index] = false;
        this.translateH[this.index] = 0;
        this.translateV[this.index] = 0;
        this.alter[this.index] = false;
        this.transform = {
            scale: this.scale[this.index],
            flipH: this.flipH[this.index],
            flipV: this.flipV[this.index],
            rotate: 0,
            translateH: this.translateH[this.index],
            translateV: this.translateV[this.index]
        };
    }

    zoomOut() {
        this.scale[this.index] -= 0.1;
        this.transform = {
            ...this.transform,
            scale: this.scale[this.index],
        };
    }

    zoomIn() {
        this.scale[this.index] += 0.1;
        this.transform = {
            ...this.transform,
            scale: this.scale[this.index],
        };
    }

    updateRotation() {
        if (this.rotation[this.index] < 0) {
            this.rotation[this.index] = 0;
            this.input.nativeElement.value = 0;
        } else if (this.rotation[this.index] > 359) {
            this.rotation[this.index] = 359;
            this.input.nativeElement.value = 359;
        }
        this.transform = {
            ...this.transform,
            rotate: this.rotation[this.index],
        };
    }

    save() {
        let files = [];
        for (let i = 0; i < this.data.imageCount; i++) {
            files.push(
                new File(
                    [base64ToFile(this.croppedImage[i])],
                    this.data.imageName[i]
                )
            );
        }
        this.dialogRef.close(files);
    }

    toLeft() {
        this.index = this.index == 0 ? 0 : this.index - 1;
        this.changeImageIndex();
    }

    toRight() {
        this.index =
            this.index == this.data.imageCount - 1
                ? this.data.imageCount - 1
                : this.index + 1;
        this.changeImageIndex();
    }

    toIndex(index: any) {
        this.index = index;
        this.changeImageIndex();
    }

    changeImageIndex() {
        this.check[this.index] = 1;
        this.init = false;
        this.transform = {
            flipH: this.flipH[this.index],
            flipV: this.flipV[this.index],
            rotate: this.rotation[this.index],
            scale: this.scale[this.index],
            translateH: this.translateH[this.index],
            translateV: this.translateV[this.index]
        };
        if (this.disabled) {
            for (let index in this.check) {
                if (this.check[index] == 0) {
                    break;
                } else {
                    if (parseInt(index) == this.check.length - 1) {
                        this.disabled = false;
                    }
                }
            }
        }
        let img = <HTMLElement>document.getElementById("crop")?.children[0];
        this.toggle.checked = true;
        this.hideResizeSquares[this.index] = false;
        this.disabledCropper[this.index] = false;
        if(img) {
            img.style.cursor = "auto";
            img.removeEventListener("wheel", this.scrollByMouse);
            img.removeEventListener("mousedown", this.moveDownByMouse);
        }
    }

    moveLeft() {
        if(this.transform.flipH) {
            if((this.rotation[this.index] >= 0 && this.rotation < 45) || (this.rotation[this.index] >= 315 && this.rotation[this.index] <= 360)) {
                this.horizonUp();
            }
            else if(this.rotation[this.index] >= 45 && this.rotation[this.index] < 135) {
                this.verticalUp();
            }
            else if(this.rotation[this.index] >= 135 && this.rotation[this.index] < 225) {
                this.horizonDown();
            }
            else if(this.rotation[this.index] >= 225 && this.rotation[this.index] < 315) {
                this.verticalDown();
            }
        }
        else {
            if((this.rotation[this.index] >= 0 && this.rotation < 45) || (this.rotation[this.index] >= 315 && this.rotation[this.index] <= 360)) {
                this.horizonDown();
            }
            else if(this.rotation[this.index] >= 45 && this.rotation[this.index] < 135) {
                this.verticalDown();
            }
            else if(this.rotation[this.index] >= 135 && this.rotation[this.index] < 225) {
                this.horizonUp();
            }
            else if(this.rotation[this.index] >= 225 && this.rotation[this.index] < 315) {
                this.verticalUp();
            }
        }
    }

    moveTop() {
        if(this.transform.flipV) {
            if((this.rotation[this.index] >= 0 && this.rotation < 45) || (this.rotation[this.index] >= 315 && this.rotation[this.index] <= 360)) {
                this.verticalDown();
            }
            else if(this.rotation[this.index] >= 45 && this.rotation[this.index] < 135) {
                this.horizonUp();
            }
            else if(this.rotation[this.index] >= 135 && this.rotation[this.index] < 225) {
                this.verticalUp();
            }
            else if(this.rotation[this.index] >= 225 && this.rotation[this.index] < 315) {
                this.horizonDown();
            }
        }
        else {
            if((this.rotation[this.index] >= 0 && this.rotation < 45) || (this.rotation[this.index] >= 315 && this.rotation[this.index] <= 360)) {
                this.verticalUp();
            }
            else if(this.rotation[this.index] >= 45 && this.rotation[this.index] < 135) {
                this.horizonDown();
            }
            else if(this.rotation[this.index] >= 135 && this.rotation[this.index] < 225) {
                this.verticalDown();
            }
            else if(this.rotation[this.index] >= 225 && this.rotation[this.index] < 315) {
                this.horizonUp();
            }
        }
    }

    moveRight() {
        if(!this.transform.flipH) {
            if((this.rotation[this.index] >= 0 && this.rotation < 45) || (this.rotation[this.index] >= 315 && this.rotation[this.index] <= 360)) {
                this.horizonUp();
            }
            else if(this.rotation[this.index] >= 45 && this.rotation[this.index] < 135) {
                this.verticalUp();
            }
            else if(this.rotation[this.index] >= 135 && this.rotation[this.index] < 225) {
                this.horizonDown();
            }
            else if(this.rotation[this.index] >= 225 && this.rotation[this.index] < 315) {
                this.verticalDown();
            }
        }
        else {
            if((this.rotation[this.index] >= 0 && this.rotation < 45) || (this.rotation[this.index] >= 315 && this.rotation[this.index] <= 360)) {
                this.horizonDown();
            }
            else if(this.rotation[this.index] >= 45 && this.rotation[this.index] < 135) {
                this.verticalDown();
            }
            else if(this.rotation[this.index] >= 135 && this.rotation[this.index] < 225) {
                this.horizonUp();
            }
            else if(this.rotation[this.index] >= 225 && this.rotation[this.index] < 315) {
                this.verticalUp();
            }
        }
    }

    moveBottom() {
        if(!this.transform.flipV) {
            if((this.rotation[this.index] >= 0 && this.rotation < 45) || (this.rotation[this.index] >= 315 && this.rotation[this.index] <= 360)) {
                this.verticalDown();
            }
            else if(this.rotation[this.index] >= 45 && this.rotation[this.index] < 135) {
                this.horizonUp();
            }
            else if(this.rotation[this.index] >= 135 && this.rotation[this.index] < 225) {
                this.verticalUp();
            }
            else if(this.rotation[this.index] >= 225 && this.rotation[this.index] < 315) {
                this.horizonDown();
            }
        }
        else {
            if((this.rotation[this.index] >= 0 && this.rotation < 45) || (this.rotation[this.index] >= 315 && this.rotation[this.index] <= 360)) {
                this.verticalUp();
            }
            else if(this.rotation[this.index] >= 45 && this.rotation[this.index] < 135) {
                this.horizonDown();
            }
            else if(this.rotation[this.index] >= 135 && this.rotation[this.index] < 225) {
                this.verticalDown();
            }
            else if(this.rotation[this.index] >= 225 && this.rotation[this.index] < 315) {
                this.horizonUp();
            }
        }
    }

    toggleCropper() {
        let img = <HTMLElement>document.getElementById("crop")?.children[0];
        if(this.toggle.checked) {
            this.hideResizeSquares[this.index] = true;
            this.disabledCropper[this.index] = true;
            if(img) {
                img.style.cursor = "move";
                img.addEventListener("wheel", this.scrollByMouse);
                img.addEventListener("mousedown", this.moveDownByMouse);
            }
        }
        else {
            this.hideResizeSquares[this.index] = false;
            this.disabledCropper[this.index] = false;
            if(img) {
                img.style.cursor = "auto";
                img.removeEventListener("wheel", this.scrollByMouse);
                img.removeEventListener("mousedown", this.moveDownByMouse);
            }
        }
    }
    horizonUp() {
        this.translateH[this.index] += 5;
        this.transform = {
            ...this.transform,
            translateH: this.translateH[this.index]
        };
    }

    horizonDown() {
        this.translateH[this.index] -= 5;
        this.transform = {
            ...this.transform,
            translateH: this.translateH[this.index]
        };
    }

    verticalUp() {
        this.translateV[this.index] -= 5;
        this.transform = {
            ...this.transform,
            translateV: this.translateV[this.index]
        };
    }

    verticalDown() {
        this.translateV[this.index] += 5;
        this.transform = {
            ...this.transform,
            translateV: this.translateV[this.index]
        };
    }

    scrollByMouse = (e: any) => {
        if(e.deltaY > 0) {
            this.zoomOut();
        }
        else {
            this.zoomIn();
        }
    }

    moveDownByMouse = (e: any) => {
        let img = <HTMLElement>document.getElementById("crop")?.children[0];
        e.preventDefault();
        this.pos3 = e.clientX;
        this.pos4 = e.clientY;
        img.addEventListener("mousemove", this.moveByMouse);
        img.addEventListener("mouseup", this.closeMoveByMouse);
    }

    moveByMouse = (e: any) => {
        let image = <HTMLElement>document.getElementById("crop")?.getElementsByTagName("img")[0];
        e.preventDefault();
        this.pos1 = this.pos3 - e.clientX;
        this.pos2 = this.pos4 - e.clientY;
        let scaleH = this.transform.scale ? this.transform.scale : 1;
        let scaleV = this.transform.scale ? this.transform.scale : 1;
        let rotate = this.transform.rotate ? this.transform.rotate : 0;
        if(this.alter[this.index]) {
            scaleH *= this.transform.flipV ? -1 : 1;
            scaleV *= this.transform.flipH ? -1 : 1;
            if(this.transform.flipH != this.transform.flipV) {
                rotate = 180 - rotate;
            }
        }
        else {
            scaleH *= this.transform.flipH ? -1 : 1;
            scaleV *= this.transform.flipV ? -1 : 1;
            if(this.transform.flipH != this.transform.flipV) {
                rotate = -rotate;
            }
        }
        let degree = (this.transform.rotate != undefined ? this.transform.rotate : 0) * Math.PI / 180;
        let degreeR = rotate * Math.PI / 180;
        let degreePos = Math.atan(this.pos1 / this.pos2);
        degreePos = degreePos < 0 ? 2 * Math.PI + degreePos : degreePos;
        let sum = Math.sqrt(this.pos1 * this.pos1 + this.pos2 * this.pos2);
        let posH = sum * Math.sin(degreePos - 2 * Math.PI + degree);
        let posV = sum * Math.cos(degreePos - 2 * Math.PI + degree);
        let degH = posH * (Math.sin(degree) * Math.sin(degreeR) + Math.cos(degree) * Math.cos(degreeR)) + posV * (Math.cos(degree) * Math.sin(degreeR) - Math.sin(degree) * Math.cos(degreeR));
        let degV = posV * (Math.sin(degree) * Math.sin(degreeR) + Math.cos(degree) * Math.cos(degreeR)) + posH * (Math.sin(degree) * Math.cos(degreeR) - Math.cos(degree) * Math.sin(degreeR));
        if(this.pos2 < 0) {
            degH *= -1;
            degV *= -1;
        }
        let transDegH = degH * Math.cos(degreeR) - degV * Math.sin(degreeR);
        let transDegV = degV * Math.cos(degreeR) + degH * Math.sin(degreeR);
        let transH = this.transform.translateH != undefined ? this.transform.translateH * scaleH : 0;
        let transV = this.transform.translateV != undefined ? this.transform.translateV * scaleV : 0;
        let transHPC = transH * Math.cos(degreeR) - transV * Math.sin(degreeR) * this.data.ratioHeight / this.data.ratioWidth;
        let transVPC = transV * Math.cos(degreeR) + transH * Math.sin(degreeR) * this.data.ratioWidth / this.data.ratioHeight;
        let width = image.offsetWidth * scaleH;
        let height = image.offsetHeight * scaleV;
        image.style.transform = `translate(calc(${transHPC}% - ${transDegH}px), calc(${transVPC}% - ${transDegV}px))
                                rotate(${rotate}deg) scaleX(${scaleH}) scaleY(${scaleV})`;
        this.translateH[this.index] = this.transform.translateH != undefined ? this.transform.translateH - degH / width * 100 : 0;
        this.translateV[this.index] = this.transform.translateV != undefined ? this.transform.translateV - degV / height * 100 : 0;
    }

    closeMoveByMouse = (e: any) => {
        let img = <HTMLElement>document.getElementById("crop")?.children[0];
        img.removeEventListener("mouseup", this.closeMoveByMouse);
        img.removeEventListener("mousemove", this.moveByMouse);
        this.transform = {
            ...this.transform,
            translateH: this.translateH[this.index],
            translateV: this.translateV[this.index]
        }
    }
}
