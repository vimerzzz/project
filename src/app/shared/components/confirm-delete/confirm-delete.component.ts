import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MessageConstants } from '../../constants/message.constants';

@Component({
    selector: 'app-confirm-delete',
    templateUrl: './confirm-delete.component.html',
    styleUrls: ['./confirm-delete.component.scss']
})
export class ConfirmDeleteComponent implements OnInit {
    title: any = MessageConstants.CONFIRM_DELETE;

    constructor(private dialogRef: MatDialogRef<ConfirmDeleteComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any) { }

    ngOnInit(): void {
        if(this.data && this.data.title) {
            this.title = this.data.title;
        }
    }

    save() {
        this.dialogRef.close(true);
    }
}
