import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
    {
        path: "",
        children: [
            {
                path: "",
                loadChildren: () => import('./pages/main/main.module').then(m => m.MainModule)
            },
            {
                path: "error",
                loadChildren: () => import('./pages/error/error.module').then(m => m.ErrorModule)
            },
            {
                path: "verification",
                loadChildren: () => import('./pages/verification/verification.module').then(m => m.VerificationModule)
            },
            {
                path: "**",
                redirectTo: "/error/not-found",
                pathMatch: "full"
            }
        ]
    }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
